import equipements from "../data/equipements.js"
import runes from "../data/runes.js"
import competences from "../data/competences.js"
import {isEmpty, fusion_obj} from "./utils.js"

export default class Equipement {
	constructor (item = {}) {
		if (item == {}) {
			this.actif = false;
			this.amelioration = {};
			this.emplacement = "arme";
			this.materiaux = "";
			this.rune = "";
			this.rune_lvl = "1";
			this.type_objet = "";
		} else {
			this.#from(item)
		}
	}

	#from(item) {
		this.actif = item.actif;
		this.amelioration = item.amelioration;
		this.emplacement = item.emplacement;
		this.materiaux = item.materiaux;
		this.rune = item.rune;
		this.rune_lvl = item.rune_lvl;
		this.type_objet = item.type_objet;
	}

	get type_objet_obj() {
		return equipements.type_objet[this.emplacement] ? equipements.type_objet[this.emplacement][this.type_objet] || {} : {};
	}

	get amelioration_obj() {
		var amelio_obj = {};
		if (equipements.amelioration[this.emplacement] && this.amelioration) {
			for (const key in this.amelioration) {
				if(this.amelioration[key] != "") {
					amelio_obj[key] = equipements.amelioration[this.emplacement][this.amelioration[key]];
				}
			}
		}
		return amelio_obj;
	}

	get materiaux_obj() {
		return this.#get_general("materiaux");
	}

	get rune_obj() {
		return runes[this.rune] || {};
	}

	calcul_cout() {
		const cout_ameliorations = equipements.cout_ameliorations;
		
		const liste_ameliorations = this.amelioration_obj;
		var prix = 0;
		var unite_materiaux = 0;
		var temps = 0;
	    var multiplicateur_prix = 1;
	    if (equipements.type_objet[this.emplacement] && equipements.type_objet[this.emplacement][this.type_objet]) {
	      prix = this.type_objet_obj.prix || 0;
	      unite_materiaux += this.type_objet_obj.unite_materiaux || 0;
	      temps += this.type_objet_obj.temps || 0;

	      //materiaux
	      prix = prix + (this.type_objet_obj.unite_materiaux || 0) * (this.materiaux_obj.prix || 0);
	      //amelioration
	      for (const key in liste_ameliorations) {
	        multiplicateur_prix = multiplicateur_prix * (liste_ameliorations[key]?.multiplicateur_prix || 1);
	        unite_materiaux += cout_ameliorations[liste_ameliorations[key]?.multiplicateur_prix || 1]?.mat || 0;
	        temps += cout_ameliorations[liste_ameliorations[key]?.multiplicateur_prix || 1]?.temps || 0;
	      }

	      prix = prix*multiplicateur_prix;
	      prix += (runes[this.rune] ? runes[this.rune].prix : 0);
	    } 
	    return {"prix": Math.round(prix), "mat" : unite_materiaux, "temps": temps};
	}

	calcul_prix(){
		return this.calcul_cout().prix
	}

	cout_html() {
		const cout = this.calcul_cout();
		const str ='<div style="display: inline-block;position: relative;"><div class="show info"><i class="fas fa-info-circle"></i></div><div style="width: 250px; left: -160px; bottom: 100%;" class="hide">Pour crafter cet item, vous auriez besoin de '+ cout.mat +' unités de matériaux et de '+cout.temps +' heures de travail</div></div>';
		return (cout.mat != 0 || cout.temps != 0 ? str : "");
	}

	#resume_entete() {
	  var str = "";
	  var equipement = this.type_objet_obj;
      if(this.emplacement == "arme") {
        str += (equipement.type ? "<h3><strong>"+ competences[equipement.type].label+"</strong></h3>" : "");
        str += (equipement.bonus ? "<strong>dégat : </strong>" + equipement.bonus.degat[0].bonus_D_dmg + "D" + equipement.bonus.degat[0].type_D + "+" + (equipement.bonus.degat[0].bonus_dmg || 0) + ", <br>" : "");
        str += (equipement.bonus ? "<strong>critique : </strong>" + equipement.bonus.degat[0].critique + ", <br>" : "");
      }
      else if (this.emplacement == "armure") {
        str += (equipement.type ? "<h3><strong>Armure "+ equipement.type.toLowerCase()+"</strong></h3>" : "");
        for(const key in equipement.bonus) {
          switch(key) {
            case 'malus_armure' :
              var bonus_str = "Malus";
              var bonus = equipement.bonus[key];
              if ( bonus < 0) {
                bonus_str = "Bonus";
                bonus = bonus * -1;
              }
              str += "<strong>"+bonus_str+' aux compétences <i title="Compétences malusées par la charge" class="fas fa-weight-hanging"></i> : </strong>'+bonus+"<br>";
              break;
            case 'bonus_initiative' :
              var bonus_str = "Bonus";
              var bonus = equipement.bonus[key];
              if ( bonus < 0) {
                bonus_str = "Malus";
                bonus = bonus * -1;
              }
              str += "<strong>"+bonus_str+" à l'initiative : </strong>"+bonus+"<br>";
              break;
            default :
              str += "<strong>"+key+" : </strong>"+equipement.bonus[key]+"<br>";
              break;
          }
        }
      }
      if(equipement.autre) {
        str += "<strong>Autre : </strong>" + equipement.autre + ", <br>";
      }
	  return str;
	}

	resume_bonus() {
		let str = "";
	  	if(this.emplacement && this.emplacement != "emplacement") {
	  		str += this.#resume_entete();

		    const liste_ameliorations = this.amelioration_obj;
		    if (!isEmpty(liste_ameliorations)) {
		      var temp = "";
		      for (const key in liste_ameliorations) {
		        temp += "<li>" + liste_ameliorations[key]?.description + "</li>";
		      }
		      str += "<strong> Améliorations : </strong> <ul>" + temp + "</ul>"
		    }

		    if (!isEmpty(this.materiaux_obj))
		    str += "<strong> Materiaux : </strong>" + this.materiaux_obj.description + ", <br>"

		    if (runes[this.rune]) {
		      str += "<strong>rune : </strong>";
		      var rune = runes[this.rune].description;
		      while (rune.match(/{{\s*[\w\.]+\s*}}/g)) {
		        const a = parseFloat(rune.match(/{{\s*[\w\.]+\s*}}/g).map(function(x) { return x.match(/[\w\.]+/)[0]; })[0]);
		        rune = rune.replace("{{"+a+"}}", ""+Math.ceil(a*this.rune_lvl));
		      }
		      str += rune;
		    }
		}
	    
	  //}
	  return (str ? ( str.slice(-6) == ", <br>" ? str.slice(0, -6) : str) : "Résumé des bonus") + ("<br><br><strong>Prix d'achat</strong> : " + this.calcul_prix() + " PB "+this.cout_html()+"<br>");
	}

	#get_general(key){
		const ameliorations_simple = ["amelioration"];
    	const ameliorations_complexes = {"materiaux" : "materiaux"};

    	if (ameliorations_simple.includes(key)) {
    		return this.#get_amelioration_simple(key);
    	}
    	if (key in ameliorations_complexes) {
    		return this.#get_ameliorations_complexes(key, ameliorations_complexes[key]);
    	}
    	return {};
	}

	#get_amelioration_simple(key) {
		try {
			return equipements[key][this.emplacement][this[key]] || {};
		} catch {
			return {};
		}
	}

	#get_ameliorations_complexes(key, key2) {
		const temp = (key == "materiaux" ? equipements[key][this.emplacement] : equipements[key]);
      	try{
          	return temp[equipements.type_objet[this.emplacement][this.type_objet][key2]][this[key]] || {};
     	 } catch {
     	 	return {};
     	}
	}

	calcul_bonus_equipement() {
	    var bonus = {};
	    var temp;

	    fusion_obj(bonus, this.type_objet_obj.bonus);
	    const amelio = this.amelioration_obj;
	    for (const key in amelio) {
	    	if(amelio[key].label == "Allégée"){
	    		fusion_obj(bonus, {"bonus_initiative" : (bonus.bonus_initiative > 0 ? 0 : bonus.bonus_initiative * -1)});
	    	}
	    	fusion_obj(bonus, amelio[key].bonus);
	    }
	    fusion_obj(bonus, this.materiaux_obj.bonus);
	    fusion_obj(bonus, this.rune_obj.bonus, this.rune_lvl);

	    if(bonus.degat && bonus.degat[0]) {
	      const liste_bonus_resume = ["bonus_D_dmg", "type_D", "bonus_dmg", "PA", "portee"]
	      for (var i = liste_bonus_resume.length-1; i >= 0 ; i--) {
	        bonus.degat[0][liste_bonus_resume[i]] = (bonus.degat[0][liste_bonus_resume[i]] || 0) + (bonus[liste_bonus_resume[i]] || 0);
	        delete bonus[liste_bonus_resume[i]];
	      }
	    }

	    return bonus;
	  }

}