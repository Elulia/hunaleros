/**
 * Define a set of template paths to pre-load
 * Pre-loaded templates are compiled and cached for fast access when rendering
 * @return {Promise}
 */
export const preloadHandlebarsTemplates = async function() {
  return loadTemplates([

    //Actor Sheet Header Partials
    "systems/hunaleros/templates/actors/parts/ressources.html",
    "systems/hunaleros/templates/actors/parts/basic_info.html",

    // Actor Sheet Partials
    "systems/hunaleros/templates/actors/parts/dons.html",
    "systems/hunaleros/templates/actors/parts/caracteristiques.html",
    "systems/hunaleros/templates/actors/parts/competences.html",
    "systems/hunaleros/templates/actors/parts/attributs_chiffres.html",
    "systems/hunaleros/templates/actors/parts/autre_bonus.html",
    "systems/hunaleros/templates/actors/parts/dons_evolutifs.html",
    "systems/hunaleros/templates/actors/parts/inventaire.html",
    "systems/hunaleros/templates/actors/parts/equipements.html",
    "systems/hunaleros/templates/actors/parts/defaut.html",
    
  ]);
};
