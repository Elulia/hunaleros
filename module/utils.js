export function isEmpty(obj) {
  return (obj
    && Object.keys(obj).length === 0
    && Object.getPrototypeOf(obj) === Object.prototype);
}

export function fusion_obj(obj1, obj2, lvl = 0) {
  for (const key in obj2) {
    if (obj2[key] === null) {

    }
    else if (Array.isArray(obj2[key])) {
      obj1[key] = (obj1[key] ? obj1[key].concat(JSON.parse(JSON.stringify(obj2[key]))) : [].concat(JSON.parse(JSON.stringify(obj2[key]))));
    }
    else if (typeof obj2[key] === 'object' && obj2[key] !== null) {
      obj1[key] = obj1[key] ? obj1[key] : {};
      fusion_obj(obj1[key], obj2[key]);
    }
    else {
      if(key.includes("multiplicateur")) {
        obj1[key] = (obj1[key] ? obj1[key] * obj2[key] : obj2[key]);
      }
      else {
        obj1[key] = (obj1[key] ? obj1[key] + obj2[key] : obj2[key]);
      }
    }
  }
  if(lvl > 0)
    fusion_obj_bonus_par_niveau(obj1, lvl)
}

function calcul_bonus_lvl(bonus, lvl) {
  for (var i = 1; i <= lvl; i++) {
    fusion_obj(bonus, bonus.lvl[i+""]);
  }
  delete bonus.lvl;
}

function fusion_obj_bonus_par_niveau(obj, lvl) {
  for(const key_par_niveaux in obj) {
    if(key_par_niveaux.includes("par_niveaux")) {
      var key = key_par_niveaux.replace("_par_niveaux",'');
      obj[key] = (obj[key] ? obj[key] + (obj[key_par_niveaux]*lvl) : obj[key_par_niveaux]*lvl);
      delete obj[key_par_niveaux];
    }
  }
  if(obj.lvl) {
    calcul_bonus_lvl(obj, lvl);
  }
}