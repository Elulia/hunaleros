import races from "../data/races.js"
import competences from "../data/competences.js"
import metiers from "../data/metiers.js"
import dons from "../data/dons.js"
import dons_evolutif from "../data/dons_evolutifs.js"
import langues from "../data/langues.js"
import equipements from "../data/equipements.js"
import runes from "../data/runes.js"
import defauts from "../data/defauts.js"
import divinites from "../data/pantheon.js"
import charge_npc from "../data/charge_npc.js"
import flight from "../data/vol.js"
import Equipement from "./equipement.js"
import {isEmpty, fusion_obj} from "./utils.js"

/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */

function plus_if_supp_zero(nombre) {
  return (nombre > 0 ? " + "+nombre : (nombre == 0 ? "" : (" "+nombre).replace("-", "- ")))
}

function checkIfNumberOrReturnZero(number) {
  const parsed_number = parseFloat(number);
  return (typeof number == 'number' ? number : (parsed_number == number ? parsed_number : 0));
}

function getObjectMoneyFromPc(total_pc) {
  return {"po" : Math.trunc(total_pc/100),
    "pa" : Math.trunc((total_pc%100)/10),
    "pc" : total_pc%10};
}

function calcul_somme_substat_inventory(inventaire, substat) {
  var total = 0;
  for (const item in inventaire) {
      if (inventaire[item])
        total += (checkIfNumberOrReturnZero(inventaire[item][substat]) || 0);
  }
  return total;
}

function calcul_poid(inventaire) {
  return calcul_somme_substat_inventory(inventaire, "poids");
}

function calcul_somme_valeur(inventaire) {
  return calcul_somme_substat_inventory(inventaire, "value");
}

function calcul_emplacement(inventaire) {
  return calcul_somme_substat_inventory(inventaire, "emplacements");
}

function calcul_malus (force, robustesse, mult_bonus, encombrement, flight) {
  const multiplicateur_base = robustesse && charge_npc[robustesse] ? charge_npc[robustesse].multiplicateur : 1.5;
  const a = force*multiplicateur_base*(mult_bonus||1) * (flight ? 0.5 : 1);
  const tableau_malus_encombrement = [
    {
      description : "Trop lourd, impossible de porter plus de "+(a * 2.333).toFixed(3)+" Kg",
      "seuil": -1,
      malus_armure : 8,
      bonus_initiative : -8
    },
    {
      description : "",
      "seuil": 0.999,
      malus_armure : 0,
      bonus_initiative : 0
    },
    {
      description : "- Stade 1 -",
      "seuil": 1.333,
      malus_armure : 1,
      bonus_initiative : -1
    },
    {
      description : "- Stade 2 -",
      "seuil": 1.666,
      malus_armure : 2,
      bonus_initiative : -2
    },
    {
      description : "- Stade 3 -",
      "seuil": 1.999,
      malus_armure : 4,
      bonus_initiative : -4
    },
    {
      description : "- Stade 4, le personnage subit 1D6 dégâts par heure -",
      "seuil": 2.333,
      malus_armure : 8,
      bonus_initiative : -8
    }
  ];

  const min = (a * tableau_malus_encombrement[1].seuil).toFixed(3)*1;
  const max = (a * tableau_malus_encombrement[tableau_malus_encombrement.length-1].seuil).toFixed(3)*1;

  /*cas force = 0 et inventaire vide*/
  if (encombrement == 0 && a == 0) {
    return tableau_malus_encombrement[1];
  }

  for(var i = 0; i < tableau_malus_encombrement.length; i++) {
    tableau_malus_encombrement[i].min = min;
    tableau_malus_encombrement[i].max = max;
    if (encombrement <= (a * tableau_malus_encombrement[i].seuil).toFixed(3)*1)
      return tableau_malus_encombrement[i];
  }
  return tableau_malus_encombrement[0];
}

function getMalusEncombrement (type, data, flight) {
  const force = (type == "character" ? data.abilities.force.value : data.abilities.force);
  const encombrement = calcul_poid(data.inventaire);
  const mult_bonus = data.attributs_chiffre ? data.attributs_chiffre.multiplicateur_charge || 1 : 1;
  return calcul_malus(force, data.robustesse, mult_bonus, encombrement, flight);
}

function is_value_in_list_dons(don, data) {
    for (const key in data.dons.list_dons) {
      if (data.dons.list_dons[key] == don)
        return true;
    }
    return false;
  }

function get_race(data){
  return races[data.basic_info.race];
}

function get_traits_racials(data){
  var list_traits_racial = []
  for (var trait in data.basic_info.trait_racial) {
    list_traits_racial.push(races[data.basic_info.race].traits_racial[data.basic_info.trait_racial[trait]]);
  } 
  return list_traits_racial;
}

function bonus_don_evo_conditionnel(don_evo_temp, data){
  var bonus_conditionel = {}
  if(don_evo_temp.bonus_conditionel) {
      var validateur = true;
      for (const key in don_evo_temp.bonus_conditionel.condition) {
        if( key == "don")
          if (!is_value_in_list_dons(don_evo_temp.bonus_conditionel.condition[key], data))
            validateur = false;
      }
      var bonus_conditionel = (validateur ? don_evo_temp.bonus_conditionel.oui : don_evo_temp.bonus_conditionel.non);
  }
  return bonus_conditionel;
}


Handlebars.registerHelper('ifCond', function (v1, operator, v2, options) {
  switch (operator) {
    case '==':
      return (v1 == v2) ? options.fn(this) : options.inverse(this);
    case '===':
      return (v1 === v2) ? options.fn(this) : options.inverse(this);
    case '!=':
      return (v1 != v2) ? options.fn(this) : options.inverse(this);
    case '!==':
      return (v1 !== v2) ? options.fn(this) : options.inverse(this);
    case '<':
      return (v1 < v2) ? options.fn(this) : options.inverse(this);
    case '<=':
      return (v1 <= v2) ? options.fn(this) : options.inverse(this);
    case '>':
      return (v1 > v2) ? options.fn(this) : options.inverse(this);
    case '>=':
      return (v1 >= v2) ? options.fn(this) : options.inverse(this);
    case '&&':
      return (v1 && v2) ? options.fn(this) : options.inverse(this);
    case '||':
      return (v1 || v2) ? options.fn(this) : options.inverse(this);
    default:
      return options.inverse(this);
  }
});

Handlebars.registerHelper( 'concat', function(a, b) {
    return a+b;
});

Handlebars.registerHelper ("setChecked", function (value, currentValue) {
  if ( value == currentValue ) {
     return "checked";
  } else {
     return "";
  }
 });

Handlebars.registerHelper ("math", function (lvalue, operator, rvalue) {
  lvalue = parseFloat(lvalue) || 0;
  rvalue = parseFloat(rvalue) || 0;
  return {
    "+": lvalue + rvalue,
    "-": lvalue - rvalue,
    "*": lvalue * rvalue,
    "/": lvalue / rvalue,
    "%": lvalue % rvalue
  }[operator];
 });

Handlebars.registerHelper('isMagie', function (data, i) {
  const don_evo = data.dons_evolutif['don_evo'+i];
  return (dons_evolutif[don_evo] ? dons_evolutif[don_evo].magie : false);
});

Handlebars.registerHelper('afficheCompetence', function (competence, options) {
  if ((competence.artisanat && options.artisanat) || (competence.arme && options.arme && !competence.pts))
    return false;
  return true;
});

Handlebars.registerHelper('styleMagie', function (i) {
  const magie = dons_evolutif[this.system.dons_evolutif['don_evo'+i]];
  return "color:"+magie.style.color+";background-image:url('systems/hunaleros/images/magies/"+magie.style.background+"');text-shadow: "+magie.style.shadow+" 2px 0px 0px, "+magie.style.shadow+" 1.75517px 0.958851px 0px, "+magie.style.shadow+" 1.0806px 1.68294px 0px, "+magie.style.shadow+" 0.141474px 1.99499px 0px, "+magie.style.shadow+" -0.832294px 1.81859px 0px, "+magie.style.shadow+" -1.60229px 1.19694px 0px, "+magie.style.shadow+" -1.97999px 0.28224px 0px, "+magie.style.shadow+" -1.87291px -0.701566px 0px, "+magie.style.shadow+" -1.30729px -1.51361px 0px, "+magie.style.shadow+" -0.421592px -1.95506px 0px, "+magie.style.shadow+" 0.567324px -1.91785px 0px, "+magie.style.shadow+" 1.41734px -1.41108px 0px, "+magie.style.shadow+" 1.92034px -0.558831px 0px;";
});

Handlebars.registerHelper('isCompetenceNotAvailable', function (competence) {
  return (this.actor.type == "character" ? (this.system.restriction ? this.system.restriction.indexOf(competence) > -1 : false) : false);
});

Handlebars.registerHelper('isDonEvoAvailable', function (str, lvl) {
  const nb_don_evo = (this.system.basic_info.niveau > 6 ? 3 : (this.system.basic_info.niveau > 3 ? 2 : 1));
  const list_dons_evo_available = Object.values(this.system.dons_evolutif).slice(0, nb_don_evo);
  const a = list_dons_evo_available.indexOf(str);
  if (a > -1 && (this.system.basic_info.niveau - (3*a)) >= lvl )
    return true;
  return false;
});


Handlebars.registerHelper('titreImage', function (titre) {
  return "<img class=\"img_titre\" alt=\""+titre+"\" src='systems/hunaleros/images/actor-sheet/"+this.system.sheet_class+"/"+titre+".png'>";
});

Handlebars.registerHelper('nbLangues', function () {
  var bonus_langue = 0;
  var trait_temp = {};
  for (var trait in this.system.basic_info.trait_racial) {
    trait_temp = races[this.system.basic_info.race].traits_racial[this.system.basic_info.trait_racial[trait]];
    trait_temp = (trait_temp ? trait_temp.bonus : {});
    bonus_langue += (trait_temp.bonus_langue || 0);
  } 
  return 2 + (races[this.system.basic_info.race].bonus_langue || 0) + (this.system.dons.bonus_dons ? this.system.dons.bonus_dons.bonus_langue || 0 : 0) + bonus_langue;
});

Handlebars.registerHelper('can_fly', function () {
  var can_fly = get_race(this.system).flight;
  const list_traits_racial = get_traits_racials(this.system);
  for (const trait of list_traits_racial) {
    can_fly = can_fly || (trait ? trait.flight :  false);
  }
  if (can_fly) {
    return true
  }
  return false
});

Handlebars.registerHelper('need_daylight_control', function () {
  var need_daylight_control = get_race(this.system).daylight;
  const list_traits_racial = get_traits_racials(this.system);
  for (const trait of list_traits_racial) {
    need_daylight_control = need_daylight_control || (trait ? trait.daylight :  false);
  }
  if (need_daylight_control) {
    return true
  }
  return false
});

Handlebars.registerHelper('nbTraitsRacial', function () {
  return 1 + (this.system.bonus_don_racial || 0);
});

Handlebars.registerHelper('languesRace', function () {
  var str = "";
  var list_langues = races[this.system.basic_info.race].langues;
  list_langues = (list_langues ? list_langues : []);
  var trait_temp = {};
  for (var trait in this.system.basic_info.trait_racial) {
    trait_temp = races[this.system.basic_info.race].traits_racial[this.system.basic_info.trait_racial[trait]];
    if(trait_temp && trait_temp.bonus && trait_temp.bonus.langue) {list_langues.push(trait_temp.bonus.langue)}
  }
  if (list_langues) {
    str = list_langues.join(', ');
  }
  return str;
});

Handlebars.registerHelper('ptsPVAndPM', function () {
  var a = ((this.system.basic_info.niveau - 1) * 10) - this.system.health.pts - this.system.mana.pts;
  return a;
});

Handlebars.registerHelper('maxNbPtsMana', function () {
  var a = ((this.system.basic_info.niveau - 1) * 10) - this.system.health.pts;
  return (a > 0 ? a : 0)
});

Handlebars.registerHelper('maxNbPtsVie', function () {
  var a = ((this.system.basic_info.niveau - 1) * 10) - this.system.mana.pts;
  return (a > 0 ? a : 0)
});

Handlebars.registerHelper('nbPtsCarac', function () {
  var bonus_Pts_Caracs = this.system.bonus_Pts_Caracs || 0;

  for (const key in this.system.dons.list_dons) {
    if (this.system.dons.list_dons[key] == "bonification")
      bonus_Pts_Caracs += Math.trunc(this.system.basic_info.niveau/3);
  }
  for (const key in this.system.choix.lvl) {
    if(parseInt(key)+1 < this.system.basic_info.niveau)
      bonus_Pts_Caracs += (this.system.choix.lvl[key] == "caractéristique" ? 1 : 0);
  }

  return 96+bonus_Pts_Caracs-(this.system.abilities.force.pts+this.system.abilities.intelligence.pts+this.system.abilities.constitution.pts+this.system.abilities.sagesse.pts+this.system.abilities.dexterite.pts+this.system.abilities.charisme.pts);
});

Handlebars.registerHelper('nbPtsCompetences', function () {
  var pts = 0;
  var a = 0;
  if (this.actor.type != "character") {
    return 0;
  }
  var bonus_Pts_Competences = this.system.bonus_Pts_Competences || 0;
  for (const competence_key in this.system.competences) {
    if(competences[competence_key]) {
      a = this.system.competences[competence_key].pts
      a = (a - 9 > 0 ? (a - 19 > 0 ? (a - 19) * 3 + 10*2 + 9  : (a - 9)*2 + 9 ) : a)
      pts += a
    }
  }
  for (const key in this.system.choix.lvl) {
    if(parseInt(key)+1 < this.system.basic_info.niveau)
      bonus_Pts_Competences += (this.system.choix.lvl[key] == "compétence" ? 5 : 0);
  }
  return 15-pts+bonus_Pts_Competences;
});

Handlebars.registerHelper('bonus_force', function () {
  const bonus_force = this.system.attributs_chiffre.bonus_force;
  return "+"+(bonus_force > 0 ? bonus_force : 0)+" dégats";
});

Handlebars.registerHelper('motivation', function () {
  const bonus_charisme = Math.round((this.system.abilities.charisme.value-10)*3);

  var str = "<br><br>Effet au choix :<br> - Le personnage accorde un bonus de 1 à la prochaine action."
  if (this.system.abilities.charisme.value > 11)
    str += "<br> - Le personnage accorde un bonus de 2 à la prochaine action."
  if (this.system.abilities.charisme.value > 14)
    str += "<br> - Le personnage accorde un bonus de 2 au critique à la prochaine action."
  if (this.system.abilities.charisme.value > 17)
    str += "<br> - Le personnage accorde un bonus de 1D à l'effet prochain (Dégâts, effets de sorts, etc)."
  if (this.system.abilities.charisme.value > 20)
    str += "<br> - Le personnage choisit d'appliquer un des effets ci-dessus en zone de type 2 autour de lui, ou bien de doubler un effet sur une seule cible."

  return "Motive sur "+(bonus_charisme > 1 ? bonus_charisme : 1)+" ou moins"+str;
});

Handlebars.registerHelper('forInRange', function(n, block) {
  var accum = '';
  for(var i = 0; i < n; ++i)
      accum += block.fn(this, {blockParams: [this.system, i]});
  return accum;
});

function donsBonus(system) {
  var trait_racial = {};
  var dons_bonus = (races[system.basic_info.race].dons || []);
  

  const nb_don_evo = (system.basic_info.niveau > 6 ? 3 : (system.basic_info.niveau > 3 ? 2 : 1));
  for (var n = 0; n < nb_don_evo; n++) {
    const don_evo = dons_evolutif[system.dons_evolutif["don_evo"+n]];
    const lvlDonEvo = (don_evo && don_evo["Rang 1"] ? system.basic_info.niveau - (3*n) + (don_evo.magie ? 2 : 0) : 0);
    for (var nb_rang = 1; nb_rang <= lvlDonEvo; nb_rang++) {
      if (don_evo["Rang "+nb_rang]) {
        for (var i = 0; i < don_evo["Rang "+nb_rang].length; i++) {
          dons_bonus = dons_bonus.concat(don_evo["Rang "+nb_rang][i].bonus ? don_evo["Rang "+nb_rang][i].bonus.dons || [] : []);
          var bonus_conditionel = bonus_don_evo_conditionnel(don_evo["Rang "+nb_rang][i], system);
          dons_bonus = dons_bonus.concat(bonus_conditionel ? bonus_conditionel.dons || [] : []);
        }
      }
    }
  }

  for (const trait in system.basic_info.trait_racial) {
    trait_racial = races[system.basic_info.race].traits_racial[system.basic_info.trait_racial[trait]];
    dons_bonus = dons_bonus.concat(trait_racial && trait_racial.bonus ? trait_racial.bonus.dons || [] : []);
  }

  return dons_bonus;
}

Handlebars.registerHelper('donsBonus', function(block) {
  var accum = '';

  var dons_bonus = donsBonus(this.system);

  for(var i = 0; i < dons_bonus.length; i++)
    accum += block.fn(this, {blockParams: [dons[dons_bonus[i]]]});
  return accum;
});

Handlebars.registerHelper('donsEvolutifs', function(block) {
  var accum = '';
  const lvl = this.system.basic_info.niveau;
  const nb_don_evo = (lvl > 6 ? 3 : (lvl > 3 ? 2 : 1));
  for(var i = 0; i < nb_don_evo; ++i)
      accum += block.fn(this, {blockParams: [i, i+1]});
  return accum;
});

Handlebars.registerHelper('donsEvolutif', function(block) {
  var accum = '';
  const lvl = this.system.basic_info.niveau;
  const nb_don_evo = (lvl > 6 ? 3 : (lvl > 3 ? 2 : 1));
  for(var i = 0; i < nb_don_evo; ++i)
      accum += block.fn(this, {blockParams: [this, i]});
  return accum;
});

Handlebars.registerHelper('donEvoProperties', function(don_evo) {
  return (don_evo.cout || don_evo.portee || don_evo.defense);
});

Handlebars.registerHelper('parsingPorteeDonEvo', function (portee, data, isMagie) {
  const bonus = (isMagie ? data.bonus_portee_sort || 0 : 0)
  const personnel = (portee.personnel ? "Personnelle" : "")
  const lance = (portee.lance ? portee.lance + (bonus || 0) : "");
  const metres = (lance ? (lance > 1 ? lance + " mètres" : "Contact") : "") + (lance && personnel ? " ou " : "") + personnel
  return metres + (metres && portee.description ? ", " : "") + (portee.description || "");
});

Handlebars.registerHelper('parsingDescriptionParNiveau', function (description, niveau) {
  var str = description;
  var a = "";
  //parse avec addition
  var liste_matchs = str.match(/{{\s*[0-9\.,]+\s*}}\+[0-9]+/g);
  for (var match of liste_matchs || []) {
    a = Math.ceil(parseFloat(match.split('+')[0].replace("{{", "").replace('}}', ""))*(niveau || 1)) + Math.ceil(parseFloat(match.split('+')[1]));
    str = str.replace(match, ""+a);
  }

  //parse sans addition
  var liste_matchs = str.match(/{{\s*[\w\.,]+\s*}}/g);
  for (var match of liste_matchs || []) {
    a = Math.ceil(parseFloat(match.replace("{{", "").replace('}}', ""))*(niveau || 1));
    str = str.replace(match, ""+a);
  }

  return (str ? str : description);
});

Handlebars.registerHelper('ifChoix', function (obj, options) {
  if(obj && obj.choix) 
    return options.fn(obj);
  else
    return options.inverse(obj);
});

Handlebars.registerHelper('eachChoixRang', function (choix, data, block) {
  const rang = choix.rang;
  const type = choix.type;
  var accum = '';
  const list_dons_evo = {};

  for(const key in dons_evolutif) {
    if (dons_evolutif[key].type == type) {
      list_dons_evo[key] = dons_evolutif[key];
    }
  }


  const list_armes = {};

  for (const key in data.competences) {
    if(data.competences[key].arme) {
      list_armes[key] = data.competences[key];
    }
  }

  switch (type) {
    case "Magie":
      for(const id_don_evo in list_dons_evo) {
        accum +='<optgroup label="'+id_don_evo+'">'
        for(var i=1; i <= rang; i++)
          for(var key=0; key < list_dons_evo[id_don_evo]["Rang "+i].length; key++)
            accum += block.fn(this, {blockParams: [id_don_evo+'_'+"Rang "+i+'_'+key, list_dons_evo[id_don_evo]["Rang "+i][key]]});
          accum +='</optgroup">';
      }
      break;

    case "Arme":
      for(const i in list_dons_evo)
        for(var key=0; key < list_dons_evo[i]["Rang "+rang].length; key++)
          if(list_dons_evo[i]["Rang "+rang][key].label.includes("Expert"))
            accum += block.fn(this, {blockParams: [i+'_'+"Rang "+rang+'_'+key, list_dons_evo[i]["Rang "+rang][key]]});
      break;

    case "Radio":
      for(const i in choix.list){
        var label = (i.includes("crit_") ? "Marge de critique " : "") + (competences[i.replace("crit_", "")] ? competences[i.replace("crit_", "")].label : i.replace("crit_", "")) + " + " + choix.list[i];
        accum += block.fn(this, {blockParams: [i, {"label": label}]});
      }
      break;

    case "Type_Arme_Melee":
      for (const key in list_armes) {
        if (list_armes[key].sous_type == "mélé" && key != "couteaux") {
          accum += block.fn(this, {blockParams: [key, list_armes[key]]});
        }
      }
      break;

    case "Type_Arme_Distance":
      for (const key in list_armes) {
        if (list_armes[key].sous_type == "distance" || key == "couteaux") {
          accum += block.fn(this, {blockParams: [key, list_armes[key]]});
        }
      }
      break;

    default : 
      for(const i in list_dons_evo)
        for(var key=0; key < list_dons_evo[i]["Rang "+rang].length; key++)
          accum += block.fn(this, {blockParams: [i+'_'+"Rang "+rang+'_'+key, list_dons_evo[i]["Rang "+rang][key]]});
      break;
  }

  return accum;
});

Handlebars.registerHelper('lookupTrait', function (i) {
 return (this.system.basic_info.race && this.races[this.system.basic_info.race] && this.system.basic_info.trait_racial['trait'+i] ? this.races[this.system.basic_info.race].traits_racial[this.system.basic_info.trait_racial['trait'+i]] : {});
});

Handlebars.registerHelper('eachChoixTrait', function (choix, data, block) {
  const list = choix.list;
  const type = choix.type;
  var accum = '';

  switch (type) {
    case "Radio":
      for(const i in list){
        var label = (i.includes("crit_") ? "Marge de critique " : "") + (competences[i.replace("crit_", "")] ? competences[i.replace("crit_", "")].label : i.replace("crit_", "")) + " + " + choix.list[i];
        accum += block.fn(this, {blockParams: [i, {"label": label}]});
      }
      break;

    default : 
      for(const i in list)
        var label = (i.includes("crit_") ? "Marge de critique " : "") + (competences[i.replace("crit_", "")] ? competences[i.replace("crit_", "")].label : i.replace("crit_", "")) + " + " + choix.list[i];
        accum += block.fn(this, {blockParams: [i, {"label": label}]});
      break;
  }

  return accum;
});

Handlebars.registerHelper('lookupDon', function (i) {
 return this.system.dons.list_dons["don"+i] ? this.dons[this.system.dons.list_dons["don"+i]] : {};
});


Handlebars.registerHelper('eachChoixDons', function (choix, data, block) {
  const type = choix.type;
  const bonus = choix.bonus;
  const list_choix = {};
  var accum = '';

  for (const key in data.competences) {
    if (type == "armes" || data.competences[key].sous_type == type) {
      if(data.competences[key].arme) {
        list_choix[key] = data.competences[key];
      }
    }
    if (type == "Radio") {
      if(Object.keys(choix.list ?? {}).indexOf(key) > -1)
      list_choix[key] = data.competences[key];
    }
  }

  for (const key in list_choix) {
    accum += block.fn(this, {blockParams: [key, list_choix[key]]});
  }

  return accum;
});

Handlebars.registerHelper('pickUpDonEvoChoosen', function (key, block) {
  const list = key ? key.split('_') : [];
  if(dons_evolutif[list[0]] && dons_evolutif[list[0]][list[1]] && dons_evolutif[list[0]][list[1]][list[2]])
    return  block.fn(dons_evolutif[list[0]][list[1]][list[2]]);
  return block.fn({"description": ""});
});

Handlebars.registerHelper('eachCompetences', function(block) {
  var accum = '';

  //sort by alphabetical order
  var sorted = [];
  for (const key in this.competences) {
      sorted[sorted.length] = key;
  }
  sorted.sort();

  var tempDict = {};
  for (var i = 0; i < sorted.length; i++) {
      tempDict[sorted[i]] = this.competences[sorted[i]];
  }

  //sort by value
  var sortable = [];
  for (var comp in this.system.competences) {
    if(this.system.competences[comp]) {
      sortable.push([comp, this.system.competences[comp].pts]);
    }
  }

  sortable.sort(function(a, b) {
      return b[1] - a[1];
  });

  var objSorted = {}
  sortable.forEach(function(item){
      objSorted[item[0]] = competences[item[0]]
  })

  //pick the desired sorted list
  const c = (this.options.competence == 0 ? this.competences : (this.options.competence == 1 ? tempDict : objSorted));

  //pick only the available competences
  var test_condition;
  for (const competence in c) {
    test_condition = false;
    if (this.competences[competence] && this.competences[competence].condition) {
      for (const cond in this.competences[competence].condition) {
        var condition_list = [];
        switch (cond) {
          case "don_evo" :
            condition_list = Object.values(this.system.dons_evolutif ?? {});
            break;
          case "don" :
            if(this.type == "character") {
              condition_list = Object.values(Object.fromEntries(
                Object.entries(this.system.dons?.list_dons ?? {}).filter(([key, value]) => {
                  return parseInt(key.slice(3)) < this.system.dons.nb_dons;
                })
              ));
              condition_list = condition_list.concat(donsBonus(this.system));
            } else {
              test_condition = true;
            }
            break;
          case "type" :
            condition_list = [this.type];
            break;
        }
        condition_list.forEach(val => test_condition = (this.competences[competence].condition[cond].indexOf(val) > -1) || test_condition)
      }
    }
    else if (this.competences[competence] && this.competences[competence].not_condition) {
      test_condition = true;
      for (const cond in this.competences[competence].not_condition) {
        var condition_obj = {};
        switch (cond) {
          case "don_evo" :
            condition_list = Object.values(this.system.dons_evolutif ?? {});
            break;
          case "don" :
            if(this.type == "character") {
              condition_list = Object.values(Object.fromEntries(
                Object.entries(this.system.dons?.list_dons ?? {}).filter(([key, value]) => {
                  return parseInt(key.slice(3)) < this.system.dons.nb_dons;
                })
              ));
              condition_list = condition_list.concat(donsBonus(this.system));
            } else {
              test_condition = true;
            }
            break;
          case "type" :
            condition_list = [this.type];
            break;
        }
        condition_list.forEach(val => test_condition = !(this.competences[competence].not_condition[cond].indexOf(val) > -1) && test_condition)
      }
    }
    else
      test_condition = true;
    if(test_condition)
      accum += block.fn(this, {blockParams: [Object.assign({},this.competences[competence],this.system.competences[competence]), competence]});
  }
  return accum;
});

Handlebars.registerHelper('margeCritCompetences', function(max_crit, competence_key){
  if(competence_key == "artisanats")
    return "";
  var crit_str = "1";
  if(competences[competence_key] && competences[competence_key].artisanat)
    max_crit += this.system.competences.artisanats ? this.system.competences.artisanats.marge_crit-1 || 0 : 0;
  for (var i = 2; i <= max_crit; i++)
    crit_str = crit_str + "-" + i;
  return crit_str;
});

Handlebars.registerHelper('eachDons', function(block) {
  const dons_temp = {};
  for (const key in dons) {
    dons_temp[dons[key].optgroup] = (dons_temp[dons[key].optgroup] ? dons_temp[dons[key].optgroup] : {});
    dons_temp[dons[key].optgroup][key] = dons[key];
  }

  var accum = '';

  for (const optgroup in dons_temp) {
    accum +='<optgroup label="'+optgroup+'">';
    for (const key in dons_temp[optgroup])
      accum += block.fn(this, {blockParams: [dons_temp[optgroup][key], key]});
    accum += '</optgroup>';
  }
  return accum;
});

Handlebars.registerHelper('eachDonsEvo', function(block) {
  const dons_temp = {};
  for (const key in dons_evolutif) {
    dons_temp[dons_evolutif[key].type] = (dons_temp[dons_evolutif[key].type] ? dons_temp[dons_evolutif[key].type] : {});
    dons_temp[dons_evolutif[key].type][key] = dons_evolutif[key];
  }

  var accum = '';

  for (const type in dons_temp) {
    accum +='<optgroup label="'+type+'">';
    for (const key in dons_temp[type])
      accum += block.fn(this, {blockParams: [dons_temp[type][key], key]});
    accum += '</optgroup>';
  }
  return accum;
});


Handlebars.registerHelper('eachDonEvo', function(n, block) {
  var accum = '';
  const don_evo = this.system.dons_evolutif['don_evo'+ n]
  const lvlDonEvo = this.system.basic_info.niveau - (3*n) + (dons_evolutif[don_evo] && dons_evolutif[don_evo].magie ? 2 : 0);
  for (const rang in dons_evolutif[don_evo])
    if(rang != "magie" && rang != "style" && rang != "type") {
        if(rang == "description")
          accum += block.fn(this, {blockParams: [[{"description":dons_evolutif[don_evo][rang]}], rang]});
        else if(Number(rang[rang.length - 1]) <= lvlDonEvo || don_evo == "Cartomancie")
          accum += block.fn(this, {blockParams: [dons_evolutif[don_evo][rang], rang]});
    }
  return accum;
});

Handlebars.registerHelper('eachTraitRacial', function(block) {
  var accum = '';
  const list_traits = races[this.system.basic_info.race].traits_racial;
  for (var i = 0; i < list_traits.length; i++) 
    accum += block.fn(this, {blockParams: [list_traits[i], i]});
  return accum;
});

Handlebars.registerHelper('eachEquipements', function(block) {
  var accum = '';
  const equipements = this.system.equipements;
  if(Object.keys(this.system.equipements).length > 0)
    for (const id in equipements) 
      accum += block.fn(this, {blockParams: [this.system, equipements[id], id]});
  return accum;
});

function eachWithOptGroup (obj, optgroupName, block) {
  const temp = {};
  for (const key in obj) {
    temp[obj[key][optgroupName]] = (temp[obj[key][optgroupName]] ? temp[obj[key][optgroupName]] : {});
    temp[obj[key][optgroupName]][key] = obj[key];
  }

  var accum = '';

  for (const optgroup in temp) {
    accum +='<optgroup label="'+optgroup+'">';
    for (const key in temp[optgroup])
      accum += block.fn(this, {blockParams: [temp[optgroup][key], key]});
    accum += '</optgroup>';
  }
  return accum;
}

Handlebars.registerHelper('eachSubEquipement', function(string, nb, block) {
    var accum = '';
    if (this.system.equipements[nb]) {
      var equipements = this.equipements[string][this.system.equipements[nb].emplacement];
      if (string == 'materiaux')
        try { equipements = this.equipements.materiaux[this.system.equipements[nb].emplacement][this.equipements.type_objet[this.system.equipements[nb].emplacement][this.system.equipements[nb].type_objet].materiaux]; } catch (error) {}      
      if (string == 'amelioration') {
        return eachWithOptGroup(equipements, 'group', block);
      }
      if(equipements) {
        for (var [equipement_key, equipement] of Object.entries(equipements)) {
          accum += block.fn(this, {blockParams: [equipement, equipement_key]});
        }
      }
    }
    return accum;
});

Handlebars.registerHelper('eachChoixDefauts', function(block) {
  const defauts_temp = {};
  for (const key in defauts.liste_defauts) {
    defauts_temp[defauts.liste_defauts[key].optgroup] = (defauts_temp[defauts.liste_defauts[key].optgroup] ? defauts_temp[defauts.liste_defauts[key].optgroup] : {});
    defauts_temp[defauts.liste_defauts[key].optgroup][key] = defauts.liste_defauts[key];
  }

  var accum = '';
  for(const optgroup in defauts_temp) {
    accum +='<optgroup label="'+optgroup+'">';
    for(const key in defauts_temp[optgroup]) {
      var defaut = defauts_temp[optgroup][key];
       accum += block.fn(this, {blockParams: [key, defaut.label]});
    }
    accum += "</optgroup>";
  }
  return accum;
});

Handlebars.registerHelper('eachAmelioration', function(ameliorations, block) {
  var accum = '';
  var id = 0;
  for(const amelioration in ameliorations) {
    id = parseInt(amelioration) +1;
    if(ameliorations[amelioration] != "") {
      accum += block.fn(this, {blockParams: [this.system, ameliorations[amelioration], amelioration]});
    }
  }
  if(!ameliorations) {
    ameliorations={};
  }
  accum += block.fn(this, {blockParams: [this.system, ameliorations[id], id]});
  return accum;
});

Handlebars.registerHelper('selectComplexe', function(objet, prefix, id, options){
  var $el = $('<select />').html( options.fn(this) );
  if(objet)
    $el.find('[value="' + objet[prefix+id] + '"]').attr({'selected':'selected'});
  return $el.html();
});

Handlebars.registerHelper('customSelect', function(objet, options){
  var $el = $('<select />').html( options.fn(this) );
  if(objet)
    $el.find('[value="' + objet + '"]').attr({'selected':'selected'});
  return $el.html();
});

Handlebars.registerHelper('selectEquipement', function(id, sufixe, options){
    var $el = $('<select />').html( options.fn(this) );
    $el.find('[value="' + this.system.equipements[id][sufixe] + '"]').attr({'selected':'selected'});
    return $el.html();
});

Handlebars.registerHelper('descriptionDon', function(id) {
  const don = dons[this.system.dons.list_dons['don'+id]];
  return ( don ? don.description : "");
});

Handlebars.registerHelper('autreBonusRace', function (id) {
  return (races[this.system.basic_info.race] ? (races[this.system.basic_info.race].t || undefined) : undefined);
});

Handlebars.registerHelper('descriptionTraitRacial', function (id) {
  const trait = races[this.system.basic_info.race].traits_racial[this.system.basic_info.trait_racial['trait'+id]];
  return ( trait ? trait.description : "");
});

Handlebars.registerHelper('encombrement', function () {
  const encombrement = calcul_poid(this.system.inventaire);
  const nb_emplacements_utilises = calcul_emplacement(this.system.inventaire).toFixed(1);
  const str_emplacements = (nb_emplacements_utilises <= 0 ? "Emplacements restant: " + (nb_emplacements_utilises*-1) : "Pas assez d'emplacements disponible <br>(" + nb_emplacements_utilises*1 + " emplacements supplémentaire nécéssaires)")+ "<br><br>";
  
  const malus = getMalusEncombrement(this.actor.type, this.system, this.options.flight);

  const str = str_emplacements + "Charge actuelle : " + encombrement.toFixed(3)+ " Kg <br>"+'<div style="position: relative">' + malus.description;
  const info = '<span class="show info" style="padding: 0 5px;"><i class="fas fa-question-circle"></i></span><div class="hide"><div>malus à l\'initiative de : '+malus.bonus_initiative+', malus aux compétences <i title="Compétences malusées par la charge" class="fas fa-weight-hanging"></i>: '+malus.malus_armure+'</div></div></div>';
  const warning_npc = '<br><span style="color:#820b0b"><i class="fas fa-exclamation-triangle"></i> Attention, le malus n\'est pas appliqué <i class="fas fa-exclamation-triangle"></i></span>';
  const indicateur = '<span style="font-size:0.7em">Poids maximum sans malus : '+malus.min+" Kg</span>";
  const max_charge = '<br><span style="color:#'+(this.actor.type == "npc" ? '820b0b' : "dddddd")+'; font-size:0.7em">impossible de porter plus de '+malus.max+' Kg</span>'
  return str + (malus > 0 ? ((this.actor.type == "npc" ? warning_npc : "") + info) : "") +"<br>"+ indicateur + max_charge;
});

Handlebars.registerHelper('calculTotalMoneyFromInventory', function () {
  const total_pc = calcul_somme_valeur(this.system.inventaire);
  const obj_total_money = getObjectMoneyFromPc(total_pc);
  return obj_total_money.po + " PO<br>" + obj_total_money.pa + " PA<br>" + obj_total_money.pc + " PB<br>";
});

Handlebars.registerHelper('malusInitiativeEncombrement', function () {
  const malus = getMalusEncombrement(this.actor.type, this.system, this.options.flight);
  return " "+plus_if_supp_zero(malus.bonus_initiative).replaceAll(" ","");
});

Handlebars.registerHelper('resumeBonus', function (id) {
  const item = new Equipement(this.system.equipements[id]);
  return item.resume_bonus();
});

Handlebars.registerHelper('resumeCouleurDonsEvo', function () {
    const list_class = {
      "passif": "Passif",
      "heal": "Heal",
      "damage": "Compétence de dégat",
      "buff": "Compétence de buff",
      "debuff": "Compétence de débuff",
      "autre": "Autre",
      "passif_situationnel": "Passif situationnel",
      "invocation": "Invocation",
      "reaction": "Réaction"
    };
    var str = "Signification du code couleur : <br/>";
    for (const key in list_class) {
      str += '<span class="'+key+'">'+list_class[key]+'</span> <br/>';
    }
    return str;
});

export class SimpleActorSheet extends ActorSheet {

  /** @override */
	static get defaultOptions() {
	  return foundry.utils.mergeObject(super.defaultOptions, {
  	  classes: ["hunaleros", "sheet", "actor"],
      width: 730,
      height: 800,
      competence: 0,
      tabs: [{navSelector: ".dons_evo_tabs", contentSelector: ".dons_evo_body", initial: "0"},
            {navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "inventaire"}]
    });
  }

  get template() {
    if ( !game.user.isGM && this.actor.limited ) return `systems/hunaleros/templates/actors/${this.actor.type}-sheet.html`;
    return `systems/hunaleros/templates/actors/${this.actor.type}-sheet.html`;
  }

  /* -------------------------------------------- */

  /** @override */
  getData() {
    const data = super.getData();
    data.dtypes = ["String", "Number", "Boolean"];

    const list = {
      "races" : races, 
      "competences" : competences, 
      "metiers" : metiers, 
      "dons" : dons, 
      "dons_evolutif" : dons_evolutif, 
      "langues" : langues, 
      "equipements" : equipements,
      "runes" : runes, 
      "defauts" : defauts, 
      "divinites" : divinites, 
      "charge_npc" : charge_npc, 
      "actor" : data.actor, 
      "options": data.options
    };
    //data.metiersArtisanat = ["Forgeron", "Mineur", "Cuisinier", "Tanneur", "Dépeceur", "Alchimiste", "Herboriste", "Tailleur", "Façonneur", "Bûcheron"]

    for( const key in list ){
        data.data[key] = list[key];
    }

    return data;
  }

  /* -------------------------------------------- */

  /** @override */
	activateListeners(html) {
    super.activateListeners(html);

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;


    html.find(".competences").on("click", ".competence-control", this._onClickKeyControl.bind(this, "competence"));
    html.find(".competences").on("click", ".arme-control", this._onClickKeyControl.bind(this, "arme"));
    html.find(".competences").on("click", ".artisanat-control", this._onClickKeyControl.bind(this, "artisanat"));
    html.find(".competences").on("click", ".edit", this._onClickKeyControl.bind(this, "edit"));
    html.find(".items").on("click", ".item-control", this._onClickTabKeyControl.bind(this, "inventaire"));
    html.find(".defauts").on("click", ".defaut-control", this._onClickTabKeyControl.bind(this, "defauts"));
    html.find(".equipements").on("click", ".equipement-control", this._onClickTabKeyControl.bind(this, "equipements"));

    html.find(".pop-up-lvl").on("click", ".close-button", this._onClickClosePopUp.bind(this, false));
    html.find(".pop-up-lvl-container").on("click", ".background", this._onClickClosePopUp.bind(this, false));
    html.find(".left-panel").on("click", ".open-button", this._onClickClosePopUp.bind(this, true));

    html.find(".bonus").on("click", ".daylight-control", this._onClickKeyControl.bind(this, "daylight"));
    html.find(".flight").on("click", ".flight-control", this._onClickKeyControl.bind(this, "flight"));

    for (const key of Object.keys(competences) ) {
      html.find(`#${key}`).on("click", ".button", this.onClickMacroButtonCompetence.bind(this, key, competences[key].label));
    }
    html.find(".initiative").on("click", ".button", this.onClickMacroButtonInitiative.bind(this));
    html.find(".pv_dices").on("click", ".button", this.onClickMacroButtonDesPV.bind(this));


    html.find(".pop-up-achat").on("click", ".close-button", this._onClickClosePopUpAchat.bind(this, false));
    html.find(".pop-up-achat").on("click", ".background", this._onClickClosePopUpAchat.bind(this, false));
    html.find(".pop-up-achat").on("click", ".valider", this._onClickAddMoneyThenClose.bind(this, false));
    html.find(".items").on("click", ".achat-button", this._onClickAchatVente.bind(this, false));
    html.find(".items").on("click", ".vente-button", this._onClickAchatVente.bind(this, true));
  }

  /* -------------------------------------------- */

  /** @override */
  setPosition(options={}) {
    const position = super.setPosition(options);
    const sheetBody = this.element.find(".sheet-body");
    return position;
  }

  /* -------------------------------------------- */

  async _onClickKeyControl(key, event) {
    event.preventDefault();
    const a = event.currentTarget;
    const action = a.dataset.action;

    if ( action === "sort") {
      const input = document.getElementById("input-"+key+"-control");
      const modulo = ((key == "competence" || key == "daylight" )? 3 : 2);
      const c = ((this.options[key] ? this.options[key] : 0 )+1)%modulo
      input.value = c
      this.options[key] = c;
      await this._onSubmit(event);
    }
  }

 async onClickMacroButtonCompetence(key, label, event) {
    const res = await this._popUpModificateur(`Jet : ${label}`);
    if (res != null) {
      const diceRes = await this._rollDice(`Jet pour la compétence ${label.toLowerCase()} :`, `1D30${res}`, event)
      const comp = this.object.system.competences[key];
      const marge = comp.value - diceRes.total;
      var message = marge >= 0 ? `Réussite de ${marge}` : "Echec";
      if(diceRes.natResults[0] == 30) {
        message = "Echec critique"
      }
      if(diceRes.natResults[0] <= comp.marge_crit) {
        message = "Réussite critique !";
      }

      ChatMessage.create({
        user: game.user._id,
        speaker: ChatMessage.getSpeaker({actor: this.object._id}),
        content: message
      },{rollMode: this.object.hasPlayerOwner ? "roll" : "gmroll"});
    }
  }

  async _popUpModificateur(title) {
    return new Promise(resolve => {
      let confirmed = false;

      new Dialog({
        title: title,
        content: `
          <form>
            <div class="form-group">
              <label>Bonus</label>
              <input id="bonus" name="bonus"/>
            </div>
            <div class="form-group">
              <label>Malus</label>
              <input id="malus" name="malus"/>
            </div>
          </form>
        `,
        buttons: {
          one: {
            icon: '<i class="fas fa-check"></i>',
            label: "Roll!",
            callback: () => {
              confirmed = true;
            }
          },
          two: {
            icon: '<i class="fas fa-times"></i>',
            label: "Cancel",
            callback: () => {
              confirmed = false;
            }
          }
        },
        default: "Cancel",
        close: function(html) {
          var result = null;
          if(confirmed) {
            var bonus = html.find('[name=bonus]')[0].value.toString();
            var malus = html.find('[name=malus]')[0].value.toString();
            result = (bonus ? '-'+bonus : "")+(malus ? '+'+malus : "");
          }
          resolve(result);
        }
      }).render(true);
    });
  }

  onClickMacroButtonInitiative(event) {
    let initiative = 0;

    if (this.object.system.attributs_chiffre) { //PJ
      initiative = this.object.system.attributs_chiffre.initiative;
    } 
    else if (this.object.system.statistiques) { //NPC
      const malus = getMalusEncombrement(this.actor.type, this.object.system, this.options.flight).bonus_initiative;
      initiative = this.object.system.statistiques.initiative + (malus >= 0 ? (malus > 0 ? "+"+malus : "") : malus);
    }
    this._rollDice("Initiative :", initiative, event);
  }

  onClickMacroButtonDesPV(event) {
    this._rollDice("Dés de vie :", this.object.system.statistiques.pv, event);
  }

  async _rollDice(titre, str_to_roll, event) {
    event.preventDefault();
      
    const roll = new Roll(str_to_roll.toUpperCase());
    await roll.evaluate();
    
    roll.toMessage({ flavor: titre, speaker: { actor: this.object._id }}, { rollMode: this.object.hasPlayerOwner ? "roll" : "gmroll" });

    await this._onSubmit(event);
    return {total: roll.total, natResults: roll.dice.map(d => d.results[0].result)};
  }

  async _onClickClosePopUp(bool, event) {
    const input = document.getElementById("input-pop-up");
    const c = bool ? 1 : 0;
    input.value = c
    this.options.popUp = c;
    await this._onSubmit(event);    
  }

  async _onClickClosePopUpAchat(bool, event) {
    const input = document.getElementById("achat-input-pop-up");
    const c = bool ? 1 : 0;
    input.value = c
    this.options.achatPopUp = c;
    if (bool) {
      this.resetAchatVenteInput();
    }
    await this._onSubmit(event);    
  }

  resetAchatVenteInput() {
    document.getElementById("achat").querySelectorAll("input")[0].value = '0'; //po
    document.getElementById("achat").querySelectorAll("input")[1].value = '0'; //pa
    document.getElementById("achat").querySelectorAll("input")[2].value = '0'; //pc
  }

  getTotalMoney(money_obj) {
    return checkIfNumberOrReturnZero(money_obj.pc) + (checkIfNumberOrReturnZero(money_obj.pa)*10) + (checkIfNumberOrReturnZero(money_obj.po)*100);
  }

  _onClickAddMoneyThenClose(bool, event) {
    var old_money = this.getTotalMoney(this.object.system.money); //calcul total ancient montant argent
    var buy_sell_money = this.getTotalMoney(this.object.system) * (this.options.achatVente ? 1 : -1); //calcul total de la dépense ou de l'ajout

    var total_pc = old_money + buy_sell_money //calcul total argent après changement en pc
    const money = getObjectMoneyFromPc(total_pc); // transformation pa/pc/po

    document.getElementById("argent").querySelectorAll("input")[2].value = money.pc;
    document.getElementById("argent").querySelectorAll("input")[1].value = money.pa;
    document.getElementById("argent").querySelectorAll("input")[0].value = money.po;
    this.resetAchatVenteInput();
    this._onClickClosePopUpAchat(bool, event);
  }

  _onClickAchatVente(bool, event) {
    this.options.achatVente = bool;
    this._onClickClosePopUpAchat(true, event);
  }

  async _onClickTabKeyControl(key, event) {
    event.preventDefault();
    const a = event.currentTarget;
    const action = a.dataset.action;
    const obj = this.object.system[key] || [];
    const form = this.form;

    const names = {
      "inventaire": {"value": "Item", "key": "name"},
      "defauts": {"value" : Object.keys(defauts.liste_defauts)[0], "key":"name"},
      "equipements": {"value" : Object.keys(equipements.emplacements)[0], "key" :"emplacement"}
    }

    if ( action === "create") {
      const nk = (Object.keys(obj).length > 0 ? Number(Object.keys(obj)[Object.keys(obj).length-1])+1 : 0);
      let newKey = document.createElement("div");
      newKey.innerHTML = `<input type="text" name="system.`+key+`.${nk}.`+names[key].key+`" value="`+names[key].value+`"/>`;
      newKey = newKey.children[0];
      form.appendChild(newKey);
      await this._onSubmit(event);
    }

    else if ( action === "delete" ) {
      const li = a.closest("."+(key == "inventaire" ? "item" : key.slice(0,-1)));
      li.parentElement.removeChild(li);
      await this._onSubmit(event);
    }
  }


  inf_zero(chiffre) {
    return (chiffre > 0 ? chiffre : 0);
  }

  init_carac(caracterique, formData) {
    const data = foundry.utils.expandObject(formData).system;
    formData['system.abilities.'+caracterique+'.pts'] = (data.abilities[caracterique] ? data.abilities[caracterique].pts || 10 : 10);
  }

  calcul_caracteristique(caracterique, formData, data, bonus) {
    if (!formData['system.abilities.'+caracterique+'.pts']) {   //surement inutile, à voir;
      this.init_carac(caracterique, formData);
    }
    formData['system.abilities.'+caracterique+'.value'] = data.abilities[caracterique].pts + bonus[caracterique];
  }

  calcul_caracteristiques(formData, bonus) {
    var data = foundry.utils.expandObject(formData).system;
    ["force", "constitution", "dexterite", "intelligence", "sagesse", "charisme"].forEach(caracterique => this.calcul_caracteristique(caracterique, formData, data, bonus));
    data = foundry.utils.expandObject(formData).system;
    fusion_obj(bonus, this.bonus_abilities(data), data.basic_info.niveau);
  }

  calcul_competence(formData, competence, bonus) {
    const data = foundry.utils.expandObject(formData).system;
    const char = this.actor.type == "character"
    formData['system.competences.'+competence+'.pts'] = (formData['system.competences.'+competence+'.pts'] || 0);
    formData['system.competences.'+competence+'.value'] = this.calcul_value_competence(char, data, competence, formData, bonus);
    if (char) {formData['system.competences.'+competence+'.marge_crit'] = 1 + (bonus["crit_"+competence] || 0) + (bonus["crit_"+competences[competence].ability] || 0) + (bonus["type_"+competence] ? bonus["type_"+competence].critique || 0 : 0);}
  }

  calcul_value_competence(isACharacter, data, competence, formData, bonus) {
    const ability_value = isACharacter ? data.abilities[competences[competence].ability].value : data.abilities[competences[competence].ability];
    let pts_competence = this.getPtsCompetence(competence, data, bonus);
    let autre_bonus = bonus[competence] || 0;

    if(competence == "perspicacite") {
      pts_competence = this.getPtsCompetence("perspicacite", data, bonus) +
                       this.getPtsCompetence("volonte", data, bonus) + (this.object.system.competences["volonte"]?.pts || 0) +
                       this.getPtsCompetence("psychologie", data, bonus) + (this.object.system.competences["psychologie"]?.pts || 0);
      autre_bonus = (bonus["perspicacite"] || 0) + (bonus["volonte"] || 0) + (bonus["psychologie"] || 0) ;
    }

    const malus_armure = (competences[competence].malus_armure ? (bonus.malus_armure || 0) : 0);
    return ability_value + (pts_competence || ((bonus.competence_non_apprise ?? 0) -4)) + autre_bonus - malus_armure;
  }

  getPtsCompetence(competence, data, bonus) {
    return (data.competences[competence]?.pts || 0) + (bonus["pts_"+competence] || 0);
  }

  calcul_competences(formData, bonus) {
    var data = foundry.utils.expandObject(formData).system;
    this.add_flight_bonus({"flight": flight}, bonus); //abcd
    if(data.competences == null) { //mode édition
      for (const competence in competences) {
        formData['system.competences.'+competence+'.pts'] = this.object.system.competences[competence]?.pts ?? 0;
      }
      data = foundry.utils.expandObject(formData).system;
    }
    for (const competence in data.competences) {
      this.calcul_competence(formData, competence, bonus)
    }

    for (const key in this.object.system.competences) {
      if (!(key in competences) || key == "artisanat") { 
        formData[`system.competences.-=${key}`] = null;
      }
    }
  }

  nb_dons(formData, bonus) {
    const data = foundry.utils.expandObject(formData).system;
    formData['system.dons.nb_dons'] = 2 + (1*data.basic_info.niveau) + (bonus.bonus_don || 0);
  }

  calcul_bonus_dons_choix(data, i) {
    const choix_res = data.choix && data.choix.dons ? data.choix.dons[i] : null;
    var choix = dons[data.dons.list_dons["don"+i]].choix;
    var init_bonus = choix.bonus || {}
    var bonus = {};
    if(choix_res) {
      if(choix.type == "Radio") {
        bonus[choix_res] = choix.list[choix_res];
      } else {
        bonus["type_"+choix_res] = {};
        for(const key in init_bonus) {
          if (key == "toucher") {
            bonus[choix_res] = init_bonus[key];
          }
          else {
            bonus["type_"+choix_res][key] = init_bonus[key];
          }
        }
      }
    }
    return bonus;
  }

  dons(formData, bonus) {
    const data = foundry.utils.expandObject(formData).system;
    var bonus_dons = {};
    var don;
    var bonus_don;

    for (const key in bonus.dons) {
      fusion_obj(bonus_dons, (dons[bonus.dons[key]] ? dons[bonus.dons[key]].bonus : {}));
    }

    //calcul bonus dons
    for (var i = 0; i < formData['system.dons.nb_dons']; i++) {
      don = dons[data.dons.list_dons['don'+i]] || {};
      if (don.choix)
        fusion_obj(bonus_dons, this.calcul_bonus_dons_choix(data, i));
      bonus_don = {}
      fusion_obj(bonus_don, (don ? don.bonus : {}));
      if( don.optgroup == "Dons de magie" && bonus.bonus_dons_magique_mana ) {
        bonus_don.pm += bonus.bonus_dons_magique_mana;
      }
      fusion_obj(bonus_dons, bonus_don);
    }
    //delete bonus dons in formdata
    for (const key in this.actor.system.dons.bonus_dons) {
      if ( !bonus_dons.hasOwnProperty(key) ) formData['system.dons.bonus_dons.-='+key] = null;
    }
    //write bonus dons in formdata
    for (const key in bonus_dons) {
      formData['system.dons.bonus_dons.'+key] = bonus_dons[key];
    }
    fusion_obj(bonus, (bonus_dons || {}), (data.basic_info ? data.basic_info.niveau : 0 ));
  }

  calcul_bonus_equipements(data) {
    var bonus = {}
    for (const key in data.equipements) {
      if (data.equipements[key] && data.equipements[key].actif){
        fusion_obj(bonus, new Equipement(data.equipements[key]).calcul_bonus_equipement());
      }
    }
    fusion_obj(bonus, {}, (data.basic_info ? data.basic_info.niveau : 0 ));
    return bonus;
  }

  bonus_abilities(data) {
    const abilities = data.abilities;
    var bonus = {};
    bonus.bonus_force = Math.round(this.inf_zero((is_value_in_list_dons("inversion", data) ? abilities.dexterite.value : abilities.force.value)-10)/3);
    bonus.bonus_consti = Math.round((abilities.constitution.value-10)/3);
    bonus.bonus_consti = (bonus.bonus_consti > 1 ? bonus.bonus_consti : 1);
    bonus.bonus_dex = Math.round(this.inf_zero(abilities.dexterite.value-10)/3);
    bonus.bonus_intel = Math.round(abilities.intelligence.value/5);
    bonus.bonus_intel = (bonus.bonus_intel > 1 ? bonus.bonus_intel : 1);
    bonus.bonus_sagesse = Math.round(abilities.sagesse.value/5);
    bonus.bonus_charisme = Math.round(this.inf_zero(abilities.charisme.value-10)*3);
    bonus.pv_par_niveaux = bonus.bonus_consti;
    bonus.pm_par_niveaux = bonus.bonus_intel;
    bonus.bonus_initiative = this.inf_zero(abilities.dexterite.value-10);
    return bonus;
  }

  add_daylight_bonus(obj, bonus) {
    if (obj && obj.daylight) {
      let daylight_option = "";
      if(this.options) {
        if (this.options.daylight == 0)
          daylight_option = "day"
        else if (this.options.daylight == 2)
          daylight_option = "night"
      }
      fusion_obj(bonus, obj.daylight[daylight_option] || {})
    }
  }

  add_flight_bonus(obj, bonus){
    if (obj && obj.flight) {
      let flight_option = "";
      if(this.options) {
        flight_option = (this.options.flight ? "vol" : "sol");
      }
      fusion_obj(bonus, obj.flight[flight_option] || {})
    }
  }

  bonus_traits(data, nb_traits) {
    var bonus_trait = {};
    var trait_temp = {};
    var trait;

    for (var i = 0; i < nb_traits; i++) {
      trait = data.basic_info.race && races[data.basic_info.race] ? races[data.basic_info.race].traits_racial[data.basic_info.trait_racial['trait'+i]] : {};
      this.add_flight_bonus(trait, bonus_trait);
      this.add_daylight_bonus(trait, bonus_trait);
      trait_temp = (trait ? trait.bonus : {});
      for (const attrname in trait_temp) { 
        bonus_trait[attrname] = bonus_trait[attrname] ? (Array.isArray(bonus_trait[attrname]) ? bonus_trait[attrname].concat(trait_temp[attrname]) : bonus_trait[attrname] + trait_temp[attrname]) : trait_temp[attrname]; 
      }
      if (trait && trait.choix && data.choix && data.choix.traits) {
        var bonus_choix = {};
        for (const key in data.choix.traits[i]) {
          bonus_choix[data.choix.traits[i][key]] = (bonus_choix[data.choix.traits[i][key]] ?? 0) + trait.choix[key].list[data.choix.traits[i][key]];
        }
        fusion_obj(bonus_trait, bonus_choix);
      }
      trait_temp = {};
    }
    fusion_obj(bonus_trait, {}, (data.basic_info ? data.basic_info.niveau : 0 ));
    return bonus_trait
  }

  pickUpChoix(n, nb_rang, i, data) {
    if(data.choix && data.choix.dons_evolutif && data.choix.dons_evolutif[n] &&
     data.choix.dons_evolutif[n]["Rang "+nb_rang] && data.choix.dons_evolutif[n]["Rang "+nb_rang][i])
        return data.choix.dons_evolutif[n]["Rang "+nb_rang][i];
    return "";
  }

  pickUpDonEvoChoosen (key) {
    const list = key ? key.split('_') : [];
    if(dons_evolutif[list[0]] && dons_evolutif[list[0]][list[1]] && dons_evolutif[list[0]][list[1]][list[2]])
      return  dons_evolutif[list[0]][list[1]][list[2]];
    return {};
  }

  bonus_dons_evo(data) {
    var bonus_dons_evo = {};
    var don_evo;
    const nb_don_evo = (data.basic_info.niveau > 6 ? 3 : (data.basic_info.niveau > 3 ? 2 : 1));
    var lvlDonEvo = 0;
    var don_evo_temp = {};
    for (var n = 0; n < nb_don_evo; n++) {
      don_evo = dons_evolutif[data.dons_evolutif["don_evo"+n]];
      lvlDonEvo = (don_evo && don_evo["Rang 1"] ? data.basic_info.niveau - (3*n) + (don_evo.magie ? 2 : 0) : 0);
      for (var nb_rang = 1; nb_rang <= lvlDonEvo; nb_rang++) {
        if (don_evo["Rang "+nb_rang]){
          for (var i = 0; i < don_evo["Rang "+nb_rang].length; i++) {
            don_evo_temp = don_evo["Rang "+nb_rang][i];
            if(don_evo_temp.choix) {
              var choix = this.pickUpChoix(n, nb_rang, i, data);
              if (don_evo_temp.choix.type == "Radio"){
                var bonus_choix_radio = {}
                bonus_choix_radio[choix] = don_evo_temp.choix.list[choix];
                fusion_obj(bonus_dons_evo, bonus_choix_radio);
              }
              else if (don_evo_temp.choix.type == "Type_Arme_Distance" || don_evo_temp.choix.type == "Type_Arme_Melee") {
                var bonus_choix_armes = {}
                bonus_choix_armes["type_"+choix] = {};
                for (const key_bonus_choix in don_evo_temp.choix) {
                  if (key_bonus_choix != "type")
                    bonus_choix_armes["type_"+choix][key_bonus_choix] = don_evo_temp.choix[key_bonus_choix];
                }
                fusion_obj(bonus_dons_evo, bonus_choix_armes);
              }
              else
                fusion_obj(bonus_dons_evo, this.pickUpDonEvoChoosen(choix).bonus);
            }
            if(don_evo_temp.bonus_conditionel) {
              fusion_obj(bonus_dons_evo, bonus_don_evo_conditionnel(don_evo_temp, data));
            }
            if(don_evo_temp.bonus) 
              fusion_obj(bonus_dons_evo, don_evo_temp.bonus);
          }
        }
      }
    }
    fusion_obj(bonus_dons_evo, {}, (data.basic_info ? data.basic_info.niveau : 0 ));
    return bonus_dons_evo;
  }

  bonus_defauts(data) {
    const bonus = {};
    for (var char_defaut in data.defauts) {
      const defaut = data.defauts[char_defaut] || {};
      var defaut_bonus = (defaut.name && defauts.liste_defauts[defaut.name] ? defauts.bonus[defauts.liste_defauts[defaut.name].type_defaut][defaut.bonus] || {} : {});
      fusion_obj(bonus, (defaut.name && defauts.liste_defauts[defaut.name] ? defauts.liste_defauts[defaut.name].bonus || {} : {}))
      fusion_obj(bonus, (defaut_bonus.bonus || {}));
    }
    fusion_obj(bonus, {}, (data.basic_info ? data.basic_info.niveau : 0 ));
    return bonus;
  }

  malus_encombrement(data, bonus) {
    const malus = getMalusEncombrement(this.actor.type, this.object.system, this.options.flight);
    return {
      "bonus_initiative" : malus.bonus_initiative,
      "malus_armure": malus.malus_armure
    }
  }

  ajout_malus_emcombrement(formData, bonus) {
    const data = foundry.utils.expandObject(formData).system;
    fusion_obj(bonus, this.malus_encombrement(data, bonus), (data.basic_info ? data.basic_info.niveau : 0 ));
  }

  calcul_bonus_races(data) {
    const race = races[data.basic_info.race];
    const bonus = (race.bonus ? fusion_obj({}, race.bonus) : {});
    for (const key in race) {
      if (key == "choix") {
        if(data.choix && data.choix.race) {
          for (const key in data.choix.race) {
            bonus[data.choix.race[key]] = (bonus[data.choix.race[key]] ?? 0) + race.choix[key]?.list[data.choix.race[key]];
          }
        }
      }
      else if (key == "daylight") {
        this.add_daylight_bonus(race, bonus);
      }
      else if (key == "flight"){
        this.add_flight_bonus(race, bonus);
      }
      else if (key != "traits_racial")
        bonus[key] = race[key];
    }
    return bonus;
  }

  bonus(formData) {
    const data = (foundry.utils.expandObject(formData).system || {});
    var bonus = {};
    const char_lvl = (data.basic_info ? data.basic_info.niveau : 0 );
    fusion_obj(bonus, this.bonus_defauts(data), char_lvl);
    fusion_obj(bonus, (data.basic_info && metiers[data.basic_info.metier] ? metiers[data.basic_info.metier].bonus : {}), char_lvl);
    fusion_obj(bonus, this.calcul_bonus_races(data), char_lvl);
    fusion_obj(bonus, this.bonus_dons_evo(data), char_lvl);
    fusion_obj(bonus, this.bonus_traits(data, (1 + (bonus.bonus_don_racial || 0))), char_lvl);
    var bonus_equipements = this.calcul_bonus_equipements(data);
    bonus_equipements.RD = (bonus_equipements.RD||0) * (bonus.multiplicateur_bonus_rd_armure||1);
    fusion_obj(bonus, bonus_equipements, char_lvl);
    return bonus;
  }

  calcul_damage(formData, bonus) {
    var temp_bonus_equipements;
    const type_no_bonus_force = ["type_arbalete", "type_arc"];
    
    if (bonus.degat) 
    bonus.degat.forEach(element => { 

      temp_bonus_equipements = {};
      fusion_obj(temp_bonus_equipements, bonus[element.type_objet] || {});
      fusion_obj(temp_bonus_equipements, bonus);
      const bonus_force = (type_no_bonus_force.indexOf(element.type_objet) > 0 ? (bonus[element.type_objet] ? bonus[element.type_objet].bonus_force || 0 : 0) : temp_bonus_equipements.bonus_force)

      const dmg = (element.bonus_D_dmg + (temp_bonus_equipements.bonus_D_dmg || 0)) + "D" + (element.type_D + (temp_bonus_equipements.bonus_classe_D || 0)) + plus_if_supp_zero(bonus_force + (element.bonus_dmg || 0) + (temp_bonus_equipements.bonus_dmg || 0));
      const crit = (element.critique || 0) + (temp_bonus_equipements.critique || 0);
      const PA = 0 + (element.PA || 0) + (temp_bonus_equipements.PA || 0);
      const portee = 0 + (element.portee || 0) + (temp_bonus_equipements.portee || 0);
      formData['system.attributs_chiffre.dmg_cac'].push({"label": element.label, "dmg": dmg, "critique":  this.getCritStr(crit), "PA": PA, "portee": portee});
    });

  }

  getCritStr(crit) {
    var crit_str = "1";
    for (var i = 2; i <= crit; i++)
      crit_str = crit_str + "-" + i;
    return crit_str
  }

  attributs_chiffres(formData, bonus) {
    const data = foundry.utils.expandObject(formData).system;
    bonus.bonus_force = bonus.bonus_force*(bonus.multiplicateur_bonus_force || 1);
    formData['system.attributs_chiffre.bonus_force'] = bonus.bonus_force;
    formData['system.health.max'] = Math.round(((data.abilities.constitution.value*2.6)+(data.health.pts || 0)+(bonus.pv || 0))*(bonus.multiplicateur_pv || 1));
    formData['system.mana.max'] = Math.round((this.inf_zero(data.abilities.sagesse.value-10) + this.inf_zero(data.abilities.intelligence.value-10)+(data.mana.pts || 0)+(bonus.pm || 0)) * ((bonus.multiplicateur_pm == null || bonus.multiplicateur_pm == undefined) ? 1 : bonus.multiplicateur_pm));
    formData['system.attributs_chiffre.pv_temp.max'] = data.abilities.constitution.value + (bonus.pv_temp || 0);
    formData['system.attributs_chiffre.pts_souffle.max'] = data.abilities.constitution.value + (bonus.pts_souffle || 0);
    formData['system.attributs_chiffre.initiative'] = "1D10" + plus_if_supp_zero(bonus.bonus_initiative || 0);
    formData['system.attributs_chiffre.regen_pv'] = data.basic_info.niveau + Math.round(data.abilities.constitution.value/5) + (bonus.regen_pv || 0);
    formData['system.attributs_chiffre.D_regen_pv'] = 2 + (bonus.D_regen_pv || 0);
    formData['system.attributs_chiffre.regen_pm'] = data.basic_info.niveau + bonus.bonus_sagesse + (bonus.regen_pm || 0);
    formData['system.attributs_chiffre.D_regen_pm'] = 2 + (bonus.D_regen_pm || 0);
    formData['system.attributs_chiffre.deplacement'] = (4 + bonus.bonus_dex + (bonus.deplacement || 0)) * (bonus.multiplicateur_deplacement || 1);
    formData['system.attributs_chiffre.dmg_cac'] = [];
    this.calcul_damage(formData, bonus);
    formData['system.attributs_chiffre.multiplicateur_charge'] = bonus.multiplicateur_charge || 1;
    formData['system.attributs_chiffre.RD'] = 0 + (bonus.RD || 0);
    formData['system.attributs_chiffre.RM'] = 0 + (bonus.RM || 0) + Math.floor((formData['system.ki.value'] || 0)/4);
    formData['system.bonus_Pts_Competences'] = 0 + bonus.bonus_Pts_Competences;
    formData['system.bonus_Pts_Caracs'] = 0 + bonus.bonus_Pts_Caracs;
    formData['system.bonus_don_racial'] = 0 + bonus.bonus_don_racial;
    formData['system.bonus_portee_sort'] = 0 + bonus.portee_sort;

    if (formData['system.health.value'] > formData['system.health.max'])
      formData['system.health.value'] = formData['system.health.max'];

    if (formData['system.mana.value'] > formData['system.mana.max'])
      formData['system.mana.value'] = formData['system.mana.max'];

    if (is_value_in_list_dons("force_intimidante", data))
      fusion_obj(bonus, {"provocation" : bonus.bonus_force});

    formData['system.restriction'] = bonus.restriction || [];
  }


  /* -------------------------------------------- */

  migration_data(data, formData) {
    if(this.actor.type == "npc") {
      if ((typeof this.object.system.competences) == "string") {
        formData['system.competences_old'] = this.object.system.competences;
        this.object.system.competences = {};
      }
    }

    if (data.defauts) {
      for (const key in data.defauts) {
        if (this.object.system.defauts[key] && this.object.system.defauts[key].name) {
          if(!defauts.liste_defauts.hasOwnProperty(this.object.system.defauts[key].name))
          data.defauts[key].precision = this.object.system.defauts[key].name;
        }
      }
    }

    //retirer séduction équipement

    return formData;
  }

  migration_amelioration_arme(formData) {
    var dict = {"arme":["emoussee", "aceree", "allonge", "encordee", "ergonomique", "faucheuse", "fourchue", "perforante", "sacrificielle", "sanglante", "antigeant", "cognedur", "critique", "ecrasante", "harponneuse", "silencieuse", "vampirique"],
                "armure": ["renforceeRD", "renforceeRM", "ajustee", "allegee", "elementaire", "discrete", "molletonnee", "aeree", "delestee", "altruiste", "impermeable", "imposante", "emissaire", "scene", "incassable", "piquante", "securise"]};
    var equipements = this.object.system.equipements;
    if(equipements) {
      for (const [equipement_key, equipement] of Object.entries(equipements) ) {
        if(equipement.amelioration) {
          for (const [amelio_key, amelio] of Object.entries(equipement.amelioration) ) {
            if (amelio && amelio != "" && !isNaN(amelio)) {
              formData[`system.equipements.${equipement_key}.amelioration.${amelio_key}`]=dict[equipement.emplacement][+amelio];
            }
          }
        }
      }
    }
    return formData;
  }

  migration_competences(formData) {
    if (this.object.system.competences) {
      if(formData["system.competences.athletisme.pts"])
        formData["system.competences.athletisme.pts"] += (this.object.system.competences["escalade"]?.pts ?? 0) + (this.object.system.competences["natation"]?.pts ?? 0);
      if(formData["system.competences.discretion.pts"])
        formData["system.competences.discretion.pts"] += (this.object.system.competences["prestidigitation"]?.pts ?? 0);
      if(formData["system.competences.acrobatie.pts"])
        formData["system.competences.acrobatie.pts"] += (this.object.system.competences["contorsion"]?.pts ?? 0);
      if(formData["system.competences.representation.pts"])
        formData["system.competences.representation.pts"] += (this.object.system.competences["deguisement"]?.pts ?? 0);
      if(formData["system.competences.survie.pts"])
        formData["system.competences.survie.pts"] += (this.object.system.competences["navigation"]?.pts ?? 0);
      if(formData["system.competences.epee.pts"])
        formData["system.competences.epee.pts"] += (this.object.system.competences["epeel"]?.pts ?? 0);
      if(formData["system.competences.hast.pts"])
        formData["system.competences.hast.pts"] += (this.object.system.competences["hastl"]?.pts ?? 0);
      if(formData["system.competences.fleaux.pts"])
        formData["system.competences.fleaux.pts"] += (this.object.system.competences["fleaul"]?.pts ?? 0);
      if(formData["system.competences.hache.pts"])
        formData["system.competences.hache.pts"] += (this.object.system.competences["hachel"]?.pts ?? 0);
      if(formData["system.competences.marteau.pts"])
        formData["system.competences.marteau.pts"] += (this.object.system.competences["marteaul"]?.pts ?? 0);
      if(formData["system.competences.eloquence.pts"])
        formData["system.competences.eloquence.pts"] += (this.object.system.competences["diplomatie"]?.pts ?? 0) + (this.object.system.competences["seduction"]?.pts ?? 0);
    }
    return formData;
  }


  /** @override */
  _updateObject(event, formData) {

    //Update for characters with processed values

    const list_to_process = { 
      "character" : ["inventaire", "defauts", "equipements"],
      "npc" : ["inventaire"]
    };

    formData = this.migration_competences(formData);
    formData = this.migration_amelioration_arme(formData);

    const data = foundry.utils.expandObject(formData).system;

    //fix moche
    formData = this.migration_data(data, formData);

    //handle the free-form items list
    list_to_process[this.object.type].forEach(item => {
      if (data) {
        const items = data[item] || {};
        // Remove items which are no longer used
        for ( let k of Object.keys(this.object.system[item]) ) {
          if ( !items.hasOwnProperty(k) ) {
            items[`-=${k}`] = null;
          }
          else if (item == "equipements") {
            if(item[k].amelioration && this.object.system[item][k].amelioration) {
              for ( let i of Object.keys(this.object.system[item][k].amelioration) ) {
                if ( !items[k].amelioration.hasOwnProperty(i) ) items[k].amelioration[`-=${i}`] = null;
              }
            }
          }
        }

        // Re-combine formData
        const a = {id: this.object.id};
        a["system."+item] =  items;
        formData = Object.entries(formData).filter(e => !e[0].startsWith("system."+item)).reduce((obj, e) => {
          obj[e[0]] = e[1];
          return obj;
        }, a);
      }
    })

    var bonus = {};
    if(this.actor.type == "character") {
      bonus = this.bonus(formData);
      this.nb_dons(formData, bonus);
      this.dons(formData, bonus);
      this.calcul_caracteristiques(formData, bonus);
    }
    this.ajout_malus_emcombrement(formData, bonus);
    this.calcul_competences(formData, bonus);

    if(this.actor.type == "character") {
      this.attributs_chiffres(formData, bonus); 
    }

    if(this.actor.type == "npc") {
      const malus = getMalusEncombrement(this.actor.type, this.object.system, this.options.flight).bonus_initiative;
      formData['system.attributs_chiffre.initiative'] = this.object.system.statistiques.initiative + (malus >= 0 ? (malus > 0 ? "+"+malus : "") : malus);
    }
    // Update the Actor
    return this.object.update(formData);
  }
}
