const divinites = [
		{
		"label" : "Ether",
		"theme" : "Déesse créatrice et fondatrice de toute chose, elle est à l'origine de la vie et de l'équilibre des forces de ce monde.",		
		"description" : "À l'époque où tout n'était que néant, un être, errant dans les abîmes, d'une puissance inimaginable décida de mettre fin à sa solitude millénaire. Il façonna l'espace, l'univers. Mais cela lui était insuffisant, tout était froid et inanimé. À l'aide des quatre éléments primordiaux, Ether, comme les mortels la nomment depuis la nuit des temps, créa le monde. Pour l'aider dans sa quête, Ether imagina des extensions d'elle-même, elle venait de créer les dieux. Quatre pour être précis : Itzal dieu des ténèbres, Ilum déesse de la lumière, Khaine déesse de la glace et Yuimen dieu de la foudre. Les océans, les montagnes, les volcans, le ciel naquirent de ses doigts avec l'aide de ses quatre bras droits. Mais il manquait encore quelque chose … d'autres êtres, capables d'apprécier son œuvre, d'y vivre et d'évoluer en son sein. L'équilibre s’installa au fil des millénaires, créant la vie et la laissant se développer et suivre son cours. Bien que précaire au commencement, l'évolution amena avec elle des êtres doués d'intelligence. D'autres dieux furent crée durant ce temps, mais aucun ne trouva autant grâces aux yeux d'Ether que ses quatre serviteurs qui l'avaient aidée à former ce qu'elle nomma la Terre. C'est alors qu'Ether préféra rester auprès de ses enfants, elle descendit sur terre, accompagnée de ses quatre bras droits. La puissance de chacun fut scellée dans des artefacts. Quant à elle, elle scinda son âme en quatre, enfermant chaque fragment d'elle dans un orbe. Les artefacts et les orbes furent confiés aux races qui peuplaient la terre et vénéraient les dieux élémentaires. Et depuis ce jour, Ether veille sur sa création et ne se réveillera que si ses enfants l'appellent à l'aide."
	},
	{
		"label" : "Ilum",
		"theme" : "Le soleil et la lumière",		
		"description" : "Lorsque Ether créa les quatre sous-éléments, Itzal le dieu des ténèbres fût le premier à voir le jour. Il plaça le monde à la faveur de la nuit perpétuelle, puis naquit Ilum, déesse du soleil, amenant avec elle la lumière sur le monde, permettant à la vie de se développer sur la terre. Jouant un rôle plus que primordial dans le cycle de la vie et l'équilibre du monde, Ilum se devait de se battre pour avoir sa place dans l'univers et partager ce monde avec son frère Itzal qui n'était pas du même avis. C'est d'ailleurs pour cela que la déesse est toujours représentée armée d'un arc et des flèches d'une immaculée lumière, car bien que bienveillante, elle ne reste pas moins une divinité forte et combative."
	},
	{
		"label" : "Itzal",
		"theme" : "La nuit et les ténèbres",		
		"description" : "Dieu de l'ombre, lui et Ilum se relaient chaque jour qui passe, depuis la création des temps, afin de perpétuer le cycle du jour et de la nuit. Bien qu'il fut une époque où la terre ne connaissait que la pénombre, Itzal dû se résigner à partager sa place avec sa sœur, Ilum, afin que la vie puisse s'implanter sur terre. Malgré son amertume envers cette période révolue durant laquelle il était le seul serviteur d'Ether, Itzal accepta sa place dans le cycle de la vie, et depuis ce jour, lui et la déesse du soleil œuvre main dans la main pour l'équilibre du monde."
	},
	{
		"label" : "Yuimen",
		"theme" : "La foudre et les orages",		
		"description" : "Quand le ciel est ébranlé et que la terre tremble, quand les éclairs déchirent le ciel, quand le tonnerre gronde, quand les vents se mettent à hurler et que la grêle cingle la terre tel un fouet, Yuimen est là. Les orages sont sacrés pour lui et ses fidèles, qui se tiennent traditionnellement sous tous les orages, montrant leur admiration pour son pouvoir et sa fureur, priant et louant leur dieu avant et après chaque déluge. Yuimen est le dernier dieu créer par Ether, il est aussi le plus fougueux, et le plus prompt à la colère. Il est d'ailleurs commun d'entendre que la foudre qui illumine le ciel par temps orageux, n'est ni plus ni moins que le fruit d'une des colères de la divinité."
	},
	{
		"label" : "Khaine",
		"theme" : "La neige et la glace",		
		"description" : "Certainement la plus fidèle des quatre bras droits d'Ether, Khaine est connu pour sa douceur, son calme et sa sagesse. C'est elle qui répand de fins voiles de neige sur le monde, qui plonge la terre dans sa torpeur hivernale, qui laisse place au calme avant l'effervescence du printemps. On raconte que Khaine battit les vastes étendues polaires du monde, exerçant ses pouvoirs et créant de gigantesques sculptures de glaces. Certaines sont d'ailleurs toujours debout et en états, bien que la plupart se sont, au fils du temps, transformées en ce que l'on appelle aujourd'hui des icebergs et des montagnes."
	},
	{
		"label" : "Kafshëve",
		"theme" : "Les monstres et les mutants",		
		"description" : "Kafshëve possède une force surnaturelle qui change les âmes et les corps et œuvre à la destruction du monde matériel. Pour les gens du commun, elle est la mère de légions de mutants ou de monstres. Elle serait d'ailleurs à l'origine de la race des vatholas et des plus rares naskagarris, qu'elle aurait créé par plaisir personnel en mélangeant humains et bêtes. Les races intelligentes comme les centaures, les sirènes, les nagas, etc. seraient également les fruits de la création de la déesse, mais difficile de séparer le vrai du faux dans les multiples histoires et légendes qui l'évoquent."
	},
	{
		"label" : "Phargonis",
		"theme" : "Les océans et les rivières",		
		"description" : "Phargonis n'est pas un Dieu bon ou mauvais, chaotique ou loyal, il est tout à la fois. Sa colère se manifeste souvent par des tempêtes, parfois très violentes… Sa clémence peut se faire ressentir en aidant un navire en perdition par un doux vent. Phargonis peut prendre plusieurs formes, sur terre, il sera un homme d'une cinquantaine d'années possédant une barbe blanche abondante et portant un immense Trident qu'il est le seul à pouvoir porter. En mer, il sera un Triton géant, mi-homme, mi-poisson. Beaucoup de marins vénèrent le dieu des océans, espérant que leurs prières apporteront des pêches abondantes et des voyages sans encombres."
	},
	{
		"label" : "Siducia",
		"theme" : "L'amour et la passion",		
		"description" : "Siducia la déesse des couples, de l'amour et du feu ardent qui les unit. Son affinité avec les amants lui vaut parfois d'être trop associée, à tors, à sa sœur Lujura. Siducia est toujours accompagnée d'un phœnix, qui représente d'ailleurs son emblème. On raconte qu'il s'agirait là de son défunt époux, un humain, qu'elle aurait ramené à la vie car ne supportant pas d'être séparée de celui qu'elle aimait. Depuis, ils ne se quitteraient plus, l'oiseau de feu accompagnant son aimée dans chacune de ses renaissances."
	},
	{
		"label" : "Lujuria",
		"theme" : "La luxure et le plaisir",		
		"description" : "Lujuria, sœur aînée de Siducia, et la déesse de la tentation, en quête insatiablement des divers plaisirs de la vie, surtout sexuels. L'activité favorite de la déesse est de charmer d’innocentes proies, avec un faible pour les hommes, et de les attirer dans leur futur tombeau telle une mante-religieuse. Lujuria arbore deux visages, l'un étant sous l'apparence d'une magnifique jeune femme peu vêtue, accompagnée de ses servantes. Une fois sa cible sous son emprise, elle révélera alors son vrai visage, bien plus hideux, et dévorera sa victime tout en se délectant du plaisir charnel que son corps peut lui procurer."
	},
	{
		"label" : "Karcina",
		"theme" : "La peinture et les couleurs",		
		"description" : "Déesse de la peinture et des couleurs, Karcina est une déesse qui croque « la vie » à pleines dents. Elle est bien souvent la muse de beaucoup artistes qui se plaisent à la représenter sous une infinité et déclinaisons de couleurs. Il est d'ailleurs difficile de ne pas connaître son nom en vue du nombre d’œuvres dont elle est le sujet. Cette popularité et cette reconnaissance ne sont pas pour déplaire à la déesse, bien au contraire. On dit que les arc-en-ciels qui surviennent après la pluie seraient un petit signe de Karcina pour ses adeptes."
	},
	{
		"label" : "Milost",
		"theme" : "La pitié et la miséricorde",		
		"description" : "Dieu des miséricordieux, souvent prié lorsque des individus, des peuples voir des empires ont tout perdus. Milost apparaîtrait aux yeux des démunies, sous formes d'une créature aux abords malades et à l'apparence plutôt miteuse, ne souhaitant pas se placer aux dessus de quiconque. Il aide les nécessiteux, se mêlant à eux, les guidant afin que ceux-ci restent dans le droit chemin, et puissent retrouver un semblant de vie, alors que l'espoir les avaient quittés. Une fois sa mission accomplie, il s'en ira aussi mystérieusement qu'il est venu, afin d'aider d'autres âmes en détresse. Milost n'est pas spécialement apprécié de beaucoup de dieux, qui le voient, pour la plupart, comme trop affable et lui reproche de se mêler aux hommes comme s'il était l'un des leurs."
	},
	{
		"label" : "Sumadhura",
		"theme" : "La musique et le rythme",		
		"description" : "Dieu de l'art de la musique et du rythme de manière générale, Sumadhura est souvent prié par les artistes, en particulier les troupes de bateleurs qui parcourent le monde en vivants de leur art. Toujours accompagné de sa raie Sinar ainsi que de son hautbois, le dieu parcourt le monde, jouant des mélodies tantôt discrètes, tantôt tapageuses, laissant son humeur et ses voyages guider ses compositions. Artiste et surtout autodidacte, il est le musicien attitré des dieux, qui se délectent toujours de ses œuvres hautes en couleurs, d'un style incomparable et surtout inimitable. Rares sont les mortels ayant eu l'honneur d'entendre ses mélodies, mais, une croyance tenace dit que les prodiges, ayant la fameuse « oreille musicale » plus poussée que la norme, aurait entendu une des œuvres du dieu et auraient ainsi obtenus une capacité hors-norme à la composition et réalisations de musiques mémorables."
	},
	{
		"label" : "Morroyak",
		"theme" : "Les animaux et les changes-formes",		
		"description" : "Morroyak, dieu de la transformation corporelle, plus particulièrement animale. Très proche de la nature, Morroyak n'est pas une divinité très populaire, hormis pour les peuples comme les elfes, toujours très connectés à la nature. Morroyak peut apparaître sous diverses apparences, mais gardant toujours quelques similitudes dans ses transformations. Il s'agira toujours d'un animal principalement noir, rehaussé de signes runiques d'un bleu chatoyant, bleu qui se retrouvera également dans les yeux de l'animal. Il est très rare que le dieu se montre sous sa véritable forme, il est même probable qu'il ne possède pas de forme humaine. Les apparitions de Morroyak, selon les écrits, ont toujours été furtifs et ses intentions assez obscures, le dialogue restant inexistant. Ce que l'on sait, c'est que lorsque Morroyak daigne se montrer, c'est qu'il est préférable de rester sur ses gardes."
	},
	{
		"label" : "Sumendi",
		"theme" : "Les volcans et la lave",		
		"description" : "Beauté endormie pendant des années, des siècles … Parfois même pendant des millénaires. Mais lorsque Sumendi sort de sa torpeur, et fait se réveiller un volcan, elle entame alors un magnifique, mais destructeur ballet. Les avis divergent à son sujet, certains parlent de punition lorsqu’un volcan entre en éruption, d'autres d'un avertissement que ferait la déesse et certains, les plus dévoués, parlent d'un spectacle rare offert aux hommes. Mais tout le monde s'accorde sur un point, Sumendi est une déesse aussi dangereuse que belle, et gare à celui ou celle qui éveillera sa colère."
	},
	{
		"label" : "Ubica",
		"theme" : "L'assassinat et la sournoiserie",		
		"description" : "Dieu de la fourberie, Ubica n'a aucun scrupule à tromper, mener en bateau et baratiner qui que ce soit pour parvenir à ses fins. Il a déjà eu l'occasion de tromper plusieurs dieux, et, force est de constater qu'il faillit y rester plus d'une fois. Heureusement pour lui, il manie aussi bien la dague que la langue de bois, Ubica se sortira toujours des situations les plus périlleuses, réussissant à chaque fois à se déculpabiliser voir accuser autrui de ses crimes. Et bien que chacun sait qu'il ment comme il respire, nul n'a réellement envie de se le mettre à dos, ses capacités de combat n'étant plus à prouver. Le plus simple étant encore, de fermer les yeux, et faire comme-ci le mensonge n'en était pas un."
	},
	{
		"label" : "Kovac",
		"theme" : "La forge et l'artisanat",		
		"description" : "Kovac est un dieu particulièrement vénéré et respecté des nains. Son apparence brute, ainsi que sa longue barbe tressée lui donne effectivement un air de ressemblances avec les petits mortels que représentent les nains. Mais ce qu'ils vénèrent par-dessous tout, c'est le grand forgeron, et plus généralement, artisan, que représente Kovac. Maître dans la forgerie, il créa les artefacts les plus puissants, les armes des dieux eux-mêmes, leur insufflant magie et puissance à chaque coup de son marteau sur le fer brûlant. On dit qu'il est toujours enfermé dans sa forge céleste, à honorer les commandes des autres dieux et à créer de nouvelles armes, qu'il destine aux futurs héros du monde."
	},
	{
		"label" : "Lurra",
		"theme" : "Les naissances et la vie",		
		"description" : "Lurra, n'est que bienveillance et amour, et bien qu'elle soit dénuée de toute notion de combat, elle est très fortement respectée et des dieux et des habitants de la terre. Elle représente la vie, celle qui l’insuffle dans chaque corps, chaque enveloppe corporelle, aussi bien pour les Hommes, les bêtes et les plantes. Les futures mères, peu importe leur origine, ont pour habitude de prier Lurra, pour que leur grossesse ainsi que l'accouchement se passe sans encombre, mais aussi pour que leurs enfants naissent en bonne santé et que leur vie soit la plus parfaite possible. Les fausses couches, les enfants morts-nés et la stérilité, sont souvent perçu comme un signe de la déesse, un avertissement, parfois une punition, bien qu'il n'en soit rien, depuis des millénaires, le faux se mêle parfois au vrai."
	},
	{
		"label" : "Borrokar",
		"theme" : "La guerre et la force",		
		"description" : "Borrokar, le puissant maître de guerre, l'imbattable guerrier, modèle pour tout combattant. Il n'est pas connu pour son intelligence ou encore son savoir vivre, ni même sa diplomatie. Non, Borrokar sait se battre, et c'est ce qu'il fait le mieux. Aucun dieu n'a encore réussir à le vaincre en combat singulier, et pourtant, beaucoup ont essayés. Beaucoup de ses adeptes tentent tant bien que mal d'atteindre une puissance colossale dans l'espoir d'attirer l’attention de Borrokar et de le défier. Car une bruit court, que le combattant invaincu se languit de ne trouver un adversaire à sa taille."
	},
	{
		"label" : "Beskyte",
		"theme" : "Les boucliers et les armures",		
		"description" : "Logée dans cette imposante armure, difficile de déterminer s'il s'agit là d'une déesse ou d'un dieu. En réalité, cela importe peu, ce n'est pas un physique que Beskyte souhaite montrer, mais bel et bien sa robustesse et sa capacité à protéger. Son équipement aurait été forgé par Kovac, dieu de la forge, en insufflant une partie de l'énergie de Beskyte dans ses créations. Depuis, on dit qu'il est impossible pour la divinité de se séparer de son armure, car toutes deux seraient liées à tout jamais."
	},
	{
		"label" : "Falsked",
		"theme" : "Les apparences et le pour-paraître",		
		"description" : "Si l'on pourrait confondre Falsked et Ubica, c'est en effet parce qu'ils vont de pair. Là où Ubica excelle dans la sournoiserie et les tours de passe-passe, Falsked lui, est plus discret, plus pondéré. Il représente le faux semblant, l'hypocrisie dissimulée derrière des masques aux différentes apparences. Expert en subtilités sociales, il parvient toujours à faire aller les autres dieux dans son sens, habile des mots et toujours accompagné d'une expression qui semble à chaque fois si sincère et véridique."
	},
	{
		"label" : "Mastilo",
		"theme" : "L'écriture et la calligraphie",		
		"description" : "Enfermé chaque jour passant dans sa bibliothèque personnelle, Mastilo n'accepte d'être dérangé que pour écouter les grandes lignes et nouveautés du monde. En effet, celui-ci retranscrit absolument tout ce qui mérite de rester dans les mémoires. Dans ses ouvrages, chacun écrit de sa main, on pourrait y trouver les réponses à toutes les questions, qu'elles concernent les dieux ou les mortels. Concernant sa propre histoire, difficile d'en savoir beaucoup plus, il existe probablement un livre autobiographique que Mastilo garde précieusement rangé dans une de ses centaines d'étagères."
	},
	{
		"label" : "Sassoiko",
		"theme" : "Les saisons et la nature",		
		"description" : "À la création du monde et plus particulièrement de la Terre, Sassoiko fût une des premières divinités à naître de par leur mère Ether. Rapidement, son existence se posa comme étant essentielle au bon déroulement de la vie et eut une place de choix parmi les dieux. Très aimée des mortels et d'Ether elle-même, Sassoiko est pourtant du genre discrète et assidue. Elle ne fait que peu de choses des louages qui lui sont adressées et se satisfait d'avantage, comme récompense, de voir que les cycles de la nature restent parfaits."
	},
	{
		"label" : "Ituna",
		"theme" : "Les pactes et les alliances",		
		"description" : "La présence d'Ituna ne signifie qu'une seule chose : Un pacte est en train d'avoir lieu et le dieu s'autoproclame comme garant de celui-ci. Son intervention n'est cependant pas à prendre à la légère, puisqu'il ne daigne apparaître que pour les pactes ou alliances ayant une potentielle importance. Si jamais l'un des participants ne tient pas parole, Ituna viendra lui-même appliqué sa sentence. Il est également possible de pactiser directement avec le dieu, malheureusement, les enjeux risques d'être bien plus conséquents qu'entre simples mortels."
	},
	{
		"label" : "Denbora",
		"theme" : "Le temps et les cycles",		
		"description" : "Déesse du temps qui passe, des jours qui s'écoulent et de la jeunesse qui se flétrit. Bien que ce ne soit pas elle qui s’occupe de la vie et de la mort ou d'autres notions, elle est souvent amenée à côtoyer respectivement : Lurra, la déesse de la vie, Heriotza la déesse de la mort et Sassoiko, la déesse des saisons. Son rôle pilier l'oblige à constamment œuvrer avec d'autres divinités, afin de faire perdurer ce que les mortels appels le cycle de la vie. Les chronomanciens vouent un culte à celle-ci, mais ils savent également qu'il est dangereux de jouer avec le temps et que malgré sa discrétion, celle-ci peut venir rétablir le fragile équilibre qu'elle s'efforce de préserver."
	},
	{
		"label" : "Svemir",
		"theme" : "L'univers et la galaxie",		
		"description" : "Malgré ses traits très fins, Svemir est un dieu et non une déesse, arborant l’apparence d'un jeune homme. Bien qu'il n'est pas crée la Terre, celui-ci, à la demande d'Ether, imagina tout ce qui se trouvait autour de la première planète. Il créa ainsi la galaxie, les étoiles, les autres planètes et plus globalement : Il combla l'espace vide qu'était l'Univers. Encore très ignorant de ce qui se trouve en dehors de leur planète, les mortels ne cessent de se questionner sur ce qui se trouve au-delà de leurs connaissances et si Svemir est toujours à l'ouvrage. Pour la plupart, la réponse est oui, bien qu'il n'y ait aucune réelle preuve de cette théorie."
	},
	{
		"label" : "Lovkinja",
		"theme" : "La chasse et les proies",		
		"description" : "Déesse vénérée des chasseurs, Lovkinja représente à la fois ceux qui chassent et ceux qui sont chassés. Lovkinja est souvent représentée armée d'un arc, mais sans carquois ou de flèche pour abattre ses cibles. Proche des animaux, sa légende veut que les proies, une fois exténuées par la traque de la déesse, s'offrirait eux-mêmes à elle, afin de conclure la chasse. Si la déesse estime que la bête à vaillamment lutté pour sa survie, elle graciera l’animal voir lui demandera de faire partie de sa meute. Celle-ci est d'ailleurs composée, d'après les récits, d'un corbeau, d'une chouette et d'un lièvre qui l'accompagnent partout."
	},
	{
		"label" : "Kaosa",
		"theme" : "Le chaos et la discorde",		
		"description" : "Si la plupart des dieux et déesses œuvrent pour une unité et un équilibre du monde et de ce qui le régit, ce n'est pas le cas de Kaosa. Quel ennui lorsque tout se déroule comme prévu, quelle monotonie, où se trouve la surprise, l'excitation ou la découverte dans tout ceci ? De par ses divergences de comportement et de pensées, Kaosa préfère faire cavalier seul et tracer sa route sous ses propres règles, c'est à dire aucune. Kaosa est imprévisible et bien que cela lui valu dors et déjà des problèmes avec les autres dieux, cela semble trop difficile à Kaosa de se prêter à la conformité. Il semblerait que sa dernière apparition date du début du conflit des aingéales et des dehamanes, qui avait semblé particulièrement l’intéresser. Y serait-elle pour quelque chose ?"
	},	
	{
		"label" : "Nocta'Mora",
		"theme" : "Les cauchemars et les songes",		
		"description" : "Quelle personne en ce monde peut se vanter de n'avoir jamais cauchemardé ? De ne jamais avoir connu la peur ? Le pouls qui s'accélère, les sueurs froides et le souffle court. Nocta'Mora aime voir ce genre de spectacles et se délecte de trouver ce que les esprits se dissimulent à eux-mêmes, et surtout le leur montrer. Observer le mal qu'ils s'infligent devant ces « révélations ». Et s'en nourrir… Chaque être pensant est une boîte de pandore qui redoute d'être ouverte, ce à quoi le dieu voue son existence. La pudeur ainsi n'est pas de mise… Il vagabonde dans les rêveries qu'il provoque, avide de surprises et de futurs tourments. Mais quelque fois, il lui est nécessaire de s'arrêter. Appelé, ou entravé, selon l'individu. Bien que cela ne soit que partie remise … Lorsque le sommeil les rattrapera."
	},
	{
		"label" : "Lehendakari",
		"theme" : "Le jugement et l'impartialité",		
		"description" : "La déesse est en réalité aveugle, son casque cachant ses yeux entièrement blancs. Mais elle n'a nul besoin de vision afin de rendre son jugement et ses tout ce qui fait sa force. Elle juge les âmes des défunts et seuls les actes menés durant leur vivant, feront basculer celles-ci vers les enfers ou les cieux. Il n'existe aucune âme qui ait échappée au jugement de Lehendakari et beaucoup de mortels ayant menés une vie de débauche et de crimes, finissent par craindre la sentence que prononcera la déesse quand leur heure sera venue."
	},
	{
		"label" : "Nahiko",
		"theme" : "Beauté et orgueil",		
		"description" : "L'humilité n'est en rien un mot pouvant qualifier Nahiko, mais plutôt un antonyme de celui-ci. La déesse, magnifique créature, modèle de perfection et de beauté chez les dieux et les mortels, connaît parfaitement ses atouts. Malheureusement, la belle déesse à une forte tendance à se croire supérieure aux autres et à traiter, même ses semblables, avec un dédain plus que prononcé. Son comportement lui valu le surnom de « Diva », à qui on la compare souvent les femmes nobles qui font trop de manières."
	},
	{
		"label" : "Gatanje",
		"theme" : "Le hasard et les jeux",		
		"description" : "Les hommes parlent souvent de la roue du paon pour évoquer cet élégant animal aux couleurs vives. Mais qu'en est-il de la roue de la chance ? Celle qui finit toujours par tourner, inexorablement. Lors de paris, de jeux d'argent ou plus simplement pour un événement important, prières et offrandes sont alors adressées à Gatanje, seule maîtresse du destin. Certains diront que le hasard est ce qui dirige les vies des mortels, mais il n'en ait rien. Seule Gatanje choisit. Lorsqu'un groupe d'aventuriers découvre inopinément un trésor, la chance n'a rien à voir la dedans, lorsque la foudre s’abat sur un homme plutôt qu'un arbre, ce n'est pas le hasard, lorsqu'une flèche visant le cœur se retrouver stoppée par une pièce située dans une poche, c'est que Gatanje l'a décidé."
	},
	{
		"label" : "Hériotza",
		"theme" : "La mort et le deuil",		
		"description" : "Antonyme de la vie et de Lurra, Hériotza reprend la vie que sa sœur a insufflé à chaque être vivant. Bien que détestée des mortels de par son rôle cruel, elle est primordiale à la continuité du cycle de la vie. Si l'on suit les histoire et récits la concernant, sa robe serait constituée d'une multitude de fils, chacun représentant une vie. Lorsque l'heure est venue pour un individu, la déesse sectionne le fil et redirige l'âme du défunt vers Lehendakari, qui la guidera jusqu'à son repos éternel."
	},
	{
		"label" : "Tajne",
		"theme" : "La sagesse et les énigmes",		
		"description" : "Tajne apparaît parfois devant les yeux d'aventuriers ou voyageurs en quête de secrets. Hormis son apparence qui le caractérise, il est facilement reconnaissable par son élocution. En effet, celui-ci ne parle qu'en vers, et souvent, il n'est pas là pour discuter, mais bien pour poser une de ses énigmes favorites. Bien entendues, celles-ci sont de lui et reflètent une complexité rien qu'à leur énonciation. Cependant, une belle récompense sera offert à ceux qui trouveront la réponse. Il pourra leur indiquer l'emplacement d'un trésor, ou alors une réponse à une question qu'ils pourraient se poser. Le dieu semble se divertir de cette façon, heureux de faire tourner en bourrique les esprits les plus vifs."
	},
	{
		"label" : "Pohlepa",
		"theme" : "La gourmandise et l'excès",		
		"description" : "D'une apparence hideuse, Pohlepa fait frémir les plus courageux des guerriers. Fort heureusement pour eux, il ne sort que très rarement le nez de ses victuailles dont il s'empiffre matin et soir. Il pourra peut-être faire son entrée des de grands banquets, sans y être invité, mais cela n'est arrivé que très rarement. Il se fiche bien d'attiser la moquerie de ses compères et le dégoût chez les mortels, seul tenter de satisfaire son insatiable appétit n'a d'importance pour lui."
	},		
	{
		"label" : "Novarion",
		"theme" : "La cupidité et l'or",		
		"description" : "Novarion ne jure que par le précieux, plus précisément l'or, qu'il trouve magnifique. Avar, il désirerait tout posséder, pour transformer le monde et ses habitants en statues d'or éternelles et inestimables. Fort heureusement pour la Terre, les autres dieux ne l'entendent pas de cette oreille et ont reclus Novarion dans une prison. Celle-ci est devenue sa prison dorée, au sens littéral, tous les meubles, les murs et ses servantes ont été transformés en or. Il vit désormais seul, dans son palais reflétant son plus grand défaut … Jusqu'à ce qu'il ne décide d'en sortir un jour."
	},
	{
		"label" : "Kletva",
		"theme" : "Les poisons et les malédictions",		
		"description" : "Très appréciée des villes et villages en marge des civilisations, les habitants brûlent régulièrement de l'encens avant les chasses ou les attaques, afin, selon leurs croyances, d'améliorer l'efficacité de leurs poisons et maléfices. Il n'y a jamais eu aucune preuve que ces prières aient une réelle utilité, mais la déesse fait partie intégrante de leur culture. Elle est également connue dans les milieux moins sauvages, mais les pratiquants se font beaucoup plus rares."
	},
	{
		"label" : "Helvede",
		"theme" : "Le plan chaosien, les démons et la destruction",		
		"description" : "Maîtresse des enfers, elle accueille les âmes ayant été jugées comme mauvaises et les gardes dans son royaume pour une souffrance éternelle. Helvede ne possède pas un tempérament doux et son apparence parle d'elle-même, elle est connue pour ses excès de colère et la destruction massive et aveugle qu'ils engendrent. Elle est également liée aux démons, qui voient en elle un leader puissant auprès de qui il vaut mieux faire profil bas et obéir."
	},
	{
		"label" : "Endéo",
		"theme" : "Le plan céleste, les anges et la pureté",		
		"description" : "À l'instar d'Helvede et les enfers, Endéo est le dirigeant du paradis. Il admet les âmes ayant été jugées comme bonnes et les acceptent pour une éternité douce et paisible. Lui et sa sœur sont en totale opposition tant sur leurs royaumes que leur comportement, cependant ils se complètent l'un et l'autre pour l’harmonie du monde. Tout comme elle et les démons, il est lié aux anges, survolant les cieux et donnant ses directives si jamais ses enfants ont besoin de lui."
	},
	{
		"label" : "Lövell",
		"theme" : "Le plan spirituel, les esprits",		
		"description" : "Lövell est représentée avec des tons pâles de bleu et de blanc, ce qui lui donne une apparence éthérée, avec de longs cheveux fluides et des robes qui se fondent dans la lumière environnante. Son visage est partiellement masqué par une sorte de masque, lui permettant de voir entre les mondes vivants et morts. Lövell est perçue comme une gardienne des esprits et une guide dans le monde spirituel. Sa présence lumineuse et sereine inspire la paix et la sagesse, suggérant qu’elle aide les âmes à trouver leur chemin dans l’au-delà ou à atteindre un état de sérénité spirituelle."
	},
];

export default divinites;