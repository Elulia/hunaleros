const defauts = {
	type_defaut :{
		"mineur": {
			"label": "Mineure"
		},
		"moyen": {
			"label": "Modérée"
		},
		"majeur": {
			"label": "Sévère"
		}
	},
	bonus: {
		"mineur": [
			{
				"label": "+ 5 points de vie",
				"bonus" : {
					"pv": 5
				}
			},
			{
				"label": "+ 5 points de mana",
				"bonus" : {
					"pm": 5
				}
			},
			{
				"label": "+ 1 case de déplacement",
				"bonus" : {
					"deplacement": 1
				}
			},
			{
				"label": "+ 2 à l'initiative",
				"bonus" : {
					"bonus_initiative": 2
				}
			},
			{
				"label": "+ 2 Po",
				"bonus" : {

				}
			},
			{
				"label": "+ 1D régénération Pv",
				"bonus" : {
					"regen_pv": 1
				}
			},
			{
				"label": "+ 1D régénération Pm",
				"bonus" : {
					"regen_pm": 1
				}
			}				
		],
		"moyen": [
			{ 
				"label": "+ 2 points de compétence",
				"bonus" : {
					"bonus_Pts_Competences": 2
				}
			},
			{
				"label": "+ 20 points de vie",
				"bonus" : {
					"pv": 20
				}
			},
			{
				"label": "+ 20 points de mana",
				"bonus" : {
					"pm": 20
				}
			},
		
			{
				"label": "+ 5 à l'initiative",
				"bonus" : {
					"bonus_initiative": 5
				}
			},
			{
				"label": "+ 4 cases de déplacement",
				"bonus" : {
					"deplacement": 4
				}
			},
			{
				"label": "+ 3D régénération Pv",
				"bonus" : {
					"regen_pv": 3
				}
			},
			{
				"label": "+ 3D régénération Pm",
				"bonus" : {
					"regen_pm": 3
				}
			},
			{ 
				"label": "+ 10 Po",
				"bonus" : {

				}
			}
		],
		"majeur": [
			{ 
				"label": "+ 1 point de caractéristique",
				"bonus" : {
					"bonus_Pts_Caracs": 1
				}
			},
			{ 
				"label": "+ 5 points de compétence",
				"bonus" : {
					"bonus_Pts_Competences": 5
				}
			},
			{
				"label": "+ 50 points de vie",
				"bonus" : {
					"pv": 50
				}
			},
			{
				"label": "+ 50 points de mana",
				"bonus" : {
					"pm": 50
				}
			},
			{
				"label": "+ 1 don",
				"bonus" : {
					"bonus_don": 1
				}
			},
			{
				"label": "+ 15 à l'initiative",
				"bonus" : {
					"bonus_initiative": 15
				}
			},
			{ 
				"label": "+ 50 Po",
				"bonus" : {

				}
			},
			{ 
				"label": "+ 1 trait racial",
				"bonus" : {
					"bonus_don_racial": 1
				}
			}
		]
	},
	
};

const liste_defauts = {
	"aboulie" : {
		"label": "Aboulie",
		"moyen" : {
			"description" : "L’aboulie se manifeste par une incapacité à agir sans impulsion extérieure. Le personnage ne peut pas utiliser la compétence volonté. Il suit facilement les ordres concrets et dictés d’un tuteur, mais il ne peut jamais résister seul à des défis mentaux ou prendre une décision par soi-même, même si sa vie en dépend.",
			"bonus" : {

			}
		},
	},	
	"addiction" : {
		"label": "Addiction",
		"mineur" : {
			"description" : "prise au moins une fois par semaine",
			"bonus" : {

			}
		},
		"moyen" : {
			"description" : "consommation au moins une fois par jour",
			"bonus" : {

			}
		},
		"description" : "Sexe, alcool, jeux d’argent, drogues, café, tabac, travail, achats, etc.<br>"+
		"Tout manquement entraînera des stades de manque, pour un maximum de 3. Dès qu’il y a manque, le personnage fera tout ce qu’il peut pour satisfaire son addiction."
	},
	"age_avance" : {
		"label": "Âge avancé",
		"description" : "La retraite, ce n’est pas encore pour tout de suite (environs 60 % de la durée de vie) !",
		"majeur" : {
			"description" : "Bonus de 3 à la sagesse, malus de 2 à la force, un malus de 4 à l’agilité et un malus de 6 aux action de séductions.",
			"bonus" : {
				"dexterite": -4,
				"force": -2,
				"sagesse": 3,

			}
		},
	},
	"anorexique" : {
		"label": "Anorexique",
		"moyen" : {
			"description" : "L’anorexie est un trouble alimentaire où la personne refuse de manger, souvent par peur de prendre du poids. Pour chaque repas, le personnage doit réussir un jet de volonté avec un malus de 4 pour réussir à ingérer sa nourriture, puis un autre jet avec les mêmes conditions pour éviter de vomir. La privation de nourriture entraîne des paliers de famine, qui sont détaillés dans l’état préjudiciable famine.",
			"bonus" : {

			}
		},
	},
	"anthropophobie" : { 
		"label": "Anthropophobie",
		"moyen" : {
	 		"description" : "Le personnage a une peur exacerbée des interactions sociales, qu’il évitera autant que possible. S’il n’a vraiment pas le choix, il devra réussir un jet de volonté avec un malus de 6 afin de réussir à dialoguer. S’il échoue, il fuira la conversation ou se refermera sur lui-même.",
	 		"bonus" : {

	 		}
	 	}
	},
	"asthmatique" : { 
		"label": "Asthmatique",
		"description" : "L’asthme est une maladie respiratoire qui se manifeste par des gênes respiratoires, les efforts physiques essoufflent le personnage. La fréquence et l’intensité des crises d’asthme varient d’un individu à l’autre.",
	 	"mineur" : {
	 		"description" : "Les points de souffle sont divisés par deux",
	 		"bonus" : {//TODO

	 		}
	 	},
	 	"moyen" : {
	 		"description" : "Les points de souffle sont divisés par deux, malus de 1 à sa vitesse de déplacement et un malus de 2 à tout jet de résistance qui englobe les poumons",
	 		"bonus" : {
            "deplacement": -1
	 		}
	 	},
	 	"majeur" : {
	 		"description" : "Les points de souffle sont divisés par deux, malus de 2 à sa vitesse de déplacement, malus de 6 à tout jet de résistance qui englobe les poumons et interdit la course.",
	 		"bonus" : {
            "deplacement": -2

	 		}
	 	}
	},
	"aveugle" : { 
		"label": "Aveugle",
		"majeur" : {
	 		"description" : "Le personnage est privé du sens de la vue, mais ses autres sens bénéficient d’un bonus de 5. Les compétences suivantes nécessitent le soutien du compagnon et subissent un malus de 6 : commerce, médecine, psychologie, discrétion, représentation, manœuvres, esquive et compétences d’arme. Tandis que la compétence de survie est impossible. Pour compenser, le personnage a développé un lien étroit avec un animal intelligent ou un allié humanoïde. Le soutien et la communication de son compagnon lui permettent d’évoluer sans difficulté. En revanche, si le compagnon ne voit pas le personnage ou ne peut pas parler, il ne pourra pas agir efficacement ni savoir quoi faire.",
	 		"bonus" : {
				"restriction": ["survie"],
				"corpsacorps":-6,
				"couteaux":-6,
				"hast":-6,
				"epee":-6,
				"fleaux":-6,
				"hache":-6,
				"marteau":-6,
				"arbalete":-6,
				"arc":-6,
				"jet":-6,
				"fouet":-6,
				"bouclier":-6,
				"esquive":-6,
				"manoeuvres":-6,
				"discretion":-6,
				"commerce":-6,
				"medecine":-6,
				"psychologie":-6,
				"representation":-6
	 		}
	 	}
	},
	"blocage_emotionnel" : { 
		"label": "Blocage émotionnel",
	 	"mineur" : {
	 		"description" : "Le personnage ne ressent que de manière extrêmement faible les émotions. Ses réactions émotionnelles sont désactivées et il est souvent perçu comme froid ou détaché. Il ne peut plus être affecté par la plupart des états émotionnels et les capacités de psychologie et éloquence lui sont interdites.",
	 		"bonus" : {
				"restriction": ["psychologie", "eloquence"]
	 		}
	 	},
	},
	"boiteux" : { 
		"label": "Boiteux",
		"description" : "Le personnage doit vivre avec un handicap physique qui l’empêche de se mouvoir correctement.",
	 	"mineur" : {
	 		"description" : "Ainsi, sa vitesse de déplacement est réduite de moitié (arrondit à l’inférieur) et la course lui est interdit.",
	 		"bonus" : {
	 		//TODO
	 		}
	 	},
	},
	"catatonie_transitoire" : {
		"label": "Catatonie transitoire",
		"majeur" : {
			"description" : "La catatonie à 10 % de chance de se déclencher dans des situations de stress intense, comme lorsqu’il subit des dégâts, fait un échec critique ou pour toute situation particulièrement stressante. Le personnage se retire alors complètement de la réalité. Il reste immobile, les yeux dans le vide, ne réagit plus aux stimulus du monde extérieur pendant une dizaine de minutes.",
			"bonus" : {

			}
		},
	},
	"cauchemars" : {
		"label": "Cauchemars",
		"mineur" : {
			"description" : "Chaque nuit, le personnage à 10 % de chance de faire des cauchemars (lui rappelant un événement traumatisant passé) et de se retrouver dans l’état fatigué 2.",
			"bonus" : {

			}
		},
	},
	"enceinte" : {
		"label": "Enceinte",
		"description" : "Le personnage porte la vie et doit supporter les aléas de la grossesse et la vie d’aventurière. À prioriser sur des scénarios courts.",
		"mineur" : {
			"description" : "1ᵉʳ trimestre : la nausée peut frapper à n’importe quel moment. Chaque jour, lancez un jet de ténacité. L’échec indique que la nausée sera un problème durant la journée et pendant 1D20 tour lorsqu’elle frappera. On lancera 1D100 pour définir l’inopportunité de la nausée",
			"bonus" : {

			}
		},
		"moyen" : {
			"description" : "2ᵉ trimestre : la vitesse de déplacement est réduite d’un quart (arrondi entier inférieur) et subit un malus de 3 à la dextérité",
			"bonus" : {
				"dexterite": -3
			}
		},
		"majeur" : {
			"description" : "3ᵉ  trimestre : la vitesse de déplacement est réduite de moitié (arrondi entier inférieur), le malus de dextérité passe à 6, en plus d’un malus de 3 en force",
			"bonus" : {
				"dexterite": -6,
				"force": -3
			}
		},
	},
	"encore_trop_jeune" : {
		"label": "Encore trop jeune",
		"description" : "Malgré un âge précoce (entre 7 et 11 ans), le personnage est déjà sur les routes sinueuses de l’aventure.",
		"majeur" : {
			"description" : "Bonus de 3 à la dextérité, malus de 2 à la sagesse et un malus de 4 à la force.",
			"bonus" : {
				"dexterite": 3,
				"force": -4,
				"sagesse": -2,

			}
		},
	},
	"haine" : {
		"label": "Haine",
		"description" : "Le personnage voue une haine sans nom à une catégorie de personnes.",		
		"mineur" : {
			"description" : "le personnage doit faire un jet de volonté, avec malus de 6, pour ne pas attaquer lors d’un dialogue avec un type de personne détesté",
			"bonus" : {

			}
		},
		"moyen" : {
			"description" : "le personnage doit faire un jet de volonté, avec un malus de 6, pour ne pas attaquer à vue",
			"bonus" : {

			}
		},
	},
	"haptophobie" : { 
		"label": "Haptophobie",
		"mineur" : {
			"description" : "Le personnage ne supporte pas le moindre contact physique qui viendrait d’une race humanoïde. Si cela se produit (soin nécessitant d’être touché, pugilat, manœuvres, etc.), le personnage devra réussir un jet de volonté avec un malus de 6. S’il échoue, il devra tout faire pour fuir la personne et/ou l’empêcher de recommencer.",
			"bonus" : {

			}
		},
	},
	"illettre" : { 
		"label": "Illettré",
		"mineur" : {
			"description" : "Le personnage ne sait pas lire et il lui sera impossible d’apprendre.",
			"bonus" : {

			}
		},
	},
	"imbu_du_soi" : { 
		"label": "Imbu de soi-même",
	 	"moyen" : {
			"description" : "Le personnage est arrogant et se considère comme étant supérieur aux autres de manière totalement démesurée. Cela se manifeste par un malus de 8 à l’éloquence et l’impossibilité de donner ou recevoir une inspiration",
	 		"bonus" : {
	 			"eloquence": -8,
	 		}
	 	},
	},	
	"immunodeficience" : {
		"label": "Immunodéficience",
		"moyen" : {
			"description" : "Le personnage à un malus de 4 à ses jets de ténacité et risque plus facilement de contracter une maladie (foule, milieux humides ou sales, concentration d’animaux).",
			"bonus" : {
				"tenacite": -4
			}
		},
	},
	"inapte_a_la_magie" : {
		"label": "Inapte à la magie",
		"moyen" : {
			"description" : "Le nombre de points de mana du personnage est divisé par deux (arrondit à l’inférieur).",
			"bonus" : {
				"multiplicateur_pm": 0.5
			}
		},
		"majeur" : {
			"description" : "Le personnage ne possède aucun point de mana.",
			"bonus" : {
				"multiplicateur_pm": 0
			}
		},
		"description" : "Le personnage ne possède pas une force spirituelle normale."
	},
	"insomniaque" : {
		"label": "Insomniaque",
		"mineur" : {
			"description" : "Le personnage doit réussir un jet de volonté chaque fois qu’il désire dormir. S’il n’y arrive pas, il est privé de sommeil et donc de sa régénération. S’il cumule les nuits blanches, le personnage recevra des malus.",
			"bonus" : {

			}
		},
	},
	"kleptomane" : {
		"label": "Kleptomane",
		"description" : "Le personnage a un besoin compulsif de voler des objets, de façon discrète et quelle que soit la valeur de l’objet visé (mais l’objet doit représenter un intérêt à être volé).",
		"mineur" : {
			"description" : "tentation au moins une fois par semaine",
			"bonus" : {

			}
		},
		"moyen" : {
			"description" : "tentation au moins une fois par jour",
			"bonus" : {

			}
		}
	},
	"masochiste" : { 
		"label": "Masochiste",
	 	"moyen" : {
			"description" : "Le personnage éprouve du plaisir à subir de la douleur ou de la souffrance. Tant qu’une situation n’est pas mortelle (20 % des PV), le personnage ne cherchera jamais à esquiver des attaques à son encontre.",
	 		"bonus" : {
	 		}
	 	},
	},
	"megalomane" : { 
		"label": "Mégalomane",
	 	"moyen" : {
			"description" : "Le personnage éprouve du plaisir à subir de la douleur ou de la souffrance. Tant qu’une situation n’est pas mortelle (20 % des PV), le personnage ne cherchera jamais à esquiver des attaques à son encontre.",
	 		"bonus" : {
	 		}
	 	},
	},
	"monomaniaque_homicidaire" : { 
		"label": "Monomaniaque homicidaire",
	 	"moyen" : {
			"description" : "L’individu semble tout à fait normal, seul un intérêt prononcé pour les armes, poisons et autres objets mortels peut trahir son état. Il est obsédé par le désir de tuer, lequel doit être satisfait périodiquement, à intervalles de 1D7 jours. Si la pulsion n’est pas assouvie, la frustration le fera entrer dans une rage aveugle et attaquer la première personne qu’il rencontre, cherchant à l’assassiner sauvagement.",
	 		"bonus" : {
	 		}
	 	},
	},	
	"mutisme_selectif" : {
		"label": "Mutisme sélectif",
		"description" : "Le personnage est très anxieux lorsqu'il doit s'adresser aux individus du sexe opposé",
		"mineur" : {
			"description" : "le personnage doit réussir un jet de volonté afin de réussir à dialoguer avec un individu du sexe opposé et subit un malus de 6 à ses jets liés au charisme dans la même situation",
			"bonus" : {

			}
		},
		"moyen" : {
			"description" : "le personnage est incapable de parler aux individus du sexe opposé",
			"bonus" : {

			}
		}
	},
	"myope" : {
		"label": "Myope",
		"mineur" : {
			"description" : "Le personnage a des difficultés à voir de loin. Il subit un malus de 6 à la perception visuelle, aux attaques à distance et à l’esquive de projectiles, à partir d’une distance de 10 mètres.",
			"bonus" : {
				
			}
		},
	},
	"mythomane" : {
		"label": "Mythomane",
		"moyen" : {
			"description" : "Le personnage ne peut s’empêcher de mentir, dans toutes les situations, même cruciales. Le personnage devra réussir un jet de volonté à chaque interaction sociale, pour résister de mentir.",
			"bonus" : {

			}
		}
	},
	"narcoleptique" : {
		"label": "Narcoleptique",
		"mineur" : {
			"description" : "Lors de chaque combat, le personnage à 5 % de chance de s’endormir. Seul un coup infligeant des dégâts pourra le réveiller, sinon, il dormira pendant 2D6 tours.",
			"bonus" : {

			}
		},
	},
	"ne_sait_pas_nager" : {
		"label": "Ne sait pas nager",
		"mineur" : {
			"description" : "Le personnage ne sait pas nager et commencera directement à se noyer s’il se retrouve immergé dans l’eau sans avoir pied.",
			"bonus" : {

			}
		}
	},
	"pica_occasionnel" : {
		"label": "Pica occasionnel",
		"mineur" : {
			"description" : "Le personnage possède un trouble du comportement alimentaire caractérisé par l’envie de manger des substances non comestibles, comme la terre, la craie, papier, cheveux… Ce trouble se manifeste principalement pendant ou après une période de stress. Le personnage devra réussir un jet de volonté, sous peine d’ingérer ces substances. Un jet de ténacité sera alors demandé pour ne pas déclarer d’intoxication alimentaire.",
			"bonus" : {

			}
		}
	},
	"phagomanie" : {
		"label": "Phagomanie",
		"mineur" : {
			"description" : "Trouble compulsif alimentaire où une personne ingère de très grandes quantités de nourriture de manière compulsive, sans comportements compensatoires. Le personnage consomme le double de nourriture à chaque repas.",
			"bonus" : {

			}
		},
	},	
	"potomane" : {
		"label": "Potomane",
		"mineur" : {
			"description" : "Le personnage a le besoin compulsif d’ingérer de grandes quantités de liquide, même nocif, s’il en a l’occasion. S’il a le choix, l’eau sera toujours privilégiée. Il faudra réussir un jet de volonté pour se contrôler et ainsi éviter d’ingérer des substances dangereuses ou provoquer une hyponatrémie.",
			"bonus" : {

			}
		},
	},	
	"presbyte" : {
		"label": "Presbyte",
		"moyen" : {
			"description" : "Le personnage a des difficultés à voir de près. Il subit un malus de 6 aux actions nécessitant une précision visuelle à 1 mètre ou moins, notamment pour les attaques et esquives au corps-à-corps, les manœuvres, l'escalade, le sabotage, l'artisanat, la médecine, et pour toute action de lecture rapprochée.",
			"bonus" : {
				"corpsacorps":-6,
				"manoeuvres":-6,
                "forgeron": -6, 
                "tanneur": -6,
                "alchimiste": -6,
                "tailleur": -6,
                "faconneur": -6,
				"sabotage":-6,
				"medecine":-6,

			}
		},
	},	
	"sadique" : {
		"label": "Sadique",
		"mineur" : {
			"description" : "Le personnage a un désir profond de faire souffrir les créatures ou les personnes qu’il rencontre. Après chaque méfait, le personnage aura un intervalle de 1D7 jours avant de recommencer. Au-delà de cette période, il faudra réussir un jet de volonté pour ne pas faire souffrir la première créature (de taille très petite minimum) à sa portée.",
			"bonus" : {

			}
		},
	},	
	"sourd" : {
		"label": "Sourd",
		"mineur" : {
			"description" : "Le personnage ne peut percevoir les sons et ne peut communiquer que par la langue des signes. En combat, toute attaque en dehors du champ de vision sera inesquivable.",
			"bonus" : {

			}
		},
	},	
	"syndrome_alice_au_pays_des_merveilles" : {
		"label": "Syndrome d’Alice au pays des merveilles",
		"moyen" : {
			"description" : "Trouble neurologique qui affecte principalement la perception des formes, tailles et distances des objets. Il peut rendre les objets plus grands, plus petits, plus proches ou plus distants qu’ils ne le sont réellement, sans schéma fixe. Les actions suivantes seront ainsi malussées de 4 : artisanat, médecine, incantation, maniements d’armes et pugilat, esquive, escalade, manœuvres et sabotage.",
			"bonus" : {
                "forgeron": -4, 
                "tanneur": -4,
                "alchimiste": -4,
                "tailleur": -4,
                "faconneur": -4,
				"medecine":-4,
				"incantation":-4,
				"esquive":-4,
				"medecine":-4,
				"corpsacorps":-4,
				"couteaux":-4,
				"hast":-4,
				"epee":-4,
				"fleaux":-4,
				"hache":-4,
				"marteau":-4,
				"arbalete":-4,
				"arc":-4,
				"jet":-4,
				"fouet":-4,
				"bouclier":-4,
				"manoeuvres":-4,
				"sabotage":-4
			}
		},
	},	
	"syndrome_main_etrangere" : {
		"label": "Syndrome de la main étrangère",
		"description" : "Trouble neurologique où l’une des mains d’une personne agit de manière indépendante, sans contrôle conscient. Malus en cas d’action nécessitant une coordination bilatérale (escalade, sabotage, médecine, ambidextrie, armes à deux mains, arc et arbalètes…).",
		"moyen" : {
			"description" : "contrôle partiel de la main, malus de 6",
			"bonus" : {
				"medecine":-6,
				"arbalete":-6,
				"arc":-6,
				"sabotage":-6
			}
		},
		"majeur" : {
			"description" : "aucun contrôle sur celle-ci, actions impossibles",
			"bonus" : {
				"restriction": ["medecine", "arbalete", "arc", "sabotage"]
			}
		}
	},
	"syllogomane" : {
		"label": "Syllogomane",
		"description" : "Le personnage accumule les objets qu’il trouve de façon compulsive, quitte à avoir du mal à tout porter. Il devra réussir un jet de volonté à chaque fois qu’il devra se séparer d’un objet (ou de groupe d’objets), ou devra renoncer à en collecter un.",
		"mineur" : {
			"bonus" : {

			}
		},
	},	
	"thanatophobie" : {
		"label": "Thanatophobie",
		"description" : "La thanatophobie est la peur intense et irrationnelle de la mort, où le personnage ressent une angoisse profonde et incontrôlable à l’idée de mourir ou de la mort en général. Dès que les points de vie du personnage ou d’un allié tombent sous les 20 %, celui-ci devra réussir un jet de volonté, malussé de 6, afin de ne pas fuir la scène. En cas de mort d’un allié, la fuite sera obligatoire, même si le groupe possède une capacité de résurrection.",
		"mineur" : {
			"bonus" : {
			}
		},
	},
	"trimethylaminurie" : {
		"label": "Triméthylaminurie",
		"description" : "Trouble rare où l’organisme produit une quantité excessive de triméthylamine, responsable d’une odeur corporelle forte et incommodante. Le charisme du personnage en est grandement impacté, puisqu’il subit un malus de 4 à celui-ci. De plus, pister ou détecter le personnage via l’odorat sera bien plus aisé.",
		"mineur" : {
			"bonus" : {
				"charisme" : -4
			}
		},
	},
	"autre" : {
		"label": "Autre",
		"mineur" : {
			"description" : "",
			"bonus" : {

			}
		},
		"moyen" : {
			"description" : "",
			"bonus" : {

			}
		},
		"majeur" : {
			"description" : "",
			"bonus" : {

			}
		},
		"description" : "à définir avec le MJ"
	}
}

const list_defauts = {};

for(const defaut in liste_defauts) {
	for (const type in defauts.type_defaut) {
		if(liste_defauts[defaut][type] != undefined) {
			var description = [liste_defauts[defaut].description, liste_defauts[defaut][type].description].filter(d => d && d != "");
			list_defauts[defaut+"_"+type] = {"optgroup": liste_defauts[defaut].label, 
				"description": description.join('<br><br>'),
				"label": liste_defauts[defaut].label+" "+defauts.type_defaut[type].label, 
				"bonus": liste_defauts[defaut][type].bonus,
				"type_defaut": type}
		}
	}
}

defauts["liste_defauts"] = list_defauts;

export default defauts;