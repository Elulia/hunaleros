const equipements = {
	emplacements : {
		"arme":{
			"label":"Arme"
		},
		"armure":{
			"label":"Armure"
		},
		"accessoire":{
			"label":"Accessoire"
		}
	},
	type_objet : {
		"arme": [
		{
			"label" : "Cestes / Gantelets cloutés",
			"type" : "corpsacorps",
			"unite_materiaux" : 2,
			"prix": 20,
			"force minimum" : "-" ,
			"autre" : "Malus de 2 si équipé sur les travaux de précision, impossible à désarmer",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 2,
				"type_D": 4}]
			},
			"poid": "500 g",
			"materiaux": "contondantes"
		},
		{
			"label" : "Dague",
			"type" : "couteaux",
			"unite_materiaux" : 2,
			"prix": 20,
			"force minimum" : "-",
			"autre" : "",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 2,
				"type_D": 4}],
				"PA": 1
			},
			"poid": "500 g",
			"materiaux": "tranchantes"
		},
		{
			"label" : "Dague coup-de-poing",
			"type" : "couteaux",
			"unite_materiaux" : 2,
			"prix": 20,
			"force minimum" : "-",
			"autre" : "",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 2,
				"type_D": 4}],
				"PA": 1
			},
			"poid": "500 g",
			"materiaux": "tranchantes"
		},
		{
			"label" : "khukuri",
			"type" : "couteaux",
			"unite_materiaux" : 2,
			"prix": 20,
			"force minimum" : "-",
			"autre" : "",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 2,
				"type_D": 4}],
				"PA": 1
			},
			"poid": "1 kg",
			"materiaux": "tranchantes"
		},
		{
			"label" : "Lamétoile",
			"type" : "couteaux",
			"unite_materiaux" : 3,
			"prix": 25,
			"force minimum" : "-",
			"autre" : "peut aussi être lancé",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 2,
				"type_D": 4}],
				"PA": 1
			},
			"poid": "1,5 kg",
			"materiaux": "tranchantes"
		},
		{
			"label" : "Mains nues",
			"type" : "corpsacorps",
			"unite_materiaux" : 0,
			"prix": 0,
			"force minimum" : "-",
			"autre" : "-",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 1,
				"type_D": 2}]
			},
			"poid": "0 kg",
			"materiaux": "contondantes"
		},
		{
			"label" : "Attaque naturelle",
			"type" : "corpsacorps",
			"unite_materiaux" : 0,
			"prix": 0,
			"force minimum" : "-",
			"autre" : "-",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 1,
				"type_D": 4}]
			},
			"poid": "0 kg",
			"materiaux": "contondantes"
		},
		{
			"label" : "Serpe/Kama",
			"type" : "couteaux",
			"unite_materiaux" : 2,
			"prix": 20,
			"force minimum" : "-",
			"autre" : "croc-en-jambe + 1",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 1,
				"type_D": 4}],
				"PA": 1
			},
			"poid": "1 kg",
			"materiaux": "tranchantes"
		},
		{
			"label" : "Canne épée",
			"type" : "epee",
			"unite_materiaux" : 3,
			"prix": 40,
			"force minimum" : "-",
			"autre" : "Arme discrète (fourreau), Possibilité d'ambidextrie, nécessitant le don « ambidextre » et un minimum de 15 en force.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 2,
				"type_D": 6}]
			},
			"poid": "2 kg",
			"materiaux": "tranchantes"
		},
		{
			"label" : "Chaîne cloutée",
			"type" : "fouet",
			"unite_materiaux" : 3,
			"prix": 15,
			"force minimum" : "15",
			"autre" : "Allonge 2 cases, Croc-en-jambe + 1, Désarmement + 1, Possibilité d'ambidextrie, nécessitant le don « ambidextre » et un minimum de 15 en force.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 2,
				"type_D": 6}],
				"portee": 2
			},
			"poid": "5 kg",
			"materiaux": "tranchantes"
		},
		{
			"label" : "Cimeterre / Rapière",
			"type" : "epee",
			"unite_materiaux" : 3,
			"prix": 50,
			"force minimum" : "-",
			"autre" : "Possibilité d'ambidextrie, nécessitant le don « ambidextre » et un minimum de 15 en force.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 3,
				"type_D": 6}]
			},
			"poid": "2 kg",
			"materiaux": "tranchantes"
		},
		{
			"label" : "Épée-fouet",
			"type" : "fouet",
			"unite_materiaux" : 3,
			"prix": 110,
			"force minimum" : "15",
			"autre" : "Allonge 2 cases, croc-en-jambe + 1, Désarmement + 1, Possibilité d'ambidextrie, nécessitant le don « ambidextre » et un minimum de 15 en force.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 1,
				"type_D": 6}],
				"portee": 2
			},
			"poid": "1,5 kg",
			"materiaux": "tranchantes"
		},
		{
			"label" : "Épée longue",
			"type" : "epee",
			"unite_materiaux" : 3,
			"prix": 50,
			"force minimum" : "-",
			"autre" : "Possibilité d'ambidextrie, nécessitant le don « ambidextre » et un minimum de 15 en force.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 1,
				"type_D": 8}]
			},
			"poid": "2 kg",
			"materiaux": "tranchantes"
		},
		{
			"label" : "Épée rhoka",
			"type" : "epee",
			"unite_materiaux" : 3,
			"prix": 60,
			"force minimum" : "-",
			"autre" : "Désarmement + 1, Possibilité d'ambidextrie, nécessitant le don « ambidextre » et un minimum de 15 en force.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 2,
				"type_D": 6}]
			},
			"poid": "2 kg",
			"materiaux": "tranchantes"
		},
		{
			"label" : "Épieu",
			"type" : "hast",
			"unite_materiaux" : 2,
			"prix": 15,
			"force minimum" : "-",
			"autre" : "Allonge 2 cases, Possibilité d'ambidextrie, nécessitant le don « ambidextre » et un minimum de 15 en force.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 2,
				"type_D": 6}],
				"portee": 2
			},
			"poid": "1,5 kg",
			"materiaux": "tranchantes"
		},
		{
			"label" : "Falcata",
			"type" : "epee",
			"unite_materiaux" : 3,
			"prix": 50,
			"force minimum" : "-",
			"autre" : "Possibilité d'ambidextrie, nécessitant le don « ambidextre » et un minimum de 15 en force.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 1,
				"type_D": 8}]
			},
			"poid": "2 kg",
			"materiaux": "tranchantes"
		},
		{
			"label" : "Fléau d’armes léger",
			"type" : "fleaux",
			"unite_materiaux" : 2,
			"prix": 15,
			"force minimum" : "-",
			"autre" : "Croc-en-jambe + 1, Possibilité d'ambidextrie, nécessitant le don « ambidextre » et un minimum de 15 en force.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 2,
				"type_D": 6}]
			},
			"poid": "2,5 kg",
			"materiaux": "tranchantes"
		},
		{
			"label" : "Fouet",
			"type" : "fouet",
			"unite_materiaux" : 3,
			"prix": 5,
			"force minimum" : "-",
			"autre" : "Allonge 4 cases, Croc-en-jambe + 1, Désarmement + 1, Impossible d’assommer, Possibilité d'ambidextrie, nécessitant le don « ambidextre » et un minimum de 15 en force.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 3,
				"type_D": 4}],
				"portee": 4
			},
			"poid": "1 kg",
			"materiaux": "cuirs"
		},
		{
			"label" : "Hachette",
			"type" : "hache",
			"unite_materiaux" : 2,
			"prix": 10,
			"force minimum" : "-",
			"autre" : "Possibilité d'ambidextrie, nécessitant le don « ambidextre » et un minimum de 15 en force.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 1,
				"type_D": 8}]
			},
			"poid": "1,5 kg",
			"materiaux": "tranchantes"
		},
		{
			"label" : "Hache d'arme",
			"type" : "hache",
			"unite_materiaux" : 3,
			"prix": 60,
			"force minimum" : "-",
			"autre" : "Possibilité d'ambidextrie, nécessitant le don « ambidextre » et un minimum de 15 en force.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 1,
				"type_D": 8}]
			},
			"poid": "3 kg",
			"materiaux": "tranchantes"
		},
		{
			"label" : "Khopesh",
			"type" : "epee",
			"unite_materiaux" : 3,
			"prix": 40,
			"force minimum" : "-",
			"autre" : "Croc-en-jambe + 1, Possibilité d'ambidextrie, nécessitant le don « ambidextre » et un minimum de 15 en force.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 2,
				"type_D": 6}]
			},
			"poid": "4 kg",
			"materiaux": "tranchantes"
		},
		{
			"label" : "Marteau de guerre",
			"type" : "marteau",
			"unite_materiaux" : 3,
			"prix": 40,
			"force minimum" : "-",
			"autre" : "Possibilité d'ambidextrie, nécessitant le don « ambidextre » et un minimum de 15 en force.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 1,
				"type_D": 8}]
			},
			"poid": "2,5 kg",
			"materiaux": "contondantes"
		},
		{
			"label" : "Masse d’armes légère",
			"type" : "marteau",
			"unite_materiaux" : 3,
			"prix": 25,
			"force minimum" : "-",
			"autre" : "Possibilité d'ambidextrie, nécessitant le don « ambidextre » et un minimum de 15 en force.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 1,
				"type_D": 8}]
			},
			"poid": "2 kg",
			"materiaux": "contondantes"
		},
		{
			"label" : "Morgenstern",
			"type" : "marteau",
			"unite_materiaux" : 3,
			"prix": 50,
			"force minimum" : "-",
			"autre" : "Possibilité d'ambidextrie, nécessitant le don « ambidextre » et un minimum de 15 en force.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 1,
				"type_D": 8}]
			},
			"poid": "3 kg",
			"materiaux": "tranchantes"
		},
		{
			"label" : "Nunchaku",
			"type" : "fleaux",
			"unite_materiaux" : 3,
			"prix": 20,
			"force minimum" : "-",
			"autre" : "Croc-en-jambe + 1, Possibilité d'ambidextrie, nécessitant le don « ambidextre » et un minimum de 15 en force.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 2,
				"type_D": 6}]
			},
			"poid": "1 kg",
			"materiaux": "contondantes"
		},
		{
			"label" : "Sabre",
			"type" : "epee",
			"unite_materiaux" : 3,
			"prix": 50,
			"force minimum" : "-",
			"autre" : "Possibilité d'ambidextrie, nécessitant le don « ambidextre » et un minimum de 15 en force.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 1,
				"type_D": 8}]
			},
			"poid": "1 kg",
			"materiaux": "tranchantes"
		},
		{
			"label" : "Saï",
			"type" : "epee",
			"unite_materiaux" : 3,
			"prix": 20,
			"force minimum" : "-",
			"autre" : "Désarmement + 1, Possibilité d'ambidextrie, nécessitant le don « ambidextre » et un minimum de 15 en force.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 1,
				"type_D": 6}]
			},
			"poid": "500 g",
			"materiaux": "tranchantes"
		},
		{
			"label" : "Siangham",
			"type" : "epee",
			"unite_materiaux" : 3,
			"prix": 20,
			"force minimum" : "-",
			"autre" : "Possibilité d'ambidextrie, nécessitant le don « ambidextre » et un minimum de 15 en force.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 2,
				"type_D": 6}]
			},
			"poid": "1 kg",
			"materiaux": "contondantes"
		},
		{
			"label" : "Sciziore",
			"type" : "hache",
			"unite_materiaux" : 3,
			"prix": 15,
			"force minimum" : "-",
			"autre" : "Possibilité d'ambidextrie, nécessitant le don « ambidextre » et un minimum de 15 en force.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 1,
				"type_D": 8}]
			},
			"poid": "1,5 kg",
			"materiaux": "tranchantes"
		},
		{
			"label" : "Shotel",
			"type" : "epee",
			"unite_materiaux" : 3,
			"prix": 80,
			"force minimum" : "-",
			"autre" : "Croc-en-jambe + 1, Possibilité d'ambidextrie, nécessitant le don « ambidextre » et un minimum de 15 en force.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 1,
				"type_D": 6}]
			},
			"poid": "1,5 kg",
			"materiaux": "tranchantes"
		},
		{
			"label" : "Trident",
			"type" : "hast",
			"unite_materiaux" : 5,
			"prix": 30,
			"force minimum" : "-",
			"autre" : "Allonge 2 cases, Désarmement + 2, Possibilité d'ambidextrie, nécessitant le don « ambidextre » et un minimum de 15 en force.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 1,
				"type_D": 6}],
				"portee": 2
			},
			"poid": "2 kg",
			"materiaux": "tranchantes"
		},
		{
			"label" : "Urumi",
			"type" : "fouet",
			"unite_materiaux" : 2,
			"prix": 100,
			"force minimum" : "-",
			"autre" : "Allonge 3 cases, Croc-en-jambe + 1, Désarmement + 1, Impossible d’assommer, Possibilité d'ambidextrie, nécessitant le don « ambidextre » et un minimum de 15 en force.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 3,
				"type_D": 4}],
				"portee": 3
			},
			"poid": "3 kg",
			"materiaux": "tranchantes"
		},
		{
			"label" : "Bardiche",
			"type" : "hache",
			"unite_materiaux" : 5,
			"prix": 45,
			"force minimum" : "15",
			"autre" : "Allonge 2 cases, Possibilité d'ambidextrie, nécessitant le don « ambidextre » et une force minimum de 18 pour manier une arme à deux mains et une arme à une main. Ce prérequis passe à 22 en cas de maniement de deux armes à deux mains.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 1,
				"type_D": 10}],
				"portee": 2
			},
			"poid": "7 kg",
			"materiaux": "tranchantes"
		},
		{
			"label" : "Bâton de guerre / Bô",
			"type" : "hast",
			"unite_materiaux" : 2,
			"prix": 10,
			"force minimum" : "-",
			"autre" : "Force 13 pour l’ambidextrie, Allonge 3 cases, Croc-en-jambe + 1, Possibilité d'ambidextrie, nécessitant le don « ambidextre » et une force minimum de 18 pour manier une arme à deux mains et une arme à une main. Ce prérequis passe à 22 en cas de maniement de deux armes à deux mains.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 1,
				"type_D": 8}],
				"portee": 3
			},
			"poid": "1,5 kg",
			"materiaux": "contondantes"
		},
		{
			"label" : "Bec de corbin lourd",
			"type" : "marteau",
			"unite_materiaux" : 5,
			"prix": 30,
			"force minimum" : "15",
			"autre" : "Allonge 2 cases, Possibilité d'ambidextrie, nécessitant le don « ambidextre » et une force minimum de 18 pour manier une arme à deux mains et une arme à une main. Ce prérequis passe à 22 en cas de maniement de deux armes à deux mains.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 1,
				"type_D": 10}],
				"portee": 2
			},
			"poid": "6 kg",
			"materiaux": "contondantes"
		},
		{
			"label" : "Cimeterre à deux mains",
			"type" : "epee",
			"unite_materiaux" : 5,
			"prix": 80,
			"force minimum" : "15",
			"autre" : "Possibilité d'ambidextrie, nécessitant le don « ambidextre » et une force minimum de 18 pour manier une arme à deux mains et une arme à une main. Ce prérequis passe à 22 en cas de maniement de deux armes à deux mains.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 1,
				"type_D": 12}]
			},
			"poid": "4 kg",
			"materiaux": "tranchantes"
		},
		{
			"label" : "Corsèque / Chauve-souris",
			"type" : "hast",
			"unite_materiaux" : 5,
			"prix": 25,
			"force minimum" : "15",
			"autre" : "Allonge 2 cases, Désarmement + 1, Possibilité d'ambidextrie, nécessitant le don « ambidextre » et une force minimum de 18 pour manier une arme à deux mains et une arme à une main. Ce prérequis passe à 22 en cas de maniement de deux armes à deux mains.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 2,
				"type_D": 8}],
				"portee": 2
			},
			"poid": "6 kg",
			"materiaux": "tranchantes"
		},
		{
			"label" : "Coutille / Doloire",
			"type" : "hast",
			"unite_materiaux" : 5,
			"prix": 30,
			"force minimum" : "15",
			"autre" : "Allonge 3 cases, Possibilité d'ambidextrie, nécessitant le don « ambidextre » et une force minimum de 18 pour manier une arme à deux mains et une arme à une main. Ce prérequis passe à 22 en cas de maniement de deux armes à deux mains.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 2,
				"type_D": 8}],
				"portee": 3
			},
			"poid": "5 kg",
			"materiaux": "tranchantes"
		},
		{
			"label" : "Épée à deux mains",
			"type" : "epee",
			"unite_materiaux" : 5,
			"prix": 80,
			"force minimum" : "15",
			"autre" : "Possibilité d'ambidextrie, nécessitant le don « ambidextre » et une force minimum de 18 pour manier une arme à deux mains et une arme à une main. Ce prérequis passe à 22 en cas de maniement de deux armes à deux mains.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 1,
				"type_D": 12}]
			},
			"poid": "4 kg",
			"materiaux": "tranchantes"
		},
		{
			"label" : "Faux",
			"type" : "hast",
			"unite_materiaux" : 5,
			"prix": 10,
			"force minimum" : "15",
			"autre" : "Allonge 3 cases, Croc-en-jambe + 1, Possibilité d'ambidextrie, nécessitant le don « ambidextre » et une force minimum de 18 pour manier une arme à deux mains et une arme à une main. Ce prérequis passe à 22 en cas de maniement de deux armes à deux mains.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 1,
				"type_D": 8}],
				"portee": 3
			},
			"poid": "5 kg",
			"materiaux": "tranchantes"
		},
		{
			"label" : "Fléau d’armes lourd",
			"type" : "fleaux",
			"unite_materiaux" : 5,
			"prix": 40,
			"force minimum" : "15",
			"autre" : "Croc-en-jambe + 1, Possibilité d'ambidextrie, nécessitant le don « ambidextre » et une force minimum de 18 pour manier une arme à deux mains et une arme à une main. Ce prérequis passe à 22 en cas de maniement de deux armes à deux mains.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 2,
				"type_D": 8}]
			},
			"poid": "5 kg",
			"materiaux": "tranchantes"
		},
		{
			"label" : "Getsugasan",
			"type" : "hast",
			"unite_materiaux" : 5,
			"prix": 25,
			"force minimum" : "15",
			"autre" : "Allonge 3 cases, Possibilité d'ambidextrie, nécessitant le don « ambidextre » et une force minimum de 18 pour manier une arme à deux mains et une arme à une main. Ce prérequis passe à 22 en cas de maniement de deux armes à deux mains.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 2,
				"type_D": 8}],
				"portee": 3
			},
			"poid": "6 kg",
			"materiaux": "tranchantes"
		},
		{
			"label" : "Grande hache",
			"type" : "hache",
			"unite_materiaux" : 5,
			"prix": 60,
			"force minimum" : "15",
			"autre" : "Allonge 2 cases, Possibilité d'ambidextrie, nécessitant le don « ambidextre » et une force minimum de 18 pour manier une arme à deux mains et une arme à une main. Ce prérequis passe à 22 en cas de maniement de deux armes à deux mains.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 1,
				"type_D": 10}],
				"portee": 2
			},
			"poid": "6 kg",
			"materiaux": "tranchantes"
		},
		{
			"label" : "Guisarme",
			"type" : "hast",
			"unite_materiaux" : 5,
			"prix": 40,
			"force minimum" : "15",
			"autre" : "Allonge 3 cases, Possibilité d'ambidextrie, nécessitant le don « ambidextre » et une force minimum de 18 pour manier une arme à deux mains et une arme à une main. Ce prérequis passe à 22 en cas de maniement de deux armes à deux mains.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 2,
				"type_D": 8}],
				"portee": 3
			},
			"poid": "6 kg",
			"materiaux": "tranchantes"
		},
		{
			"label" : "Hallebarde",
			"type" : "hast",
			"unite_materiaux" : 5,
			"prix": 50,
			"force minimum" : "15",
			"autre" : "Allonge 3 cases, Possibilité d'ambidextrie, nécessitant le don « ambidextre » et une force minimum de 18 pour manier une arme à deux mains et une arme à une main. Ce prérequis passe à 22 en cas de maniement de deux armes à deux mains.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 2,
				"type_D": 8}],
				"portee": 3
			},
			"poid": "6 kg",
			"materiaux": "tranchantes"
		},
		{
			"label" : "Lance/Pique",
			"type" : "hast",
			"unite_materiaux" : 3,
			"prix": 20,
			"force minimum" : "-",
			"autre" : "Allonge 2 cases, Force 13 pour l’ambidextrie, Croc-en-jambe + 1, Possibilité d'ambidextrie, nécessitant le don « ambidextre » et une force minimum de 18 pour manier une arme à deux mains et une arme à une main. Ce prérequis passe à 22 en cas de maniement de deux armes à deux mains.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 2,
				"type_D": 8}],
				"portee": 2
			},
			"poid": "3 kg",
			"materiaux": "tranchantes"
		},
		{
			"label" : "Marteau de guerre lourd",
			"type" : "marteau",
			"unite_materiaux" : 5,
			"prix": 70,
			"force minimum" : "15",
			"autre" : "Possibilité d'ambidextrie, nécessitant le don « ambidextre » et une force minimum de 18 pour manier une arme à deux mains et une arme à une main. Ce prérequis passe à 22 en cas de maniement de deux armes à deux mains.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 1,
				"type_D": 12}]
			},
			"poid": "8 kg",
			"materiaux": "contondantes"
		},
		{
			"label" : "Naginata",
			"type" : "hast",
			"unite_materiaux" : 5,
			"prix": 80,
			"force minimum" : "15",
			"autre" : "Allonge 2 cases, Possibilité d'ambidextrie, nécessitant le don « ambidextre » et une force minimum de 18 pour manier une arme à deux mains et une arme à une main. Ce prérequis passe à 22 en cas de maniement de deux armes à deux mains.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 2,
				"type_D": 8}],
				"portee": 2
			},
			"poid": "4,5 kg",
			"materiaux": "contondantes"
		},
		{
			"label" : "Sansetsukon",
			"type" : "fleaux",
			"unite_materiaux" : 5,
			"prix": 20,
			"force minimum" : "-",
			"autre" : "Croc-en-jambe + 1, Désarmement + 1, Possibilité d'ambidextrie, nécessitant le don « ambidextre » et une force minimum de 18 pour manier une arme à deux mains et une arme à une main. Ce prérequis passe à 22 en cas de maniement de deux armes à deux mains.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 3,
				"type_D": 6}]
			},
			"poid": "1,5 kg",
			"materiaux": "contondantes"
		},
		{
			"label" : "Taiaha",
			"type" : "hast",
			"unite_materiaux" : 5,
			"prix": 20,
			"force minimum" : "15",
			"autre" : "Allonge 2 cases, Possibilité d'ambidextrie, nécessitant le don « ambidextre » et une force minimum de 18 pour manier une arme à deux mains et une arme à une main. Ce prérequis passe à 22 en cas de maniement de deux armes à deux mains.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 2,
				"type_D": 8}],
				"portee": 2
			},
			"poid": "4 kg",
			"materiaux": "tranchantes"
		},
		{
			"label" : "Terbudje",
			"type" : "marteau",
			"unite_materiaux" : 5,
			"prix": 25,
			"force minimum" : "15",
			"autre" : "Allonge 2 cases, Possibilité d'ambidextrie, nécessitant le don « ambidextre » et une force minimum de 18 pour manier une arme à deux mains et une arme à une main. Ce prérequis passe à 22 en cas de maniement de deux armes à deux mains.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 1,
				"type_D": 10}],
				"portee": 2
			},
			"poid": "3 kg",
			"materiaux": "tranchantes"
		},
		{
			"label" : "Tetsubo",
			"type" : "marteau",
			"unite_materiaux" : 5,
			"prix": 20,
			"force minimum" : "15",
			"autre" : "Allonge 2 cases, Possibilité d'ambidextrie, nécessitant le don « ambidextre » et une force minimum de 18 pour manier une arme à deux mains et une arme à une main. Ce prérequis passe à 22 en cas de maniement de deux armes à deux mains.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 1,
				"type_D": 10}],
				"portee": 2
			},
			"poid": "5 kg",
			"materiaux": "tranchantes"
		},
		{
			"label" : "Vouge",
			"type" : "hache",
			"unite_materiaux" : 5,
			"prix": 20,
			"force minimum" : "15",
			"autre" : "Allonge 3 cases, Possibilité d'ambidextrie, nécessitant le don « ambidextre » et une force minimum de 18 pour manier une arme à deux mains et une arme à une main. Ce prérequis passe à 22 en cas de maniement de deux armes à deux mains.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 1,
				"type_D": 8}],
				"portee": 3
			},
			"poid": "5,5 kg",
			"materiaux": "tranchantes"
		},
		{
			"label" : "Arbalète de poing",
			"type" : "arbalete",
			"unite_materiaux" : 2,
			"prix": 80,
			"force minimum" : "-",
			"autre" : "Incrément : 15 – 30 – 45 cases, Possibilité d'ambidextrie, nécessitant le don « ambidextre » et un minimum de 15 en dextérité et 12 en force, couplées avec le don évolutif arbalétrier occulte.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 1,
				"type_D": 4,
				"bonus_dmg": 1}]
			},
			"poid": "1 kg",
			"materiaux": "tranchantes"
		},
		{
			"label" : "Arbalète légère",
			"type" : "arbalete",
			"unite_materiaux" : 3,
			"prix": 90,
			"force minimum" : "-",
			"autre" : "Incrément : 15 – 30 – 45 cases, Possibilité d'ambidextrie, nécessitant le don « ambidextre » et un minimum de 15 en dextérité et 12 en force, couplées avec le don évolutif arbalétrier occulte.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 1,
				"type_D": 6,
				"bonus_dmg": 1}]
			},
			"poid": "2 kg",
			"materiaux": "tranchantes"
		},
		{
			"label" : "Arbalète lourde",
			"type" : "arbalete",
			"unite_materiaux" : 4,
			"prix": 120,
			"force minimum" : "15",
			"autre" : "Incrément : 24 – 48 – 72 cases, 2 tours de recharge, Possibilité d'ambidextrie, nécessitant le don « ambidextre » et un minimum de 15 en dextérité et 12 en force, couplées avec le don évolutif arbalétrier occulte.",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 1,
				"type_D": 16,
				"bonus_dmg": 1}]
			},
			"poid": "4 kg",
			"materiaux": "tranchantes"
		},
		{
			"label" : "Arc court",
			"type" : "arc",
			"unite_materiaux" : 3,
			"prix": 30,
			"force minimum" : "-",
			"autre" : "Incrément : 16 – 32 – 48 cases",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 2,
				"type_D": 6}]
			},
			"poid": "1 kg",
			"materiaux": "bois"
		},
		{
			"label" : "Arc long",
			"type" : "arc",
			"unite_materiaux" : 3,
			"prix": 40,
			"force minimum" : "14",
			"autre" : "Incrément : 20 – 40 – 60 cases",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 2,
				"type_D": 8}]
			},
			"poid": "1,5 kg",
			"materiaux": "bois"
		},
		{
			"label" : "Boomerang",
			"type" : "jet",
			"unite_materiaux" : 2,
			"prix": 8,
			"force minimum" : "-",
			"autre" : "Incrément : 6 cases",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 2,
				"type_D": 6}]
			},
			"poid": "1,5 kg",
			"materiaux": "contondantes"
		},
		{
			"label" : "Chakram",
			"type" : "jet",
			"unite_materiaux" : 3,
			"prix": 25,
			"force minimum" : "-",
			"autre" : "Incrément : 6 cases",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 2,
				"type_D": 6}]
			},
			"poid": "500 g",
			"materiaux": "tranchantes"
		},
		{
			"label" : "Hunga munga",
			"type" : "jet",
			"unite_materiaux" : 2,
			"prix": 10,
			"force minimum" : "-",
			"autre" : "Incrément : 6 cases",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 2,
				"type_D": 6}]
			},
			"poid": "1,5 kg",
			"materiaux": "tranchantes"
		},
		{
			"label" : "Javeline",
			"type" : "jet",
			"unite_materiaux" : 2,
			"prix": 5,
			"force minimum" : "-",
			"autre" : "Incrément : 6 cases",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 2,
				"type_D": 6}]
			},
			"poid": "1 kg",
			"materiaux": "tranchantes"
		},
		{
			"label" : "Shuriken / Couteau de lancer",
			"type" : "jet",
			"unite_materiaux" : 2,
			"prix": 25,
			"force minimum" : "-",
			"autre" : "Incrément : 8 cases",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 3,
				"type_D": 3}]
			},
			"poid": "250 g",
			"materiaux": "tranchantes"
		},
		{
			"label" : "Écu",
			"type" : "bouclier",
			"unite_materiaux" : 3,
			"prix": 20,
			"force minimum" : "-",
			"autre" : "+ 1 RD ",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 1,
				"type_D": 4}],
				"RD": 1
			},
			"poid": "4 kg",
			"materiaux": "contondantes"
		},
		{
			"label" : "Madu",
			"type" : "bouclier",
			"unite_materiaux" : 3,
			"prix": 30,
			"force minimum" : "-",
			"autre" : "-",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 1,
				"type_D": 5}]
			},
			"poid": "4 kg",
			"materiaux": "contondantes et tranchantes"
		},
		{
			"label" : "Pavois",
			"type" : "bouclier",
			"unite_materiaux" : 5,
			"prix": 40,
			"force minimum" : "14",
			"autre" : "+ 2 RD, 1 De malus d'armure ",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 1,
				"type_D": 3}],
				"RD": 2,
				"malus_armure": 1,
				"bonus_initiative": -1
			},
			"poid": "6 kg",
			"materiaux": "contondantes"
		},
		{
			"label" : "Rondache",
			"type" : "bouclier",
			"unite_materiaux" : 3,
			"prix": 20,
			"force minimum" : "-",
			"autre" : "+ 1 RD ",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 1,
				"type_D": 4}],
				"RD": 1
			},
			"poid": "4 kg",
			"materiaux": "contondantes"
		},
		{
			"label" : "Targe",
			"type" : "bouclier",
			"unite_materiaux" : 3,
			"prix": 20,
			"force minimum" : "-",
			"autre" : "+ 1 RD ",
			"bonus" : {
				"degat" : [{"bonus_D_dmg": 1,
				"critique" : 1,
				"type_D": 4}],
				"RD": 1
			},
			"poid": "4 kg",
			"materiaux": "contondantes"
		}
		],
		"armure": [
		{
			"label": "Armure en tissu",
			"type": "Très légère",
			"unite_materiaux" : 6,
			"temps" : 6,
			"prix": 15,
			"materiaux": "tissus",
			"bonus":{
				"malus_armure": -1,
				"bonus_initiative": 1
			}
		},
		{
			"label": "Armure matelassée",
			"type": "Légère",
			"unite_materiaux" : 7,
			"temps" : 7,
			"prix": 40,
			"materiaux": "tissus",
			"autre": "+ 3 de résistance face au froid",
			"bonus":{
				"RD": 1
			}
		},
		{
			"label": "Armure en cuir simple",
			"type": "Légère",
			"unite_materiaux" : 6,
			"temps" : 35,
			"prix": 40,
			"materiaux": "cuirs",
			"autre": "+ 3 résistance contre le feu",
			"bonus":{
				"RD": 1
			}
		},
		{
			"label": "Armure en cuir bouilli",
			"type": "Moyenne",
			"materiaux": "cuirs",
			"unite_materiaux" : 6,
			"temps" : 40,
			"prix": 80,
			"autre": "+ 3 résistance contre le feu",
			"bonus":{
				"RD": 2,
				"malus_armure": 1,
				"bonus_initiative": -1
			}
		},
		{
			"label": "Armure de cuir clouté",
			"type": "Moyenne",
			"materiaux": "cuirs",
			"unite_materiaux" : 6,
			"temps" : 40,
			"prix": 80,
			"autre": "+ 3 résistance contre le feu",
			"bonus":{
				"RD": 2,
				"malus_armure": 1,
				"bonus_initiative": -1
			}
		},
		{
			"label": "Cotte de mailles",
			"type": "Intermédiaire",
			"unite_materiaux" : 6,
			"temps" : 44,
			"prix": 160,
			"materiaux": "metaux",
			"bonus":{
				"RD": 3,
				"malus_armure": 2,
				"bonus_initiative": -2
			}
		},
		{
			"label": "Armure d'écailles",
			"type": "Intermédiaire",
			"unite_materiaux" : 6,
			"temps" : 46,
			"prix": 160,
			"materiaux": "metaux",
			"autre": "+ 1 de RD contre les projectiles",
			"bonus":{
				"RD": 3,
				"malus_armure": 2,
				"bonus_initiative": -2,
				"discretion": -3
			}
		},
		{
			"label": "Plastron et plaques",
			"type": "Lourde",
			"unite_materiaux" : 8,
			"temps" : 48,
			"prix": 500,
			"materiaux": "metaux",
			"bonus":{
				"RD": 4,
				"malus_armure": 3,
				"bonus_initiative": -3
			}
		},
		{
			"label": "Armure de plaque complète",
			"type": "Complète",
			"unite_materiaux" : 10,
			"temps" : 50,
			"prix": 700,
			"materiaux": "metaux",
			"autre": "Course et vol interdits",
			"bonus":{
				"RD": 5,
				"malus_armure": 4,
				"bonus_initiative": -4
			}
		}
		],
		"accessoire":[
			{
				"label":"Bracelet"
			},
			{
				"label": "Lunettes de vue",
				"bonus":{
					"perception": 6
				}
			}			
		]
	},
	materiaux: {
		"armure": {
			"tissus": [
			{
				"label" : "Chanvre Orque",
				"description": "Textile très résistant, proposant une RD de 2",
				"prix": 150,
				"qualite": "Rare",
				"bonus": {
					"RD": 2
				}
			},
			{
				"label" : "chanvre",
				"description": "Habits habituels, sans bonus ni malus",
				"prix": 0,
				"qualite": "Standard",
				"bonus": {
				}
			},
			{
				"label" : "Coton",
				"description": "Habits habituels, sans bonus ni malus",
				"prix": 0,
				"qualite": "Standard",
				"bonus": {
				}
			},
			{
				"label" : "Coton Saurien",
				"description": "Si doux et léger, le coton fabriqué avec les plantes des marais offre une aisance incomparable. Offre un bonus de 1 aux compétences possédants le signe*",
				"prix": 12,
				"qualite": "Complexe",
				"bonus": {
					"malus_armure": -1
				}
			},
			{
				"label" : "Jute",
				"description": "Malus de 2 aux interactions sociales. Le personnage a le mot pauvre écrit sur le front, mais au moins, il est habillé ",
				"prix": -1,
				"qualite": "Standard",
				"bonus": {
				    "bluff": -2,
				    "representation": -2,
				    "eloquence": -2,
				    "provocation": -2
				}
			},
			{
				"label" : "Laine",
				"description": "Une couverture ou un vêtement fait à partir de cette laine offrira un bonus de 4 à la résistance au froid. De la laine d'animal plus puissant pourra apporter d'autres bonus définit par le MJ",
				"prix": 3,
				"qualite": "Standard",
				"bonus": {
				}
			},
			{
				"label" : "Lin halfelin",
				"description": "Doués pour les taches minutieuses, les halfelins ont le secret d’un textile en lin bien plus résistant que la moyenne. Celui-ci offre un bonus à la RD de 1",
				"prix": 12,
				"qualite": "Complexe",
				"bonus": {
					"RD": 1
				}
			},
			{
				"label" : "Soie",
				"description": "Matière noble, utilisée par les plus riches. Elle n’offre aucune protection, mais offre un bonus de 2 aux interactions",
				"prix": 10,
				"qualite": "Complexe",
				"bonus": {
				    "bluff": 2,
				    "representation": 2,
				    "eloquence": 2,
				    "provocation": 2
				}
			},
			{
				"label" : "Soie elfique",
				"description": "Plus rare et raffinée que la soie normale, celle-ci, en plus des bonus de la soie, offre une RM de 1",
				"prix": 100,
				"qualite": "Rare",
				"bonus": {
					"RM": 1
				}
			},
			{
				"label" : "Tissu ignifugé",
				"description": "Réduit les dégâts de feu de moitié et empêche de prendre feu",
				"prix": 100,
				"qualite": "Rare",
				"bonus": {
				}
			},
			{
				"label" : "Tissu simple",
				"description": "Obtenu grâce aux plantes communes, n'offre aucun bonus ni malus",
				"prix": 0,
				"qualite": "Inhabituelle",
				"bonus": {
				}
			},
			{
				"label" : "Tissu rigide",
				"description": "Obtenu grâce aux plantes peu communes, offre un bonus à la RD de 1",
				"prix": 10,
				"qualite": "Inhabituelle",
				"bonus": {
					"RD": 1
				}
			},
			{
				"label" : "Tissu ferme",
				"description": "Obtenu grâce aux plantes rares, offre un bonus à la RD de 2",
				"prix": 100,
				"qualite": "Inhabituelle",
				"bonus": {
					"RD": 2
				}
			},
			{
				"label" : "Tissu résistant",
				"description": "Obtenu grâce aux plantes très rares, offre un bonus à la RD et la RM de 2",
				"prix": 150,
				"qualite": "Inhabituelle",
				"bonus": {
					"RD": 2,
					"RM": 2
				}
			},
			{
				"label" : "Tissu de fer",
				"description": "Obtenu grâce aux plantes rarissimes, offre un bonus à la RD et la RM de 3",
				"prix": 200,
				"qualite": "Inhabituelle",
				"bonus": {
					"RD": 3,
					"RM": 3
				}
			}
			],
			"metaux": [
			{
				"label": "Acier",
				"description": "RD +1, les malus liés aux armures +1",
				"prix" : 9,
				"bonus": {
					"RD": 1,
					"malus_armure": 1,
					"bonus_initiative": -1
				}
			},
			{
				"label": "Bronze",
				"description": "Très sujet à la rouille et la corrosion, mais bonus de 4 à l'éloquence",
				"prix" : 12,
				"bonus": {
					"eloquence": 4
				}
			},
			{
				"label": "Cobalt",
				"description": "RD +2",
				"prix" : 13,
				"bonus": {
					"RD": 2
				}
			},
			{
				"label": "Etherium",
				"description": "RD et RM +5",
				"prix" : 4000,
				"bonus": {
					"RD": 5,
					"RM": 5
				}
			},
			{
				"label": "Fer",
				"description": "N’offre ni bonus ni malus mais est très sujet à la rouille et la corrosion",
				"prix" : 0,
				"bonus": {
				}
			},
			{
				"label": "Mithril",
				"description": "RD +5",
				"prix" : 1600,
				"bonus": {
					"RD": 5
				}
			},
			{
				"label": "Nickel",
				"description": "RD +2",
				"prix" : 11,
				"bonus": {
					"RD": 2
				}
			},
			{
				"label": "Platine",
				"description": "RD +4",
				"prix" : 1400,
				"bonus": {
					"RD": 4
				}
			},
			{
				"label": "Argent",
				"description": "",
				"prix" : 120,
				"bonus": {
				}
			},
			{
				"label": "Obsidienne",
				"description": "",
				"prix" : 100,
				"bonus": {
				}
			},
			{
				"label": "Chrome",
				"description": "Le chrome est très résistant à la corrosion, offrant une excellente durabilité face à l'humidité. Cependant, il est relativement fragile et moins adapté aux impacts",
				"prix": 10,
				"bonus": {
				}
			},
			{
				"label": "Laiton",
				"description": "Alliage de cuivre et de zinc, le laiton est résistant à la corrosion et a une belle couleur dorée. Moins solide que l'acier, il est parfait pour des objets décoratifs ou des pièces peu sollicitées.",
				"prix": 12,
				"bonus": {
				}
			}
			],
			"cuirs": [
			{
				"label":"Cuir simple",
				"description": "Prélevés sur les animaux ayant une RD entre 0 et 1, n’octroyant ni bonus ni malus",
				"prix" : 0,
				"bonus": {
				}
			},
			{
				"label":"Cuir dur",
				"description": "Prélevés sur les animaux ayant une RD entre 2 et 3, offre un bonus à la RD de 1",
				"prix" : 8,
				"bonus": {
					"RD": 1
				}
			},
			{
				"label":"Cuir solide",
				"description": "Prélevés sur les animaux ayant une RD entre 4 et 5, offre un bonus à la RD de 2",
				"prix" : 12,
				"bonus": {
					"RD": 2
				}
			},
			{
				"label":"Cuir magique",
				"description": "Offre un bonus à la RM, équivalent à celui de l’animal /2, minimum 1",
				"prix" : 0,
				"bonus": {
				}
			},
			{
				"label":"Cuir incroyable",
				"description": "Prélevés sur les animaux ayant une RD entre 6 et 8, offre un bonus à la RD de 3",
				"prix" : 300,
				"bonus": {
					"RD": 3
				}
			},
			{
				"label":"Cuir fantastique",
				"description": "(RD entre 8 et 10) Bonus de 4 à la RD + la moitié de la RM de la bête",
				"prix" : 1300,
				"bonus": {
					"RD": 4
				}
			},
			{
				"label":"Cuir légendaire",
				"description": "(RD 11 et plus) La moitié de la RD et la RM sont en bonus à l’équipement",
				"prix" : 3000,
				"bonus": {
				}
			}
			]
		},
		"arme": {
			"contondantes": [
			{
				"label": "Fer",
				"description": "-",
				"prix" : 0,
				"bonus": {
				}
			},
			{
				"label": "Acier",
				"description": "-",
				"prix" : 9,
				"bonus": {
				}
			},
			{
				"label": "Argent",
				"description": "Double les dégâts face aux créatures sensibles à l’argent",
				"prix" : 120,
				"bonus": {
				}
			},
			{
				"label": "Mithril",
				"description": "Ajoute 1D (4D->5D) ou une classe de dé (2D6->2D8), au choix",
				"prix" : 1600,
				"bonus": {
				}
			},
			{
				"label": "Obsidienne",
				"description": "-",
				"prix" : 100,
				"bonus": {
				}
			},
			{
				"label": "Bronze",
				"description": "-",
				"prix" : 12,
				"bonus": {
				}
			},
			{
				"label": "Cobalt",
				"description": "-",
				"prix" : 13,
				"bonus": {
				}
			},
			{
				"label": "Etherium",
				"description": "-",
				"prix" : 4000,
				"bonus": {
				}
			},
			{
				"label": "Nickel",
				"description": "-",
				"prix" : 11,
				"bonus": {
				}
			},
			{
				"label": "Platine",
				"description": "-",
				"prix" : 1400,
				"bonus": {
				}
			}
			],
			"tranchantes": [
			{
				"label": "Fer",
				"description": "-",
				"prix" : 0,
				"bonus": {
				}
			},
			{
				"label": "Acier",
				"description": "-",
				"prix" : 9,
				"bonus": {
				}
			},
			{
				"label": "Obsidienne",
				"description": "Augmente la pénétration d'armure de 2 (Ne peut excéder 4) et chaque attaque inflige un saignement de 2 (Par tour, si jet de ténacité raté)",
				"prix" : 100,
				"bonus": {
					"PA": 2
				}
			},
			{
				"label": "Argent",
				"description": "Double les dégâts face aux créatures sensibles à l’argent",
				"prix" : 120,
				"bonus": {
				}
			},
			{
				"label": "Mithril",
				"description": "-",
				"prix" : 1600,
				"bonus": {
				}
			},
			{
				"label": "Bronze",
				"description": "-",
				"prix" : 12,
				"bonus": {
				}
			},
			{
				"label": "Cobalt",
				"description": "-",
				"prix" : 13,
				"bonus": {
				}
			},
			{
				"label": "Etherium",
				"description": "-",
				"prix" : 4000,
				"bonus": {
				}
			},
			{
				"label": "Nickel",
				"description": "-",
				"prix" : 11,
				"bonus": {
				}
			},
			{
				"label": "Platine",
				"description": "-",
				"prix" : 1400,
				"bonus": {
				}
			}
			],
			"contondantes et tranchantes": [
			{
				"label": "Fer",
				"description": "-",
				"prix" : 0,
				"bonus": {
				}
			},
			{
				"label": "Acier",
				"description": "-",
				"prix" : 9,
				"bonus": {
				}
			},
			{
				"label": "Obsidienne",
				"description": "Augmente la pénétration d'armure de 5 et chaque attaque inflige un saignement de 2 (Par tour, si jet de ténacité raté)",
				"prix" : 100,
				"bonus": {
					"PA": 5
				}
			},
			{
				"label": "Argent",
				"description": "Double les dégâts face aux créatures sensibles à l’argent",
				"prix" : 120,
				"bonus": {
				}
			},
			{
				"label": "Mithril",
				"description": "Ajoute 1D (4D->5D) ou une classe de dé (2D6->2D8), au choix",
				"prix" : 1600,
				"bonus": {
				}
			},
			{
				"label": "Bronze",
				"description": "-",
				"prix" : 12,
				"bonus": {
				}
			},
			{
				"label": "Cobalt",
				"description": "-",
				"prix" : 13,
				"bonus": {
				}
			},
			{
				"label": "Etherium",
				"description": "-",
				"prix" : 4000,
				"bonus": {
				}
			},
			{
				"label": "Nickel",
				"description": "-",
				"prix" : 11,
				"bonus": {
				}
			},
			{
				"label": "Platine",
				"description": "-",
				"prix" : 1400,
				"bonus": {
				}
			}
			],
			"bois": [
				{
					"label": "Bois simple",
					"description": "-",
					"prix" : 0,
					"bonus": {
					}
				}
			],
			"cuirs": [
				{
					"label": "Cuir simple",
					"description": "-",
					"prix" : 0,
					"bonus": {
					}
				}
			]
		}
	},
	"amelioration": {
		"arme": {
			"aceree": {
				"label": "Acérée",
				"description": "Augmente la classe de dé (une fois seulement)",
				"group": "Craftable",
				"multiplicateur_prix": 1.75,
				"bonus": {
					"bonus_classe_D": 2
				}
			},
			"allonge": {
				"label": "Allonge",
				"description": "Augmente la portée d'attaque d'une case (une fois seulement)",
				"group": "Craftable",
				"multiplicateur_prix": 2,
				"bonus": {
					"portee": 1
				}
			},
			"antigeant": {
				"label": "Anti-géants",
				"description": "Augmente les dégats de 4 face aux créatures de taille supérieure au personnage",
				"group": "Craftable",
				"multiplicateur_prix": 1.5,
				"bonus": {
				}
			},
			"cognedur": {
				"label": "Cogne dur",
				"description": "Les coups critiques des jets d'attaque inflige 1D de dégâts supplémentaires",
				"group": "Craftable",
				"multiplicateur_prix": 1.75,
				"bonus": {
				}
			},
			"critique": {
				"label": "Critique",
				"description": "Augmente la marge de critique de 1",
				"group": "Craftable",
				"multiplicateur_prix": 1.75,
				"bonus": {
					"critique": 1
				}
			},
			"ecrasante": {
				"label": "Écrasante",
				"description": "Augmente les dégats contondants de 3",
				"group": "Achetable",
				"multiplicateur_prix": 1.75,
				"bonus": {
					"bonus_dmg": 3
				}
			},
			"emoussee": {
				"label": "Émoussée",
				"description": "(après 10 combats) Dégâts divisés par 2",
				"group": "Craftable",
				"multiplicateur_prix": 1,
				"bonus": {
				}
			},
			"encordee": {
				"label": "Encordée",
				"description": "Empêche le désarmement",
				"group": "Achetable",
				"multiplicateur_prix": 1.12,
				"bonus": {
				}
			},
			"ergonomique": {
				"label": "Ergonomique",
				"description": "Bonus de 4 pour résister aux manœuvres adverses",
				"group": "Achetable",
				"multiplicateur_prix": 1.5,
				"bonus": {
				}
			},
			"faucheuse": {
				"label": "Faucheuse",
				"description": "Bonus de 2 pour effectuer un croc-en-jambe",
				"group": "Achetable",
				"multiplicateur_prix": 1.25,
				"bonus": {
				}
			},
			"fourchue": {
				"label": "Fourchue",
				"description": "Bonus de 2 pour effectuer un désarmement",
				"group": "Achetable",
				"multiplicateur_prix": 1.25,
				"bonus": {
				}
			},
			"harponneuse": {
				"label": "Harponneuse",
				"description": "Rapproche la cible (max de taille moyenne) de 1 case",
				"group": "Craftable",
				"multiplicateur_prix": 1.5,
				"bonus": {
				}
			},
			"perforante": {
				"label": "Perforante",
				"description": "Augmente les dégats de 2 (une fois seulement)",
				"group": "Craftable",
				"multiplicateur_prix": 1.25,
				"bonus": {
					"bonus_dmg": 2
				}
			},
			"sacrificielle": {
				"label": "Sacrificielle",
				"description": "Arme imprégnée de la magie vaudou, pouvant infliger des dégâts via un vecteur.",
				"group": "Achetable",
				"multiplicateur_prix": 1.1,
				"bonus": {
				}
			},
			"sanglante": {
				"label": "Sanglante",
				"description": "Permet d'infliger l'état saignement en plus des dégâts normaux",
				"group": "Achetable",
				"multiplicateur_prix": 1.25,
				"bonus": {
				}
			},
			"silencieuse": {
				"label": "Silencieuse",
				"description": "Aucun bruit n'est produit lors de l'utilisation, dont attaque sournoise",
				"group": "Craftable",
				"multiplicateur_prix": 1.12,
				"bonus": {
				}
			},
			"tranchante": {
				"label": "Tranchante",
				"description": "Augmente les dégats tranchants de 3",
				"group": "Achetable",
				"multiplicateur_prix": 1.75,
				"bonus": {
					"bonus_dmg": 3
				}
			},
			"vampirique": {
				"label": "Vampirique",
				"description": "Chaque coup porté qui inflige des dégats rend 2 points de vie au porteur",
				"group": "Craftable",
				"multiplicateur_prix": 1.75,
				"bonus": {
				}
			}
		},
		"armure": {
			"aeree": {
				"label": "Aérée",
				"description": "Augmente la résistance à la chaleur de 4 (Ne se combine pas avec molletonnée)",
				"group": "Achetable",
				"multiplicateur_prix": 1.25,
				"bonus": {
				}
			},
			"ajustee": {
				"label": "Ajustée",
				"description": "Bonus de 1 sur les compétences sujettes aux malus d'armure (Une seule fois)",
				"group": "Craftable",
				"multiplicateur_prix": 1.75,
				"bonus": {
					"malus_armure": -1,
					"bonus_initiative": 1
				}
			},
			"allegee": {
				"label": "Allégée",
				"description": "Retire le malus d'initiative provenant des armures",
				"group": "Achetable",
				"multiplicateur_prix": 1.5,
				"bonus": {
				}
			},
			"altruiste": {
				"label": "Altruiste",
				"description": "Rend 2 pv à chaque coup reçu",
				"group": "Craftable",
				"multiplicateur_prix": 1.75,
				"bonus": {
				}
			},
			"delestee": {
				"label": "Délestée",
				"description": "Réduit le poids de l'armure de 50 %",
				"group": "Achetable",
				"multiplicateur_prix": 1.12,
				"bonus": {
				}
			},
			"discrete": {
				"label": "Discrète",
				"description": "Bonus de 1 à la discrétion (Ne se combine pas avec renforcée)",
				"group": "Craftable",
				"multiplicateur_prix": 1.5,
				"bonus": {
					"discretion": 2
				}
			},
			"elementaire": {
				"label": "Élémentaire",
				"description": "Bonus de 2 à la résistance d’un (et un seul) élément",
				"group": "Craftable",
				"multiplicateur_prix": 1.5,
				"bonus": {
				}
			},
			"impermeable": {
				"label": "Imperméable",
				"description": "Ne subit pas de malus en combat aquatique",
				"group": "Craftable",
				"multiplicateur_prix": 1.5,
				"bonus": {
				}
			},
			"imposante": {
				"label": "Imposante",
				"description": "Offre un bonus de 2 à l'intimidation (Ne se combine pas avec émissaire et de scène)",
				"group": "Achetable",
				"multiplicateur_prix": 1.5,
				"bonus": {
					"intimidation": 2
				}
			},
			"emissaire": {
				"label": "Émissaire",
				"description": "Offre un bonus de 2 à l'éloquence (Ne se combine pas avec imposante et de scène)",
				"group": "Achetable",
				"multiplicateur_prix": 1.5,
				"bonus": {
					"eloquence": 2
				}
			},
			"scene": {
				"label": "De scène",
				"description": "Offre un bonus de 2 à la représentation (Ne se combine pas avec imposante et émissaire)",
				"group": "Achetable",
				"multiplicateur_prix": 1.5,
				"bonus": {
					"representation": 2
				}
			},
			"incassable": {
				"label": "Incassable",
				"description": "Ne peut pas être détruite",
				"group": "Craftable",
				"multiplicateur_prix": 1.75,
				"bonus": {
				}
			},
			"molletonnee": {
				"label": "Molletonnée",
				"description": "Augmente la résistance au froid de 4 (Ne se combine pas avec aérée)",
				"group": "Achetable",
				"multiplicateur_prix": 1.25,
				"bonus": {
				}
			},
			"piquante": {
				"label": "Piquante",
				"description": "Renvoi 2 pts de dégats si l'attaque est au corps-à-corps",
				"group": "Craftable",
				"multiplicateur_prix": 1.75,
				"bonus": {
				}
			},
			"renforceeRD": {
				"label": "Renforcée - RD",
				"description": "Augmente la RD de 1, pour un maximum de : 1 pour les armures en tissu 3 pour les armures en cuir 5 pour les armures forgées",
				"group": "Achetable",
				"multiplicateur_prix": 1.75,
				"bonus": {
					"RD": 1
				}
			},
			"renforceeRM": {
				"label": "Renforcée - RM",
				"description": "Augmente la RM de 1, pour un maximum de : 1 pour les armures en tissu 3 pour les armures en cuir 5 pour les armures forgées",
				"group": "Achetable",
				"multiplicateur_prix": 1.75,
				"bonus": {
					"RM": 1
				}
			},
			"securise": {
				"label": "Sécurisée",
				"description": "Réduit de moitié les dégats de chute",
				"group": "Craftable",
				"multiplicateur_prix": 1.5,
				"bonus": {
				}
			}
		}
	},
	"cout_ameliorations" : {
		1 : {"mat" : 0, "temps": 0},
		1.12 : { "mat" : 1, "temps" : 4},
		1.25 : { "mat" : 2, "temps" : 6},
		1.45 : { "mat" : 2, "temps" : 8},
		1.5 : { "mat" : 3, "temps" : 10},
		1.75 : { "mat" : 3, "temps" : 12},
		2 : {"mat" : 4, "temps" : 14}
	}
}

equipements.type_objet.arme.forEach(arme => {
	arme.bonus.degat[0].label = arme.label
	arme.bonus.degat[0].type_objet = "type_"+ arme.type;
});

export default equipements;