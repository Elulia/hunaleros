const charge_npc = {
  "faible": {
    "label": "Faible",
    "multiplicateur" : 0.2
  },
  "malingre": {
    "label": "Malingre",
    "multiplicateur" : 0.5
  },
  "frele": {
    "label": "Frêle",
    "multiplicateur" : 0.8
  },
  "chetif": {
    "label": "Chétif",
    "multiplicateur" : 1
  },
  "normal": {
    "label": "Normal",
    "multiplicateur" : 1.5
  },
  "fort": {
    "label": "Fort",
    "multiplicateur" : 2
  },
  "robuste": {
    "label": "Robuste",
    "multiplicateur" : 3
  },
  "robuste_ii": {
    "label": "Robuste II",
    "multiplicateur" : 4.5
  },
  "robuste_iii": {
    "label": "Robuste III",
    "multiplicateur" : 6.5
  },
  "robuste_iv": {
    "label": "Robuste IV",
    "multiplicateur" : 9
  },
  "robuste_v": {
    "label": "Robuste V",
    "multiplicateur" : 11
  }
};

export default charge_npc;