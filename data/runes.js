const runes = [
	{
		"label" : "Rune aqueuse",
		"description" : "{{500}} ml d'eau potable (500 * niveau de la rune), ne fonctionne que sur un récipient",
		"prix" : 80,
		"bonus" : {
			
		}
	},
	{
		"label" : "Rune lumineuse",
		"description" : "Crée une lumière de l'intensité d'une torche",
		"prix" : 80,
		"bonus" : {
			
		}
	},
	{
		"label" : "Rune de vitesse",
		"description" : "Ajoute un bonus de {{2}} à l'initiative (2 * niveau de la rune)",
		"prix" : 100,
		"bonus" : {
			"bonus_initiative_par_niveaux": 2
		}
	},
	{
		"label" : "Rune informative",
		"description" : "La rune se déclenche avec un son mental, lorsqu'une cible précise ou vague, définie au préalable, se présente dans un rayon de 50 mètres",
		"prix" : 100,
		"bonus" : {
			
		}
	},
	{
		"label" : "Rune de communication",
		"description" : "Deux runes doivent être apposées sur deux objets distincts. Les porteurs des objets pourront rester en communication et chaque saura où se trouve l'autre. L'effet persiste dans un rayon de 30 mètres + {{5}} (5* niveau de la rune)",
		"prix" : 100,
		"bonus" : {
			
		}
	},
	{
		"label" : "Rune arcanique",
		"description" : "Permet d'enchanter une arme et de rendre ses dégâts magiques, pendant {{1}} attaques (niveau de la rune)",
		"prix" : 300,
		"bonus" : {
			
		}
	},
	{
		"label" : "Rune de stockage",
		"description" : "Permet d'enfermer un sort d'un grimoire autre que la magie runique, permattant de le lancer sans jet et gratuitement, une fois par jour",
		"prix" :300,
		"bonus" : {
			
		}
	},
	{
		"label" : "Rune de légèreté",
		"description" : "Permet d'alléger un objet de {{10}} kilos (10* niveau de la rune), de retirer la condition de force maniemum pour le maniement d'une arme ou de retirer {{0.5}} de malus lié d'armure (tous les deux niveaux de la rune)",
		"prix" :300,
		"bonus" : {
			
		}
	},
	{
		"label" : "Rune fortifiante",
		"description" : "Ajoute 1D{{1}} (niveau de la rune) en dégâts à une arme",
		"prix" : 600,
		"bonus" : {
			
		}
	},
	{
		"label" : "Rune de serrure",
		"description" : "Permet de vérrouiller un objet qui comporte une serrure ou dans dévérouiller une. Les personne définie à la création de la rune peuvent vérrouiller et dévérouiller l'objet à volonté",
		"prix" : 600,
		"bonus" : {
			
		}
	},
	{
		"label" : "Rune de protection physique",
		"description" : "Offre un bonus de {{1}} à la RD (niveau de la rune)",
		"prix" : 600,
		"bonus" : {
			"RD_par_niveaux": 1
		}
	},
		{
		"label" : "Rune de protection magique",
		"description" : "Offre un bonus de {{1}} à la RM (niveau de la rune)",
		"prix" : 600,
		"bonus" : {
			"RM_par_niveaux": 1
		}
	},
	{
		"label" : "Rune régénératrice",
		"description" : "Une foispar jours, permet de soigner le porteur de 2D4 points de vie + {{1}} (niveau de la rune)",
		"prix" : 600,
		"bonus" : {
			
		}
	},
	{
		"label" : "Rune anti-magie",
		"description" : "Une fois par jour, bloque de façon automatique un sort qui cible le joueur, peut importe la source ou le sort. Seules les zones qui ne cibles pas directement le mage ne sont pas bloquées",
		"prix" : 1200,
		"bonus" : {
			
		}
	},
	{
		"label" : "Rune chercheuse",
		"description" : "Rune à graver sur un projectile, qui le fera automatiquement toucher, une fois par jour",
		"prix" : 1200,
		"bonus" : {
			
		}
	},
	{
		"label" : "Rune contrecoup",
		"description" : "Une fois par jour, permet de renvoyer automatiquement une attaque au corps-à-corps qui touchait le porteur de l'armure (L'attaquant pourra se défendre du renvoi)",
		"prix" : 2400,
		"bonus" : {
			
		}
	},
	{
		"label" : "Rune de guérison",
		"description" : "Soigne {{2}} points de vie (2* niveau de la rune), à chaque fois que le porteur reçoit une attaque. L'effet se cumule et tous les points de vie sont rendus en même temps au début du tour du porteur",
		"prix" : 2400,
		"bonus" : {
			
		}
	},
	{
		"label" : "Rune affaiblissante - Force",
		"description" : "Hormis le porteur, quiconque est touché par l'objet runique voit sa caractéristique de force baisser de {{2}} (2 *niveau de la rune)",
		"prix" : 5000,
		"bonus" : {
			
		}
	},
		{
		"label" : "Rune affaiblissante - Constitution",
		"description" : "Hormis le porteur, quiconque est touché par l'objet runique voit sa caractéristique de Constitution baisser de {{2}} (2 *niveau de la rune)",
		"prix" : 5000,
		"bonus" : {
			
		}
	},
		{
		"label" : "Rune affaiblissante - Dextérité",
		"description" : "Hormis le porteur, quiconque est touché par l'objet runique voit sa caractéristique de dextérité baisser de {{2}} (2 *niveau de la rune)",
		"prix" : 5000,
		"bonus" : {
			
		}
	},
		{
		"label" : "Rune affaiblissante - Intelligence",
		"description" : "Hormis le porteur, quiconque est touché par l'objet runique voit sa caractéristique d'intelligence baisser de {{2}} (2 *niveau de la rune)",
		"prix" : 5000,
		"bonus" : {
			
		}
	},
		{
		"label" : "Rune affaiblissante - Sagesse",
		"description" : "Hormis le porteur, quiconque est touché par l'objet runique voit sa caractéristique de sagesse baisser de {{2}} (2 *niveau de la rune)",
		"prix" : 5000,
		"bonus" : {
			
		}
	},
		{
		"label" : "Rune affaiblissante - Charisme",
		"description" : "Hormis le porteur, quiconque est touché par l'objet runique voit sa caractéristique de charisme baisser de {{2}} (2 *niveau de la rune)",
		"prix" : 5000,
		"bonus" : {
			
		}
	},
	{
		"label" : "Rune d'invisibilité",
		"description" : "Rend le porteur de la rune invisible, mais pas silencieux. Il sera en plus détectactable avec le don perception magique",
		"prix" : 5000,
		"bonus" : {
			
		}
	},
	{
		"label" : "Rune de mort",
		"description" : "Ajoute un bonus de {{2}} à l'initiative (2* niveau de la rune)",
		"prix" : 10000,
		"bonus" : {
			
		}
	},
	{
		"label" : "Rune explosive",
		"description" : "Posée sur un objet, la rune explosera dans un laps de temps définit ou quand une créature passera à côté. La créature subit 8D6 dégâts à 1 case d'intervalle, 6D6 à 2 cases, 4D6 dégâts à 3 cases, 2D6 dégâts à 5 cases et 1D6 dégâts à 6 cases",
		"prix" : 10000,
		"bonus" : {
			
		}
	},
];

export default runes;