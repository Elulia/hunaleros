const competences = {
  "athletisme": {
    "label":"Athlétisme",
    "ability": "force",
    "malus_armure": true
  },
  "corpsacorps": {
    "label":"Corps-à-corps",
    "ability": "force"
  },
  "manoeuvres": {
    "label":"Manœuvres",
    "ability": "force"
  },
  "intimidation": {
    "label":"Intimidation",
    "ability": "force"
  },
  "tenacite": {
    "label":"Ténacité",
    "ability": "constitution"
  },
  "discretion": {
    "label":"Discrétion",
    "ability": "dexterite",
    "malus_armure": true
  },
  "acrobatie": {
    "label":"Acrobatie",
    "ability": "dexterite",
    "malus_armure": true
  },
  "sabotage": {
    "label":"Sabotage",
    "ability": "dexterite"
  },
  "attaque_naturelle": {
    "label": "Attaque naturelle",
    "ability": "dexterite",
    "condition": {
      "type": "npc"
    }
  },
  "couteaux": {
    "label":"Couteaux",
    "ability": "dexterite",
    "arme": true,
    "sous_type": "mélé"
  },
  "hast": {
    "label":"Hast",
    "ability": "dexterite",
    "arme": true,
    "sous_type": "mélé"
  },
  "epee": {
    "label":"Épées",
    "ability": "dexterite",
    "arme": true,
    "sous_type": "mélé"
  },
  "fleaux": {
    "label":"Fléaux",
    "ability": "dexterite",
    "arme": true,
    "sous_type": "mélé"
  },
  "hache": {
    "label":"Haches",
    "ability": "dexterite",
    "arme": true,
    "sous_type": "mélé"
  },
  "marteau": {
    "label":"Masses ou marteaux",
    "ability": "dexterite",
    "arme": true,
    "sous_type": "mélé"
  },
  "arbalete": {
    "label":"Arbalètes",
    "ability": "dexterite",
    "arme": true,
    "sous_type": "distance"
  },
  "arc": {
    "label":"Arcs",
    "ability": "dexterite",
    "arme": true,
    "sous_type": "distance"
  },
  "jet": {
    "label":"Armes de jet",
    "ability": "dexterite",
    "arme": true,
    "sous_type": "distance"
  },
  "fouet": {
    "label":"Fouets et chaînes",
    "ability": "dexterite",
    "arme": true,
    "sous_type": "mélé"
  },
  "bouclier": {
    "label":"Boucliers",
    "ability": "dexterite",
    "arme": true,
    "sous_type": "mélé"
  },
  "equitation": {
    "label":"Équitation",
    "ability": "dexterite"
  },
  "esquive": {
    "label":"Esquive / Défense",
    "ability": "dexterite",
    "malus_armure": true
  },
  "dressage": {
    "label":"Dressage",
    "ability": "intelligence"
  },
  "erudition": {
    "label":"Erudition",
    "ability": "intelligence"
  },
  "commerce": {
    "label":"Commerce",
    "ability": "intelligence"
  },
  "incantation": {
    "label":"Incantation",
    "ability": "intelligence",
    "condition": {
      "don": ["mage"]
    }
  },
  "medecine": {
    "label":"Médecine",
    "ability": "intelligence"
  },
  "artisanats": {
    "label":"Artisanats",
    "ability": "intelligence"
  },
  "forgeron": {
    "label": "Forgeron",
    "ability": "intelligence",
    "artisanat": true
  },
  "tanneur": {
    "label": "Tanneur",
    "ability": "intelligence",
    "artisanat": true
  },
  "alchimiste": {
    "label": "Alchimiste",
    "ability": "intelligence",
    "artisanat": true
  },
  "tailleur": {
    "label": "Tailleur",
    "ability": "intelligence",
    "artisanat": true
  },
  "faconneur": {
    "label": "Façonneur",
    "ability": "intelligence",
    "artisanat": true
  },
  "survie": {
    "label":"Survie",
    "ability": "sagesse"
  },
  "perception": {
    "label":"Perception",
    "ability": "sagesse"
  },
  "psychologie": {
    "label":"Psychologie",
    "ability": "sagesse",
    "not_condition": {
      "don_evo": ["Vaudouisant", "Officier diplomatique"]
    }
  },
  "volonte": {
    "label":"Volonté",
    "ability": "sagesse",
    "not_condition": {
      "don_evo": ["Vaudouisant"]
    }
  },
  "commandement": {
    "label":"Commandement",
    "ability": "charisme",
    "condition": {
      "type": "npc"
    }
  },
  "bluff": {
    "label":"Baratin / Bluff",
    "ability": "charisme"
  },
  "representation": {
    "label":"Représentation",
    "ability": "charisme"
  },
  "eloquence": {
    "label":"Éloquence",
    "ability": "charisme",
    "not_condition": {
      "don_evo": ["Officier diplomatique"]
    }
  },
  "provocation": {
    "label":"Provocation",
    "ability": "charisme"
  },
  "foi": {
    "label": "Foi",
    "ability": "charisme",
    "condition": {
      "don_evo": ["Paladin sacré"]
    }
  },
  "rituel": {
    "label": "Rituel",
    "ability": "intelligence",
    "condition": {
      "don_evo": ["Cultiste du mal"]
    }
  },
    "devotion": {
    "label": "Dévotion",
    "ability": "sagesse",
    "condition": {
      "don_evo": ["Cultiste du bien"]
    }
  },
    "instigation": {
    "label": "Instigation",
    "ability": "charisme",
    "condition": {
      "don_evo": ["Officier diplomatique"]
    }
  },
    "perspicacite": {
    "label": "Perspicacité",
    "ability": "sagesse",
    "condition": {
      "don_evo": ["Vaudouisant"]
    }
  }
};

export default competences;