const metiers = {
	"agitateur": 
	{
		"label": "Agitateur",
		"description": "Défenseur passionné de diverses causes, agissant avec véhémence pour rallier des partisans. Leur stratégie implique des discours enflammés, la distribution de tracts, et la persuasion des volontaires.",
		"description_bonus": "Bonus à l’érudition de 1",
		"bonus": {
			"erudition": 1
		}
	},
	"alchimiste": 
	{
		"label": "Alchimiste",
		"description": "Experts érudits dans l’étude des propriétés de la matière, naviguant entre la magie et la science. Leur domaine englobe la préparation de composants chimiques et la reconnaissance des minéraux.",
		"description_bonus": "Bonus en artisanat alchimie de 2",
		"bonus": {
			"alchimiste": 2
		}
	},
	"antiquaire": 
	{
		"label": "Antiquaire",
		"description": "Gardiens passionnés du passé, collectionnant, expertisant et vendant des artefacts historiques pour préserver la mémoire des époques révolues.",
		"description_bonus": "Bonus au commerce de 1",
		"bonus": {
			"commerce": 1
		}
	},
	"apothicaire": 
	{
		"label": "Apothicaire",
		"description": "Spécialistes de la préparation et de la vente de médicaments prescrits par les médecins, utilisant leur expertise pour fournir des remèdes efficaces aux maux de leurs clients.",
		"description_bonus": "Bonus à la médecine de 1",
		"bonus": {
			"medecine": 1
		}
	},
	"archeologue": 
	{
		"label": "Archéologue",
		"description": "Ils étudient les vestiges du passé pour découvrir et interpréter les civilisations disparues, enrichissant ainsi la compréhension de l’histoire et des différentes cultures.",
		"description_bonus": "Bonus à l'érudition de 1",
		"bonus": {
			"perception": 1
		}
	},
	"artificier": 
	{
		"label": "Artificier",
		"description": "Maîtres de la manipulation de la poudre noire, ils créent des fusées colorées et spectaculaires, plutôt que des armes à feu.",
		"description_bonus": "Bonus aux armes de jet de 1 lorsqu’il s’agît d’explosifs",
		"bonus": {
			"jet": 1
		}
	},
	"athlete": 
	{
		"label": "Athlète",
		"description": "Champions acclamés des compétitions sportives, ils captivent un public enthousiaste par leurs performances athlétiques remarquables.",
		"description_bonus": "Bonus à l’athlétisme de 1",
		"bonus": {
			"athletisme": 1
		}
	},
	"aubergiste": 
	{
		"label": "Aubergiste / Tavernier",
		"description": "Hôtes des voyageurs et des habitués, ils offrent nourriture, boisson et hébergement.",
		"description_bonus": "Bonus en commerce de 1",
		"bonus": {
			"commerce": 1
		}
	},
	"baleinier": 
	{
		"label": "Baleinier",
		"description": "Navigateurs intrépides des mers, ils traquent les baleines pour leur précieux spermaceti et leur huile.",
		"description_bonus": "Bonus de 1 à la survie",
		"bonus": {
			"survie": 1
		}
	},
	"bateleur": 
	{
		"label": "Bateleur",
		"description": "Personne qui fait des tours d’acrobaties, d’escamotage, des prestations artistiques sur les places publiques, dans les foires.",
		"description_bonus": "Bonus à la représentation de 1",
		"bonus": {
			"representation": 1
		}
	},
	"batelier": 
	{
		"label": "Batelier",
		"description": "Navigateurs experts des voies fluviales, ils transportent passagers et marchandises le long des cours d’eau.",
		"description_bonus": "Bonus à l’athlétisme 1",
		"bonus": {
			"athletisme": 1
		}
	},
	"bibliothecaire": 
	{
		"label": "Bibliothécaire",
		"description": "Ils organisent, préservent et fournissent l’accès aux précieuses collections de livres et de documents.",
		"description_bonus": "Bonus à l’érudition de 1",
		"bonus": {
			"erudition": 1
		}
	},
	"bouffon": 
	{
		"label": "Bouffon",
		"description": "Amuseurs de cour, ils divertissent les nobles et le public avec leurs pitreries, leurs jeux de mots et leurs acrobaties.",
		"description_bonus": "Bonus à l’acrobatie de 1",
		"bonus": {
			"acrobatie": 1
		}
	},
	"brigand": 
	{
		"label": "Brigand",
		"description": "Hors-la-loi des routes et des bois, ils vivent de pillages et de rapines.",
		"description_bonus": "Bonus au bluff de 1",
		"bonus": {
			"bluff": 1
		}
	},
	"bucheron": 
	{
		"label": "Bûcheron",
		"description": "Ils abattent, ébranchent et débitent les arbres pour fournir différents matériaux essentiels.",
		"description_bonus": "Bonus à l’athlétisme de 1",
		"bonus": {
			"athletisme": 1
		}
	},
	"cartographe": 
	{
		"label": "Cartographe",
		"description": "Ils cartographient les terres inconnues, tracent des routes et des frontières, fournissant des guides précieux pour les navigateurs, les explorateurs et les gouvernements.",
		"description_bonus": "Bonus en survie de 1",
		"bonus": {
			"survie": 1
		}
	},
	"chaman": 
	{
		"label": "Chaman",
		"description": "Gardiens des traditions spirituelles et des savoirs ancestraux, ils communiquent avec les esprits et la nature pour guider leur communauté, prodiguant soins, conseils et rituels sacrés.",
		"description_bonus": "Bonus à la médecine de 1",
		"bonus": {
			"medecine": 1
		}
	},
	"charbonnier": 
	{
		"label": "Charbonnier",
		"description": "Ils transforment le bois en charbon de bois, fournissant un combustible vital.",
		"description_bonus": "Bonus au commerce de 1",
		"bonus": {
			"commerce": 1
		}
	},
	"chasseur": 
	{
		"label": "Chasseur",
		"description": "Habiles pisteurs et tireurs précis, ils traquent les animaux avec une intuition remarquable, assurant une chasse efficace et respectueuse.",
		"description_bonus": "Bonus en survie de 1",
		"bonus": {
			"survie": 1
		}
	},
	"chiffonnier": 
	{
		"label": "Chiffonnier",
		"description": "Ils récupèrent les déchets et rebuts pour leur subsistance, troquant leurs trouvailles contre quelques pièces.",
		"description_bonus": "Bonus à la ténacité de 1",
		"bonus": {
			"tenacite": 1
		}
	},
	"chirurgienbarbier": 
	{
		"label": "Chirurgien barbier",
		"description": "Réputés pour leurs interventions médicales telles que les saignées, les opérations chirurgicales et les amputations, leur expertise va au-delà de l’entretien de barbes.",
		"description_bonus": "Bonus à la médecine de 1",
		"bonus": {
			"medecine": 1
		}
	},
	"collecteur": 
	{
		"label": "Collecteur d’impôts",
		"description": "Figures méprisées, chargées de prélever les taxes auprès des citoyens pour le compte du gouvernement.",
		"description_bonus": "Bonus à l’intimidation de 1",
		"bonus": {
			"intimidation": 1
		}
	},
	"colon": 
	{
		"label": "Colon",
		"description": "Pionniers courageux, ils quittent leur terre natale pour s’installer dans des terres lointaines, cultivant et exploitant les ressources naturelles au nom de leur pays d’origine, afin d’étendre les frontières de leur royaume.",
		"description_bonus": "Bonus en survie de 1",
		"bonus": {
			"survie": 1
		}
	},
	"colporteur": 
	{
		"label": "Colporteur",
		"description": "Commerçants itinérants, ils parcourent les villages en vendant une variété de marchandises et en transmettant les nouvelles et les ragots urbains.",
		"description_bonus": "Bonus au commerce de 1",
		"bonus": {
			"commerce": 1
		}
	},
	"commercant": 
	{
		"label": "Commerçant",
		"description": "Entrepreneurs établis, ils proposent leurs marchandises dans leur boutique ou sur leur étalage.",
		"description_bonus": "Bonus au commerce de 1",
		"bonus": {
			"commerce": 1
		}
	},
	"commissionnaire": 
	{
		"label": "Commissionnaire",
		"description": "Relais au sein d’une forteresse, ils assurent la transmission rapide et sécurisée des messages officiels, des ordres de mobilisation et des invitations.",
		"description_bonus": "Bonus à l’éloquence de 1",
		"bonus": {
			"eloquence": 1
		}
	},
	"contrebandier": 
	{
		"label": "Contrebandier",
		"description": "Marchands clandestins opérant en marge de la loi, ils font passer illégalement des marchandises prohibées ou soumises à des restrictions à travers les frontières.",
		"description_bonus": "Bonus à l’intimidation de 1",
		"bonus": {
			"provocation": 1
		}
	},
	"couturier": 
	{
		"label": "Couturier",
		"description": "Experts de la mode et de la haute couture, leur savoir-faire leur permet de créer des pièces de grande qualité.",
		"description_bonus": "Bonus à l’artisanat couture de 2",
		"bonus": {
			"tailleur": 2
		}
	},
	"crieur": 
	{
		"label": "Crieur public",
		"description": "Annonceurs ambulants, ils parcourent les villes et villages en utilisant tambours ou clochettes pour attirer l’attention, puis déclament des nouvelles, des annonces officielles ou des événements à venir.",
		"description_bonus": "Bonus à l’éloquence de 1",
		"bonus": {
			"deloquence": 1
		}
	},
	"cuisinier": 
	{
		"label": "Cuisinier",
		"description": "Maîtres des saveurs et des textures, ils supervisent et créent des plats exquis qui ravissent les convives.",
		"description_bonus": "Bonus à la représentation de 1",
		"bonus": {
			"representation": 1
		}
	},
	"cultivateurdrogues": 
	{
		"label": "Cultivateur de drogues",
		"description": "Spécialistes de la culture clandestine, ils font pousser des plantes narcotiques dans des environnements discrets, souvent accompagnés d’un petit laboratoire pour la transformation des substances chimiques.",
		"description_bonus": "Bonus à l'artisanat alchimie de 2",
		"bonus": {
			"alchimiste": 2,
		}
	},
	"demoiselle": 
	{
		"label": "Demoiselle de compagnie",
		"description": "Jeunes femmes nobles, mais modestes. Elles offrent leur compagnie et assistance à des dames plus âgées et riches, tout en bénéficiant de l’éducation et des opportunités offertes par leur position sociale.",
		"description_bonus": "Bonus à l'éloquence de 1",
		"bonus": {
			"eloquence": 1
		}
	},
	"distillateur": 
	{
		"label": "Distillateur",
		"description": "Marchands spécialisés dans la fabrication et la vente d’eau-de-vie et de liqueurs.",
		"description_bonus": "Bonus au commerce de 1",
		"bonus": {
			"commerce": 1
		}
	},
	"docker": 
	{
		"label": "Docker",
		"description": "Travailleurs portuaires chargés du chargement et du déchargement des navires.",
		"description_bonus": "Bonus à l’athlétisme 1",
		"bonus": {
			"athletisme": 1
		}
	},
	"domestique": 
	{
		"label": "Domestique",
		"description": "Employés au service des ménages aisés, ils assurent diverses tâches domestiques telles que le nettoyage, la cuisine et la garde d’enfants.",
		"description_bonus": "Bonus de 1 en volonté",
		"bonus": {
			"volonte": 1
		}
	},
	"dresseur": 
	{
		"label": "Dresseur",
		"description": "Experts dans l’entraînement d’une large variété d’animaux pour des tâches spécifiques ou des tours.",
		"description_bonus": "Bonus au dressage de 1",
		"bonus": {
			"dressage": 1
		}
	},
	"ecuyer": 
	{
		"label": "Écuyer",
		"description": "Assistants dévoués des chevaliers, ils s’occupent des chevaux, de l’équipement et des tâches logistiques, tout en recevant une formation dans les arts de la guerre et de la chevalerie.",
		"description_bonus": "Bonus à l’équitation de 1",
		"bonus": {
			"equitation": 1
		}
	},
	"egoutier": 
	{
		"label": "Égoutier",
		"description": "Ouvriers chargés de la construction, de l’entretien et du nettoyage des réseaux d’égouts.",
		"description_bonus": "Bonus à la ténacité de 1",
		"bonus": {
			"tenacite": 1
		}
	},
	"emissaire": 
	{
		"label": "Émissaire",
		"description": "Envoyés diplomatiques ou messagers officiels, chargés de transmettre des messages, de négocier des accords ou de représenter un gouvernement ou une organisation dans des affaires internationales ou intercommunautaires.",
		"description_bonus": "Bonus à l’éloquence de 1",
		"bonus": {
			"eloquence": 1
		}
	},
	"esclave": 
	{
		"label": "Esclave",
		"description": "Personnes privées de liberté et contraintes de travailler pour un maître ou une autorité, souvent dans des conditions de servitude et de dépendance extrême, ne possédant pas de droits ni de libertés personnelles.",
		"description_bonus": "Bonus à la volonté de 1",
		"bonus": {
			"volonte": 1
		}
	},
"fermier": 
	{
		"label": "Fermier / Paysan",
		"description": "Travaillant la terre et élevant du bétail, ils développent des cultures vivrières et exploitent des fermes pour produire des aliments et des ressources agricoles.",
		"description_bonus": "Bonus à l’athlétisme de 1",
		"bonus": {
			"athletisme": 1
		}
	},
	"forgeron": 
	{
		"label": "Forgeron",
		"description": "Artisans spécialisés dans le façonnage du métal. Ils fabriquent des armes, armures et objets métalliques.",
		"description_bonus": "Bonus à l’artisanat forgeron de 2",
		"bonus": {
			"forgeron": 2
		}
	},
	"garde": 
	{
		"label": "Garde",
		"description": "Gardiens de la sécurité et de l’ordre, ils protègent des biens et des personnes, patrouillant les lieux publics, les propriétés privées ou les installations militaires pour prévenir les intrusions et les actes de malveillance.",
		"description_bonus": "Bonus à la perception de 1",
		"bonus": {
			"perception": 1
		}
	},
	"gardecorps": 
	{
		"label": "Garde du corps",
		"description": "Protecteurs chargés d’assurer la sécurité et la protection rapprochée d’individus.",
		"description_bonus": "Bonus à la perception de 1",
		"bonus": {
			"perception": 1
		}
	},
	"gardechasse": 
	{
		"label": "Garde-chasse",
		"description": "Gardiens des terrains de chasse et des réserves naturelles, ils régulent la chasse et surveillent les activités des chasseurs pour assurer la préservation de l’écosystème et le respect des réglementations environnementales.",
		"description_bonus": "Bonus à la survie de 1",
		"bonus": {
			"survie": 1
		}
	},
	"geolier": 
	{
		"label": "Geôlier",
		"description": "Surveillants des prisons et des lieux de détention, chargés de surveiller les détenus, de maintenir l’ordre et la sécurité à l’intérieur des établissements pénitentiaires.",
		"description_bonus": "Bonus à la psychologie de 1",
		"bonus": {
			"psychologie": 1
		}
	},
	"gitan": 
	{
		"label": "Gitan",
		"description": "Nomades vivants traditionnellement sur les routes, pratiquant des métiers ambulants pour subsister.",
		"description_bonus": "Bonus de représentation de 1",
		"bonus": {
			"representation": 1
		}
	},
	"gladiateur": 
	{
		"label": "Gladiateur",
		"description": "Combattants professionnels entraînés pour divertir les foules dans des arènes, engageant des combats spectaculaires contre d’autres gladiateurs, des animaux sauvages ou même des condamnés à mort.",
		"description_bonus": "Bonus de 1 sur les manœuvres",
		"bonus": {
			"manoeuvres": 1
		}
	},   
	"gouteur": 
	{
		"label": "Goûteur",
		"description": "Individus chargés de tester les aliments et les boissons avant qu’ils ne soient servis à une personne importante, afin de détecter toute présence de poison ou de substances nocives.",
		"description_bonus": "Bonus à la ténacité de 1",
		"bonus": {
			"tenacite": 1
		}
	},
	"horslaloi": 
	{
		"label": "Hors-la-loi",
		"description": "Individus vivants en marge de la loi, souvent recherchés par les autorités pour des activités criminelles.",
		"description_bonus": "Bonus de 1 à la discrétion",
		"bonus": {
			"discretion": 1
		}
	},
	"informateur": 
	{
		"label": "Informateur",
		"description": "Individus spécialisés dans la collecte et la vente d’informations confidentielles.",
		"description_bonus": "Bonus à la psychologie de 1",
		"bonus": {
			"psychologie": 1
		}
	},
	"jardinier": 
	{
		"label": "Jardinier",
		"description": "Experts des plantes et des espaces verts, ils cultivent, entretiennent et embellissent les jardins, les parcs et les espaces paysagers.",
		"description_bonus": "Bonus à l’athlétisme de 1",
		"bonus": {
			"athletisme": 1
		}
	},
	"joueur": 
	{
		"label": "Joueur professionnel",
		"description": "Individus qui gagnent leurs vies en participant à des jeux d’argent, utilisant compétences et stratégies pour maximiser leurs gains.",
		"description_bonus": "Bonus à la psychologie de 1",
		"bonus": {
			"psychologie": 1
		}
	},
	"libraire": 
	{
		"label": "Libraire",
		"description": "Ils sont spécialisés dans la vente de livres, magazines et autres articles imprimés.",
		"description_bonus": "Bonus au commerce de 1",
		"bonus": {
			"commerce": 1
		}
	},
	"lutteur": 
	{
		"label": "Lutteur",
		"description": "Athlètes spécialisés dans les compétitions de lutte, utilisant des techniques de combat corps à corps pour affronter d’autres adversaires dans un cadre compétitif.",
		"description_bonus": "Bonus au corps-à-corps de 1",
		"bonus": {
			"corpsacorps": 1
		}
	},
	"magnetiseur": 
	{
		"label": "Magnétiseur",
		"description": "Praticiens utilisant des techniques de magnétisme pour influencer le bien-être physique et mental des individus, manipulant l’énergie autour du corps afin de soulager la douleur, réduire le stress ou favoriser la guérison.",
		"description_bonus": "Bonus à la médecine de 1",
		"bonus": {
			"medecine": 1
		}
	},
	"marechal": 
	{
		"label": "Maréchal ferrant",
		"description": "Artisans qualifiés dans le travail du métal et du fer, ils sont spécialisés dans la fabrication, l’ajustement et le ferrage des fers à cheval, assurant ainsi le confort et la santé des chevaux.",
		"description_bonus": "Bonus au dressage de 1",
		"bonus": {
			"dressage": 1
		}
	},
	"matelot": 
	{
		"label": "Matelot",
		"description": "Membres d’équipage travaillant à bord de navires, ils assistent dans diverses tâches maritimes.",
		"description_bonus": "Bonus à l’athlétisme 1",
		"bonus": {
			"athletisme": 1
		}
	},
	"medecin": 
	{
		"label": "Médecin",
		"description": "Professionnels de la santé aptes à diagnostiquer, traiter et prévenir les maladies et les blessures chez les patients.",
		"description_bonus": "Bonus à la médecine de 1",
		"bonus": {
			"medecine": 1
		}
	},
	"mendiant": 
	{
		"label": "Mendiant",
		"description": "Individus qui sollicitent la charité en raison de leur pauvreté ou de leur incapacité à subvenir à leurs besoins de manière autonome.",
		"description_bonus": "Bonus à l’éloquence de 1",
		"bonus": {
			"eloquence": 1
		}
	},
	"menestrel": 
	{
		"label": "Ménestrel",
		"description": "Artistes itinérants, ils divertissent avec leur musique, leurs chants, leurs histoires et leurs jongleries.",
		"description_bonus": "Bonus à la représentation de 1",
		"bonus": {
			"representation": 1
		}
	},
	"menuisier": 
	{
		"label": "Menuisier",
		"description": "Spécialisés dans le travail du bois, ils fabriquent une variété d’objets et de structures en utilisant des techniques de coupe, d’assemblage et de finition.",
		"description_bonus": "Bonus à l’artisanat façonnage de 2",
		"bonus": {
			"faconneur": 2
		}
	},    
	"mercenaire": 
	{
		"label": "Mercenaire",
		"description": "Soldats professionnels engagés pour combattre au service d’une faction, d’un État ou d’un individu moyennant rémunération.",
		"description_bonus": "Bonus de 1 aux manœuvres",
		"bonus": {
			"manoeuvres": 1
		}
	},
	"meunier": 
	{
		"label": "Meunier",
		"description": "Producteurs de la farine, ils exploitent des moulins pour broyer le grain en poudre.",
		"description_bonus": "Bonus à l’athlétisme de 1",
		"bonus": {
			"athletisme": 1
		}
	},
	"mineur": 
	{
		"label": "Mineur",
		"description": "Experts dans l’extraction de minerais et de minéraux des profondeurs de la terre.",
		"description_bonus": "Bonus à l’athlétisme de 1",
		"bonus": {
			"athletisme": 1
		}
	},
	"oracle": 
	{
		"label": "Oracle",
		"description": "Individus possédant une capacité à prédire l’avenir ou à communiquer avec des forces divines, ils sont consultés pour obtenir des conseils, des prédictions ou des réponses à des questions existentielles.",
		"description_bonus": "Bonus au bluff de 1",
		"bonus": {
			"bluff": 1
		}
	},
	"patre": 
	{
		"label": "Pâtre",
		"description": "Gardiens de troupeaux, ils dirigent et veillent sur les animaux domestiques dans les pâturages.",
		"description_bonus": "Bonus au dressage de 1",
		"bonus": {
			"dressage": 1
		}
	},
	"pecheur": 
	{
		"label": "Pêcheur",
		"description": "Ils capturent des poissons et d’autres fruits de mer dans les eaux et vendent leurs prises au marché.",
		"description_bonus": "Bonus à la survie de 1",
		"bonus": {
			"survie": 1
		}
	},
	"pilleur": 
	{
		"label": "Pilleur de tombes",
		"description": "Individus qui pillent des tombes ou des sépultures pour voler des artefacts, des bijoux ou d’autres objets de valeur historique ou culturelle, souvent pour les revendre sur le marché noir.",
		"description_bonus": "Bonus à la perception de 1",
		"bonus": {
			"perception": 1
		}
	},
	"pirate": 
	{
		"label": "Pirate",
		"description": "Marins hors-la-loi opérant en mer, ils attaquent et pillent des navires pour voler leur cargaison ou capturer des otages en échange de rançons.",
		"description_bonus": "Bonus à l’intimidation de 1",
		"bonus": {
			"intimidation": 1
		}
	},
	"politicien": 
	{
		"label": "Politicien",
		"description": "Dirigeants et conseillers choisis pour gouverner et légiférer au sein d’un royaume ou d’une cité.",
		"description_bonus": "Bonus à l’éloquence de 1",
		"bonus": {
			"eloquence": 1
		}
	},
	"predicateur": 
	{
		"label": "Prédicateur",
		"description": "Orateurs religieux chargés de transmettre des enseignements spirituels. Ils prêchent lors de rassemblements religieux, guidant les fidèles dans leur foi et leur pratique religieuse.",
		"description_bonus": "Bonus à l’éloquence de 1",
		"bonus": {
			"eloquence": 1
		}
	},
		"preneurparis": 
	{
		"label": "Preneur de paris",
		"description": "Individus qui acceptent et enregistrent des paris sur des événements sportifs, des courses de chevaux ou d’autres compétitions, souvent agissant en tant qu’intermédiaires entre les parieurs et les opérateurs de jeu.",
		"description_bonus": "Bonus au commerce de 1",
		"bonus": {
			"commerce": 1
		}
	},
	"preteur": 
	{
		"label": "Prêteur",
		"description": "Personnes qui prêtent de l’argent, moyennant des intérêts lors du remboursement.",
		"description_bonus": "Bonus au commerce de 1",
		"bonus": {
			"commerce": 1
		}
	},
	"prostituee": 
	{
		"label": "Prostituée",
		"description": "Personnes qui offrent des services sexuels en échange d’argent ou d’autres formes de rémunération, opérant souvent dans des quartiers ou des établissements désignés à cet effet.",
		"description_bonus": "Bonus à l’éloquence de 1",
		"bonus": {
			"eloquence": 1
		}
	},
	"purineur": 
	{
		"label": "Purineur",
		"description": "Travailleurs chargés de nettoyer les rues et de collecter les déchets humains et animaux dans les villes pour ensuite aller le déverser dans les plaines ou le revendre aux fermiers de la région comme engrais.",
		"description_bonus": "Bonus au commerce de 1",
		"bonus": {
			"commerce": 1
		}
	},
	"raconteur": 
	{
		"label": "Raconteur",
		"description": "Conteurs talentueux qui divertissent les gens avec leurs récits et leurs histoires de toutes sortes.",
		"description_bonus": "Bonus à l’érudition de 1",
		"bonus": {
			"erudition": 1
		}
	},
	"ratier": 
	{
		"label": "Ratier",
		"description": "Éleveurs et dresseurs de chiens de chasse spécialisés dans la chasse aux rats et autres nuisibles.",
		"description_bonus": "Bonus à la survie de 1",
		"bonus": {
			"survie": 1
		}
	},
	"recruteur": 
	{
		"label": "Recruteur",
		"description": "Officiers chargés de recruter de nouveaux soldats pour renforcer les rangs de l’armée, souvent en menant des campagnes de recrutement dans les villes et les villages, convainquant les citoyens de s’enrôler en offrant des incitations financières ou en exploitant le patriotisme et le devoir civique.",
		"description_bonus": "Bonus à l’éloquence de 1",
		"bonus": {
			"eloquence": 1
		}
	},
	"sagefemme": 
	{
		"label": "Sage-femme",
		"description": "Professionnels de la santé spécialisés dans l’assistance pendant la grossesse, l’accouchement et le post-partum.",
		"description_bonus": "Bonus à la médecine de 1",
		"bonus": {
			"medecine": 1
		}
	},
	"serrurier": 
	{
		"label": "Serrurier",
		"description": "Artisans perfectionnés dans la fabrication et la réparation de serrures, de clés et d’autres dispositifs de sécurité.",
		"description_bonus": "Bonus au sabotage de 1",
		"bonus": {
			"sabotage": 1
		}
	},
	"soldat": 
	{
		"label": "Soldat",
		"description": "Membres des forces militaires engagés dans la protection du royaume, des frontières et participent aux conflits armés. Ils sont formés au combat et équipés pour servir leur seigneur ou leur roi sur les champs de bataille.",
		"description_bonus": "Bonus de 1 dans la compétence d’arme bouclier",
		"bonus": {
			"bouclier": 1
		}
	},
	"sourcier": 
	{
		"label": "Sourcier",
		"description": "Individus dotés du don de radiesthésie, ils utilisent leur sensibilité pour détecter les sources souterraines d’eau.",
		"description_bonus": "Bonus à la survie de 1",
		"bonus": {
			"survie": 1
		}
	},
	"tatoueur": 
	{
		"label": "Tatoueur",
		"description": "Ils dessinent et pigmentent la peau de leurs clients et créer des motifs permanents.",
		"description_bonus": "Bonus à la représentation de 1",
		"bonus": {
			"representation": 1
		}
	},
	"taxidermiste": 
	{
		"label": "Taxidermiste",
		"description": "Artisans spécialisés dans la préservation des animaux morts, pour l’exposition et l’étude.",
		"description_bonus": "Bonus à la représentation de 1",
		"bonus": {
			"representation": 1
		}
	},
"trafiquant": 
	{
		"label": "Trafiquant de cadavres",
		"description": "Agissant en dehors des lois et des normes éthiques, ils récupèrent et vendent des cadavres humains, souvent à des fins médicales, de recherche ou de formation.",
		"description_bonus": "Bonus au commerce de 1",
		"bonus": {
			"commerce": 1
		}
	},
	"trappeur": 
	{
		"label": "Trappeur",
		"description": "Chasseurs expérimentés spécialisés dans la capture d’animaux sauvages pour leur fourrure, utilisant des pièges et d’autres méthodes de capture pour ne pas les abîmer.",
		"description_bonus": "Bonus à la survie de 1",
		"bonus": {
			"survie": 1
		}
	},
	"zoologiste": 
	{
		"label": "Zoologiste",
		"description": "Scientifiques spécialisés dans l’étude des animaux et de leur comportement, ils observent, classifient et analysent les différentes espèces animales.",
		"description_bonus": "Bonus au dressage de 1",
		"bonus": {
			"dressage": 1
		}
	}
};

export default metiers;