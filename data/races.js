const general = {
    "traits_racial": [
    ]
}

const humains = {
    "label": "Humains",
    "force": 0,
    "constitution": 0,
    "dexterite": 0,
    "intelligence": 0,
    "sagesse": 0,
    "charisme": 0,
    "bonus_don": 1,
    "bonus_langue": 1,
    "bonus_Pts_Caracs": 3,
    "traits_racial": [
    {
        "label": "Adaptabilité",
        "description": "La polyvalence est l’atout clef des humains. Même s’il n’ont pas mis de point dans une compétence, ils ne peuvent être considérés comme non formés et ne subissent pas le malus de 4.",
        "bonus": {
            "competence_non_apprise": 4
        }
    },
    {
        "label": "Campagnard",
        "description": "Les humains nés dans les zones rurales sont habitués à travailler dur. Ils gagnent un bonus de 1 à la ténacité, ainsi qu’à l’athlétisme.",
        "bonus": {
            "athletisme": 1,
            "tenacite": 1
        }
    },
    {
        "label": "Citadin",
        "description": "Les humains vivant dans les villes subsistent essentiellement par leur activité et en marchandant leurs produits. Ils gagnent un bonus de 2 dans deux domaines d’artisanats, ainsi qu’un bonus de 1 en commerce.",
        "bonus": {
            "commerce": 1
        },
        "choix": [{ 
            "type": "Radio", 
            "list": { 
                "forgeron": 2, 
                "tanneur": 2,
                "alchimiste": 2,
                "tailleur": 2,
                "faconneur": 2
            } 
        },{
            "type": "Radio", 
            "list": { 
                "forgeron": 2, 
                "tanneur": 2,
                "alchimiste": 2,
                "tailleur": 2,
                "faconneur": 2
            }
        }]
    },
    {
        "label": "Cosmopolite",
        "description": "Les humains ayant conquis beaucoup de territoires, ils sont souvent amenés à côtoyer d’autres espèces. C’est donc tout naturellement qu’ils gagnent un bonus de 1 en commerce et 1 en éloquence.",
        "bonus": {
            "eloquence": 1,
            "commerce": 1
        }
    },
    {
        "label": "Descendant noble",
        "description": "Le personnage a été élevé dans les plus hautes sphères de la société humaine, un destin princier l’attend. Il gagne un bonus de 2 à l'éloquence et bénéficiera d’un paiement de 5 pièces d’or, versé par sa famille, à chaque niveau.",
        "bonus": {
            "eloquence": 2
        }
    },
    {
        "label": "Enfant des plaines",
        "description": "Les humains vivant dans les steppes vivent souvent en tribu nomade ou sédentaire. Ils gagnent un bonus de 1 en représentation et en dressage.",
        "bonus": {
            "representation": 1,
            "dressage": 1
        }
    },
    {
        "label": "Homme des mers",
        "description": "Certains humains ont l’habitude des voyages en mer ainsi que des us et coutumes des marins. Ils gagnent un bonus de 1 en athlétisme et 1 à la survie.",
        "bonus": {
            "athletisme": 1,
            "survie": 1
        }
    },
    {
        "label": "Milieu aride",
        "description": "Les humains qui survivent dans les déserts sont habitués aux fortes chaleurs et à l’autosuffisance. De par ce fait, ils gagnent un bonus de 1 à la ténacité et à la survie.",
        "bonus": {
            "tenacite": 1,
            "survie": 1
        }
    },
    {
        "label": "Rat d’égout",
        "description": "Les humains qui subsistent dans les bas-fonds des villes sont vifs et rusés, ils gagnent un bonus de 1 aux tests d’acrobatie et de 1 à la discrétion.",
        "bonus": {
            "acrobatie": 1,
            "discretion": 1
        }
    },
    {
        "label": "Montagnard",
        "description": "Les humains qui vivent dans les alpages savent gérer les faibles températures et les terrains montagneux. Ils gagnent un bonus de 1 en ténacité et de 1 en acrobatie.",
        "bonus": {
            "tenacite": 1,
            "acrobatie": 1
        }
    },
    {
        "label": "Aristocrate",
        "description": "Les nobles, qui vivent dans les hauts quartiers des villes, sont habitués aux discours maniérés et gagnent un bonus de 2 à l'éloquence'.",
        "bonus": {
            "eloquence": 2
        }
    },    
    {
        "label": "Pionnier",
        "description": "Le personnage est issu d’une famille de pionniers, ou peut-être en est-il un lui-même ? Le fait est qu’il a appris à se débrouiller en terrain hostile, afin de survivre et prospérer. Il gagne donc un bonus à l’érudition de 1 et à la ténacité de 1.",
        "bonus": {
            "erudition": 1,
            "tenacite": 1
        }
    },
    {
        "label": "Race dominante",
        "description": "Ce n’est un secret pour personne, les humains ont conquis la quasi-totalité du globe et gagnent un bonus à la volonté, ainsi qu’à l’intimidation de 1.",
        "bonus": {
            "volonte": 1,
            "intimidation": 1
        }
    },
    {
        "label": "Vie tropicale",
        "description": "La vie dans ce type de forêts n’est pas de tout repos. Les dangers y sont multiples et il est nécessaire d’être constamment sur le qui-vive et de pouvoir réagir à toute éventualité. Ils gagnent un bonus à la perception et à la médecine de 1.",
        "bonus": {
            "medecine": 1,
            "perception": 1
        }
    }
    ],
    "t": "trois caractéristiques où ils ajoutent un bonus de 1 (compté dans le nombre de points à attribuer)"
}

const elfes = {
    "label": "Elfes",
    "force": 0,
    "constitution": -2,
    "dexterite": 1,
    "intelligence": 1,
    "sagesse": 0,
    "charisme": 1,
    "perception": 2,
    "incantation": 1,
    "traits_racial": [
    {
        "label": "Arcaniste",
        "description": "Les elfes sont étroitement liés à la magie et de par ce fait, leur corps a développé une résistance contre celle-ci. Ils gagnent un bonus à la RM de 2.",
        "bonus": {
            "RM": 2
        }
    },
    {
        "label": "Chasseur",
        "description": "Les elfes sont formés à la chasse, qu’elle soit pour survivre ou pour traquer d’éventuels ennemis. Ils gagnent un bonus à la discrétion, ainsi qu’à la survie de 1.",
        "bonus": {
            "survie": 1,
            "discretion": 1
        }
    },
    {
        "label": "Cliché",
        "description": "Qui n’a jamais vu un elfe manier un arc avec aisance, faisant pleuvoir un déluge de flèches sur ses ennemis ? Ces elfes gagnent un bonus en compétence d’arme « arcs » de 2.",
        "bonus": {
            "arc": 2
        }
    },
    {
        "label": "Elfe citadin",
        "description": "Certains elfes ont vécu dans les cités humaines pendant des années, habitués aux coutumes de ceux-ci, ils gagnent un bonus de 1 en commerce et en éloquence.",
        "bonus": {
            "commerce": 1,
            "eloquence": 1
        }
    },
    {
        "label": "Elfe des bois",
        "description": "La plupart des elfes ont l’habitude de vivre en forêt, ils gagnent un bonus de 1 aux tests de survie et de perception.",
        "bonus": {
            "survie": 1,
            "perception": 1
        }
    },
    {
        "label": "Elfe du soleil",
        "description": "Il existe des elfes qui ont quitté les forêts verdoyantes pour les rejoindre les contrées désertiques. Ils gagnent un bonus à la ténacité de 2.",
        "bonus": {
            "survie": 2
        }
    },
    {
        "label": "Elfe lié au mana",
        "description": "D’autres elfes ont connu une initiation particulière à la magie. De ce fait, ils obtiennent automatiquement le don perception magique, ainsi qu’un bonus à l’incantation de 1.",
        "bonus": {
            "dons": ["perception_magique"],
            "incantation": 1
        }
    },
    {
        "label": "Fait main",
        "description": "Les elfes vivent de manière simple et proche de la nature. Ils ont l’habitude de se débrouiller par eux-mêmes pour subsister, prélevant les ressources directement dans la forêt. Ce mode de vie leur offre un bonus de 4 sur deux domaines d’artisanat.",
        "bonus": {
        },
        "choix": [{ 
            "type": "Radio", 
            "list": { 
                "forgeron": 2, 
                "tanneur": 2,
                "alchimiste": 2,
                "tailleur": 2,
                "faconneur": 2
            } 
        },{
            "type": "Radio", 
            "list": { 
                "forgeron": 2, 
                "tanneur": 2,
                "alchimiste": 2,
                "tailleur": 2,
                "faconneur": 2
            }
        }]
    },
    {
        "label": "Haut elfe",
        "description": "Arborant une chevelure opaline, ils représentent la noblesse elfique. Leur rang leur offre un bonus de 2 à l'éloquence'.",
        "bonus": {
            "eloquence": 2
        }
    },
    {
        "label": "Longiforme",
        "description": "Les elfes sont des êtres naturellement très fins, élancés et souples. Cette morphologie exacerbée leur fait gagner un bonus à l’acrobatie de 2.",
        "bonus": {
            "acrobatie": 2
        }
    },
    {
        "label": "Manufacture elfique",
        "description": "Les elfes, afin de conserver leur agilité naturelle, doivent se vêtir d’armures tout aussi légères qu’eux. Le personnage porte justement une de ces armures et le malus lié au poids de celle-ci diminue de 2.",
        "bonus": {  
            "malus_armure": -2,
            "bonus_initiative": 2
        }
    },    
    {
        "label": "Pieds agiles",
        "description": "De par leur taille fine, les elfes sont relativement légers, ce qui leur offre une aisance remarquable. Ils gagnent un bonus à la vitesse de déplacement de 2.",
        "bonus": {
            "deplacement": 2
        }
    },
    {
        "label": "Sagesse",
        "description": "Les elfes sont connus pour leur espérance de vie exceptionnelle qui leur permet d’assimiler bon nombre de connaissances. Ils gagnent naturellement un bonus à l’érudition de 2.",
        "bonus": {
            "erudition": 2
        }
    },
    {
        "label": "Source de pouvoirs",
        "description": "Certains individus sont plus endurants que d’autres et leur puissance magique dépasse celle de leurs camarades. Le personnage gagne 2 points de mana supplémentaire par niveau.",
        "bonus": {
            "pm_par_niveaux": 2
        }
    }
    ],
    "t": ""
};

const sauriens = {
    "label": "Sauriens",
    "force": 0,
    "constitution": 2,
    "dexterite": 0,
    "intelligence": -1,
    "sagesse": 0,
    "charisme": 0,
    "pts_souffle": 1200,
    "RD": 1,
    "traits_racial": [
    {
        "label": "Animal de compagnie",
        "description": "Certains sauriens aiment s’entourer d’animaux aux caractéristiques reptiliennes. Ils gagnent un bonus de 4 aux tests de dressage sur tous les reptiles.",
        "bonus": {

        }
    },
    {
        "label": "Autosuffisant",
        "description": "Les sauriens vivent dans des villages bien plus primitifs que les autres civilisations. De par ce fait, chacun doit faire preuve d’une autonomie modèle. Lorsque le personnage dépense un point de compétence en artisanat, celui-ci ne sera pas effectif pour deux, mais trois domaines simultanés.",
        "bonus": {

        }
    },    
    {
        "label": "Bestialité",
        "description": "Lorsque le personnage réussit une attaque au corps-à-corps, celui-ci a le droit à une attaque de morsure supplémentaire. La réussite sera automatique et l’attaque infligera 1D4 +1 par niveau dégâts.",
        "bonus": {

        }
    },
    {
        "label": "Chasseur squamé",
        "description": "Les sauriens vivent de la chasse, de la cueillette et essentiellement de la pêche. Ils gagnent un bonus de 1 à la perception et à la survie.",
        "bonus": {
            "perception": 1,
            "survie": 1
        }
    },
    {
        "label": "Chef de tribu",
        "description": "Votre saurien est habitué à gérer son clan ainsi que les relations avec l’extérieur, ce rang permet d’obtenir un bonus de 2 en éloquence.",
        "bonus": {
            "eloquence": 2
        }
    },
    {
        "label": "Collerette",
        "description": "Bien que rares, certains sauriens possèdent une collerette qu’ils peuvent déplier à volonté. Grâce à cet atout, ils gagnent un bonus à l’intimidation de 2.",
        "bonus": {
            "intimidation": 2
        }
    },
    {
        "label": "Cuir impénétrable",
        "description": "Quelques sauriens ont la peau bien plus dure que leurs congénères. Leur bonus naturel à la RD est augmenté de 1.",
        "bonus": {
            "RD": 1
        }
    },
    {
        "label": "Faux caméléon",
        "description": "Les sauriens ont un panel de couleur d’écailles assez étendu. Certains, nés avec des couleurs plus sobres, arrivent facilement à se fondre dans l’environnement et gagnent un bonus à la discrétion de 2.",
        "bonus": {
            "discretion": 2
        }
    },
    {
        "label": "Fouet",
        "description": "Leur queue servant de balancier, il est plus difficile de leur faire perdre l’équilibre, ils gagnent un bonus de 4 contre les jets de manœuvres visant à les déséquilibrer. De plus, ils peuvent utiliser une attaque latérale qui inflige 1D6 dégâts + le bonus de force et repousse les ennemis au corps-à-corps d’une case.",
        "bonus": {

        }
    },
    {
        "label": "Historien",
        "description": "Les sauriens sont une race ancienne qui a eu loisir de voir évoluer les autres races. Certains sauriens sont d’ailleurs chargés de transmettre le savoir aux nouvelles générations et reçoivent un bonus de 2 en érudition.",
        "bonus": {
            "erudition": 2
        }
    },
    {
        "label": "Œil reptilien",
        "description": "Les yeux des sauriens sont moins sensibles à la baisse de luminosité et peuvent voir dans la pénombre non magique presque aussi bien qu’en plein jour.",
        "bonus": {

        }
    },    
    {
        "label": "Raptor",
        "description": "Certains sauriens ont les pieds pourvus d’ergots aux griffes très aiguisées. La charge est modifiée et permet au saurien de renverser une cible et de lui infliger potentiellement un état de saignement, en plus des autres effets habituels de la charge.",
        "bonus": {

        }
    },
    {
        "label": "Sang chaud",
        "description": "Lorsque le saurien s'expose directement à la chaleur du soleil pendant au moins une heure, il peut lancer ses dés de régénération. De plus, il bénéficie d’un bonus d’une classe de dé pour les 5 heures suivantes.",
        "bonus": {

        }
    },
    {
        "label": "Vigueur",
        "description": "Le peuple saurien vit avec peu de ressources. Ceux-ci doivent bien souvent trimer pour survivre et ne sont pas effrayés par les tâches physiques. Ils gagnent un bonus de 1 en athlétisme et 1 en ténacité.",
        "bonus": {
            "athletisme": 1,
            "tenacite": 1
        }
    }
    ],
    "t": "Les sauriens sont aussi à l’aise dans l’eau que sur terre et ont un bonus de 2 sur les actions de natation"
};

const draconides = {
    "label": "Draconides",
    "force": 2,
    "constitution": 1,
    "dexterite": 0,
    "intelligence": 0,
    "sagesse": -2,
    "charisme": 0,
    "bonus_don_racial": 1,
    "traits_racial": [
    {
        "label": "Constance",
        "description": "Leur queue servant de balancier, les draconides gagent un bonus au don « attaque latérale ». Ils peuvent choisir d’augmenter les dégâts d’une classe dé, ou d’avoir un bonus de 2 sur cette manœuvre.",
        "bonus": {

        }
    },
    {
        "label": "Cousins éloignés",
        "description": "Les draconides, descendants lointains des dragons, possèdent toujours un lien avec ceux-ci. Toutes les interactions sociales avec des dragons, ou de créatures apparentées (wyrms, drakes, etc …) se font avec un bonus de 4. De plus, ils obtiennent automatiquement la langue draconique.",
        "bonus": {
            "langue": "draconique"
        }
    },
    {
        "label": "Filiation magique",
        "description": "De par leur lignée draconique, les draconides sont naturellement à l’aise avec l’utilisation de la magie. Le personnage gagne un bonus de 2 en incantation.",
        "bonus": {
            "incantation": 2
        }
    },
    {
        "label": "Guerrier dragon",
        "description": "La plupart des draconides sont entraînés au combat depuis leur plus tendre enfance. Ils gagnent un bonus de 2 dégâts pour les attaques et manœuvres physiques.",
        "bonus": {
            "bonus_dmg": 2
        }
    },
    {
        "label": "Héritage draconique",
        "description": "Fiers descendants des dragons, les draconides ont hérité de certaines caractéristiques de leurs ancêtres. Leur peau plus dure et écailleuse par endroit en fait partie. Ces zones sur le corps des draconides leur accordent un bonus à la RD ou à la RM de 1.",
        "bonus": {
        },
        "choix": [{ 
            "type": "Radio", 
            "list": { 
                "RD": 1, 
                "RM": 1 
            } 
        }]
    },
    {
        "label": "Immémorial",
        "description": "Les draconides ont accumulé une immense quantité de connaissances, retranscrites dans les milliers de livres qui composent leurs bibliothèques. Certains draconides ont favorisé le savoir au combat et gagnent donc un bonus à l’érudition de 2.",
        "bonus": {
            "erudition": 2
        }
    },
    {
        "label": "Membre du conseil",
        "description": "Draconides de haut rang, ils décident de la place d’un individu dans leur société, ses tâches, ainsi que ses obligations pour le reste de sa vie. Leurs fonctions leur offrent un bonus à l’éloquence de 2.",
        "bonus": {
            "eloquence": 2
        }
    },
    {
        "label": "Né pour combattre",
        "description": "Les draconides sont des guerriers aguerris et sont habitués aux diverses manœuvres de combat. Ils gagnent donc un bonus de 2 aux manœuvres.",
        "bonus": {
            "manoeuvres": 2
        }
    },
    {
        "label": "Œil reptilien",
        "description": "Les yeux des draconides sont moins sensibles à la baisse de luminosité et peuvent voir dans la pénombre non magique presque aussi bien qu’en plein jour.",
        "bonus": {

        }
    },    
    {
        "label": "Pilier",
        "description": "Les draconides ont hérité de la robustesse des dragons. Ils gagnent un bonus de 2 points de vie supplémentaires à chaque montée en niveau.",
        "bonus": {
            "pv_par_niveaux": 2
        }
    },

    {
        "label": "Régénération avancée",
        "description": "L’organisme des draconides est solide, ils gagnent un bonus de 2 points de vie supplémentaires régénérés par temps de pause.",
        "bonus": {

        }
    },
    {
        "label": "Rumeurs avantageuses",
        "description": "Les récits sur la puissance du sang draconique qui coule dans les veines des draconides sont souvent exagérés. Néanmoins, cette ascendance confère au personnage un bonus de 2 en intimidation.",
        "bonus": {
            "intimidation": 2
        }
    },
    {
        "label": "Santé inébranlable",
        "description": "Les draconides ont une santé de fer et sont connus pour être de véritables forces de la nature. Le personnage est insensible aux poisons et aux maladies non magiques.",
        "bonus": {

        }
    },
    {
        "label": "Souffle",
        "description": "Les draconides disposent d’une capacité de souffle, d’un élément au choix, comme de vrais dragons. Il forme un cône long de deux mètres devant le lanceur et inflige 1D6 dégâts + {{2}}. La capacité se lance sur un jet de constitution brute (avec un malus de 4) et possède un temps de recharge de 3 tours (souffle, x, x, x, souffle).",
        "bonus": {

        }
    },
    {
        "label": "Transcendance d’armure",
        "description": "Les draconides sont des adeptes du combat. Leurs entraînements et leur carrure imposante leur permettent sans mal de porter de lourdes armures. Ils réduisent de 2 les malus liés à celles-ci.",
        "bonus": {  
            "malus_armure": -2,
            "bonus_initiative": 2
        }
    }
    ],
    "t": "Les draconides peuvent se servir d’attaques naturelles (griffes,queue et souffle)"
};

const nains = {
    "label": "Nains",
    "force": 1,
    "constitution": 1,
    "dexterite": -1,
    "intelligence": 0,
    "sagesse": 0,
    "charisme": 0,
    "tenacite": 1,
    "traits_racial": [
    {
        "label": "Atelier nain",
        "description": "La totalité des armes et armures dont se servent les nains est directement créée dans leurs forges. La qualité de leurs créations n’est plus à prouver et le personnage à la chance d’en posséder une. Au choix, le personnage pourra avoir une arme dont les dégâts sont augmentés de 2 ou une armure dont la RD est augmentée de 2.",
        "bonus": {
            //TODO choix
        }
    },
    {
        "label": "Appât du gain",
        "description": "Les nains aiment l’or, les bijoux, les pierres précieuses, ou tout ce qui, en somme, brille et a de la valeur. Ils reçoivent un bonus de perception de 1 et un bonus au commerce de 1.",
        "bonus": {
            "perception": 1,
            "commerce": 1
        }
    },
    {
        "label": "Artisan",
        "description": "Leur réputation les précède : les nains sont d’excellents artisans. Ils reçoivent un bonus de 2 dans deux domaines d’artisanat.",
        "bonus": {
        },
        "choix": [{ 
            "type": "Radio", 
            "list": { 
                "forgeron": 2, 
                "tanneur": 2,
                "alchimiste": 2,
                "tailleur": 2,
                "faconneur": 2
            } 
        },{
            "type": "Radio", 
            "list": { 
                "forgeron": 2, 
                "tanneur": 2,
                "alchimiste": 2,
                "tailleur": 2,
                "faconneur": 2
            }
        }]
    },
    {
        "label": "Bien équipé",
        "description": "Dès lors où un nain utilise un outil, peu importe sa qualité, il fera des merveilles. Le bonus accordé par un outil sera doublé quand le nain l’utilisera.",
        "bonus": {

        }
    },
    {
        "label": "Bonne mémoire",
        "description": "Les nains sont réputés pour la force de leur rancœur, mais celle-ci n’est pas le fruit du hasard. La plupart des nains sont dotés d’une excellente mémoire, ce qui leur accorde un bonus de 2 sur les jets d’érudition.",
        "bonus": {
            "erudition": 2
        }
    },
    {
        "label": "Borné",
        "description": "Les nains sont têtus, très têtus. Ils reçoivent un bonus à la volonté de 2.",
        "bonus": {
            "volonte": 2
        }
    },
    {
        "label": "Cliché",
        "description": "Difficile d’imaginer un nain sans son fidèle marteau, aussi bien utile pour la forge que pour le combat. Ils gagnent un bonus en compétence d’arme « marteaux » de 2.",
        "bonus": {
            "marteau": 2,
            "marteaul": 2
        }
    },
    {
        "label": "Façonneur",
        "description": "Les nains sont connus pour leurs prouesses artisanales, dont la création de runes. Le personnage possède une rune avec un enchantement de type permanent (utilisable une fois par jour, sans jet de dé) au choix :<br>"+
        "• L’utilisation de la rune régénère immédiatement 1D4 points de vie + {{1}}<br>"+
        "• Ajoute 1D4 + {{1}} dégâts à la prochaine attaque<br>"+
        "• L’utilisation de la rune régénère de suite 1D4 points de mana + {{1}}<br>"+
        "• Augmente la RD de 1D4 + {{1}}, pendant 2 tours<br>"+
        "• ugmente la RM de 1D4 + {{1}}, pendant 2 tours",
        "bonus": {

        }
    },
    {
        "label": "Grippe-sou",
        "description": "« Payer », voilà un mot dur à entendre pour un nain. Celui-ci cherchera toujours à négocier pour débourser le moins possible. Le personnage reçoit un bonus au commerce de 2.",
        "bonus": {
            "commerce": 2
        }
    },
    {
        "label": "Habitué des tavernes",
        "description": "Les nains aiment l’alcool fort et à force d’enchaîner les pintes, cela à familiarisé leur organisme à lutter contre diverses substances nocives. Ils gagnent donc un bonus aux jets de ténacité de 2.",
        "bonus": {
            "tenacite": 2
        }
    },
    {
        "label": "Manufacture maîtrisée",
        "description": "Les nains sont sans aucun doute la race dont la facture est la plus recherchée et la plus qualitative du monde. Lorsqu’un nain effectue un travail d’artisanat, le nombre de composants nécessaires est divisé par deux (arrondit à l’inférieur). De plus, le nombre de jets ratés avant destruction de l’objet ou des composants est augmenté de 1.",
        "bonus": {

        }
    },
    {
        "label": "Montagnard",
        "description": "Habitués à la vie dans les montagnes, ils gagnent un bonus de 1 en ténacité et en acrobatie.",
        "bonus": {
            "tenacite": 1,
            "acrobatie": 1
        }
    },    
    {
        "label": "Trapu",
        "description": "Une fois par combat, un nain peut décider d’esquiver n’importe quelle manœuvre ennemie, sans opposition.",
        "bonus": {

        }
    },
    {
        "label": "Vision des souterrains",
        "description": "Leur vie souterraine a habitué les yeux des nains à la pénombre non magique, ils peuvent donc voir dans l’obscurité aussi bien que s’ils étaient à la lumière du jour.",
        "bonus": {

        }
    }
    ],
    "t": "les nains naturellement doués en artisanat. Ils reçoivent un bonus de 2 dans deux domaines d’artisanat.",
    "choix": [{ 
        "type": "Radio", 
        "list": { 
            "forgeron": 2, 
            "tanneur": 2,
            "alchimiste": 2,
            "tailleur": 2,
            "faconneur": 2
        }
    },{ 
        "type": "Radio", 
        "list": { 
            "forgeron": 2, 
            "tanneur": 2,
            "alchimiste": 2,
            "tailleur": 2,
            "faconneur": 2
        }
    }]
};

const orques = {
    "label": "Orques",
    "force": 2,
    "constitution": 2,
    "dexterite": 0,
    "intelligence": -1,
    "sagesse": 0,
    "charisme": -2,
    "intimidation": 4,
    "traits_racial": [
    {
        "label": "Barbarie",
        "description": "Les orques cognent dur et sans pondération. Lorsqu’ils réalisent un coup critique au combat, en plus de doublé les dégâts de l’attaque, il faudra relancer un jet de dégâts qui s’additionnera au précédent (Sans doubler celui-ci)",
        "bonus": {

        }
    },
    {
        "label": "Belliqueux",
        "description": "Les autres races considèrent les orques comme étant trop agressifs pour être côtoyés. De par ce fait, les personnes qui seraient normalement neutres à l’égard du personnage deviennent systématiquement inamicales. En contrepartie, les jets d’intimidation se font avec un bonus de 2, avec impossibilité d’échec critique sur cette compétence.",
        "bonus": {
            "intimidation": 2
        }
    },
    {
        "label": "Berserker",
        "description": "Le personnage obtient automatiquement le don Berserk, en plus d’un bonus à celui-ci. Le coût en mana et les conditions restent les mêmes, mais le malus cumulable de 1 pour sortir de l’état disparaît.",
        "bonus": {
            "dons": ["berserk"]
        }
    },
    {
        "label": "Bête et méchant",
        "description": "Les orques ne sont pas réputés pour être des lumières, loin de là. Un orque possédant ce trait alternatif pourra convertir 2 points d’intelligence, en 2 points de force. Permettant ainsi d’outrepasser le minimum de 10 points dans une caractéristique.",
        "bonus": {

        }
    },
    {
        "label": "Brute",
        "description": "Les orques sont connus pour leur brutalité caractéristique. Lorsque le personnage effectue un coup critique sur une attaque au corps-à-corps, l’attaque infligera automatiquement les dégâts maximums, sans devoir jeter les jets de dégâts.",
        "bonus": {

        }
    },
    {
        "label": "Chef de clan",
        "description": "Dirigeants des troupes, souvent reconnus et respectés pour leur force physique supérieure, ils sont habitués à se faire obéir et voir contester leur autorité. Ils gagnent un bonus au corps-à-corps de 2.",
        "bonus": {
            "corpsacorps": 2
        }
    },
    {
        "label": "Dernières forces",
        "description": "Lorsque les points de vie d’un orque chutent à 0, au lieu de tomber dans le coma, le personnage voit ses points de vie remonter à 1. Cette aptitude n’est utilisable qu’une fois par jour seulement.",
        "bonus": {

        }
    },
    {
        "label": "Esclavagiste",
        "description": "Les orques sont tristement célèbres pour malmener les races qu’ils considèrent comme plus faibles en ayant recours à l’esclavage. Ils gagnent un bonus à la psychologie de 2.",
        "bonus": {
            "psychologie": 2
        }
    },
    {
        "label": "Force brute",
        "description": "Les orques ont une musculature impressionnante, tout comme leur force de frappe. Ils gagnent un bonus de 2 aux dégâts sur tous les types de corps-à-corps.",
        "bonus": {

        }
    },
    {
        "label": "Insalubrité",
        "description": "Les orques ne vivent pas dans de belles cités, mais dans des camps miteux peu entretenus. Leur système immunitaire s’est renforcé et ils reçoivent un bonus aux jets de ténacité de 2.",
        "bonus": {
            "tenacite": 2
        }
    },
    {
        "label": "Né pour combattre",
        "description": "Les orques sont des machines de guerre qui manient les armes dès leur plus jeune âge. Le personnage peut ainsi ajouter la compétence d’arme de son choix, avec un score initial de 2, sans dépenser de point de caractéristique supplémentaire.",
        "bonus": {

        }
    },    
    {
        "label": "Pilier",
        "description": "Les orques sont des forces de la nature, leur endurance est sans pareille. Ils gagnent un bonus de 2 points de vie supplémentaires à chaque montée en niveau.",
        "bonus": {
            "pv_par_niveaux": 2
        }
    },
    {
        "label": "Transcendance d’armure",
        "description": "Les orques sont des créatures robustes, la lourdeur d’une armure les affecte peu. Ils réduisent de 2 les malus liés aux armures.",
        "bonus": {  
            "malus_armure": -2,
            "bonus_initiative": 2
        }
    },
    {
        "label": "Traqueur né",
        "description": "Les orques savent suivre une piste sanglante sans même avoir besoin de trace visuelle. Ils sont experts dans la traque de toutes sortes de créatures et gagnent un bonus de 2 sur les jets de survie.",
        "bonus": {
            "survie": 2
        }
    }
    ],
    "t": "bonus de 2 contre les jets de peur"
};

const halfelins = {
    "label": "Halfelins",
    "force": -2,
    "constitution": 0,
    "dexterite": 2,
    "intelligence": 0,
    "sagesse": 0,
    "charisme": 1,
    "discrétion": 2,
    "acrobatie": 1,
    "traits_racial": [
    {
        "label": "Chanceux",
        "description": "Les halfelins ont la réputation d’être particulièrement chanceux, ils reçoivent un bonus de 20 sur leurs jets de chance.",
        "bonus": {

        }
    },
    {
        "label": "Combattant d’arène",
        "description": "Les esclavagistes traînent leurs prisonniers dans des combats contre d’autres asservis, pariant sur le vainqueur. Les esclaves halfelins ont dû survivre coûte que coûte et gagnent un bonus de 2 au corps-à-corps.",
        "bonus": {
            "corpsacorps": 2
        }
    },
    {
        "label": "Coup bas",
        "description": "Les petits halfelins n’ont d’autre choix que de se battre contre plus grand qu’eux. Leur marge de coup critique augmente de 1 sur leur maîtrise d’arme, contre les cibles plus grandes qu’eux.",
        "bonus": {

        }
    },
    {
        "label": "Créatif",
        "description": "Les halfelins ont développé toutes sortes de techniques pour survivre et possèdent donc plus d’un tour dans leur sac. Et ce n’est pas qu’une expression ! Le personnage, sans aucun jet, assemble ou désassemble une de ses trois créations en une heure :<br>"+
        "– Jouet mécanique : Un petit jouet sur roulette faisant du bruit et avançant d’un mètre par tour.<br>"+
        "– Feu de détresse : Un petit dispositif (nécessitant d’être allumé) qui explose dans un léger flash potentiellement aveuglant et qui libère un nuage de fumée colorée qui va s’élever dans les airs.<br>"+
        "– Piège : Ressemble de près à un piège utilisé par les trappeurs, afin d’immobiliser une cible de taille moyenne. Il inflige 5 dégâts fixes et peut être ouvert sous un jet de force brute ou de sabotage réussi.",
        "bonus": {

        }
    },
    {
        "label": "Détecteur de mensonges",
        "description": "Les halfelins sont constamment sur leurs gardes, que ce soit en plein combat ou lors d’un échange verbal. Leur expérience leur permet de déjouer les ruses les plus subtiles. Le personnage bénéficie ainsi d’un bonus de 2 en psychologie.",
        "bonus": {
            "psychologie": 2
        }
    },
    {
        "label": "Esclave en fuite",
        "description": "Les halfelins sont sans aucun doute les plus victimes de l’esclavagisme. Ils ont développé une résistance particulière en vue de nombreuses années de vie passées à souffrir. Ils reçoivent un bonus aux jets de volonté de 2.",
        "bonus": {
            "volonte": 2
        }
    },
    {
        "label": "Esquiver les ennuis",
        "description": "Les halfelins ont plus souvent l’habitude de fuir les problèmes que de les affronter directement. Ils gagnent un bonus à l’esquive et à la discrétion de 1..",
        "bonus": {
            "esquive": 1,
            "discretion": 1
        }
    },
    {
        "label": "Faux-semblants",
        "description": "Maître dans l’art de la fourberie, le personnage gagne un bonus de dégâts supplémentaires de 1D à ses attaques sournoises.",
        "bonus": {

        }
    },
    {
        "label": "Globe-trotteur",
        "description": "Habitués à parcourir le monde, les halfelins se sont rodés au voyage et gagnent un bonus de 2 à la survie.",
        "bonus": {
            "survie": 2
        }
    },    
    {
        "label": "Patte-blanche",
        "description": "Lorsqu’on est si petit, il faut savoir faire le dos rond pour ne pas se faire remarquer et s’attirer d’ennui. Les halfelins gagnent un bonus à l'éloquence de 2.",
        "bonus": {
            "eloquence": 2
        }
    },
    {
        "label": "Petite taille",
        "description": "Certains halfelins ont appris à utiliser leur petitesse à leur avantage. Doués pour se sortir des situations les plus délicates, ils gagnent un bonus de 2 aux jets de discrétion.",
        "bonus": {
            "discretion": 2
        }
    },
    {
        "label": "Promptitude",
        "description": "Être petit n’a pas que des désavantages. En situation de combat, les halfelins peuvent traverser une case occupée par une créature plus grande, sans avoir besoin de la contourner. En plus de ceci, ils gagnent un bonus à l’esquive de 2, en cas d’attaque d’opportunité.",
        "bonus": {

        }
    },
    {
        "label": "Techniques critiquables",
        "description": "Tous les moyens sont bons pour survivre, même si cela inclut des méthodes passablement déloyales. Une fois par combat, le personnage peut effectuer une manœuvre, automatiquement réussie, avec en marge de réussite le score en intelligence du personnage.",
        "bonus": {

        }
    },
    {
        "label": "Tête de turc",
        "description": "Accommodés avec le fait de se faire malmener, les halfelins ont naturellement appris à se défendre, même en cas de surnombre d’assaillants. Le personnage ne peut plus être pris au dépourvu.",
        "bonus": {

        }
    }
    ],
    "t": "créatures de taille petite"
};

const dehamanes = {
    "label": "Dehamanes",
    "force": 0,
    "constitution": 0,
    "dexterite": 0,
    "intelligence": 2,
    "sagesse": 1,
    "charisme": -2,
    "bluff": 2,
    "survie": 1,
    "traits_racial": [
    {
        "label": "Ailes",
        "description": "Certains dehamanes ont une musculature suffisamment développée pour que leurs ailes puissent les porter. Voir les détails du vol dans le chapitre des déplacements.",
        "bonus": {

        },
        "flight": {

        }
    },
    {
        "label": "Aura terrifiante",
        "description": "La simple vue d’un dehamane peut suffire à ressentir soudainement un malaise ou de la peur. Les dehamanes gagnent un bonus de 2 à l’intimidation.",
        "bonus": {
            "intimidation": 2
        }
    },
    {
        "label": "Démon des flammes",
        "description": "Chaque famille possède des ancêtres démoniaques différents et certains ont vu des habilités ressortirent, questions de chance et d’hérédité. Le personnage est capable de manier les flammes et peut générer un cône de feu dans une zone de type 1, sur une portée de 5 mètres. L’action coûtera 2 points de mana et sera lancée sur la caractéristique d’intelligence, infligeant 1D6 dégâts + {{2}} par niveau du lanceur.",
        "bonus": {

        }
    },
    {
        "label": "Héritage infernal",
        "description": "Une capacité issue des démons qui, contre 4 points de mana, permet de modifier son apparence pendant {{1}} heure(s). Il sera alors possible d’arborer la physionomie d’un individu d’une autre race. Se transformer prend un tour et ne modifie aucunement les capacités du personnage.",
        "bonus": {

        }
    },
    {
        "label": "Les voix dans ta tête",
        "description": "Une fois par jour, un dehamane peut forcer une cible intelligente à faire une action, à la suite d’un jet de volonté opposé. Les ordres suicidaires ou opposés à l’éthique de la cible ne seront jamais suivis.",
        "bonus": {

        }
    },
    {
        "label": "Maître txakurra",
        "description": "Les txakurras sont les familiers des dehamanes. Il est très fréquent d’en posséder, même pour les exilés. Le personnage se voit attribuer l’animal gratuitement et gagne un bonus de 1 au dressage.",
        "bonus": {
            "dressage": 1
        }
    },
    {
        "label": "Moisson des âmes",
        "description": "Restes de leurs ancêtres, certains dehamanes sont capables de manipuler les âmes des défunts. Le personnage peut s’approprier l’âme fraîchement sortie du corps de chaque ennemi tué et pourra la convertir en points de vie, de mana ou en dégâts magiques. La valeur d’une âme équivaut au Fp de la victime.",
        "bonus": {

        }
    },
    {
        "label": "Mots piquants",
        "description": "Les espiègles dehamanes savent taper là où ça fait mal. De ce fait, ils gagnent un bonus de 2 aux jets de provocation.",
        "bonus": {
            "provocation": 2
        }
    },    
    {
        "label": "Peur nourrissante",
        "description": "Les démons ont toujours inspiré la crainte et le personnage possède un don pour s’en nourrir. Pour chaque ennemi effrayé en combat, le personnage gagne un bonus de 2 à ses dégâts.",
        "bonus": {

        }
    },
    {
        "label": "Regard démotivant",
        "description": "Par un simple regard, le personnage inflige un malus à la volonté de sa cible équivalent à son niveau. Pour réaliser cette action, il faudra réussir un jet de psychologie opposé et dépenser 6 points de mana.",
        "bonus": {

        }
    },
    {
        "label": "Relié aux ombres",
        "description": "Liés aux ténèbres de par leurs origines, les dehamanes n’ont aucun problème à voir normalement parmi les ténèbres, même magiques. Cependant, à contrario, ceux-ci se voient attribuer une faiblesse aux dégâts de lumière et à l’aveuglement de 4.",
        "bonus": {

        }
    },
    {
        "label": "Succube et incube",
        "description": "Certains dehamanes ont acquis la capacité des succubes à entendre les pensées des personnes environnantes. Contre 4 points de mana et pendant {{1}} minutes, celui-ci pourra entre les pensées de toutes créatures dans un rayon de 10 mètres autour de lui.",
        "bonus": {

        }
    },
    {
        "label": "Survie rudimentaire",
        "description": "Devant fréquemment voler pour subsister, les parias dehamanes gagnent un bonus de 1 à la discrétion, ainsi qu’au sabotage.",
        "bonus": {
            "discretion": 1,
            "sabotage": 1
        }
    },
    {
        "label": "Volute",
        "description": "Une fois par combat, le personnage est capable de générer un sort occulte. Il peut lancer un projectile d’ombre sur 10 mètres qui inflige 1D4 + {{2}} dégâts. Le sort ne coûte pas de mana et est automatiquement réussi avec une marge de réussite égale au score d’intelligence du personnage.",
        "bonus": {

        }
    }
    ],
    "t": ""
};

const aingeales = {
    "label": "Aingéales",
    "force": 0,
    "constitution": -1,
    "dexterite": -1,
    "intelligence": 0,
    "sagesse": 1,
    "charisme": 2,
    "eloquence": 2,
    "psychologie": 1,
    "traits_racial": [
    {
        "label": "Ailes",
        "description": "Certains aingéales ont une musculature suffisamment développée pour que leurs ailes puissent les porter. Voir les détails du vol dans le chapitre des déplacements.",
        "bonus": {

        },
        "flight": {

        }
    },
    {
        "label": "Apaisement",
        "description": "En raison de leur faculté à tempérer les foudres, les aingéales peuvent gagner grâce aux yeux d’autrui. Une fois par séance, le personnage peut choisir d’apaiser les tensions et les doutes qui règnent entre lui et une personne, améliorant ses chances de réussir un dialogue (une personne neutre deviendra amicale).",
        "bonus": {

        }
    },
    {
        "label": "Aura sainte",
        "description": "Dans un rayon de 3 cases autour du personnage, l’aura bénite de celui-ci fait que toute personne présente dans la zone recevra un bonus sur ses soins ou buffs égal de {{1}}.",
        "bonus": {

        }
    },
    {
        "label": "Bénédiction",
        "description": "Certains aingéales sont capables d’insuffler à leur équipement une partie des pouvoirs qui leur ont été transmis. L’armure du personnage, en plus de ses attributs habituels, gagne un bonus à la RM de 2.",
        "bonus": {
            "RM": 2
        }
    },
    {
        "label": "Bouclier des anges",
        "description": "Bénis depuis leur naissance, certains aingéales ont toujours été protégés. Ils gagnent un bouclier passif et invisible (qui se réinitialise chaque jour), bloquant {{10}} points de dégâts.",
        "bonus": {

        }
    },
    {
        "label": "Caravanier",
        "description": "Contrairement aux dehamanes, les aingéales ont des échanges avec le monde. Ils se fournissent en matériaux et en exportent. De ce fait, ils gagnent un bonus de 2 en commerce.",
        "bonus": {
            "commerce": 2
        }
    },
    {
        "label": "Douceur",
        "description": "Le sang angélique qui parcourt leurs veines, ainsi que leur apparence ingénue, fait que leurs ennemis hésitent à les attaquer. Superstition ? Sûrement, mais les aingéales comptent bien tirer profit de cet avantage et gagnent un bonus à l’initiative de 4.",
        "bonus": {
            "bonus_initiative": 4
        }
    },
    {
        "label": "Halo aveuglant",
        "description": "Une fois par combat, sans avoir à dépenser de point de mana, ni faire de jet, le personnage illumine son corps d’une lumière vive. Une fraction de seconde plus tard, la luminosité va grimper en flèche et aveugler pendant 1D4 tours + {{1}} toute personne qui regarderait le personnage dans une zone de 3 cases autour de lui. Le score de charisme du personnage déterminera sa marge de réussite, qui sera opposée à un jet de ténacité ennemi.",
        "bonus": {

        }
    },
    {
        "label": "Œil aiguisé",
        "description": "Capable d’exploiter les failles les plus minimes d’un adversaire, le personnage peut convertir ses points de la compétence psychologie en dégâts additionnels. Il est possible d’utiliser cette capacité {{1}} fois par combat.",
        "bonus": {

        }
    },
    {
        "label": "Paladin de la lumière",
        "description": "Difficile d’imaginer un aingéale sans une belle armure et son bouclier tel un défenseur des cieux. Les aingéales gagnent un bonus de 1 au bouclier et diminuent de 1 le malus lié aux armures.",
        "bonus": {  
            "malus_armure": -1,
            "bonus_initiative": 1,
            "bouclier": 1
        }
    },    
    {
        "label": "Protection divine",
        "description": "Une fois par jour, les aingéales sont aptes de générer un miracle sans avoir à faire de jet. Sans maîtriser la luciomancie, ils sont capables de lancer un sort de soin régénérant 1D6 points de vie + {{2}}",
        "bonus": {

        }
    },
    {
        "label": "Sentinelle bénie",
        "description": "Proche de l’élément de la lumière, le personnage peut générer des flèches ou carreaux lumineux, contre 1 point de mana par projectile. Ils infligeront des dégâts standards, mais seront de nature magique.",
        "bonus": {

        }
    },
    {
        "label": "Sourire angélique",
        "description": "Les aingéales savent profiter de leur succès auprès des populations. Lorsque le personnage s’adresse à une personne d’une autre race, il peut (une fois par séance) choisir de réussir automatiquement un jet lié au charisme. La marge de réussite est égale au score dans cette même caractéristique.",
        "bonus": {

        }
    },
    {
        "label": "Voix envoûtante",
        "description": "Lorsqu’un aingéale chante, sa voix sonne comme une douce mélodie et sa grâce n’a rien à envier aux elfes. Les aingéales sont un plaisir à regarder et à entendre. Ils gagnent donc tout naturellement un bonus de 4 pour les jets de représentation.",
        "bonus": {
            "representation": 4
        }
    }
    ],
    "t": ""
};

const demiElfes = {
    "label": "Demi-elfes",
    "force": 0,
    "constitution": -1,
    "dexterite": 0,
    "intelligence": 0,
    "sagesse": 0,
    "charisme": 1,
    "bonus_don": 1,
    "bonus_Pts_Caracs": 2,
    "traits_racial": humains.traits_racial.concat(elfes.traits_racial),
    "t": "bonus de 2 dans une caractéristique au choix (compté dans le nombre de points à attribuer)"
};

const demiNains = {
    "label": "Demi-nains",
    "force": 0,
    "constitution": 1,
    "dexterite": -1,
    "intelligence": 0,
    "sagesse": 0,
    "charisme": 0,
    "tenacite": 1,
    "bonus_don": 1,
    "bonus_Pts_Caracs": 2,
    "traits_racial": humains.traits_racial.concat(nains.traits_racial),
    "t": "bonus de 2 dans une caractéristique au choix (compté dans le nombre de points à attribuer)"
};

const demiOrques = {
    "label": "Demi-orques",
    "force": 1,
    "constitution": 0,
    "dexterite": 0,
    "intelligence": -1,
    "sagesse": 0,
    "charisme": 0,
    "bonus_don": 1,
    "bonus_Pts_Caracs": 2,
    "traits_racial": humains.traits_racial.concat(orques.traits_racial),
    "t": "bonus de 2 dans une caractéristique au choix (compté dans le nombre de points à attribuer)"
};

const vatholas = {
    "label": "Vatholas",
    "force": 0,
    "constitution": 0,
    "dexterite": 1,
    "intelligence": 1,
    "sagesse": 0,
    "charisme": -1,
    "perception": 2,
    "bonus_don": 1,
    "traits_racial": [
    {
        "label": "Agile",
        "description": "Dotés d’un corps généralement fin et vif, les vatholas reçoivent un bonus de 2 en acrobatie.",
        "bonus": {
            "acrobatie": 2
        }
    },
    {
        "label": "Charge",
        "description": "Le personnage dispose d’une corne ou de bois, ce qui rend sa charge bien plus violente que la normale. Il reçoit un bonus de 1D de dégâts lorsqu’il effectue cette action.",
        "bonus": {

        }
    },
    {
        "label": "Discrétion",
        "description": "Chasseurs dans l’âme et habitués à ne pas se faire remarquer, les vatholas reçoivent un bonus à la discrétion de 2.",
        "bonus": {
            "discretion": 2
        }
    },
    {
        "label": "Empoisonneur",
        "description": "Les vatholas ont pour habitude de recouvrir leur lame ou leurs flèches de poison, qu’ils confectionnent eux-mêmes à base de plantes. Ils reçoivent un bonus de 4 en artisanat d’herboristerie et d’alchimie.",
        "bonus": {

        }
    },
    {
        "label": "Endurcis",
        "description": "Les enfants de Kafshëve sont des survivants, confrontés à la dureté du monde depuis leur naissance. Ils obtiennent un bonus à la volonté de 2.",
        "bonus": {
            "volonte": 2
        }
    }, 
    {
        "label": "Esprit de la meute",
        "description": "Les instincts animaux des vatholas influencent encore leur comportement. Lorsqu’un allié (hors invocation) attaque une cible que le personnage a attaquée durant ce tour, il bénéficie d’un bonus de 1 aux dégâts, cumulatif pour chaque autre allié ayant également attaqué cette cible ce tour.",
        "bonus": {
            
        }
    }, 
    {
        "label": "Grâce de Kafshëve",
        "description": "Le personnage semble particulièrement divertir la déesse des monstres, qui à l’air de lui avoir accordé quelques particularités spéciales. Celui-ci possède des caractéristiques qui tiennent plus de la bête que de l’humain, ce qui lui permet de se transformer entièrement en animal. La capacité fonctionne comme le don change-forme, à la différence que l’animal devra correspondre à votre forme (un vatholas avec des bois se dirigera plutôt vers un cervidé) et que celle-ci évoluera avec le personnage. Il faudra voir avec le MJ pour les modifications concernées, en temps voulu.",
        "bonus": {

        }
    },
    {
        "label": "Homme-poisson",
        "description": "Le personnage possède des branchies au niveau de son cou lui permettant de respirer sous l’eau et ne subit pas les malus lors de combats subaquatiques.",
        "bonus": {

        }
    },
    {
        "label": "Instinct",
        "description": "Armés de leurs griffes, les dégâts des attaques à mains nues obtiennent un bonus de deux classes de dé.",
        "bonus" : {
             "type_corpsacorps" : {
                "bonus_classe_D": 4
                }
            }
        },
    {
        "label": "Lien animal",
        "description": "Les vatholas, d’une nature animale certaine, comprennent les animaux mieux que quiconque. Ils gagnent un bonus au dressage de 2.",
        "bonus": {
            "dressage": 2
        }
    },    
    {
        "label": "Sens accrus",
        "description": "Leurs origines animales aiguisent leurs sens. Les vatholas obtiennent un bonus à la perception de 2.",
        "bonus" : {
            "perception": 2
          }
    },
    {
        "label": "Sprinteur",
        "description": "La course, qu’elle soit pour s’échapper ou traquer est un impératif chez certains individus. Ils gagnent donc un bonus à l’athlétisme de 1, ainsi qu’une case de déplacement supplémentaire par tour.",
        "bonus": {
            "deplacement": 1,
            "athletisme": 1

        }
    },
    {
        "label": "Vie en nature",
        "description": "Les vatholas sont des individus proches de la nature. Et en conséquent, ils gagnent un bonus de 2 en survie.",
        "bonus": {
            "survie": 2
        }
    },
    {
        "label": "Vision nocturne",
        "description": "Grâce à leur nature animale les vatholas peuvent voir dans l’obscurité naturelle aussi bien que s’ils étaient à la lumière du jour.",
        "bonus": {

        }
    }
    ],
    "t": ""
};

const landares = {
    "label": "Landarès",
    "force": -1,
    "constitution": -1,
    "dexterite": 0,
    "intelligence": 2,
    "sagesse": 2,
    "charisme": 0,
    "traits_racial": [
    {
        "label": "Air pur",
        "description": "Tout comme les plantes, mais de manière plus optimale et rapide, les landarès peuvent emmagasiner les substances néfastes de l’air (fumée, poison volatil, diverses spores, etc.) et s’en servir pour produire un air saint autour d’elles. Une landarès utilisant cette capacité perdra 2 points de vie par tour passé dans une zone dont l’air est irrespirable et produira de l’oxygène pur dans une zone de type 1 autour d’elle. Elle peut augmenter cette zone, pour la passer en zone de type 2, mais perdra 4 points de vie par tour.",
        "bonus": {

        }
    },
    {
        "label": "Connexion végétale",
        "description": "Les landarès sont capables de dresser les plantes monstrueuses qui ne sont ordinairement pas amicales. De plus, elles gagnent un bonus de 1 au dressage.",
        "bonus": {
            "dressage": 1
        }
    },
    {
        "label": "Empoisonneuse",
        "description": "Les landarès peuvent produire du poison et peuvent ainsi en recouvrir des armes afin d’infecter les ennemis qui rateraient un jet de ténacité ({{2}} de dégâts de poison). Celui-ci reste sur les lames durant {{2}} tours.",
        "bonus": {

        }
    },
    {
        "label": "Épines protectrices",
        "description": "Lorsque les landarès subissent une attaque au corps à corps, elles peuvent projeter des épines acérées, infligeant 1D4 dégâts + leur niveau à tous les assaillants situés dans une case autour d'elles. Cette capacité passive peut se déclencher une fois par tour au maximum.",
        "bonus": {
            
        }
    },
    {
        "label": "Lierre",
        "description": "Les landarès sont capables de produire du lierre, directement depuis la paume de leurs mains, qui agrippe les surfaces et offre un bonus aux jets de manœuvres de 2.",
        "bonus": {
            "manoeuvres": 2
        }
    },
    {
        "label": "Odeur captivante",
        "description": "Une fois par combat, une landarès peut produire des spores véhiculant une délicieuse odeur jusqu’à sa cible. Le jet sera automatiquement réussi, en prenant comme marge le score en charisme de la landarès. À cela, l’ennemi aura le droit à un jet de volonté opposé, s’il échoue, il passera dans l’état fasciné.",
        "bonus": {

        }
    },
    {
        "label": "Peau d’écorce",
        "description": "Des individus, plus affiliés aux arbres qu’aux petites plantes, voient leur corps recouvert d’une fine écorce qui augmente leur RD de 2.",
        "bonus": {
            "RD": 1
        }
    },
    {
        "label": "Photosynthèse",
        "description": "Les landarès, en tant que plantes, adorent le soleil et profitent avec plaisir de ses bienfaits. Elles peuvent régénérer {{1}} point(s) de vie ou de mana par heure passée au soleil (peut se combiner à la méditation ou le sommeil).",
        "bonus": {

        }
    },
    {
        "label": "Piquante",
        "description": "Le corps de certaines landarès est capable de se hérisser d’épines. En l’occurrence, cela inflige des dégâts à toute personne attaquant une landarès par contact direct, pour un total de {{2}} dégâts.",
        "bonus": {

        }
    },
    {
        "label": "Plante animée",
        "description": "Les landarès, humanoïdes animées par la magie, sont de par nature liées au mana. Et de ce fait, elles gagnent un bonus de 2 à la RM.",
        "bonus": {
            "RM": 2
        }
    },
    {
        "label": "Plante carnivore",
        "description": "Le personnage à la capacité de digérer et d’assimiler un repas de viande. Et lorsque cela se produit, les substances nutritives apportées par le repas vont accorder un bonus de 1D de dégât pendant 8 heures.",
        "bonus": {

        }
    },
    {
        "label": "Sève curative",
        "description": "Il arrive que leur sève ait des propriétés médicinales étonnantes. La plante subit 2 points de dégâts pour l’entaille nécessaire à la production du liquide, pour ensuite l’appliquer sur une plaie, aidant à la cicatrisation de celle-ci. Une seule application par plaie, pour 1D6 + {{1}} points de vie rendus.",
        "bonus": {

        }
    },
    {
        "label": "Sève occulte",
        "description": "Les landarès, en tant que créatures magiques, obtiennent automatiquement le don perception magique, ainsi qu’un bonus à l’incantation de 1.",
        "bonus": {
            "dons": ["perception_magique"],
            "incantation": 1
        }
    },
    {
        "label": "Spores fongiques",
        "description": "Une fois par combat, une landarès peut faire graviter des spores similaires à des miasmes. Le jet sera forcément réussi, en prenant comme marge de réussite le score en constitution de la landarès. À cela, l’ennemi aura le droit à un jet de ténacité opposé, s’il échoue, il passera dans l’état nauséeux.",
        "bonus": {

        }
    },
    {
        "label": "Stapélia",
        "description": "Les landarès évoluant dans les lieux arides sont habituées aux fortes chaleurs. Elles gagnent un bonus de 1 à la ténacité, ainsi qu’à la survie.",
        "bonus": {
            "tenacite": 1,
            "survie": 1
        }
    }
    ],
    "dons": ["dialogue_vegetal"],
    "t": "jets d’entraves physiques ou magiques +2, besoin que d’eau et de soleil pour survivre, faiblesse naturelle de 4 contre les dégâts de feu, résistance aux maladies et au poison<br>"
};

const naskagarris = {
    "force": 0,
    "constitution": 0,
    "dexterite": 0,
    "intelligence": 0,
    "sagesse": 0,
    "charisme": -2,
    "perception": 2,
    "bonus_don_racial": 1,
    "traits_racial": [
    {
        "label": "Caméléon",
        "description": "La peau de certains reptiles est capable de changer de couleur selon les humeurs ou de l’environnement. Ils gagnent un bonus à la discrétion de 2.",
        "bonus": {
            "discretion": 2
        }
    },
    {
        "label": "Cuir épais",
        "description": "Certains naskagarris ont un cuir qui les protège plus efficacement, leur RD augmente alors de 2.",
        "bonus": {
            "RD": 1
        }
    },
    {
        "label": "Écholocalisation",
        "description": "Permet de percevoir son environnement à l’aide d’ultra-sons, ne donnant pas de malus dans la pénombre (même magique), la fumée ou le brouillard. Mais le personnage possède une vision primitive ne pouvant discerner les couleurs ou des formes précises.",
        "bonus": {

        }
    },
    {
        "label": "Odorat supérieur",
        "description": "Rien que par l’odeur, le personnage peut déterminer l’humeur d’une cible et sa condition de santé. Il est désormais possible de suivre une piste grâce à l’odeur. Le nez permet également de détecter toute présence (mais pas son emplacement exact) dans un rayon de 10 mètres. Attention aux fortes odeurs qui masqueront les informations.",
        "bonus": {

        }
    },
    {
        "label": "Retomber sur ses pattes",
        "description": "Certaines espèces ont des jambes faites pour le saut, réduisant les dégâts de chute de moitié et octroyant un bonus de 1 à l’acrobatie.",
        "bonus": {
            "acrobatie": 1
        }
    },
    {
        "label": "Rugissement",
        "description": "Votre cri bestial est on ne peut plus impressionnant. Il offre un bonus à l’intimidation de 2.",
        "bonus": {
            "intimidation": 2
        }
    },
    {
        "label": "Sixième sens",
        "description": "Il est dit que les animaux ont la capacité de voir le monde de l’invisible. Le personnage gagne le don vision éthérique.",
        "bonus": {
            "dons": ["vision_etherique"]
        }
    },
    {
        "label": "Toile",
        "description": "Certaines espèces d’insectes sont capables de produire des toiles à une vitesse impressionnante. Dans une zone de type 1 et sur une portée de 5 mètres, le personnage peut tisser une toile presque invisible sur une surface, collant tous ceux qui passeront dessus. Ils devront réussir un jet d’acrobatie pour s’en extirper.",
        "bonus": {

        }
    },
    {
        "label": "Venimeux",
        "description": "Certains naskagarris possèdent des glandes, des dards ou tout simplement une peau sécrétant du venin. Ils peuvent ainsi l’appliquer sur des lames ou des flèches, infligeant {{2}} dégâts par tour, jusqu’à ce que le corps de la victime ait réussi à purger le poison.",
        "bonus": {

        }
    },
    {
        "label": "Vision à 360°",
        "description": "Les insectes et certains reptiles sont capables de bouger leurs yeux indépendamment de leur tête et peuvent ainsi observer les alentours sans forcément attirer l’attentione. Mais surtout, les attaques subies dans le dos n’infligent plus de malus.",
        "bonus": {

        }
    },
    {
        "label": "Vision thermique",
        "description": "Il y a des types d’yeux ou de langues qui permettent de voir l’environnement par niveaux de chaleur. Les êtres vivants, ou les sources de chaleur apparaissent sous des couleurs chaudes et à l’inverse, les zones froides se présentent par des couleurs froides.",
        "bonus": {

        }
    },
    {
        "label": "Vol",
        "description": "Tous les naskagarris ayant des ailes ne possèdent pas systématiquement la capacité de voler. Le personnage, lui, jouit de ses pleines capacités. Voir les détails du vol dans le chapitre des déplacements",
        "bonus": {

        },
        "flight": {

        },
    }
    ].concat(vatholas.traits_racial),
    "dons": ["dialogue_animal"],
    "t": ""
};

const naskagarrisPetit = {
    "label": "Naskagarris de petite carrure",
    "force": -2,
    "dexterite": 2,
    "intelligence": 2,
    "bonus_Pts_Caracs": 1,
    "t": "Ils reçoivent 1 point à distribuer entre la dextérité et l’intelligence (compté dans le nombre de points à attribuer)"
};

const naskagarrisMoyen = {
    "label": "Naskagarris de carrure moyenne",
    "bonus_Pts_Caracs": 3,
    "t": "Ils reçoivent 5 points à distribuer entre trois caractéristiques au choix, avec un maximum de 2, <br> Ils subissent un malus de 2 dans une caractéristique au choix <br>(compté dans le nombre de points à attribuer)"
};

const naskagarrisGrand = {
    "label": "Naskagarris de grande carrure",
    "intelligence": -2,
    "force": 2,
    "constitution": 2,
    "bonus_Pts_Caracs": 1,
    "t": "Ils reçoivent 1 point à distribuer entre la force et la constitution (compté dans le nombre de points à attribuer)"
};

const demiElementaires = {
    "force": 0,
    "constitution": 0,
    "dexterite": 0,
    "intelligence": 0,
    "sagesse": -2,
    "charisme": 0,
    "bonus_don": 1,
    "bonus_Pts_Caracs": 2,
    "traits_racial": [
    {
        "label": "Affinités",
        "description": "Si le personnage manipule la magie élémentaire, il peut cumuler deux invocations sur le terrain. Cette capacité est cumulable avec le don âmes liées.",
        "bonus": {

        }
    },
    {
        "label": "Proximité de l’élément",
        "description": "Le personnage, en raison de ses origines élémentaires et magiques, obtient automatiquement le don «mana pur» et gagne un bonus de 1 à l’incantation.",
        "bonus": {
            "dons": ["mana_pur"],
            "incantation": 1
        }
    },
    {
        "label": "Enchantement",
        "description": "L’élément transpire du corps d’un demi-élémentaire et contre deux points de mana, il peut enchanter une arme pour que celle-ci inflige des dégâts magiques liés à son élément durant {{1}} tour(s).",
        "bonus": {

        }
    },
    {
        "label": "Familier élémentaire",
        "description": "Le personnage peut soumettre les élémentaires et commence l’aventure avec un familier du même élément et d’un Fp égal au niveau du personnage.",
        "bonus": {

        }
    },
    {
        "label": "Magie fondamentale",
        "description": "Les demi-élémentaires sont très liés à la magie. Ils possèdent une source d’énergie plus considérable que les autres races et gagnent 2 points de mana supplémentaires par niveau.",
        "bonus": {
            "pm_par_niveaux": 2
        }
    },    
    {
        "label": "Résistance primitive",
        "description": "Toutes les 3 attaques subies et liées à un élément, naturel ou magique (de feu, d’eau, de terre, d’air, d’électricité, ou de glace), le personnage gagne 2 de RM jusqu’à la fin du combat.",
        "bonus": {

        }
    },
    {
        "label": "Sang élémentaire",
        "description": "Le demi-élémentaire peut doubler l’effet, ou le temps d’un sort, si celui-ci est du même élément que lui. Il sera possible d’effectuer cette action {{1}} fois par jour.",
        "bonus": {

        }
    },
    {
        "label": "Solitaire",
        "description": "Les demi-élémentaires vivent souvent en pleine nature, au contact direct de leur élément. Ceux-ci sont donc plus habitués à se débrouiller à l’extérieur. Ils gagnent un bonus de 2 sur deux types d’artisanats, ainsi qu’un bonus à la survie de 1.",
        "bonus": {
            "survie": 1
        },
        "choix": [{
            "type": "Radio", 
            "list": { 
                "forgeron": 2, 
                "tanneur": 2,
                "alchimiste": 2,
                "tailleur": 2,
                "faconneur": 2
            }
        },{
            "type": "Radio", 
            "list": { 
                "forgeron": 2, 
                "tanneur": 2,
                "alchimiste": 2,
                "tailleur": 2,
                "faconneur": 2
            }
        }]
    },
    {
        "label": "Suffisance",
        "description": "Les demi-élémentaires sont des êtres arrogants, fiers de leur parenté avec les forces primaires du monde. Ils gagnent un bonus de 2 à la volonté.",
        "bonus": {
            "volonte": 2
        }
    }
    ],
    "t": "<br>+1 dans deux caractéristiques au choix (compté dans le nombre de points à attribuer)"
};

const demiElementairesEau = {
    "label": "Demi-élémentaires d’eau",
    "sagesse": 3,
    "volonte": 2,
    "constitution": -2,
    "RM": 1,
    "traits_racial" : [
    {
        "label": "Amphibie",
        "description": "Les élémentaires d’eau n’ont aucun mal à respirer sous l’eau et n’ont d’ailleurs aucun malus quand ils évoluent dans leur élément.",
        "bonus": {

        }
    },
    {
        "label": "Eau potable",
        "description": "Les élémentaires d’eau peuvent tout bonnement produire 1D4 litres d’eau par jour. L’eau est parfaitement potable et permet aux aventuriers de toujours rester hydratés.",
        "bonus": {

        }
    },
    {
        "label": "Immersion",
        "description": "Lorsqu’un demi-élémentaire d’eau peut plonger au moins les ¼ de son corps dans de l’eau, celui-ci régénéra {{1}} point(s) de vie par tour.",
        "bonus": {

        }
    },
    {
        "label": "Marcher sur l’eau",
        "description": "Les élémentaires d’eau peuvent choisir de marcher sur l’eau plutôt que de plonger dedans, ils y seront aussi stables que sur la terre ferme.",
        "bonus": {

        }
    },
    {
        "label": "Propulsion aquatique",
        "description": "Les élémentaires d’eau n’ont aucun mal à se déplacer dans leur élément. Sous l’eau, ils se déplacent trois fois plus vite qu’à la normale et peuvent faire des bonds de plusieurs mètres hors de l’eau.",
        "bonus": {

        }
    }
    ],
}

const demiElementairesFeu = {
    "label": "Demi-élémentaires de feu",
    "force": 2,
    "intimidation": 2,
    "intelligence": -1,
    "traits_racial" : [
    {
        "label": "Ardeur",
        "description": "Les élémentaires de feu aiment se battre, donnant tout au combat. Ils obtiennent un bonus aux dégâts de 1D.",
        "bonus": {
            "bonus_D_dmg": 1
        }
    },
    {
        "label": "Atmosphère ardente",
        "description": "Les élémentaires de feu aiment se battre dans les endroits propices à la chaleur. Si la température du lieu de combat excède 25 degrés, le personnage augmentera ses chances de critique de 5.",
        "bonus": {

        }
    },
    {
        "label": "Chaleur tonifiante",
        "description": "La peau de certains élémentaires de feu demeure naturellement bouillante. Cela a pour effet de les rendre insensibles aux dégâts de chaleur et leur permet également de ne pas subir les effets du froid.",
        "bonus": {

        }
    },
    {
        "label": "Esprit brûlant",
        "description": "Les élémentaires de feu ne rateront jamais une occasion de se battre et faire éclater leur fougue, ils gagnent un bonus à l’initiative de 2.",
        "bonus": {
            "bonus_initiative": 2
        }
    },
    {
        "label": "Torche humaine",
        "description": "Les élémentaires de feu peuvent embraser leur chevelure et éclairer la zone comme s’il s’agissait d’une torche, à la différence qu’elle ne produit aucune chaleur.",
        "bonus": {

        }
    }
    ],
    "t": "ls reçoivent un bonus de 5 pour résister aux dégâts de feu et de chaleur"
}
const demiElementairesAir = {
    "label": "Demi-élémentaires d’air",
    "dextérité": 2,
    "discretion": 2,
    "sagesse": -1,
    "traits_racial" : [
    {
        "label": "Lévitation mineure",
        "description": "Contre 5 points de mana, le personnage fait appelle aux vents afin de léviter. Il obtient alors un vol magique pendant son niveau en minutes.",
        "bonus": {

        }
    },
    {
        "label": "Pieds lestes",
        "description": "Les élémentaires de vent sont légers, ils gagnent un bonus à l’initiative de 2.",
        "bonus": {
            "bonus_initiative": 2
        }
    },
    {
        "label": "Poussée ventée",
        "description": "Armés de la vélocité du vent, les demi-élémentaires de vont gagne 2 cases de déplacement supplémentaires.",
        "bonus": {
            "deplacement": 2
        }
    },
    {
        "label": "Souffle distordant",
        "description": "L’élémentaire est entouré de bourrasques, ce qui a pour effet de dévier tous les projectiles physiques arrivant en sa direction.",
        "bonus": {

        }
    },
    {
        "label": "Vivacité",
        "description": "La légèreté des élémentaires leur offre un bonus de 2 à leurs tests d’acrobaties.",
        "bonus": {
            "acrobatie": 2
        }
    }
    ],
    "t": "Ils ont la capacité naturelle de planer (pas voler) pour éviter les dégâts de chute"
}
const demiElementairesElectricite = {
    "label": "Demi-élémentaires d’électricité",
    "intelligence": 2,
    "acrobatie": 2,
    "charisme": -1,
    "traits_racial" : [
    {
        "label": "Canal",
        "description": "Si l’élémentaire touche une armure ou une arme en métal, celui-ci pourra envoyer une décharge à sa cible, ajoutant {{1}} dégât(s) magique(s) à son attaque.",
        "bonus": {

        }
    },
    {
        "label": "Chaîne conductrice",
        "description": "Lorsque le personnage attaque une cible, il pourra également toucher toute entité se trouvant aux côtés de son ennemi. L’effet se répand tant que des cibles se trouvent côte à côte. À chaque personne touchée, les dégâts diminuent de moitié. Si une cible annexe réussit une esquive simple, l’effet ne se propage pas.",
        "bonus": {

        }
    },
    {
        "label": "Flash",
        "description": "Une fois par combat, il est possible d’éblouir un ennemi au contact à l’aide d’un éclair. Aucun jet ne sera nécessaire et le score en dextérité du personnage servira de marge de réussite.",
        "bonus": {

        }
    },
    {
        "label": "Impétuosité",
        "description": "La vitesse des élémentaires de foudre leur offre l’avantage sur l’ennemi, augmentant leur initiative de 2",
        "bonus": {
            "bonus_initiative": 2
        }
    },
    {
        "label": "Réactivité foudroyante",
        "description": "Le demi-élémentaire possède un influx nerveux supérieur à la moyenne, ce qui lui accorde un bonus de 2 à l’esquive.",
        "bonus": {
            "esquive": 2

        }
    },
    ],
    "t": "Ils reçoivent un bonus de 5 pour résister aux dégâts de foudre et à la paralysie"
}
const demiElementairesGlace = {
    "label": "Demi-élémentaires de glace",
    "charisme": 2,
    "incantation": 2,
    "force": -1,
    "traits_racial" : [
    {
        "label": "Cristallisation",
        "description": "Les élémentaires de glace peuvent geler l’eau d’un simple toucher et former un bloc solide à sa surface. De plus, ils peuvent marcher sur la glace sans malus.",
        "bonus": {

        }
    },
    {
        "label": "Glaçon",
        "description": "Les élémentaires de glace ont la peau froide et dure, leur RD augmente de 1.",
        "bonus": {
            "RD": 1
        }
    },
    {
        "label": "Hypothermie coutumière",
        "description": "La température corporelle du personnage avoisine les 25 degrés, ce qui le rend insensible au froid ainsi qu’aux maladies non mentales (les bactéries ne peuvent se développer dans un organisme si froid).",
        "bonus": {

        }
    },
    {
        "label": "Ralentissement",
        "description": "Les demi-élémentaires de glace produisent du froid à outrance. Le personnage est entouré d’une aura froide qui inflige un malus de 2 aux jets, à l’initiative et aux déplacements des personnes qui l’entourent.",
        "bonus": {

        }
    },
    {
        "label": "Réflexion",
        "description": "La peau brillante et presque réfléchissante des demi-élémentaires de glace n’est pas que plaisir pour l’œil. Le personnage devient insensible aux projectiles magiques, qui sont alors déviés.",
        "bonus": {

        }
    }
    ],
    "t": "Ils reçoivent un bonus de 5 pour résister aux dégâts de froid et de glace"
}
const demiElementairesTerre = {
    "label": "Demi-élémentaires de terre",
    "constitution": 2,
    "tenacite": 2,
    "dexterite": -1,
    "RD": 1,
    "traits_racial" : [
    {
        "label": "Attraction magnétique",
        "description": "Les demi-élémentaires de terre manient également le magnétisme. Contre 5 points de mana et un jet de volonté réussi, le personnage peut attirer jusqu’à lui un objet composé majoritairement de métal. Si l’objet est dans les mains d’une personne, celle-ci devra réussir un jet de force pour ne pas se la faire arracher des mains. Il est cependant impossible d’attirer des objets de plus de 8 kilos.",
        "bonus": {

        }
    },
    {
        "label": "Dureté",
        "description": "Les poings des demi-élémentaires de terres sont durs comme la roche. De ce fait, leurs attaques aux poings augmentent d’une classe de dé et d’un dé.",
        "bonus": {
             "type_corpsacorps" : {
                "bonus_D_dmg": 1,
                "bonus_classe_D": 2

                }
            }
        },
    {
        "label": "Incassable",
        "description": "Il n’est pas rare qu’un coup de l’ennemi rebondisse sur un demi-élémentaire de terre. Si le personnage subit une attaque, mais qu’il ne subit aucun dégât, celui-ci aura droit à une riposte gratuite.",
        "bonus": {

        }
    },
    {
        "label": "Muscles indolores",
        "description": "Les élémentaires de pierre ont des muscles bien plus solides que la normale et peuvent porter le double de poids sur leurs épaules.",
        "bonus": {
            "multiplicateur_charge": 2
        }
    },
    {
        "label": "Peau de pierre",
        "description": "La peau des élémentaires de pierre est composée en partie de roche, ils gagnent un bonus de 2 à la RD.",
        "bonus": {
            "RD": 1
        }
    }
    ],
}

const sylvaniens = {
    "force": 3,
    "constitution": 0,
    "dexterite": -3,
    "intelligence": 0,
    "sagesse": 2,
    "charisme": 0,
    "esquive": -3,
    "multiplicateur_pv": 2,
    "traits_racial": [
    {
        "label": "Arbre animé",
        "description": "Les sylvaniens sont naturellement liés au mana, puisque ce sont des individus animés par la magie. Cette particularité leur offre un bonus de 2 à la RM.",
        "bonus": {
            "RM": 2
        }
    },
    {
        "label": "Arbre fruitier",
        "description": "Bien qu’ils soient animés magiquement et par une volonté propre, les sylvaniens n’en restent pas moins des arbres. Certains sont tout bonnement des arbres fruitiers et peuvent, une fois par jour et de manière accélérée, générer 1D10 fruit(s) juteux et comestibles.",
        "bonus": {

        }
    },
    {
        "label": "Chêne",
        "description": "Les sylvaniens ont une force colossale et leur bonus de dégâts liés à la force est doublé.",
        "bonus": {
            "multiplicateur_bonus_force": 3
        }
    },
    {
        "label": "Cosse",
        "description": "Certaines espèces d’arbres produisent naturellement et rapidement des cosses. Cette capacité permet au sylvanien d’attaquer à distance, de son bonus de force en mètres. Une cosse infligera {{2}} dégâts et si la tête est visée, il sera possible de doubler les dégâts ou d’assommer la cible.",
        "bonus": {

        }
    },
    {
        "label": "Détermination du protecteur",
        "description": "La volonté des sylvaniens prêts à défendre leur terre est inébranlable. Ils bénéficient d’un bonus de 2 aux jets de volonté.",
        "bonus": {
            "volonte": 2
        }
    },
    {
        "label": "Écorce rigide",
        "description": "L’écorce de certains sylvaniens est plus dure et dense que d’autres, augmentant leur RD de 2.",
        "bonus": {
            "RD": 1
        }
    },
    {
        "label": "Écrasement",
        "description": "Les sylvaniens sont capables de réduire en charpie un ennemi par un simple coup. Lorsqu’un sylvanien effectue un coup critique, celui-ci inflige le triple des dégâts. En revanche, sa marge de coup critique diminue de 2. Si celle-ci passe en négatif, l’effet ne fonctionne tout simplement pas.",
        "bonus": {
            "critique": -2
        }
    },
    {
        "label": "Esprit de maturité",
        "description": "Les sylvaniens vivent longtemps et ont tout leur temps pour écouter les dires des habitants et voyageurs des forêts. De ce fait, ils sont au courant de beaucoup de choses et gagnent un bonus à l’érudition de 2",
        "bonus": {
            "erudition": 2
        }
    },
    {
        "label": "Essence magique",
        "description": "Les sylvaniens ont une vue différente des autres races. Ils perçoivent le monde en visualisant les flux magiques de toute chose. De ce fait, ils gagnent le don perception magique, qui devient permanent et gratuit.",
        "bonus": { 
            "dons": ["perception_magique"],
        }
    },
    {
        "label": "Habitant des forêts",
        "description": "Les végétaux forment leur foyer, leurs amis et voisins. Le personnage est incollable sur le sujet et réussit automatiquement ses jets d’herboristerie et de récolte.",
        "bonus": {

        }
    },
    {
        "label": "Humanisation",
        "description": "Mesurer plusieurs mètres de long n’est pas toujours facile, surtout lorsqu’il faut entrer dans des bâtiments. Des sylvaniens ont cependant réussi à remédier à ce problème. Contre 10 points de mana, un sylvanien peut prendre un tour pour passer à taille humaine. Cela entraînera diverses modifications qui nécessiteront une seconde fiche, avec des attributs identiques à un personnage de race humaine. Durant ce laps de temps, le sylvanien arborera toujours une écorce, ainsi qu’une apparence végétale, mais avec une morphologie plus humanisée. Certains traits seront conservés et d’autres modifiés :<br>"+
        "Les caractéristiques liées à la race changent pour devenir celles des humains<br>"+
        "Les sylvaniens se nourrissent toujours d’eau<br>"+
        "Ils gardent le don dialogue végétal, ainsi que les bonus de RD et de RM<br>"+
        "Les renversements et désarmements sont désormais possibles<br>"+
        "Les dégâts liés au feu restent doublés<br>"+
        "Il est désormais possible de porter des armures, de manier des armes et l’attaque de poings fait les mêmes dégâts que s’il s’agissait d’un humain.<br>"+
        "En cas de transformation, il faudra penser à gérer son équipement et le retirer, ou l’enfiler en cas de besoin.",
        "bonus": {

        }
    },    
    {
        "label": "Hurlement tonitruant",
        "description": "La taille des sylvaniens fait que leur cri est bien plus impressionnant que les races de taille moyenne. Leurs jets d’intimidation se font avec un bonus de 2.",
        "bonus": {
            "intimidation": 2
        }
    },
    {
        "label": "Photosynthèse",
        "description": "Les sylvaniens, comme toute plante, adorent le soleil et profitent avec plaisir de ses bienfaits. Les sylvaniens peuvent régénérer {{1}} point(s) de vie ou de mana par heure passée au soleil (peut se combiner à la méditation ou le sommeil).",
        "bonus": {

        }
    },
    {
        "label": "Sève curative",
        "description": "Il arrive que leur sève ait des propriétés médicinales étonnantes. La plante subit 2 points de dégâts pour l’entaille nécessaire à la production du liquide, pour ensuite l’appliquer sur une plaie, aidant à la cicatrisation de celle-ci. Une seule application par plaie, pour 1D6 + {{1}} points de vie rendus.",
        "bonus": {

        }
    }
    ],
    "restriction": ["acrobatie", "bluff", "commerce", "sabotage", "equitation"],
    "dons": ["dialogue_vegetal"],
    "t": "Les sylvaniens n’ont besoin que d’eau et de soleil pour survivre.<br> ils sont insensibles au renversement, au désarmement, aux maladies et au poison ils subissent le double des dégâts liés au feu<br>"+
    "ils ne sont soignables qu’à l’aide de sorts ou d’un équipement médical spécifique.<br> Sauf si le MJ décide du contraire, ils commencent sans argent ni équipement<br>"+
    "ils reçoivent un bonus à la discrétion de 20 en forêt, mais un malus équivalent lorsque ceux-ci se trouvent ailleurs.<br> ils sont des créatures de très grande taille."
};

const sylvaniensProtecteur = {
    "label" : "Sylvanien de l’ordre des protecteurs",
    "pv_par_niveaux" : 3,
    "RD": 1,
    "D_regen_pv": 2,
    "lvl": {
        "4": {
            "RM": 1
        },
        "7": {
            "bonus_D_dmg": 1
        }
    },
    "t": "Ils possèdent une attaque naturelle de corps-à-corps infligeant 5D3.<br>"+
    "Un échec critique qui briserait normalement l’arme, est remplacé par le fait de se faire mal soi-même<br>"+
    "Les magies et dons évolutifs exotiques, hormis belluaire, leur sont interdits<br>"+
    "Ils ne peuvent porter d’armure ou d’arme, mais peuvent les former directement à partir de leur corps (Corps-à-corps et fouets uniquement).<br>"+
    "Ainsi, ils peuvent avoir accès aux dons évolutifs classiques d’armes et armure. Les caractéristiques des armes et armures sont"+
    "identiques à l’équipement standard et peut se coupler aux dons associés, mais pas aux améliorations.<br>"
};

const sylvaniensMemoires = {
    "label" : "Sylvanien de l’ordre des mémoires",
    "pm_par_niveaux" : 3,
    "RM": 1,
    "D_regen_pm": 2,
    "dons": ["perception_magique"],
    "lvl": {
        "4": {
            "crit_incantation": 1
        }
    },
    "t": "Ils possèdent une attaque naturelle de corps-à-corps infligeant 4D3.<br>"+
    "Ils reçoivent un bonus de 1D à leurs sorts au niveau 7.<br>"+
    "Ils ne peuvent porter ni arme, ni armure<br>"+
    "Ils ont accès aux dons évolutifs exotiques et aux magies, mais pas aux dons classiques<br>"
};

const eletavirs = {
    "label": "Eletavirs",
    "force": 0,
    "constitution": -1,
    "dexterite": 2,
    "intelligence": 0,
    "sagesse": -1,
    "charisme": 2,
    "eloquence": 2,
    "traits_racial": humains.traits_racial.concat([
    {
        "label": "Acclimatation",
        "description": "À force de vivre au milieu de peuples aux modes de vie diurnes, certains eletavirs ont vu leur sensibilité à la lumière diminuer. La sensibilité à la lumière n’est plus qu’un lointain souvenir.",
        "bonus": {

        }
    },
    {
        "label": "Délice",
        "description": "L’effet de régénération du don «Soif de sang» est quadruplé. De plus, il est désormais possible de voler le fluide vital d’une cible en combat, grâce à un jet de corps-à-corps opposé à celui de la cible..",
        "bonus": {

        }
    },
    {
        "label": "Environnement hostile",
        "description": "Les eletavirs sont régulièrement contraints de fuir lorsque leur vraie nature est découverte. Ils gagnent un bonus à l’athlétisme de 1, ainsi qu’un bonus de 1 à leur vitesse de déplacement.",
        "bonus": {
            "deplacement": 1,
            "athletisme": 1
        }
    },
    {
        "label": "Forme de voyage",
        "description": "À l’instar des vampires, certains eletavirs sont capables de se transformer en chauves-souris. Cette forme ne permet pas d’attaquer et à la moindre source de dégâts, l’effet prend fin. Cependant, lorsqu’il se transforme, le demi-vampire à la capacité de voler et surtout, n’éveillera guère les soupçons si jamais il écoute une scène, pendu à une poutre d’un plafond. L’effet n’a pas de durée maximale et le personnage peut reprendre sa forme initiale quand il le souhaite. La transformation n’affecte pas l’équipement du personnage, car l’activation de cette capacité coûte 2 points de mana, permettant, en outre, d’épargner ses vêtements et ses objets.",
        "bonus": {

        }
    },
    {
        "label": "Goût du sang",
        "description": "Tout eletavir qui se respecte dira que tous les sangs n’ont pas le même goût. Lorsqu’un eletavir se nourrit, il faudra tirer 1D6, qui représentera les 6 différentes caractéristiques. Durant 24 heures le personnage gagnera un bonus de 2 aux jets liés à l’une des caractéristiques. Si jamais le demi-vampire se nourrit plusieurs fois sur une même personne, on renouvellera simplement l’effet. À rappeler que les eletavirs se nourrissent une à deux fois par semaine, trop se nourrir pourrait les rendre malades.",
        "bonus": {

        }
    },
    {
        "label": "Pouvoirs nocturnes",
        "description": "Les vampires sont connus pour évoluer la nuit et bien que les eletavirs ne soient que des demis, la nuit les rend plus forts. Ils gagnent {{1}} dégâts en cas de combat nocturne et le bonus de dégâts liés à la force est doublé.",
        "bonus": {

        },
         "daylight": {
            "night": {
                "bonus_dmg_par_niveaux" : 1,
                "multiplicateur_bonus_force" : 2
            }
        }
    },
    {
        "label": "Renouvellement sanguin",
        "description": "Se nourrir exclusivement de sang n’a pas que des désavantages. En effet, à chaque repas, le demi-vampire régénère 1 point de vie + {{2}}. À rappeler que les eletavirs se nourrissent une à deux fois par semaine, trop se nourrir pourrait les rendre malades.",
        "bonus": {

        }
    },
    {
        "label": "Régénération crépusculaire",
        "description": "Lorsque le personnage se situe dans une zone dénuée de lumière ou qu’il évolue durant la nuit, celui-ci voit ses capacités de régénération resurgir. À chaque début de tour, un eletavir se soignera de {{1}} point(s) de vie. L’effet fonctionne aussi hors combat et permet au demi-vampire de se soigner seul sur une très courte durée. Si le personnage vient à tomber dans le coma, il récupère des PVT à la place, mais ne peut pas se stabiliser.",
        "bonus": {

        }
    },
        {
        "label": "Semi-vivant",
        "description": "Les demi-vampires, ne connaissant ni fatigue ni véritable épuisement, ont appris à tirer parti de leur endurance surnaturelle et gagnent donc un bonus de 2 en ténacité.",
        "bonus": {
            "tenacite": 2
        }
    },
    ]),
    "dons": ["soif_de_sang","vision_nocturne"],
    "daylight": {
        "day": {
            "force": -2,
            "constitution": -2
        },
        "night": {
            "force": 2,
            "constitution": 2
        }
    },
    "t": "dmg lumière *2, téna -4 quand aveuglement, éviter la trop forte exposition sinon 1d6 dégâts par demi heure"+
    " Doivent boire du sang d’humanoïdes entre 1 et 2 fois par semaine"+
    " Bonus de 6 en ténacité contre le froid"+
    " Lorsque la vraie nature d’un eletavir est découverte, celui-ci sera traité comme un monstre par tous"
};

const gobelins = {
    "label": "Gobelins",
    "force": -3,
    "constitution": -2,
    "dexterite": 4,
    "intelligence": 0,
    "sagesse": 2,
    "charisme": 0,
    "discretion": 2,
    "sabotage": 1,
    "bluff": 1,
    "bonus_langue": -2,
    "langues": ["gobelin"],
    "traits_racial": [
    {
        "label": "Armé pour survivre",
        "description": "Les oreilles et les yeux si visibles des gobelins ne sont pas qu’un inconvénient à la discrétion. Ils leur permettent une meilleure perception de leur environnement. Celle-ci augmente de 2.",
        "bonus": {
            "perception": 2
        }
    },
    {
        "label": "Chevaucheurs",
        "description": "De par leur petite taille, il est difficile pour les gobelins de traverser de grandes distances. Pour pallier ce problème, il leur fallut apprendre à dresser et chevaucher des créatures. Les gros chiens sont très appréciés des gobelins de par leur puissance et leur docilité. Les compétences d’équitation et de dressage du personnage augmentent de 1.",
        "bonus": {
            "equitation": 1,
            "dressage": 1
        }
    },
    {
        "label": "Comme l’éclair",
        "description": "Lors d’un désengagement avec un ennemi, le personnage ne subit aucune attaque d’opportunité.",
        "bonus": {

        }
    },
    {
        "label": "Coup bas",
        "description": "Petits, les gobelins ont dû apprendre à se battre contre plus grand qu’eux. Leur marge de coups critique augmente de 1 sur leur maîtrise d’arme, contre les cibles plus grandes qu’eux.",
        "bonus": {

        }
    },
    {
        "label": "Découpeur",
        "description": "Les gobelins manient les dagues et autres couteaux depuis des décennies et cet héritage de techniques se transmet au fil du temps. Les gobelins gagnent un bonus de 2 au maniement des couteaux.",
        "bonus": {
            "couteaux": 2
        }
    },
    {
        "label": "Fourbe",
        "description": "Les gobelins ne sont pas réputés pour leur fair-play. C’est naturellement qu’ils gagnent un bonus de 1D et une classe de dé sur les attaques sournoises.",
        "bonus": {

        }
    },
    {
        "label": "L’ami d’un ami",
        "description": "Les gobelins sont nombreux, très nombreux. Et un gobelin peut toujours compter sur ses fourmillants congénères. Une fois par scénario, le joueur peut demander l’aide d’un PNJ, qui pourra venir en aide (dans la limite de la logique) au personnage.",
        "bonus": {

        }
    },
    {
        "label": "Nez fin",
        "description": "Lors de lancers de dés de chance, les gobelins ont un bonus de 40 à ceux-ci, pour la découverte de butins.",
        "bonus": {

        }
    },    
    {
        "label": "Pillard",
        "description": "L’or, c’est toute leur vie, avec la viande ! Ils ne peuvent pas s’en passer. À la création du personnage, les gobelins reçoivent un bonus de 20 po.",
        "bonus": {

        }
    },
    {
        "label": "Preums",
        "description": "Rapides, les gobelins ont toujours un coup d’avance. Ils gagnent une case de déplacement supplémentaire par tour et un bonus de 1 à leurs jets d’initiative.",
        "bonus": {
            "deplacement": 1,
            "bonus_initiative": 1
        }
    },
    {
        "label": "Profiteur",
        "description": "Certains gobelins ont vécu près des villes et cités modernes et se sont adaptés. Ils se nourrissent des déchets et divers restes des populations, ainsi que des animaux domestiques égarés. Leur compétence de perception est augmentée de 2. La survie peut également être utilisée en ville afin de se nourrir.",
        "bonus": {
            "survie": 2
        }
    },
    {
        "label": "Requin",
        "description": "Les gobelins excellent également dans les transactions, où leur habileté à négocier sans scrupule fait merveille. Ils gagnent donc un bonus de 2 au commerce.",
        "bonus": {
            "commerce": 2
        }
    }, 
    {
        "label": "Résistant",
        "description": "Les gobelins sont habitués à devoir survivre dans les milieux les plus insalubres, mais aussi au gré du temps et des températures. Leur compétence de ténacité augmente de 2.",
        "bonus": {
            "tenacite": 2
        }
    },
    {
        "label": "Vautour",
        "description": "Les estomacs des gobelins sont robustes et leur permet de digérer à peu près tout et n’importe quoi. Manger de la viande avariées ne les rendra pas malade, tout aliment périmé ne les affectera pas, tout comme l’eau croupie. D’ailleurs, les poisons bénins et normaux à ingérer n’ont aucun effet sur eux.",
        "bonus": {

        }
    }
    ],
    "t": "créatures de taille petite "+
    " Si le gobelin est sujet à la faim, ou se trouve fasse à un objet de grande valeur, il faudra réussir un test de volonté pour ne pas qu’il cherche à assouvir sa pulsion par tous les moyens à sa portée."+
    " La présence d’un gobelin est tolérée, mais sous deux conditions : Il doit toujours être accompagné par un membre d’une race dite intelligente et civilisée et ne pas dépasser un regroupement de plus de trois gobelins. Si ces deux conditions ne sont pas respectées, les PNJ seront automatiquement hostiles envers le ou les gobelins."
};

const satyres = {
    "label": "Satyres",
    "force": 0,
    "constitution": 0,
    "dexterite": 0,
    "intelligence": 2,
    "sagesse": -2,
    "charisme": 1,
    "eloquence": 2,
    "bluff": 2,
    "bonus_langue": -2,
    "representation": 2,
    "langues": ["druidique","sylvestre"],
    "traits_racial": [
    {
        "label": "Amadoueur",
        "description": "Le baratinage est monnaie courante chez les satyres. Leur langue de bois leur offre un bonus de 1 à l’éloquence et au bluff.",
        "bonus": {
            "bluff": 1,
            "eloquence": 1
        }
    },
    {
        "label": "Astrologue",
        "description": "Les satyres ont pris l’habitude de lire l’avenir au travers des étoiles selon leur positionnement. Une fois par séance, le joueur peut demander au MJ ce qui va advenir dans le futur sur un sujet. Il faudra alors réussir un jet de sagesse. Plus la question est précise, plus elle subira de malus. Par exemple, « quel temps fera-t-il demain ? » se fera sans malus. Alors que « Est-ce que la nuit prochaine, nous allons subir une attaque de bandits ? » se fera avec un malus de 6.",
        "bonus": {

        }
    },
    {
        "label": "Chevrette",
        "description": "Les satyres disposent de sabots semblables à ceux des chèvres et de puissantes pattes pour arpenter tous les types de terrains. Les satyres gagnent un bonus de 2 en athlétisme.",
        "bonus": {
            "athletisme": 2
        }
    },
    {
        "label": "Coup de cornes",
        "description": "Les satyres sont des fonceurs dans l'âme. La charge ne possède plus de restriction de distance ou de jet et inflige 1D14 dégâts au lieu de 1D8.",
        "bonus": {

        }
    },
    {
        "label": "Course à pied",
        "description": "Les satyres galopent vite et gagnent donc un bonus de 1 aux déplacements, ainsi qu’à l’initiative.",
        "bonus": {
            "bonus_initiative": 1,
            "deplacement": 1
        }
    },
    {
        "label": "Désinhibition",
        "description": "Les satyres ont tout vu, tout testé, surtout en ce qui concerne l’alcool, la drogue et toute autre activité euphorisante. Leur corps s’y est habitué et ils gagnent un bonus de 2 à la ténacité.",
        "bonus": {
            "tenacite": 2,
        }
    },
    {
        "label": "Féeriques",
        "description": "Les satyres sont (même si leur physique semble démontrer le contraire) de la famille des fées. C’est d’ailleurs de là qu’ils tiennent leur espièglerie. Mais surtout, cela fait d’eux des créatures magiques qui gagnent un bonus à l’incantation de 1, et reçoivent le don perception magique.",
        "bonus": {
            "dons": ["perception_magique"],
            "incantation": 1
        }
    },
    {
        "label": "Gredin",
        "description": "Le satyre peut utiliser sa flûte une fois par jour, afin de se rendre invisible (Mais pas silencieux), pendant {{1}} minute(s).",
        "bonus": {

        }
    },
    {
        "label": "Irrésistible",
        "description": "Le personnage voit son don flûte de Pan s’améliorer. Lorsque celui-ci envoûte une cible, il peut faire tomber une personne amoureuse de lui (bonus de 6 en social avec cette cible et sera peu sujette à contredire le satyre).",
        "bonus": {

        }
    },
    {
        "label": "Multifonctions",
        "description": "Lorsque le personnage réalise un coup critique, celui-ci peut utiliser sa flûte pour motiver (Utilisable en combat mais seulement sur quelqu’un d’autre). Il peut, au choix : <br>"+
        "• Donner un bonus de {{1}} à la prochaine action<br>"+
        "• Augmenter les dégâts de la prochaine attaque de 1D<br>"+
        "• Donner un bonus de {{1}} cases de déplacement au prochain tour<br>"+
        "• Soigner de {{1}}pv",
        "bonus": {

        }
    },
    {
        "label": "Musicien",
        "description": "Les satyres savent jouer de la flûte et danser presque aussi naturellement qu’ils ne respirent. Ils gagnent un bonus de représentation de 4.",
        "bonus": {
            "representation": 4
        }
    },
    {
        "label": "Polyvalent",
        "description": "Le personnage voit son don flûte de Pan s’améliorer, le satyre peut désormais utiliser ce don en combat.",
        "bonus": {

        }
    },
    {
        "label": "Rose sauvage",
        "description": "Les satyres ont plus d’un tour dans leur sac pour arriver à leurs fins. Contre 2 points de mana, ils peuvent faire pousser une rose, dont le parfum enivrant fera rendra la cible plus amicale envers le personnage. Il faudra cependant ne pas offrir cette fleur n’importe comment ou lors de cas inadaptés.",
        "bonus": {

        }
    },
    {
        "label": "Sacripant",
        "description": "Le personnage voit son don flûte de Pan s’améliorer, le satyre peut désormais retirer un des cinq sens de sa cible, pendant {{1}} en heures.",
        "bonus": {

        }
    }
    ],
    "dons": ["flute_de_pan"],
    "t": "Ils se servent de leur instrument et peuvent ainsi charmer, effrayer, ou endormir un humanoïde pendant {{1}} minute. Il sera possible d’utiliser ce don {{1}} fois, mais uniquement hors combat. Un jet de représentation, opposé à la volonté de la cible sera nécessaire."+
    " • – Les satyres devront réussir un test de volonté, pour ne pas céder aux diverses sources de distraction qui les entourent (Beaux PNJ, alcool, fêtes, représentations artistiques, etc.)"+
    " • – Si un satyre passe plus d’une heure à s’ennuyer, le joueur devra jouer son personnage, comme s’il jouait un enfant turbulent, refusant d’écouter son groupe et oubliant ses responsabilités pour se distraire."+
    " • * Il faudra choisir un centre d’intérêt supplémentaire au personnage, en plus du sexe, de l’alcool et la musique."
};

const elfesNoirs = {
    "label": "Elfes noirs",
    "force": -1,
    "constitution": 0,
    "dexterite": 1,
    "intelligence": 1,
    "sagesse": 1,
    "charisme": 0,
    "survie": 1,
    "incantation": 2,
    "traits_racial": [
    {
        "label": "Arcaniste",
        "description": "Les elfes sont étroitement liés à la magie et de par ce fait, leur corps a développé une résistance contre celle-ci. Ils gagnent un bonus à la RM de 2.",
        "bonus": {
            "RM": 2
        }
    },
    {
        "label": "Dans les ombres",
        "description": "Lorsque le personnage réussit une attaque sournoise, celui-ci ajoute 2D6 dégâts au lieu de 1D6.",
        "bonus": {

        }
    },
    {
        "label": "Elfe lié au mana",
        "description": "Les elfes noirs ont reçu une initiation particulière à la magie, ils obtiennent ainsi le don perception magique, ainsi qu’un bonus à l’incantation de 1.",
        "bonus": {
            "dons": ["perception_magique"],
            "incantation": 1
        }
    },
    {
        "label": "Enfant d’Itzal",
        "description": "Le personnage est un adepte du dieu de la nuit et des ténèbres. Il reçoit son {{0.5}} de bonus au critique lorsque la nuit est tombée.",
        "bonus": {

        },
         "daylight": {
            "night": {
                "critique_par_niveaux" : 0.5
            }
        }
    },
    {
        "label": "Imprégnation",
        "description": "Lorsque le personnage reçoit les effets d’un sort de soin, une partie de l’énergie utilisée est absorbée par son corps. Cela lui permet de restaurer {{1}} point(s) de mana, tout en bénéficiant des effets de guérison du sort.",
        "bonus": {
          
        }
    },
    {
        "label": "Indépendant",
        "description": "Les elfes noirs ont appris à se débrouiller par eux-mêmes pour survivre. Le personnage gagne un bonus de 2, dans deux domaines d’artisanats au choix.",
        "bonus": {
        },
        "choix":[{ 
            "type": "Radio", 
            "list": { 
                "forgeron": 2, 
                "tanneur": 2,
                "alchimiste": 2,
                "tailleur": 2,
                "faconneur": 2
            } 
        },{
            "type": "Radio", 
            "list": { 
                "forgeron": 2, 
                "tanneur": 2,
                "alchimiste": 2,
                "tailleur": 2,
                "faconneur": 2
            }
        }]
    },
    {
        "label": "La fin justifie les moyens",
        "description": "Les rares elfes noirs qui osent sortir de leurs cités souterraines doivent manier la langue de bois pour pouvoir survivre. Ils gagnent un bonus de 2 au bluff.",
        "bonus": {
            "bluff": 2
        }
    },
    {
        "label": "Mana corrompu",
        "description": "Les elfes noirs peuvent canaliser leur mana de manière brutale, doublant les dégâts par niveau des sort. En contrepartie, ils subissent {{1}} point(s) de dégât (non réductible), en raison de la surcharge magique.",
        "bonus": {
            
        }
    },    
    {
        "label": "Pouvoirs nocturnes",
        "description": "Les elfes noirs sont connus pour évoluer sous terre et les ténèbres les rendent plus forts. Ils gagnent {{1}} dégâts en cas de combat nocturne, ou dans un lieu très sombre.",
        "bonus": {

        },
         "daylight": {
            "night": {
                "bonus_dmg_par_niveaux" : 1
            }
        }
    },
    {
        "label": "Source de pouvoirs",
        "description": "Certains individus sont plus endurants que d’autres et leur puissance magique dépasse celle de leurs camarades. Le personnage gagne 2 points de mana supplémentaire par niveau.",
        "bonus": {
            "pm_par_niveaux": 2
        }
    },
    {
        "label": "Ténébreux",
        "description": "Lorsque le personnage utilise un sort d’occultomancie, avec des effets sur la durée, ceux-ci durent plus longtemps. On ajoutera {{1}} tours supplémentaires, par exemple, un sort durant 1D4 + 2, pour un personnage niveau 2, durera 1D4 + 2 + 2 tours.",
        "bonus": {

        }
    },
    {
        "label": "Un avec les ombres",
        "description": "Le personnage transforme son corps en ombre, lui permettant de se déplacer librement sur les surfaces, tout en étant intangible. Ce pouvoir coûte 5 points de mana et persiste jusqu’à ce que le mage y mette fin.",
        "bonus": {

        }
    },
    {
        "label": "Volute",
        "description": "Une fois par combat, le personnage est capable de générer un sort occulte. Il peut lancer un projectile d’ombre sur 10 mètres qui inflige 1D4 + {{2}} dégâts. Le sort ne coûte pas de mana et est automatiquement réussi avec une marge de réussite égale au score d’intelligence du personnage.",
        "bonus": {

        }
    }
    ],
    "dons": ["vision_nocturne"],
    "t": " • – Les PNJ elfes seront automatiquement inamicaux, voir hostiles (en groupe), avec le personnage<br>"+
    " • * Les elfes noirs sont sensibles à la lumière, les dégâts reçus de l’élément sont doublés. De plus, ils sont sujets aux changements de luminosité, flash et lumières vives, ils reçoivent un malus à la ténacité de 6 à l’aveuglement."
};

const centaures = {
    "label": "Centaures",
    "force": 2,
    "constitution": 0,
    "dexterite": 0,
    "intelligence": -2,
    "sagesse": 2,
    "charisme": -1,
    "bonus_langue": -2,
    "langues": ["nessor"],
    "deplacement": 5,
    "multiplicateur_charge": 4,
    "traits_racial": [
    {
        "label": "Armes imposantes",
        "description": "Les centaures manient des armes proportionnelles à leur taille et donc, plus grandes que la moyenne. Le personnage débute l’aventure avec des armes plus massives qui infligent une classe de dé supplémentaire.",
        "bonus": {

        }
    },
    {
        "label": "Barde",
        "description": "Lorsqu’un centaure porte une armure, celui-ci à la possibilité d’habiller également la partie animale de son corps. Pour cela, il faudra augmenter le coût de l’armure de 200 % et doubler le score de RD.",
        "bonus": {
            "multiplicateur_bonus_rd_armure": 2
        }
    },
    {
        "label": "Chaman",
        "description": "Le personnage reçoit un bonus de 2 à l’incantation, si celui-ci manie une magie élémentaire ou chamanique.",
        "bonus": {

        }
    },
    {
        "label": "Charge puissante",
        "description": "Les pattes puissantes des centaures les propulsent avec force. Charger n’offre plus 1D de dégât supplémentaire, mais 2.",
        "bonus": {

        }
    },
    {
        "label": "Chasseur",
        "description": "Les centaures sont habitués à subvenir eux-mêmes aux besoins de leur clan. De par ce fait, ils gagnent un bonus à la survie de 1 et un bonus de 1 à la compétence d’arme d’arc.",
        "bonus": {
            "arc": 1,
            "survie": 1
        }
    },
    {
        "label": "Doigts de fée",
        "description": "Les centaures ne peuvent compter que sur eux-mêmes pour confectionner leurs équipements et provisions. De ce fait, lorsque le joueur prend la compétence d’artisanat, il peut choisir 3 artisanats au lieu de 2.",
        "bonus": {

        }
    },
    {
        "label": "Endurance équine",
        "description": "Les centaures n’ont pas l’habitude de se morfondre sur leur sort et ce qui pourrit sembler être de grosses plaies, sont pour eux de petits bobos. La régénération naturelle des points de vie des centaures est doublée.",
        "bonus": { // TODO, regen pv x2
        }
    },    
    {
        "label": "Galop",
        "description": "La course des centaures est plus rapide que celle des autres humanoïdes. Lorsque le personnage entreprend une course, sa vitesse de déplacement est triplée.",
        "bonus": {

        }
    },
    {
        "label": "Galopade effrénée",
        "description": "Les centaures frappent naturellement avec élan et leurs coups sont dévastateurs. Pour chaque tranche de 5 cases parcourues pendant un tour, les dégâts de l’arme sont augmentés de 5.",
        "bonus": {

        }
    },    
    {
        "label": "Hippique",
        "description": "Le centaure attaque et court en même temps. Le personnage peut continuer ses déplacements, même après avoir attaqué (sauf si quelque chose l’en empêche).",
        "bonus": {

        }
    },
    {
        "label": "Licorne",
        "description": "Certains chevaux sont plus magiques que d’autres, il en va de même pour les centaures. Le personnage est capable de soigner une cible de 1D8 + {{1}} point de vie, utilisable 3 fois par jour.",
        "bonus": {

        }
    },    
    {
        "label": "Ruade",
        "description": "Lorsque le personnage subit une attaque dans le dos (qu’elle touche ou non), celui-ci a le droit à une riposte de ruade. La ruade inflige 2D6 + bonus de force en dégâts et repousse d’une case si l’adversaire rate un jet d’acrobaties.",
        "bonus": {

        }
    },
    {
        "label": "Second souffle",
        "description": "Les centaures sont des êtres résistants et tout en muscles, même pour les plus fins d’entre eux. Lorsque ceux-ci ratent un jet d’athlétisme ou de ténacité, ils peuvent tenter de relancer le jet (Une fois seulement). De plus, ceux-ci ne peuvent pas souffrir des malus liés à l’encombrement (Mais la limite maximale reste la même).",
        "bonus": {

        }
    },
    {
        "label": "Sprinter",
        "description": "Le bonus de vitesse de déplacement est calculé différemment. Le score d’agilité au-dessus de 15, représentera le bonus de cases de déplacement.",
        "bonus": {

        }
    }
    ],
    "restriction":["equitation"],
    "dons": ["vision_etherique"],
    "t": "Les centaures ne peuvent entreprendre d’action incluant l’escalade, l’équitation et le déguisement. Créatures de grande taille"
};

const letjetis = {
    "label": "Letjetis",
    "force": -2,
    "constitution": -1,
    "dexterite": 1,
    "intelligence": 2,
    "sagesse": 0,
    "charisme": 1,
    "incantation": 1,
    "pm_par_niveaux": 2,
    "traits_racial": [
    {
        "label": "Agnosie volontaire",
        "description": "Contre 8 points de mana, le personnage peut rendre amnésique une cible à son contact. Celle-ci oubliera alors la dernière heure, mais aura tout de même droit à un jet de volonté. L’effet ne peut être répété sur une cible ayant déjà oublié, ou qui aurait résisté.",
        "bonus": {

        }
    },
    {
        "label": "Aimable",
        "description": "Les letjetis sont altruistes de nature. Tous les sorts bénéfiques lancés par le personnage ont un bonus de 1 au lancer, ainsi que {{0.5}} en bonus supplémentaire en tours ou aux effets.",
        "bonus": {

        }
    },
    {
        "label": "Amie de la nature",
        "description": "Vivants en forêt, les letjetis vivent au contact des animaux. Ainsi, elles ont appris à comprendre leurs besoins et comportements. Elles gagnent donc un bonus au dressage de 2.",
        "bonus": {
            "dressage": 2
        }
    },
    {
        "label": "Aura de malchance",
        "description": "Dans un rayon de 4 cases autour du personnage, les ennemis se verront subir une malédiction. À chaque lancer de dé, ils devront en lancer deux et seul le plus mauvais des deux sera choisi (hors coup critique).",
        "bonus": {

        }
    },
    {
        "label": "Charmeuse",
        "description": "Contre 10 points de mana, le personnage peut faire modifier d’un incrément l’attitude d’un personnage. La cible a cependant droit à un jet de volonté pour résister à l’effet. Le sort ne peut être lancé plusieurs fois sur une même cible.",
        "bonus": {

        }
    },
    {
        "label": "Fée aérienne",
        "description": "Les attaques en vol ne subissent plus de malus. Et les attaques au corps-à-corps voient le bonus en force doubler lors d’une attaque en plein vol.",
        "bonus": {

        },
        "flight": {
            "vol": {
                "multiplicateur_bonus_force" : 2
            }
        }
    },
    {
        "label": "Puits",
        "description": "Lorsque le personnage médite, celui-ci peut en plus restaurer 2 points de vie ou de mana par heure, aux autres personnages autour de lui.",
        "bonus": {

        }
    },
    {
        "label": "Magie féerique",
        "description": "En tant que créatures magiques, les letjetis obtiennent le don perception magique, ainsi qu’un bonus à l’incantation de 1.",
        "bonus": {
            "incantation": 1,
            "dons": ["perception_magique"]
        }
    },
    {
        "label": "Marchand de sable",
        "description": "Pour un nombre d’utilisations par jour, égal au charisme du personnage divisé par deux et contre 4 points de mana, le personnage peut faire s’endormir une cible au contact, pendant {{1}} minutes. La cible aura tout de même droit à un jet de volonté. Les bonus et malus seront donnés en fonction de la situation (Cible fatiguée, énergique, malade etc)",
        "bonus": {

        }
    },
    {
        "label": "Miracle",
        "description": "Une fois par jour et contre 2 points de mana, le personnage peut soigner un allié ou lui-même. Les soins seront égaux au score d'intelligence + 2 par niveau de la letjeti.",
        "bonus": {

        }
    },
    {
        "label": "Nymphe",
        "description": "Méditer à proximité d’un point d’eau naturel, double la régénération lors des temps de repos.",
        "bonus": {

        }
    },    
    {
        "label": "Source de pouvoirs",
        "description": "Certains individus sont plus endurants que d’autres et leur puissance magique dépasse celle de leurs camarades. Le personnage gagne 2 points de mana supplémentaire par niveau.",
        "bonus": {
            "pm_par_niveaux": 2
        }
    },
    {
        "label": "Vice-versa",
        "description": "Contre 8 points de mana, le personnage peut modifier l’humeur de sa cible au contact. On acceptera : la colère, la tristesse, la joie, la peur et le dégoût. La cible aura tout de même droit à un jet de volonté pour tenter de résister. L’intensité de l’émotion demandée entraînera des malus (simple : 0, forte : 4, extrême : 8). Le sort ne peut être lancé plusieurs fois sur une même cible.",
        "bonus": {

        }
    },
    {
        "label": "Vol féerique",
        "description": "Les letjetis volent aussi naturellement qu’elles ne respirent. De par ce fait, elles doublent leur vitesse de déplacement en vol.",
        "bonus": {

        },
        "flight": {
            "vol": {
                "multiplicateur_deplacement": 2
            }
        }
    }
    ],
    "choix": [{
        "type": "Radio",
        "list": {
            "eloquence": 1,
            "bluff": 1
        }
    }],
    "flight": {

    },
    "t": " Les letjetis sont naturellement dotés de la capacité de vol (Magique)"+
    " Les ailes des letjetis étant d’origine magique, elles ne pourront être utilisées si le personnage est dans l’incapacité d’utiliser la magie (À cause d’un sort, d’un EC, manque de mana etc). De plus, elles sont également sensibles à l’eau et risquent de devenir inutilisables si elles sont mouillées."+
    " Les letjetis sont connus pour leur lien avec la magie. Beaucoup de gens risquent de chercher à les attraper, pour leurs pouvoirs très spécifiques, les exposer, les étudier ou encore tenter d’extraire leur magie. À chaque ville où le personnage se rend, il faudra jeter 1D100-10 pour savoir si sa présence déclenchera un événement."
};

function fusion_race(a,b,key) {
    if(Array.isArray(b[key])) {
        a[key] = a[key] ? a[key].concat(b[key]) : [].concat(b[key]);
    }
    else {
        a[key] = a[key] ? a[key] + b[key] : b[key];
    }
}

//combinaison des demi-élémentaires de différents éléments et des demi-élémentaires tout court
for (const key in demiElementaires) {
    [demiElementairesEau, demiElementairesFeu, demiElementairesAir, demiElementairesElectricite, demiElementairesGlace, demiElementairesTerre].forEach(demiElementaire => {
        fusion_race(demiElementaire, demiElementaires, key);
    });
}

//combinaison des naskagarris de différentes carrures et des naskagarris tout court
for (const key in naskagarris) {
    [naskagarrisPetit, naskagarrisMoyen, naskagarrisGrand].forEach(naskagarri => {
        fusion_race(naskagarri, naskagarris, key);
    });
}

//combinaison des sylvaniens
for (const key in sylvaniens) {
    [sylvaniensProtecteur, sylvaniensMemoires].forEach(sylvanien => {
        fusion_race(sylvanien, sylvaniens, key);
    });
}

const races = {
    "humains": humains,
    "elfes": elfes,
    "sauriens": sauriens,
    "draconides": draconides,
    "nains": nains,
    "orques": orques,
    "halfelins": halfelins,
    "dehamanes": dehamanes,
    "aingéales": aingeales,
    "demi-elfes": demiElfes,
    "demi-nains": demiNains,
    "demi-orques": demiOrques,
    "vatholas": vatholas,
    "landarès": landares,
    "naskagarrisPetit": naskagarrisPetit,
    "naskagarrisMoyen": naskagarrisMoyen,
    "naskagarrisGrand": naskagarrisGrand,
    "demi-élémentairesEau": demiElementairesEau,
    "demi-élémentairesFeu": demiElementairesFeu,
    "demi-élémentairesAir": demiElementairesAir,
    "demi-élémentairesElectricite": demiElementairesElectricite,
    "demi-élémentairesGlace": demiElementairesGlace,
    "demi-élémentairesTerre": demiElementairesTerre,
    "sylvaniensProtecteurs": sylvaniensProtecteur,
    "sylvaniensMemoires": sylvaniensMemoires,
    "eletavirs": eletavirs,
    "gobelins": gobelins,
    "satyres": satyres,
    "elfes noirs": elfesNoirs,
    "centaures": centaures,
    "letjetis": letjetis,
};

//ajout des traits raciaux généraux à tout le monde
for (const key in races) {
    races[key].traits_racial = races[key].traits_racial.concat(general.traits_racial);
}

export default races;