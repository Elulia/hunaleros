const dons_evolutif = {
	"Armures légères et très légères":{
		"type": "Armure",
		"Rang 1":[
		{
			"label":"Apprenti éclaireur",
			"description":"Lorsque le personnage porte une armure légère ou très légère, il gagne un bonus de 1 à la RM (résistance magique) et 1 au critique à l’esquive. De plus, les malus liés aux terrains difficiles sont supprimés.",
			"type" : "passif",
			"bonus": {
				"RM": 1,
				"critique": 1
			}
		},
		{
			"label":"Légèreté",
			"description":"Le personnage peut réduire ses dégâts de chute. Contre 2 points de mana par tranche de 3 mètres, l’armure absorbe entièrement les dégâts.",
			"type" : "autre",
			"cout" : "2 PM tous les 3 mètres",
			"bonus" : {

			}
		}
		],
		"Rang 2":[
		{
			"label":"Confiance",
			"description":"Lorsque le personnage réussit une esquive, celui-ci gagne un bonus de 2 à sa prochaine action, hormis la défense. L’effet peut se cumuler si plusieurs esquives sont réussies avant le début du tour.",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		},
		{
			"label":"Virevolte",
			"description":"Contre 4 points de mana, le personnage peut réussir automatiquement un jet d’acrobaties, peu importe la difficulté, tant que cela reste physiquement possible.",
			"type" : "autre",
			"cout" : "4 PM",			
			"bonus" : {

			}
		}
		],
		"Rang 3":[
		{
			"label":"Les yeux fermés",
			"description":"Contre 4 points de manade mana, le personnage peut ajouter {{1}} en bonus sur son prochain jet d’initiative ou sur une compétence sujette aux malus d’armure.",
			"type" : "buff",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label":"Expert éclaireur",
			"description":"Lorsque le personnage porte une armure légère ou très légère, il gagne un bonus de 1 à la RD. Il reçoit également un bonus de 1 à l’esquive, ainsi que sur la marge de critique de cette compétence.",
			"type" : "passif",
			"bonus": {
				"RD": 1,
				"esquive": 1,
				"critique": 1
			}
		}
		],
		"Rang 4":[
		{
			"label":"Ni vu ni connu",
			"description":"Contre 4 points de mana et un jet d’acrobaties réussi, le personnage se désengage sans recevoir d’attaque d’opportunité. Si le jet rate, l’attaque d’opportunité aura bien lieu. Cette capacité peut se lancer avant ou après l’attaque et le déplacement.",
			"type" : "autre",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label":"Déplacement silencieux",
			"description":"Contre 2 points de mana, le personnage est capable de se déplacer sans faire de bruit pendant 5 minutes.",
			"type" : "buff",
			"cout" : "2 PM",
			"bonus" : {

			}
		}
		],
		"Rang 5":[
		{
			"label":"Absorption",
			"description":"Lorsque le personnage n’arrive pas à esquiver un sort, il peut utiliser 1 point de mana par point de dégât qu’il souhaite nullifié. Par la suite, les points absorbés par l’armure se transformeront en bonus pour le prochain jet de régénération passive de mana.",
			"type" : "autre",
			"cout" : "1 PM pour 1 point de dégât",
			"bonus" : {

			}
		},
		{
			"label":"Fusion",
			"description":"Contre 6 points de mana, le personnage peut additionner ses scores de RD et de RM pendant {{1}} tour(s). Les propriétés de chacune s’additionnent également.",
			"type" : "buff",
			"cout" : "6 PM",
			"bonus" : {

			}
		}
		],
		"Rang 6":[
		{
			"label":"Maître éclaireur",
			"description":"Lorsque le personnage porte une armure légère ou très légère, il gagne un bonus de 1 à sa marge de critique, 2 de RM et un bonus de 1 à la RD.",
			"type" : "passif",
			"bonus" : {
				"RD": 1,
				"RM": 2,
				"critique": 1
			}
		}
		]
	},
	"Armures moyennes et intermédiaires":{
		"type": "Armure",
		"Rang 1":[
		{
			"label":"Apprenti factionnaire",
			"description":"Lorsque le personnage porte une armure moyenne ou intermédiaire, il gagne un bonus de 1 à la RD et un bonus de 1 sur la compétence d’esquive ou de bouclier, au choix.",
			"type" : "passif",
			"bonus" : {
				"RD": 1
			},
			"choix" : {
				"type": "Radio",
				"list": { 
					"esquive": 1,
					"bouclier": 1
				}
			}
		},
		{
			"label":"Assaut",
			"description":"Contre 2 points de mana, le personnage augmente instantanément sa vitesse de déplacement de 3+{{0.5}} pour le tour.",
			"type" : "buff",
			"cout" : "2 PM",
			"bonus" : {

			}
		}
		],
		"Rang 2":[
		{
			"label":"Exaltation",
			"description":"Contre 3 points de mana, le personnage augmente sa vivacité et ne peut plus subir de malus en cas d’attaques multiples, jusqu’à la fin du combat.",
			"type" : "autre",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		{
			"label":"À un cheveu près",
			"description":"Si jamais le personnage rate un jet d’esquive, il peut, contre 4 points de mana, avoir un bonus de {{1}} et permettre éventuellement à l’action de réussir.",
			"type" : "buff",
			"cout" : "4 PM",
			"bonus" : {

			}
		}
		],
		"Rang 3":[
		{
			"label":"Sacralisation",
			"description":"Contre 5 points de mana, le personnage enchante son armure, ce qui aura pour effet d’augmenter les soins magiques reçus. Pendant {{1}} tour(s), les soins recevront un bonus de 2D.",
			"type" : "buff",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label":"Expert factionnaire",
			"description":"Lorsque le personnage porte une armure moyenne ou intermédiaire, il gagne un bonus de 1 à la RD et un bonus de 1 au critique sur la compétence d’esquive ou de bouclier, au choix.",
			"type" : "passif",
			"bonus": {
				"RD": 1
			},
			"choix" : {
				"type": "Radio",
				"list": { 
					"crit_esquive": 1,
					"crit_bouclier": 1
				}
			}
		}
		],
		"Rang 4":[
		{
			"label":"Assurance tous risques",
			"description":"Contre 3 points de mana, le personnage augmente son score en RM de {{1}} et ce, pour 2+{{1}} tours.",
			"type" : "buff",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		{
			"label":"Reprise de forces",
			"description":"Lorsque le personnage réalise un critique sur un jet d’esquive, celui-ci regagne automatiquement 2D6 + {{1}} en points de vie ou points de mana.",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		}		
		],
		"Rang 5":[
		{
			"label":"Soldat accompli",
			"description":"Contre 6 points de mana et sur une distance de 5 mètres maximum, le personnage peut imprégner une armure et renforcer sa structure en créant une barrière protectrice. Il pourra choisir entre augmenter la RD ou la RM de l’armure d’un montant de {{1}} (mais pas cumuler les deux) et ce pendant 3 tours.",
			"type" : "buff",
			"cout" : "6 PM",
			"bonus" : {

			}
		},
		{
			"label":"Contre-attaque virulente",
			"description":"Lorsque le personnage réalise un critique sur son esquive, celui-ci regagne automatiquement 1D de dégâts ou une classe dé sur la contre-attaque offerte par le critique.",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		}
		],
		"Rang 6":[
		{
			"label":"Maître factionnaire",
			"description":"Lorsque le personnage porte une armure moyenne ou intermédiaire, il gagne un bonus de 1 sur la compétence d’esquive ou de bouclier, un bonus de 2 à la RM et à la RD et les malus lié à l’armure diminuent de 2 (si le malus passe en dessous de 0, tout excédent deviendra un bonus).",
			"type" : "passif",
			"bonus": {
				"RM": 2,
				"RD": 2,
				"malus_armure": -2, //TODO
				"bonus_initiative": 2
			},
			"choix" : {
				"type": "Radio",
				"list": { 
					"esquive": 1,
					"bouclier": 1
				}
			}
		}
		]
	},
	"Armures lourdes et complètes":{
		"type": "Armure",
		"Rang 1":[
		{
			"label":"Apprenti chevalier",
			"description":"Lorsque le personnage porte une armure lourde ou complète, il gagne un bonus de 1 à la RD et 1 au critique sur la compétence d’esquive ou de bouclier.",
			"type" : "passif",			
			"bonus": {
				"RD": 1
			},
			"choix" : {
				"type": "Radio",
				"list": { 
					"crit_esquive": 1,
					"crit_bouclier": 1
				}
			}
		},
		{
			"label":"Inébranlable",
			"description":"La charge du personnage est améliorée. En plus du bonus de dégâts initial, on ajoutera une classe de dé par tranche de 4 points de force. Tant que le personnage n’a pas accès à «deuxième peau», charger consomme 5 points de mana.",
			"type" : "autre",
			"cout" : "5 PM",
			"bonus" : {

			}
		}
		],
		"Rang 2":[
		{
			"label":"Ça suffit",
			"description":"Lorsque le personnage est touché par 3 attaques (pas obligatoirement à la suite) dans un même combat, celui-ci gagne 1D de dégâts supplémentaire à sa prochaine attaque. Si celle-ci rate, l’effet est tout de même consommé.",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		},
		{
			"label":"Ça chatouille",
			"description":"Lorsqu’une attaque est portée au personnage, mais que son armure bloque l’entièreté des dégâts, celui-ci a le droit à une riposte gratuite (sous les conditions habituelles de réussite).",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		}
		],
		"Rang 3":[
		{
			"label":"Armure délestée",
			"description":"Contre 4 points de mana et pendant 5 minutes, le personnage peut retirer le malus lié à l’armure sur une de ses compétences ou sur l’initiative",
			"type" : "autre",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label":"Expert chevalier",
			"description":"Lorsque le personnage porte une armure lourde ou complète, il gagne un bonus de 1 à la RD, ainsi que sur la compétence d’esquive ou de bouclier et un bonus de 1 sur la marge de critique d’une des deux compétences.",
			"type" : "passif",
			"bonus": {
				"RD": 1
			},
			"choix" : {
				"type": "Radio",
				"list": { 
					"esquive": 1,
					"bouclier": 1
				}
			}
		}
		],
		"Rang 4":[
		{
			"label":"Blocage anticipé",
			"description":"Contre 6 points de mana, le personnage peut diviser par 2 les dégâts reçus d’une attaque. Le joueur devra se manifester avant de savoir si l’attaque va toucher.",
			"type" : "autre",
			"cout" : "6 PM",
			"bonus" : {

			}
		},
		{
			"label":"Deuxième peau",
			"description":"Le personnage est tellement habitué à son armure et à se mouvoir dedans que la course et le vol lui sont désormais possibles.",
			"type" : "passif",
			"bonus" : {

			}
		}
		],
		"Rang 5":[
		{
			"label":"Régénération forcée",
			"description":"Contre 5 points de mana par coup encaissé, le personnage peut soigner une partie des dégâts reçus. Le don ne peut être utilisé que pour soigner des dégâts infligés (après éventuelles réductions) par un ennemi lors d’un combat. On prendra alors les dés de dégâts bruts (sans les +) de l’attaquant, qu’on divisera par 2 (arrondi au supérieur) pour les convertir en soins.",
			"type" : "heal",
			"cout" : "5 PM par coup",
			"bonus" : {

			}
		},
		{
			"label":"Alternance",
			"description": "Contre 4 points de mana, le personnage peut décider d’intervertir ses scores de RD et de RM. L’effet perdure pour dans 2+{{1}} tours, mais peut être annulé à tout moment si besoin.",
			"type" : "autre",
			"cout" : "4 PM",
			"bonus" : {

			}
		}
		],
		"Rang 6":[
		{
			"label":"Maître chevalier",
			"description":"Lorsque le personnage porte une armure lourde ou complète, il gagne un bonus de 1 sur la marge de critique de la compétence de bouclier ou d’esquive. Il profite également d’une diminution de 2 des malus liés à l’armure et d’un bonus de 2 à la RD.",
			"type" : "passif",
			"bonus": {
				"malus_armure":-2,
				"bonus_initiative": 2,
				"RD": 2
			},
			"choix" : {
				"type": "Radio",
				"list": { 
					"crit_esquive": 1,
					"crit_bouclier": 1
				}
			}
		}
		]
	},
	"Couteaux":{
		"type": "Arme",
		"Rang 1":[
		{
			"label":"Apprenti assassin",
			"description":"Lorsque le personnage manie un couteau, il gagne un bonus de 1 aux dégâts, ainsi qu’un bonus de 1 sur la marge de critique de l’arme.",
			"type" : "passif",
			"bonus" : {
				"type_couteaux": {
					"critique": 1,
					"bonus_dmg": 1
				}
			}
		},
		{
			"label":"Couteau dentelé",
			"description":"Contre 2 points de mana, le personnage inflige un saignement aux dégâts égaux au nombre de ses dés d’attaque (2 dégâts de saignement pour 2D6). Peut se combiner avec d’autres améliorations du saignement, mais ne peut se cumuler sur une même cible.",
			"type" : "damage",
			"cout" : "2 PM",
			"bonus" : {

			}
		}
		],
		"Rang 2":[
		{
			"label":"Hémorragie",
			"description":"Les saignements qu’inflige le personnage ne peuvent plus être soignés de façon naturelle. Il faudra soit se soigner avec la médecine ou la magie.",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		},
		{
			"label":"Saut arrière",
			"description":"Contre 2 points de mana et un jet d’acrobaties réussi, cette manœuvre, permet au personnage de se désengager sans recevoir d’attaque d’opportunité",
			"type" : "autre",
			"cout" : "2 PM",
			"bonus" : {

			}
		}
		],
		"Rang 3":[
		{
			"label":"Roulade",
			"description":"Lorsque le personnage subit un renversement, il peut utiliser 3 points de mana pour reculer d’une case en arrière et ainsi éviter de se retrouver au sol.",
			"type" : "autre",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		{
			"label":"Expert assassin",
			"description":"Lorsque le personnage manie un couteau, il gagne un bonus de 2D de dégâts, un bonus de 1 sur la compétence de couteaux et un bonus de 1 sur sa marge de critique.",
			"type" : "passif",
			"bonus": {
				"couteaux": 1,
				"type_couteaux": {
					"bonus_D_dmg": 2,
					"critique": 1
				}
			}
		}
		],
		"Rang 4":[
		{
			"label":"Combat à deux armes",
			"description":"Le malus lié à l’ambidextrie est diminué de 2.",
			"type" : "passif",
			"bonus" : {

			}
		},
		{
			"label":"Renvoi de l’insaisissable",
			"description":"Contre 5 points de mana, le personnage esquive un projectile et le redirige sur un de ses ennemis. Pour cela, il faudra réussir un jet d’arme et l’opposé à la défense de la cible. ",
			"type" : "autre",
			"cout" : "5 PM",
			"bonus" : {

			}
		}
		],
		"Rang 5":[
		{
			"label":"Distraction",
			"description":"Lorsqu’il attaque, le personnage réalise une suite rapide de mouvements aléatoires qui perturbent son adversaire et permettent de le frapper là où il s’y attend le moins. Les adversaires visés par le personnage reçoivent automatiquement un malus de 5 à leur défense.",
			"type" : "passif",
			"bonus" : {

			}
		},
		{
			"label":"Points faibles",
			"description":"Contre 5 points de mana, le personnage améliore sa concentration et repère les failles dans la l’armure adverse, afin de passer outre la RD ennemie pendant {{1}} tour(s).",
			"type" : "buff",
			"cout" : "5 PM",
			"bonus" : {

			}
		}
		],
		"Rang 6":[
		{
			"label":"Maître assassin",
			"description":"Lorsque le personnage manie un couteau, il gagne un bonus de 2D de dégâts, un bonus de 2 sur la compétence de couteaux et un bonus de 1 sur sa marge de critique.",
			"type" : "passif",
			"bonus": {
				"couteaux": 2,
				"type_couteaux" : {
					"bonus_D_dmg": 2,
					"critique": 1
				}
			}
		}
		]
	},
	"Pugiliste":{
		"type": "Arme",
		"Rang 1":[
		{
			"label":"Ceinture blanche",
			"description":"Lorsque le personnage manie l’art du pugilat, il gagne un bonus de 1 au toucher, ainsi qu’un bonus de 1 sur sa marge de critique.",
			"type" : "passif",
			"bonus": {
				"corpsacorps": 1,
				"type_corpsacorps" : {
					"critique": 1
				}
			}
		},
		{
			"label":"Brise garde",
			"description":"Contre 4 points de mana, le personnage porte un coup bien placé qui inflige des dégâts normaux, mais qui brise la défense de son adversaire. Jusqu’au prochain tour de l’ennemi, celui-ci subira un malus de 4 à sa défense.",
			"type" : "debuff",
			"cout" : "4 PM",
			"bonus" : {

			}
		}
		],
		"Rang 2":[
		{
			"label":"Poings enflammés",
			"description":"Contre 2 points de mana, les poings du personnage s’entourent de flammes éthérées, transformant les dégâts standards en dégâts de feu pour le tour actuel.",
			"type" : "buff",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label":"Paré !",
			"description":"Lorsqu’un assaillant rate une attaque contre le personnage, celui-ci a le droit à une riposte sous les mêmes conditions qu’un jet standard, mais avec les dégâts divisés par deux. Une seule riposte par ennemi est autorisée.",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		}
		],
		"Rang 3":[
		{
			"label":"Tornade",
			"description":"Contre 4 points de mana, le personnage peut toucher toutes les cibles autour de lui, tout en les repoussant d’une case.",
			"type" : "damage",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label":"Ceinture verte",
			"description":"Lorsque le personnage manie l’art du pugilat, il gagne un bonus de 2D de dégâts, ainsi qu’un bonus de 1 sur sa marge de critique et au toucher.",
			"type" : "passif",
			"bonus": {
				"corpsacorps": 1,
				"type_corpsacorps" : {
					"critique": 1,
					"bonus_D_dmg": 2
				}
			}
		}
		],
		"Rang 4":[
		{
			"label":"Onde de choc",
			"description":"Contre 6 points de mana, le personnage frappe puissamment le sol et génère une onde de choc dans une zone de type 1 autour de lui. Les ennemis présents dans la zone, qui ratent un jet d’acrobaties, sont alors renversés.",
			"type" : "autre",
			"cout" : "6 PM",
			"bonus" : {

			}
		},
		{
			"label":"Fureur du dragon",
			"description":"Le personnage est devenu un expert en arts martiaux et est capable d’effectuer 2 attaques consécutives, sous les mêmes conditions que l’ambidextrie.",
			"type" : "passif",
			"bonus" : {

			}
		}
		],
		"Rang 5":[
		{
			"label":"Uppercut",
			"description":"Contre 5 points de mana, le personnage peut augmenter instantanément sa marge de coup critique de {{1}} pour le tour. ",
			"type" : "buff",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label":"Muda, muda, muda",
			"description":"Contre 8 points de mana, le personnage retire tous les malus qu’imposent les attaques multiples pour ce tour. Le bonus de force s’applique alors sur les deux mains et le personnage termine son tour avec une manœuvre de son choix.",
			"type" : "buff",
			"cout" : "8 PM",
			"bonus" : {

			}
		}
		],
		"Rang 6":[
		{
			"label":"Ceinture noire",
			"description":"Lorsque le personnage manie l’art du pugilat, il gagne un bonus de 2D de dégâts, un bonus de 1 sur sa marge de critique et un bonus de 2 à l’esquive.",
			"type" : "passif",
			"bonus": {
				"esquive": 2,
				"type_corpsacorps" : {
					"critique": 1,
					"bonus_D_dmg": 2
				}
			}
		}
		]
	},
	"Armes d’hast légères":{
		"type": "Arme",
		"Rang 1":[
		{
			"label":"Apprenti rétiaire",
			"description":"Lorsque le personnage manie une arme d’hast à une main, il gagne un bonus au toucher de 1, ainsi qu’un bonus à sa marge de critique de 1.",
			"type" : "passif",
			"bonus": {
				"hast": 1,
				"type_hast" : {
					"critique": 1
				}
			}
		},
		{
			"label":"Estoc",
			"description":"Lorsque le personnage frappe un ennemi à l’aide d’une attaque d’opportunité, celle-ci inflige {{1}} dégât(s) supplémentaire(s).",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		}
		],
		"Rang 2":[
		{
			"label":"Entrave du gladiateur",
			"description":"Le personnage peut désormais manier le filet de combat, en même temps que l’arme d’hast légère. Sur un jet d’arme, il est possible d’enchevêtrer une cible de taille moyenne ou moins, si celle-ci rate son jet d’acrobaties opposé.",
			"type" : "autre",
			"bonus" : {

			}
		},
		{
			"label":"Puissance de frappe",
			"description":"Contre 2 points de mana, le personnage peut échanger 1 point (ajouté) de sa compétence d’arme, pour le transformer en point de dégât. Il est possible de transmuter autant de points que voulu, à condition de dépenser le mana à chaque fois.",
			"type" : "autre",
			"cout" : "2 PM par point transformé",
			"bonus" : {

			}
		}
		],
		"Rang 3":[
		{
			"label":"Empalement",
			"description":"Contre 4 points de mana, le personnage lance son arme (sur une distance égale à sa force divisée par 2) ou la rappelle vers lui (instantanément) et transperce un ennemi, lui infligeant 1D de dégât supplémentaire et un état de saignement.",
			"type" : "damage",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label":"Expert rétiaire",
			"description":"Lorsque le personnage manie une arme d’hast à une main, il gagne un bonus à l’esquive de 2, ainsi qu’un bonus aux dégâts de 2D.",
			"type" : "passif",
			"bonus": {
				"esquive": 2,
				"type_hast" : {
					"bonus_D_dmg": 2
				}
			}
		}
		],
		"Rang 4":[
		{
			"label":"Maîtrise du filet",
			"description":"Contre 6 points de mana, le personnage imprègne son filet de magie, qui s’adapte désormais à la taille de l’adversaire, tout en appliquant un malus au toucher de 4 et divise les dégâts de la cible par deux.",
			"type" : "buff",
			"cout" : "6 PM",
			"bonus" : {

			}
		},
		{
			"label":"Harponnage",
			"description":"Contre 1 point de mana supplémentaire, « empalement » peut désormais ramener la cible en face du personnage, lorsqu’il rappelle son arme vers lui.",
			"type" : "autre",
			"cout" : "1 PM",
			"bonus" : {

			}
		}
		],
		"Rang 5":[
		{
			"label":"Allonge",
			"description":"Contre 4 points de mana et de façon instantanée, l’arme peut croître et doubler son allonge le temps d’un tour.",
			"type" : "buff",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label":"Détonation arcanique",
			"description":"Lorsqu’un ennemi est harponné, le personne peut, contre 7 points de mana, envoyer une vague d’énergie qui va parcourir son arme, atteindre la cible sans possibilité d’esquive et infliger 2D10 + {{1}} points de dégâts.",
			"type" : "damage",
			"cout" : "7 PM",
			"bonus" : {

			}
		}
		],
		"Rang 6":[
		{
			"label":"Maître rétiaire",
			"description":"Lorsque le personnage manie une arme d’hast à une main, il gagne un bonus au toucher de 1, un bonus aux dégâts de 1D, un bonus de 1 à sa marge de critique et un bonus de 2 à la défense.",
			"type" : "passif",
			"bonus": {
				"hast": 2,
				"type_hast" : {
					"bonus_D_dmg": 1,
					"critique": 1
				}
			}
		},
		]
	},
	"Épées légères":{
		"type": "Arme",
		"Rang 1":[
		{
			"label":"Apprenti bretteur",
			"description":"Lorsque le personnage manie une épée à une main, il gagne un bonus à sa marge de critique de 1, ainsi qu’un bonus de dégâts de 1.",
			"type" : "passif",
			"bonus" : {
				"type_epee" : {
					"critique": 1,
					"bonus_dmg": 1
				}
			}
		},
		{
			"label":"Frappe basse",
			"description":"Contre 3 points de mana, le personnage peut lancer deux fois ses jets de dégât et garder celui qu’il désire (utilisation avant le jet de toucher). L’attaque est soumise aux mêmes conditions de réussites qu’une attaque traditionnelle.",
			"type" : "autre",
			"cout" : "3 PM",
			"bonus" : {

			}
		}
		],
		"Rang 2":[
		{
			"label":"Fusion de l’esprit",
			"description":"La lame du guerrier devient éthérée en échange de 2 points de mana par tour, permettant d’infliger des dégâts magiques.",
			"type" : "buff",
			"cout" : "2 PM par tour",
			"bonus" : {

			}
		},
		{
			"label":"Replacement",
			"description":"Chaque fois que le personnage réussit à toucher son adversaire, celui-ci peut se déplacer d’une case (même en diagonale) sans provoquer d’attaque d’opportunité. Il est possible d’outrepasser son nombre de cases de déplacement avec cette manœuvre.",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		}
		],
		"Rang 3":[
		{
			"label":"Enchaînement",
			"description":"En cas de mise en coma d’un adversaire, le personnage a le droit à une attaque gratuite, si un autre antagoniste se trouve à portée. Il est possible d’utiliser 2 points de mana par case de déplacement, afin d’atteindre une autre cible éloignée. Le nombre de cases de déplacement restantes au personnage ne peut cependant pas être dépassé.",
			"type" : "passif_situationnel",
			"cout" : "2 PM (optionnel)",
			"bonus" : {

			}
		},
		{
			"label":"Expert bretteur",
			"description":"Lorsque le personnage manie une épée à une main, il gagne un bonus au toucher de 1, un bonus à sa marge de critique de 1, ainsi qu’un bonus de dégâts de 2D.",
			"type" : "passif",
			"bonus" : {
				"epee": 1,
				"type_epee" : {
					"bonus_D_dmg": 2,
					"critique": 1
				}
			}
		}
		],
		"Rang 4":[
		{
			"label":"Aparté",
			"description":"Quand un ennemi entre dans sa zone de frappe, le personnage peut lancer une attaque (standard) contre celui-ci, même si ce n’est pas son tour.",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		},
		{
			"label":"Tactiques de combat",
			"description":"Contre 5 points de mana, le personnage peut choisir son ordre de passage lors de la lancée des jets d’initiative. Ce don va prévaloir sur tous les autres jets, même les critiques.",
			"type" : "autre",
			"cout" : "5 PM",
			"bonus" : {

			}
		}
		],
		"Rang 5":[
		{
			"label":"Esquive habile",
			"description":"Si le personnage subit une attaque critique, cette dernière ne bénéficiera pas des dégâts doublés, mais sera considérée comme une attaque normale.",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		},
		{
			"label":"Équilibre naturel",
			"description":"Chaque ennemi achevé par le personnage ajoute une charge d’anima (avec pour maximum, l’intelligence divisée par 8), l’anima permet l’annulation pure et simple d’une attaque ou d’un sort qui aurait dû normalement toucher.",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		}
		],
		"Rang 6":[
		{
			"label":"Maître bretteur",
			"description":"Chaque ennemi achevé par le personnage ajoute une charge d’anima (avec pour maximum, l’intelligence divisée par 8, arrondit au supérieur), l’anima permet l’annulation pure et simple d’une attaque ou d’un sort qui aurait dû normalement toucher le personnage.",
			"type" : "autre",
			"bonus" : {
				"epee": 1,
				"esquive": 2,
				"type_epee" : {
					"bonus_D_dmg": 1,
					"critique": 1
				}
			}
		}
		]
	},
	"Fléaux légers":{
		"type": "Arme",
		"Rang 1":[
		{
			"label":"Apprenti assaillant",
			"description":"Lorsque le personnage manie un fléau léger, il gagne un bonus de dégâts de 1, ainsi qu’un bonus de 1 sur sa marge de critique.",
			"type" : "passif",
			"bonus" : {
				"type_fleaux" : {
					"critique": 1,
					"bonus_dmg": 1
				}
			}
		},
		{
			"label":"Déploiement",
			"description":"L’arme est améliorée et gagne un bonus de portée d’attaque de 2.",
			"type" : "passif",
			"bonus" : {
				"portee" : 2
			}
		}
		],
		"Rang 2":[
		{
			"label":"Bélier",
			"description":"Contre 2 points de mana, le personnage charge un adversaire et le repousse de 1D6 + {{1}}, en plus de lui infliger les dégâts habituels. Cependant, chaque incrément de taille supérieure au personnage retirera 4 cases, alors qu’à l’inverse, les plus petites tailles seront repoussées de 4 cases supplémentaires.",
			"type" : "autre",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label":"Élan",
			"description":"Lorsque le personnage a effectué un déplacement d’au moins trois cases avant d’attaquer, celui-ci gagne un bonus d’une classe de dé à ses dégâts.",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		}
		],
		"Rang 3":[
		{
			"label":"Garde tourbillonnante",
			"description":"Contre un jet de manœuvres opposé à l’attaque ennemie, le personnage fait tournoyer rapidement son arme et dévie tout projectile non magique qui le viserait.",
			"type" : "autre",
			"bonus" : {

			}
		},
		{
			"label":"Expert assaillant",
			"description":"Lorsque le personnage manie un fléau léger, il gagne un bonus de 2D à ses dégâts, ainsi qu’un bonus de 2 au toucher.",
			"type" : "passif",
			"bonus" : {
				"fleaux": 2,
				"type_fleaux": {
					"bonus_D_dmg": 2
				}
			}
		}
		],
		"Rang 4":[
		{
			"label":"Force d’attaque",
			"description":"Contre 4 points de mana, le personnage peut augmenter sa marge de coup critique de 2 sur sa compétence d’arme, pendant son niveau en tours.",
			"type" : "buff",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label":"Repoussement",
			"description":"Contre 5 points de mana, le personnage fait tourner son arme à grande vitesse et repousse (de son niveau en cases) tous les adverses se trouvant à son contact. La défense ne pourra se faire que sur un jet de bouclier simple. Sinon, il faudra également réussir un jet d’acrobaties opposé, afin de ne pas être renversé après avoir été repoussé.",
			"type" : "autre",
			"cout" : "5 PM",
			"bonus" : {

			}
		}
		],
		"Rang 5":[
		{
			"label":"Coup éloigné",
			"description":"Contre 6 points de mana, le personnage conduit magiquement la chaîne de son arme et propulse la masse de fer droit vers l’ennemi. En plus de faire reculer la cible (le bonus lié à la force en cases), l’attaque inflige 2D de dégâts supplémentaires.",
			"type" : "damage",
			"cout" : "6 PM",
			"bonus" : {

			}
		},
		{
			"label":"Ricochet",
			"description":"Contre 8 points de mana, le personnage à la possibilité de faire ricocher 4 fois son attaque dans une zone de type 1 autour de lui. Un seul jet de dé sera nécessaire pour toucher, mais chaque cible aura la possibilité de faire un jet d’esquive opposé. L’arme ne peut ricocher deux fois sur une même cible et fera les dégâts totaux pour chaque cible.",
			"type" : "buff",
			"cout" : "8 PM",
			"bonus" : {

			}
		}
		],
		"Rang 6":[
		{
			"label":"Maître assaillant",
			"description":"Lorsque le personnage manie un fléau léger, il gagne un bonus de 1D à ses dégâts, un bonus de 1 à sa marge de critique et un bonus de 1 au toucher.",
			"type" : "passif",
			"bonus" : {
				"fleaux": 2,
				"type_fleaux" : {
					"bonus_D_dmg": 1,
					"critique": 1
				}
			}
		}
		]
	},
	"Haches légères":{
		"type": "Arme",
		"Rang 1":[
		{
			"label":"Apprenti barbare",
			"description":"Lorsque le personnage manie une hache à une main, il gagne un bonus de 1 dégât, ainsi qu’un bonus de 1 sur sa marge de critique.",
			"type" : "passif",
			"bonus" : {
				"type_hache" : {
					"critique": 1,
					"bonus_dmg": 1
				}
			}
		},
		{
			"label":"Énergie sanguine",
			"description":"Au lieu des points de mana, le personnage peut directement puiser dans ses points de vie pour utiliser des compétences et dons de combat en mêlée.",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		}
		],
		"Rang 2":[
		{
			"label":"Shonen",
			"description":"Contre 5 points de mana, le personnage puise dans sa rage de vaincre, faisant que ses points de vie ne peuvent pas descendre en dessous de 1, pendant {{1}} tour(s).",
			"type" : "autre",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label":"Lancer brutal",
			"description":"La hache à une main peut être lancée sans malus. En revanche, il faudra aller la récupérer.",
			"type" : "passif",
			"bonus" : {

			}
		}
		],
		"Rang 3":[
		{
			"label":"Retour",
			"description":"Contre 2 points de mana, le personnage peut faire revenir sa hache dans sa main. Bien que l’action prenne un tour, la hache peut frapper plusieurs cibles lors de sa trajectoire linéaire. Afin d’esquiver, il faudra réussir un jet d’esquive simple (avec malus selon la position de l’ennemi face à l’arme).",
			"type" : "autre",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label":"Expert barbare",
			"description":"Lorsque le personnage manie une hache à une main, il gagne un bonus de 2D de dégâts, ainsi qu’un bonus de 1 sur sa marge de critique et à son toucher.",
			"type" : "passif",
			"bonus" : {
				"hache": 2,
				"type_hache" : {
					"bonus_D_dmg": 2,
					"critique": 1
				}
			}
		}
		],
		"Rang 4":[
		{
			"label":"Frénésie de coups",
			"description":"Lorsque le personnage réussit à toucher une même cible 3 fois, son prochain coup gagnera 2 de PA. Si l’attaque est parée 3 fois par un bouclier, celui-ci aura une chance de se briser sous l’assaut répété.",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		},
		{
			"label":"Slash",
			"description":"Contre 5 points de mana, le personnage se propulse en avant de son niveau en cases et assène un coup en quart de cercle devant lui (si le personnage manie deux haches, cela formera un arc de cercle). Cette technique a un bonus de 1 sur la marge de critique.",
			"type" : "damage",
			"cout" : "5 PM",
			"bonus" : {

			}
		}
		],
		"Rang 5":[
		{
			"label":"Trajectoire calculée",
			"description":"Lorsque l’arme du personnage est lancée sur une zone ou un ennemi, celui-ci à la capacité de se téléporter dans une zone de type 1 autour de l’impact. Cela consomme 5 points de mana et peut être utilisé dans le même tour que celui du lancer.",
			"type" : "autre",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label":"Meurtre",
			"description":"Lorsque le personnage achève une cible, sa marge de critique augmente de 3, jusqu’à avoir réalisé un coup critique. Si aucun coup critique n’est effectué durant l’heure qui suit, l’effet s’estompe.",
			"type" : "passif",
			"bonus" : {

			}
		}
		],
		"Rang 6":[
		{
			"label":"Maître barbare",
			"description":"Lorsque le personnage manie une hache à une main, il gagne un bonus de 1D aux dégâts de l’arme, un bonus de 2 à son toucher et une bonus de 1 sur sa marge de critique.",
			"type" : "passif",
			"bonus" : {
				"hache": 1,
				"esquive": 2,
				"type_hache" : {
					"bonus_D_dmg": 1,
					"critique": 1
				}
			}
		}
		]
	},
	"Masses ou marteaux légers":{
		"type": "Arme",
		"Rang 1":[
		{
			"label":"Apprenti homme d’armes",
			"description":"Lorsque le personnage manie une masse ou un marteau à une main, il gagne un bonus de 2 aux dégâts de l’arme.",
			"type" : "passif",
			"bonus" : {
				"type_marteau": {
					"bonus_dmg": 2
				}
			}
		},
		{
			"label":"Frappe de taille",
			"description":"Le personnage ignore les bonus octroyés par le casque d’un ennemi, à condition de viser la tête.",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		}
		],
		"Rang 2":[
		{
			"label":"Montagne",
			"description":"Chaque tour passé sans effectuer de déplacement, offre un bonus de 2 au personnage pour résister à tous types de manœuvres, hormis l’étourdissement.",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		},
		{
			"label":"Frappe de fer",
			"description":"Contre 4 points de mana, le personnage vise l’armure ou l’arme de son ennemi et charge un coup dévastateur. Si l’ennemi ne réussit pas un jet d’esquive ou de bouclier, son équipement devra obligatoirement subir un test de résistance.",
			"type" : "autre",
			"cout" : "4 PM",
			"bonus" : {

			}
		}
		],
		"Rang 3":[
		{
			"label":"Maladresse",
			"description":"Contre un jet de manœuvres, le personnage effectue une attaque basse circulaire et inflige un renversement aux ennemis situés à son contact. Chaque cible aura droit à un jet d’esquive ou d’acrobaties opposé.",
			"type" : "autre",
			"bonus" : {

			}
		},
		{
			"label":"Expert homme d’armes",
			"description":"Lorsque le personnage manie une masse ou un marteau à une main, il gagne un bonus de 2D aux dégâts de l’arme, ainsi qu’un bonus de 2 aux jets de manœuvres.",
			"type" : "passif",
			"bonus" : {
				"Manœuvres": 2,
				"type_marteau": {
					"bonus_D_dmg": 2
				}
			}
		}
		],
		"Rang 4":[
		{
			"label":"Gong",
			"description":"Si un ennemi est renversé (pas forcément par le personnage) et qu’il se trouve à portée de frappe, le personnage a le droit à une attaque d’opportunité.",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		},
		{
			"label":"Forceur",
			"description":"Le personnage va tout tenter pour effectuer un coup critique. Le joueur choisit le montant qu’il souhaite recevoir en bonus au critique et dépense le même coût en points de mana. On imposera également un malus au toucher équivalent à la valeur souhaitée, multipliée par deux.",
			"type" : "buff",
			"cout" : "x PM",
			"bonus" : {

			}
		}
		],
		"Rang 5":[
		{
			"label":"Vengeance",
			"description":"Toutes les 4 sources de dégâts subies, le personnage explose de colère et frappe gratuitement (et sans possibilité d’esquive) une cible se trouvant à portée, même si ce n’est pas son tour. L’action ne peut cependant pas recevoir d’améliorations, mais tolère l’ambidextrie.",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		},
		{
			"label":"Crétinisation",
			"description":"Contre 3 points de mana, le personnage effectue une attaque standard qui inflige également un état de confusion. L’attaque s’évitera de façon habituelle et il faudra réussir un jet de ténacité, opposé à la réussite du personnage, pour ne pas être confus. Si la défense rate, l’état perdurera jusqu’à réussir un jet de ténacité.",
			"type" : "debuff",
			"cout" : "3 PM",
			"bonus" : {

			}
		}
		],
		"Rang 6":[
		{
			"label":"Maître homme d’armes",
			"description":"Lorsque le personnage manie une masse ou un marteau à une main, il gagne un bonus de 1D aux dégâts de l’arme, un bonus de 2 au toucher, ainsi qu’un bonus à sa marge de critique de 1.",
			"type" : "passif",
			"bonus" : {
				"marteau": 1,
				"esquive": 2,
				"type_marteau": {
					"bonus_D_dmg": 1,
					"critique": 2
				}
			}
		}
		]
	},
	"Armes d’hast lourdes":{
		"type": "Arme",
		"Rang 1":[
		{
			"label":"Apprenti hallebardier",
			"description":"Lorsque le personnage manie une arme d’hast à deux mains, il gagne un bonus au toucher de 1, ainsi qu’un bonus sur sa marge de critique et aux dégâts de 1.",
			"type" : "passif",
			"bonus" : {
				"hast": 1,
				"type_hast" : {
					"bonus_dmg": 1,
					"critique": 1
				}
			}
		},
		{
			"label":"Va-t’en",
			"description":"Contre 2 points de mana, le personnage peut, lors d’une attaque normale, repousser une cible de 3 cases.",
			"type" : "autre",
			"cout" : "2 PM",
			"bonus" : {

			}
		}
		],
		"Rang 2":[
		{
			"label":"Virevolte",
			"description":"Lorsque le personnage effectue un coup critique, celui-ci peut se servir de son arme et du poids de son adversaire comme levier et se repositionner (dans la limite de la portée de son arme) autour de lui..",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		},
		{
			"label":"Puissance de frappe",
			"description":"Contre 2 points de mana, le personnage peut échanger 1 point (ajouté) de sa compétence d’arme, pour le transformer en point de dégât. Il est possible de transmuter autant de points que voulu, à condition de dépenser le mana à chaque fois.",
			"type" : "autre",
			"cout" : "2 PM par point transformé",
			"bonus" : {

			}
		}
		],
		"Rang 3":[
		{
			"label":"Balayage",
			"description":"Contre 4 points de mana, le personnage peut repousser toutes les cibles situées autour de lui.",
			"type" : "autre",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label":"Expert hallebardier",
			"description":"Lorsque le personnage manie une arme d’hast à deux mains, il gagne un bonus aux manœuvres de 2, un bonus aux dégâts de 1D et un bonus de 3 aux dégâts.",
			"type" : "passif",
			"bonus" : {
				"manoeuvres": 2,
				"type_hast" : {
					"bonus_dmg": 3,
					"bonus_D_dmg": 1
				}
			}
		}
		],
		"Rang 4":[
		{
			"label":"Découpage",
			"description":"Le personnage peut attaquer une cible, lorsque celle-ci s’apprête à être repoussée. Le personnage ne pourra porter qu’une seule attaque par cible et celle-ci ne pourra bénéficier d’aucune amélioration..",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		},
		{
			"label":"Tourbillon",
			"description":"Le personnage fait tourner son arme et en augmente la vitesse à l’aide de 4 points de mana par tour. Ainsi, il pourra frapper en ligne droite et parallèlement à sa position, afin de toucher toutes les cibles s’y trouvant. La portée de l’arme sera divisée entre la partie avant de l’arme et la partie arrière, la main du personnage étant le centre.",
			"type" : "damage",
			"cout" : "4 PM par tour",
			"bonus" : {

			}
		}
		],
		"Rang 5":[
		{
			"label":"Allonge",
			"description":"Contre 4 points de mana et de façon instantanée, l’arme peut croître et doubler son allonge le temps d’un tour.",
			"type" : "buff",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label":"Embrochement",
			"description":"Contre 5 points de mana, le personnage déchaîne sa force et empale toutes unités alignées, dans la mesure de la portée de son arme. Toutes les cibles situées derrière la première sont considérées comme embusquées et subissent un malus de 6 à l’esquive.",
			"type" : "damage",
			"cout" : "5 PM",
			"bonus" : {

			}
		}
		],
		"Rang 6":[
		{
			"label":"Maître hallebardier",
			"description":"Lorsque le personnage manie une arme d’hast à deux mains, il gagne un bonus au toucher de 2, un bonus de dégâts de 1D, un bonus de 1 aux dégâts et un bonus de 1 aux manœuvres.",
			"type" : "passif",
			"bonus" : {
				"hast": 2,
				"manoeuvres": 1,
				"type_hast" : {
					"bonus_D_dmg": 1,
					"bonus_dmg": 1
				}
			}
		}
		]
	},
	"Épées lourdes":{
		"type": "Arme",
		"Rang 1":[
		{
			"label":"Apprenti épéiste",
			"description":"Lorsque le personnage manie une épée à deux mains, il gagne un bonus aux dégâts de 1, ainsi qu’un bonus sur sa marge de critique de 1.",
			"type" : "passif",
			"bonus" : {
				"type_epee" : {
					"bonus_dmg": 1,
					"critique": 1
				}
			}
		},
		{
			"label":"Accumulation",
			"description":"À chaque fois que le personnage subit une attaque, celui-ci peut dépenser 2 points de mana, afin de stocker sa rage et la faire exploser par la suite. Chaque esquive peut faire gagner 1D{{1}}, qui sera ajouté automatiquement au prochain coup critique d’attaque.",
			"type" : "damage",
			"cout" : "2 PM",
			"bonus" : {
			}
		}
		],
		"Rang 2":[
		{
			"label":"Garde particulière",
			"description":"En vue de la taille de son arme, le personnage peut utiliser un jet d’arme pour supplanter un jet de bouclier et ainsi éviter certaines attaques spécifiques.",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		},
		{
			"label":"Croix",
			"description":"Contre 4 points de mana, l’épéiste plante son épée dans le sol et envoie des lames d’énergie tranchantes, formants une croix de 2 cases de long autour du lanceur. Les dégâts de l’arme sont alors divisés par 2, mais passent en dégâts magiques.",
			"type" : "damage",
			"cout" : "4 PM",
			"bonus" : {

			}
		}
		],
		"Rang 3":[
		{
			"label":"Enchaînement",
			"description":"En cas de mise en coma d’un adversaire, le personnage a le droit à une attaque gratuite, si un autre antagoniste se trouve à portée. Il est possible d’utiliser 2 points de mana par case de déplacement, afin d’atteindre une autre cible éloignée. Le nombre de cases de déplacement restantes au personnage ne peut cependant pas être dépassé.",
			"type" : "passif_situationnel",
			"cout" : "2 PM (optionnel)",
			"bonus" : {

			}
		},
		{
			"label":"Expert épéiste",
			"description":"Lorsque le personnage manie une épée à deux mains, il gagne un bonus de dégâts de 1D, un bonus de 1 au toucher et un bonus aux dégâts de 2.",
			"type" : "passif",
			"bonus" : {
				"epee": 1,
				"type_epee" : {
					"bonus_D_dmg": 1,
					"bonus_dmg": 2
				}
			}
		}
		],
		"Rang 4":[
		{
			"label":"Chargez !",
			"description":"Contre 4 points de mana, l’épéiste plante son épée dans le sol, envoyant des lames d’énergie tranchantes sur deux cases devant et derrière lui ainsi que sur sa gauche et sa droite. (Forme une croix de 2 cases de long autour du lanceur) Les dégâts de l’arme sont alors divisés par 2, mais passent en dégâts magiques.",
			"type" : "damage",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label":"Aiguisage mystique",
			"description":"Contre 5 points de mana par tour, le personnage ajuste magiquement sa lame afin que tous les scores en RD de ses ennemis soient ignorés.",
			"type" : "buff",
			"cout" : "5 PM par tour",
			"bonus" : {

			}
		}
		],
		"Rang 5":[
		{
			"label":"Lame sorcière",
			"description":"Contre 8 points de mana et un jet de psychologie réussi, l’attaque de ce tour ne peut pas rater sa cible. Si le jet de psychologie échoue, le coût en mana sera dépensé, mais l’attaque sera tout ce qu’il y a de plus classique.",
			"type" : "buff",
			"cout" : "8 PM",
			"bonus" : {

			}
		},
		{
			"label":"Équilibre naturel",
			"description":"Chaque ennemi achevé par le personnage ajoute une charge d’anima (avec pour maximum, l’intelligence divisée par 8), l’anima permet l’annulation pure et simple d’une attaque ou d’un sort qui aurait dû normalement toucher.",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		}
		],
		"Rang 6":[
		{
			"label":"Maître épéiste",
			"description":"Lorsque le personnage manie une épée à deux mains, il gagne un bonus au toucher de 2, un bonus de dégâts de 1D, un bonus de 1 à sa marge de critique et un bonus de 1 aux manœuvres.",
			"type" : "passif",
			"bonus" : {
				"manoeuvres": 1,
				"epee": 2,
				"type_epee" : {
					"bonus_D_dmg": 1,
					"critique": 1
				}
			}
		}
		]
	},
	"Fléaux lourds":{
		"type": "Arme",
		"Rang 1":[
		{
			"label":"Apprenti homme d’arme",
			"description":"Lorsque le personnage manie un fléau lourd, il gagne un bonus de 1 de dégât, ainsi qu’un bonus de 1 sur ses jets de manœuvres.",
			"type" : "passif",
			"bonus" : {
				"manoeuvres": 1,
				"type_fleaux" : {
					"bonus_dmg": 1
				}
			}
		},
		{
			"label":"Pointes exacerbées",
			"description":"Contre 3 points de mana, le personnage peut gagner un bonus à la pénétration de 1. Il est possible de cumuler l’effet et le coût, pour un bonus maximum de 4. L’effet perdure pendant 1D2 +{{1}} tours.",
			"type" : "buff",
			"cout" : "3 PM par augmentation",
			"bonus" : {

			}
		}
		],
		"Rang 2":[
		{
			"label":"Ligne de piques",
			"description":"Contre 4 points de mana, le personnage frappe son arme sur le sol et génère une continuité des piques de son arme en ligne droite. L’attaque s’étendra sur {{1}} case(s), avec une PA de 4 et infligera 1D8 + {{1}} dégâts physiques.",
			"type" : "damage",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label":"Bris’arme",
			"description":"À chaque fois que le personnage réalise un coup critique sur un jet d’arme, celui-ci à une chance de briser l’arme de son assaillant. On lancera alors un jet de résistance, pour voir si l’arme tient ou non le choc.",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		}
		],
		"Rang 3":[
		{
			"label":"Coups latéraux",
			"description":"Lorsque le personnage attaque, il peut toucher une cible principale, ainsi que deux autres ennemis. Le joueur choisira soit de taper de deux cases à droite ou à gauche de la première cible, ou bien d’une case de chaque côté. Ne se combine pas avec des manœuvres ou coups spéciaux incluant l’arme.",
			"type" : "damage",
			"bonus" : {

			}
		},
		{
			"label":"Expert homme d’arme",
			"description":"Lorsque le personnage manie un fléau lourd, il gagne un bonus de 2D de dégâts, ainsi qu’un bonus de 1 au critique.",
			"type" : "passif",
			"bonus" : {
				"type_fleaux" : {
					"bonus_D_dmg": 2,
					"critique": 1
				}
			}
		}
		],
		"Rang 4":[
		{
			"label":"Force d’attaque",
			"description":"Contre 4 points de mana, le personnage peut augmenter sa marge de coup critique de 2 sur sa compétence d’arme, pendant son niveau en tours.",
			"type" : "buff",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label":"Repoussement",
			"description":"Contre 5 points de mana, le personnage fait tourner son arme à grande vitesse et repousse (de son niveau en cases) tous les adverses se trouvant à son contact. La défense ne pourra se faire que sur un jet de bouclier simple. Sinon, il faudra également réussir un jet d’acrobaties opposé, afin de ne pas être renversé après avoir été repoussé.",
			"type" : "autre",
			"cout" : "5 PM",
			"bonus" : {

			}
		}
		],
		"Rang 5":[
		{
			"label":"Décuplement",
			"description":"Contre 6 points de mana, le personnage augmente drastiquement la taille de sa masse et augmente ainsi ses dégâts de 1D durant {{1}} tour(s).",
			"type" : "buff",
			"cout" : "6 PM",
			"bonus" : {

			}
		},
		{
			"label":"Éboulement",
			"description":"Contre 8 points de mana, le personnage abat son arme sur une case (si elle est occupée, cela compte comme une attaque), se sert de l’inertie afin de se projeter deux cases plus loin (et possiblement passer au-dessus une cible, sans provoquer d’attaque d’opportunité) et écrase de nouveau son arme sur une case adjacente en profitant de dégâts doublés sur l’attaque.",
			"type" : "damage",
			"cout" : "8 PM",
			"bonus" : {

			}
		}
		],
		"Rang 6":[
		{
			"label":"Maître homme d’arme",
			"description":"Lorsque le personnage manie un fléau lourd, il gagne un bonus de 1D aux dégâts de l’arme, ainsi qu’un bonus de 2 à son toucher, un bonus de 1 aux manœuvres et sur sa marge de critique.",
			"type" : "passif",
			"bonus" : {
				"fleaux": 2,
				"manoeuvres": 1,
				"type_fleaux" : {
					"bonus_D_dmg": 1,
					"critique": 1
				}
			}
		}
		]
	},
	"Haches lourdes":{
		"type": "Arme",
		"Rang 1":[
		{
			"label":"Apprenti ostrogoth",
			"description":"Lorsque le personnage manie une hache à deux mains, il gagne un bonus de 1 au toucher, ainsi qu’un bonus de 1 sur sa marge de critique.",
			"type" : "passif",
			"bonus" : {
				"hache": 1,
				"type_hache" : {
					"critique": 1
				}
			}
		},
		{
			"label":"Téméraire",
			"description":"Le personnage gagne un bonus de {{1}} sur ses jets d'intimidation, lorsqu’il tient sa hache en main.",
			"type" : "passif_situationnel",
			"bonus" : {
			}
		}
		],
		"Rang 2":[
		{
			"label":"Frappe de sang",
			"description":"Contre 3 points de mana, l’attaque du tour infligera en plus un état de saignement si la cible échoue un jet de ténacité simple. De plus, durant {{1}} tour(s), chaque coup porté à la cible augmentera les dégâts des saignements de 1.",
			"type" : "buff",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		{
			"label":"Projection",
			"description":"Contre 4 points de mana, le personnage projette un double éthéré de son arme sur une case située à un maximum de 8 mètres et inflige 1D10 + {{1}} dégâts magiques.",
			"type" : "damage",
			"cout" : "4 PM",
			"bonus" : {

			}
		}
		],
		"Rang 3":[
		{
			"label":"Agression ! Réaction !",
			"description":"Lorsque le personnage réussit une esquive ou un blocage, celui-ci a le droit de tenter une attaque gratuite (simple) envers son assaillant.",
			"type" : "passif_situationnel",
			"bonus" : {
			}
		},
		{
			"label":"Expert ostrogoth",
			"description":"Lorsque le personnage manie une hache à deux mains, il gagne un bonus de 1D de dégâts, ainsi qu’un bonus de 3 à ceux-ci.",
			"type" : "passif",
			"bonus" : {
				"type_hache" : {
					"bonus_dmg": 3,
					"bonus_D_dmg": 1
				}
			}
		}
		],
		"Rang 4":[
		{
			"label":"Explosion sanguine",
			"description":"Lorsqu’un ennemi a passé un tour à saigner, le personnage peut instantanément dépenser 2 points de mana afin de lui infliger 2D4 dégâts inesquivables et irréductibles. Si la cible passe plusieurs tours consécutifs à saigner, il est possible de combiner les effets et le coût.",
			"type" : "damage",
			"cout" : "x fois 2 PM",
			"bonus" : {
				
			}
		},
		{
			"label":"Slash",
			"description":"Contre 5 points de mana, le personnage se propulse en avant de son niveau en cases et assène un coup en quart de cercle devant lui (si le personnage manie deux haches, cela formera un arc de cercle). Cette technique a un bonus de 1 sur la marge de critique.",
			"type" : "damage",
			"cout" : "5 PM",
			"bonus" : {

			}
		}
		],
		"Rang 5":[
		{
			"label":"Fendoir",
			"description":"Contre 4 points de mana, le personnage peut tripler la valeur de son bonus de dégâts pour le tour. La capacité ne s’applique que sur une arme à la fois, il faudra doubler le coût en mana pour l’appliquer à deux armes en même temps.",
			"type" : "buff",
			"cout" : "4 ou 8 PM",
			"bonus" : {

			}
		},
		{
			"label":"Meurtre",
			"description":"Lorsque le personnage achève une cible, sa marge de critique augmente de 3, jusqu’à avoir réalisé un coup critique. Si aucun coup critique n’est effectué durant l’heure qui suit, l’effet s’estompe.",
			"type" : "passif",
			"bonus" : {

			}
		}
		],
		"Rang 6":[
		{
			"label":"Maître ostrogoth",
			"description":"Lorsque le personnage manie une hache à deux mains, il gagne un bonus de 1D aux dégâts de l’arme, ainsi qu’un bonus de 2 au son toucher, un bonus de 1 sur sa marge et un bonus aux manœuvres de 1.",
			"type" : "passif",
			"bonus" : {
				"hache": 2,
				"manoeuvres": 1,
				"type_hache" : {
					"bonus_D_dmg": 1,
					"critique": 1
				}
			}
		}
		]
	},
	"Masses et marteaux lourds":{
		"type": "Arme",
		"Rang 1":[
		{
			"label":"Apprenti broyeur",
			"description":"Lorsque le personnage manie une masse ou un marteau lourd, il gagne un bonus de 1 au toucher, ainsi qu’à ses dégâts.",
			"type" : "passif",
			"bonus" : {
				"marteau": 1,
				"type_marteau" : {
					"bonus_dmg": 1
				}
			}
		},
		{
			"label":"Dégage !",
			"description":"Lorsqu’il réussit une attaque, le personnage peut déplacer la cible d’un nombre de cases égal à son bonus de force (maximum). Chaque case déplacée coûte 1 point de mana. Si la cible est plus grande que le personnage, le coût est doublé. Cette capacité ne peut pas être utilisée si la différence de taille est trop importante.",
			"type" : "autre",
			"cout" : "1 à 2 PM par case",
			"bonus" : {

			}
		}
		],
		"Rang 2":[
		{
			"label":"Souffle coupé",
			"description":"Lorsqu’il effectue un coup critique, le personnage empêche également sa cible d’utiliser des capacités spéciales jusqu’à la fin de son prochain tour.",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		},
		{
			"label":"Brise garde",
			"description":"En action bonus, le personnage peut, après avoir effectué une attaque, tenter un jet de manœuvre opposé à l’esquive ou à la compétence de bouclier de l’ennemi. En cas de succès, l’ennemi subit un malus de {{1}} sur ces compétences. L’effet ne se cumule pas et persiste jusqu’à la fin du combat.",
			"type" : "debuff",
			"bonus" : {

			}
		}
		],
		"Rang 3":[
		{
			"label":"Destruction",
			"description":"Contre 5 points de mana, le personnage frappe son adversaire avec puissance et précision, réduisant sa RD de {{1}} et ce pendant {{1}} tours.",
			"type" : "debuff",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label":"Expert broyeur",
			"description":"Lorsque le personnage manie une masse ou un marteau lourd, il gagne un bonus de 1D de dégâts, ainsi qu’un bonus de 3 dégâts.",
			"type" : "passif",
			"bonus" : {
				"type_marteau" : {
					"bonus_D_dmg": 1,
					"bonus_dmg": 3
				}
			}
		}
		],
		"Rang 4":[
		{
			"label":"Lancer",
			"description":"Contre 3 points de mana, le personnage saisit son arme et la lance avec une force incroyable. Cette dernière vole sur une distance équivalant au bonus de force × 2 du personnage et inflige les dégâts habituels. Pour la récupérer, il faudra dépenser 2 points de mana supplémentaires.",
			"type" : "damage",
			"cout" : "3 + 2 PM",
			"bonus" : {

			}
		},
		{
			"label":"Ébranlement Terrestre",
			"description":"Contre 6 points de mana, le personnage frappe le sol, créant une onde de choc dans un rayon de {{1}} case(s) autour de lui. Les ennemis touchés subissent 1D{{1}} + bonus de force de dégâts et doivent réussir un jet d’acrobatie opposé. En cas d'échec, ils chuteront.",
			"type" : "damage",
			"cout" : "6 PM",
			"bonus" : {

			}
		}
		],
		"Rang 5":[
		{
			"label":"K.O",
			"description":"Lorsque le personnage réalise un coup critique, les dégâts effectués ne sont plus doublés, mais triplés.",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		},
		{
			"label":"Tourbillon écrasant",
			"description":"Le personnage tourbillonne en utilisant le poids de son arme, infligeant des dégâts en zone autour de lui, tout en se déplaçant jusqu’à la moitié (arrondi inférieur) de ses cases de déplacement. Cette capacité ne protège pas des attaques d’opportunité. Il peut la maintenir à chaque tour, mais doit réussir un jet de ténacité dès le deuxième tour, afin de continuer à tournoyer. En cas d’échec, il est pris de vomissements pour le tour et ne pourra réutiliser la capacité pendant les deux tours suivants. Les malus s’aggravent de 2 à chaque tour supplémentaire passé à tourner.",
			"type" : "damage",
			"bonus" : {

			}
		}
		],
		"Rang 6":[
		{
			"label":"Maître broyeur",
			"description":"Lorsque le personnage manie une masse ou un marteau lourd, il gagne un bonus de 1D aux dégâts de l’arme, un bonus de 1 au toucher, ainsi qu’un bonus de 2 aux manœuvres.",
			"type" : "passif",
			"bonus" : {
				"marteau": 1,
				"manoeuvres": 2,
				"type_marteau" : {
					"bonus_D_dmg": 1
				}
			}
		},
		]
	},
	"Arbalètes":{
		"type": "Arme",
		"Rang 1":[
		{
			"label":"Apprenti arbalétrier",
			"description":"Lorsque le personnage manie une arbalète, il gagne un bonus au toucher de 1, ainsi qu’un bonus de dégâts de 1.",
			"type" : "passif",
			"bonus" : {
				"arbalete": 1,
				"type_arbalete": {
					"bonus_dmg": 1
				}
			}
		},
		{
			"label":"Recharge rapide",
			"description":"Contre 2 points de mana, pour les arbalètes lourdes, si le personnage n’a pas bougé durant le tour (le pas de côté est autorisé), la recharge est instantanée. Alors que les arbalètes légères bénéficieront d’un bonus de 4 au toucher.",
			"type" : "passif_situationnel (arbalètes légères)",
			"cout" : "2 PM (arbalètes lourdes)",
			"bonus" : {

			}
		}
		],
		"Rang 2":[
		{
			"label":"Adresse",
			"description":"Contre 3 points de mana, l’arbalétrier peut augmenter les dégâts de sa prochaine attaque d’une classe de dé supplémentaire.",
			"type" : "buff",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		{
			"label":"Acuité",
			"description":"L’action de viser, octroie toujours un bonus de 2 au toucher, mais également un bonus de 1 à la PA (cumulable jusqu’à 4 fois maximum)",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		}
		],
		"Rang 3":[
		{
			"label":"Enfilage",
			"description":"Contre 4 points de mana, le personnage fait s’allonger et s’épaissir son carreau et le propulse. Il embroche alors deux entités se trouvant sur des cases adjacentes, les blesse toutes deux et les empêche de se mouvoir indépendamment l’une de l’autre. Une unité peut également se retrouver bloquée sur un décor. Si un ennemi occupe plusieurs cases, on considère que seule une partie du corps est harponnée.",
			"type" : "damage",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label":"Expert arbalétrier",
			"description":"Lorsque le personnage manie une arbalète, il gagne un bonus au toucher et à sa marge de critique de 1. De plus, ses dégâts sont augmentés de 2D.",
			"type" : "passif",
			"bonus" : {
				"arbalete": 1,
				"type_arbalete": {
					"critique": 1,
					"bonus_D_dmg": 2
				}
			}
		}
		],
		"Rang 4":[
		{
			"label":"Baïonnette cinglante",
			"description":"Le personnage peut utiliser son arme au corps-à-corps. L’arme fera les dégâts normaux, ne consommera pas de carreaux et l’action se fera sans malus.",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		},
		{
			"label":"Remparts de la sentinelle",
			"description":"Contre 3 points de mana et durant {{1}} tours, le personnage génère un couvert derrière lequel il peut tirer en réduisant son exposition. Il gagne alors un bonus de 4 à l’esquive tant qu’il reste derrière.",
			"type" : "autre",
			"cout" : "3 PM",
			"bonus" : {

			}
		}
		],
		"Rang 5":[
		{
			"label":"Harcèlement",
			"description":"Le personnage peut désormais infliger des attaques d’opportunité à distance. Tout ennemi se déplaçant dans une zone de {{2}} mètres recevra une attaque (simple) de la part du personnage.",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		},
		{
			"label":"Réceptacle",
			"description":"Contre 3 points de mana, le personnage enchante une munition et peut la faire imprégner d’un sort qui se consumera à l’impact. Seuls les sorts non améliorés et ayant une zone d’effet peuvent fonctionner.",
			"type" : "autre",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		],
		"Rang 6":[
		{
			"label":"Maître arbalétrier",
			"description":"Lorsque le personnage manie une arbalète, il gagne un bonus aux dégâts de 1D, un bonus sur la marge de critique de 1, ainsi qu’un bonus de 3 à la perception.",
			"type" : "passif",
			"bonus" : {
				"perception": 3,
				"type_arbalete": {
					"bonus_D_dmg": 1,
					"critique": 1
				}
			}
		}
		]
	},
	"Arcs":{
		"type": "Arme",
		"Rang 1":[
		{
			"label":"Apprenti archer",
			"description":"Lorsque le personnage manie un arc, il gagne un bonus au toucher de 1, ainsi qu’un bonus de dégâts de 1.",
			"type" : "passif",
			"bonus" : {
				"arc": 1,
				"type_arc": {
					"bonus_dmg": 1
				}
			}
		},
		{
			"label":"Flèche empoisonnée",
			"description":"Contre 3 points de mana, l’archer altère magiquement sa flèche. Les dégâts restent les mêmes, mais lorsqu’elle touche une cible, celle-ci entre dans l’état nauséeux jusqu’à la fin du combat si elle rate un jet de ténacité simple.",
			"type" : "debuff",
			"cout" : "3 PM",
			"bonus" : {

			}
		}
		],
		"Rang 2":[
		{
			"label":"Compas dans l’œil",
			"description":"Réduit de 1 les malus liés aux incréments de portée de l’arme.",
			"type" : "passif",
			"bonus" : {

			}
		},
		{
			"label":"Acuité",
			"description":"L’action de viser, octroie désormais un bonus de 3 au toucher et 1 au critique (cumulable jusqu’à trois fois).",
			"type" : "passif",
			"bonus" : {

			}
		}
		],
		"Rang 3":[
		{
			"label":"Déluge",
			"description":"Contre 4 points de mana et un jet d’arcs, le personnage tire une épaisse flèche en l’air qui va ensuite se morceler en une multitude de flèches. Celles-ci vont retomber dans une zone de type 2 et infliger 2D6 + {{1}} de dégâts magiques.",
			"type" : "damage",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label":"Expert archer",
			"description":"Lorsque le personnage manie un arc, il gagne un bonus aux dégâts de 2D, ainsi qu’un bonus à son toucher et à sa marge de critique de 1.",
			"type" : "passif",
			"bonus" : {
				"arc": 1,
				"type_arc": {
					"bonus_D_dmg": 2,
					"critique": 1
				}
			}
		}
		],
		"Rang 4":[
		{
			"label":"Seconde chance",
			"description":"Lorsque le personnage rate une attaque suite à une esquive ennemie (pas bouclier), il peut utiliser 5 points de mana afin de la faire fuser de nouveau et attaquer la cible dans le dos. L’ennemi devra alors se défendre deux fois, cumulant les malus d’attaque dans le dos tout en étant prise au dépourvu. La flèche ne pourra cependant plus bénéficier d’améliorations.",
			"type" : "damage",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label":"Flèche explosive",
			"description":"Contre 5 points de mana, l’archer modifie l’embout de sa flèche pour que celui-ci explose au moindre impact. Au centre de l’explosion, la cible subira 4D6 dégâts de feu, à 1 case 3D6, à 2 cases 2D6 et 3 cases 1D6. Un jet d’acrobatie simple sera nécessaire pour esquiver l’attaque.",
			"type" : "damage",
			"cout" : "5 PM",
			"bonus" : {

			}
		}
		],
		"Rang 5":[
		{
			"label":"Headshot",
			"description":"Le malus imposé au tir à la tête est réduit de 6.",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		},
		{
			"label":"Tir perçant",
			"description":"Contre 8 points de mana, l’archer bande son arc et insuffle son énergie magique dans une flèche unique. Celle-ci infligera 2D10 + {{1}} en dégâts magiques et touchera toutes les cibles alignées à la première, sur une longueur de 10 cases.",
			"type" : "damage",
			"cout" : "8 PM",
			"bonus" : {

			}
		}
		],
		"Rang 6":[
		{
			"label":"Maître archer",
			"description":"Lorsque le personnage manie un arc, il gagne un bonus aux dégâts de 1D, un bonus sur la marge de critique de 1, ainsi qu’un bonus de 3 au toucher.",
			"type" : "passif",
			"bonus" : {
				"arc": 3,
				"type_arc": {
					"bonus_D_dmg": 1,
					"critique": 2
				}
			}
		}
		]
	},
	"Armes de jet":{
		"type": "Arme",
		"Rang 1":[
		{
			"label":"Apprenti lanceur",
			"description":"Lorsque le personnage manie une arme de jet, celui-ci gagne un bonus aux dégâts de 3 sur ce type d’arme.",
			"type" : "passif",
			"bonus" : {
				"type_jet": {
					"bonus_dmg": 3
				}
			}
		},
		{
			"label":"Retour",
			"description":"Un filon magique invisible rattache l’arme au personnage et permet de la faire revenir dans la main après avoir été lancé, sans utiliser d’action supplémentaire. ",
			"type" : "passif",
			"bonus" : {

			}
		}
		],
		"Rang 2":[
		{
			"label":"Vivacité I",
			"description":"Le personnage peut désormais lancer 2 projectiles en un tour, à la suite d’un jet malussé de 2. Si le personnage possède le don double encoche, le nombre de lancers passe à 4. Il faudra alors deux jets et cumuler les malus.",
			"type" : "passif",
			"bonus" : {

			}
		},
		{
			"label":"Virtuosité",
			"description":"Contre 2 points de mana, le bonus de dégât lié à la force n’est plus exclusif à la main directrice et peut être appliqué sur chacun des lancers",
			"type" : "buff",
			"cout" : "2 PM",
			"bonus" : {

			}
		}
		],
		"Rang 3":[
		{
			"label":"Crampon",
			"description":"Lorsque l’arme se plante dans un décor ou un ennemi, le personnage peut dépenser 2 points de mana afin de se tirer vers sa cible, ou la tirer jusqu’à lui.",
			"type" : "autre",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label":"Expert lanceur",
			"description":"Lorsque le personnage manie une arme de jet, celui-ci gagne un bonus de 1D de dégât et un bonus de 2 sur la marge de critique.",
			"type" : "passif",
			"bonus" : {
				"type_jet": {
					"bonus_D_dmg": 1,
					"critique": 2
				}
			}
		}
		],
		"Rang 4":[
		{
			"label":"Vivacité II",
			"description":"Le personnage peut désormais lancer 3 projectiles en un tour, à la suite d’un jet malussé de 4. Si le personnage possède le don double encoche, le nombre de lancers passe à 6. Il faudra alors deux jets et cumuler les malus.",
			"type" : "passif",
			"bonus" : {

			}
		},
		{
			"label":"Force accrue",
			"description":"Contre 3 points de mana et durant {{1}} tours, le personnage apporte magiquement une impulsion à ses projectiles et permet d’augmenter la portée d’attaque de {{2}}.",
			"type" : "buff",
			"cout" : "3 PM",
			"bonus" : {

			}
		}
		],
		"Rang 5":[
		{
			"label":"Arme liée",
			"description":"Le personnage peut augmenter ses chances de toucher grâce à son énergie spirituelle. Par tranche de 2 points de mana utilisés, le toucher augmente de 1.",
			"type" : "buff",
			"cout" : "x fois 2 PM",
			"bonus" : {

			}
		},
		{
			"label":"Lignes haute tension",
			"description":"Pour chaque projectile planté dans un adversaire, le personnage peut dépenser 4 points de mana afin d’en électrifier le fil et infliger 2D{{1}} de dégâts magiques supplémentaires.",
			"type" : "damage",
			"cout" : "4 PM par fil",
			"bonus" : {

			}
		}
		],
		"Rang 6":[
		{
			"label":"Maître lanceur",
			"description":"Lorsque le personnage, manie une arme de lancer, celui-ci gagne un bonus de 1D de dégât et un bonus de 3 sur la marge de critique et à ses dégâts en maniant ce type d’arme.",
			"type" : "passif",
			"bonus" : {
				"type_jet": {
					"bonus_D_dmg": 1,
					"critique": 3,
					"bonus_dmg": 3
				}
			}
		}
		]
	},
	"Fouets et chaînes":{
		"type": "Arme",
		"Rang 1":[
		{
			"label":"Apprenti tortionnaire",
			"description":"Lorsque le personnage manie un fouet ou une chaîne, il gagne un bonus au toucher de 1, ainsi qu’un bonus sur sa marge de critique de 1.",
			"type" : "passif",
			"bonus" : {
				"fouet": 1,
				"type_fouet": {
					"critique": 1
				}
			}
		},
		{
			"label":"Saisie",
			"description":"Permet au personnage de saisir un objet de moins de 10 kg et de l’amener vers lui. Saisie permet également de lancer un objet agrippé deux fois plus loin et même de se hisser.",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		}
		],
		"Rang 2":[
		{
			"label":"Réimplantation",
			"description":"Contre 2 points de mana, le personnage agrippe une unité (pesant moins de son score en force × 8) et la déplace sur une autre case non occupée, dans la limite de sa portée. Il sera possible d’esquiver à l’aide d’un jet d’acrobaties opposé. Néanmoins, le personnage ne pourra déposer une cible à un endroit qu’il ne voit pas.",
			"type" : "autre",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label":"Atterrissage brutal",
			"description":"Contre 4 points de mana supplémentaires, additionnés à réimplantation, le personnage peut violemment projeter contre le sol un ennemi agrippé. Cette attaque triple alors les dégâts octroyés par le bonus de force.",
			"type" : "damage",
			"cout" : "4 PM",
			"bonus" : {

			}
		}
		],
		"Rang 3":[
		{
			"label":"Toile",
			"description":"Entre ses tours, le personnage laisse son arme pendre sur le sol et se multiplier magiquement, formant des sortes de racines. Contre 2 points de mana par tour, les entités entrant dans sa zone d’attaque sont considérées comme étant sur un terrain difficile.",
			"type" : "autre",
			"cout" : "2 PM par tour",
			"bonus" : {

			}
		},
		{
			"label":"Expert tortionnaire",
			"description":"Lorsque le personnage manie un fouet ou une chaîne, il gagne un bonus au toucher de 1, un bonus aux dégâts de 2D et l’allonge est augmentée de 1.",
			"type" : "passif",
			"bonus" : {
				"fouet": 1,
				"type_fouet": {
					"portee" : 1,
					"bonus_D_dmg": 2
				}
			}
		}
		],
		"Rang 4":[
		{
			"label":"Sadisme",
			"description":"Lorsque le personnage réussit une intimidation, celui-ci peut attaquer automatiquement, avec une attaque simple (sans activables), les cibles ayant raté un jet de volonté opposé. Une cible ne peut être intimidée qu’une seule fois.",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		},
		{
			"label":"Entrechoquement",
			"description":"Le personnage utilise ses deux armes (s’il n’en possède qu’une, l’autre devra être créée magiquement, contre 4 points de mana) et réalise un jet de manœuvres afin d’agripper deux cibles qui rateraient un jet d’esquive. Il les fait alors s’entrechoquer l’une contre l’autre. La manœuvre inflige les dégâts de l’arme, mais rend les cibles étourdies si elles ratent un jet de ténacité opposé. Cela a également pour effet de replacer les deux cibles devant le personnage.",
			"type" : "damage",
			"cout" : "4 PM (optionnel)",
			"bonus" : {

			}
		}
		],
		"Rang 5":[
		{
			"label":"Kinky",
			"description":"Le personnage frappe un allié avec son fouet, mais de manière à ne pas le blesser. Contre 5 points de mana, l’allié est motivé et gagne un bonus de {{1}} à toutes ses actions durant le combat.",
			"type" : "buff",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label":"Chaîne nébulaire",
			"description":"Contre 3 points de mana par cible et par tour, le personnage génère des extensions de son arme, qui vont venir toucher toutes les cibles présentes dans sa portée d’attaque.",
			"type" : "buff",
			"cout" : "x PM",
			"bonus" : {

			}
		}
		],
		"Rang 6":[
		{
			"label":"Maître tortionnaire",
			"description":"Lorsque le personnage manie un fouet ou une chaîne, il gagne un bonus à sa marge de critique de 2, un bonus aux manœuvres de 1, ainsi qu’un bonus aux dégâts de 2D.",
			"type" : "passif",
			"bonus" : {
				"esquive": 1,
				"type_fouet": {
					"critique": 2,
					"bonus_D_dmg": 2
				}
			}
		}
		]
	},
	"Boucliers":{
		"type": "Arme",
		"Rang 1":[
		{
			"label":"Apprenti garde",
			"description":"Lorsque le personnage manie un bouclier, il gagne un bonus de 1 à la RD et 1 au critique.",
			"type" : "passif",
			"bonus" : {
				"RD": 1,
				"type_bouclier": {
					"critique": 1
				}
			}
		},
		{
			"label":"Charge étourdissante",
			"description":"Lorsqu’il effectue une charge, contre 2 points de mana, le personnage (en plus des effets habituels) se donne une impulsion et ajoute les dégâts de son bouclier. De plus, si la cible rate un jet de ténacité simple, elle sera étourdie.",
			"type" : "damage",
			"cout" : "2 PM",
			"bonus" : {

			}
		}
		],
		"Rang 2":[
		{
			"label":"Coup de bouclier",
			"description":"Contre 3 points de mana et après un blocage, le personnage peut effectuer une riposte instantanée avec son arme et son bouclier.",
			"type" : "damage",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		{
			"label":"Rempart de sang",
			"description":"À chaque fois que le personnage subit des dégâts, il gagne 1 de RM ou 1 de RD (maximum égal au niveau) en fonction du type d’attaque. Cependant, le moindre échec d’un jet d’attaque remet ce bonus à 0",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		}
		],
		"Rang 3":[
		{
			"label":"Flexibilité",
			"description":"Appliquez le rang 3 avec le nom « EXPERT » du don évolutif d’arme que vous souhaitez à la place de cette aptitude.",
			"type" : "passif",
			"choix" : {
				"rang": 3,
				"type": "Arme"
			},
			"bonus" : {

			}
		},
		{
			"label":"Expert garde",
			"description":"Lorsque le personnage manie un bouclier, il gagne un bonus de 1 à la RD et 1 au critique.",
			"type" : "passif",
			"bonus" : {
				"RD": 1,
				"type_bouclier": {
					"critique": 1
				}
			}
		}
		],
		"Rang 4":[
		{
			"label":"Gardien du royaume",
			"description":"Contre 5 points de mana, le personnage se place entre un allié et un ennemi qui l’attaque (à condition qu’il soit à votre portée de déplacement, sans course). Le personnage effectue alors un jet de bouclier à la place de l’allié, avec comme malus, le Fp de l’attaquant.",
			"type" : "autre",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label":"Assimilation",
			"description":"Chaque fois que le personnage bloque une attaque de mêlée avec son bouclier, celui-ci aspire son {{1}} points de mana et les vole à sa cible.",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		}
		],
		"Rang 5":[
		{
			"label":"Protecteur ultime",
			"description":"Une fois par jour, le personnage peut choisir de convertir ses points de mana, en points de vie et inversement. Il n’y a aucune limitation, le personnage peut convertir la totalité de ses points s’il le désire (limite de 1 PV restant). L’action ne consomme pas de mana, ne nécessite pas de jet et perdure une journée.",
			"type" : "autre",
			"bonus" : {

			}
		},
		{
			"label":"Écran",
			"description":"Contre 4 points de mana par tour, le personnage déploie son bouclier et l’étend magiquement en une ligne horizontale occupant 7 cases (pour 4 mètres de haut) devant lui. Les parades sont automatiquement réussies, mais le personnage ne peut pas attaquer tant que le bouclier est aussi proéminent.",
			"type" : "autre",
			"cout" : "4 PM par tour",
			"bonus" : {

			}
		}
		],
		"Rang 6":[
		{
			"label":"Maître garde",
			"description":"Lorsque le personnage manie un bouclier, il gagne un bonus de 3 en bouclier et de 1 à sa marge de critique.",
			"type" : "passif",
			"bonus" : {
				"bouclier": 3,
				"type_bouclier": {
					"critique": 1
				}
			}
		}
		]
	},
	"Suaire toxique":{
		"type": "Autre",
		"Rang 1":[
		{
			"label":"Araignée",
			"description":"Après un rituel provoquant une mutation, le personnage développe des glandes capables de produire différents venins. La sécrétion se fait via un jet de ténacité. Les effets sur les cibles suivent les règles classiques des poisons. Le personnage gagne également un bonus de 1 en ténacité.",
			"type" : "passif",
			"bonus": {
				"tenacite": 1
			}
		},
		{
			"label":"Formule toxique",
			"description":"Le personnage produit un poison qu’il peut appliquer sur des lames. Ce poison inflige de 1D6 + {{1}} dégâts supplémentaires et doit être renouvelé à chaque combat. Ce poison est de gravité normale (injection).",
			"type" : "autre",
			"bonus" : {

			}
		}
		],
		"Rang 2":[
		{
			"label":"Asepsie",
			"description":"Contre 5 points de mana, le personnage peut soigner les effets d’un poison. Selon la gravité du poison, la capacité de soin dépendra du niveau du personnage (2 bénins, 3 normaux, 4 inquiétants, 5 grave et 6 très graves).",
			"type" : "heal",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label":"Vapeur sournoise",
			"description":"Contre 4 points de mana, le personnage peut exhaler une vapeur toxique (gaz, normal) sur une cible sur une portée de 5 mètres. Cette vapeur diminue la ténacité de la cible de 1D4 + {{1}} pendant 3 tours, réduisant ses chances de purger les poisons.",
			"type" : "debuff",
			"cout" : "4 PM",
			"bonus" : {

			}
		}
		],
		"Rang 3":[
		{
			"label":"Scorpion",
			"description":"En plus d’un bonus à la ténacité de 1, le personnage devient totalement insensible à toutes sortes de poisons, toxines ou venins.",
			"type" : "passif",
			"bonus": {
				"tenacite": 1
			}
		},
		{
			"label":"Ambiance toxique",
			"description":"Contre 4 points de mana, le personnage peut créer une aura toxique (inquiétant, gaz) dans une zone de type 2 autour de lui. Les ennemis dans cette zone sont empoisonnés et subissent 1D8 + {{1}} dégâts. La zone ne dure qu’un tour, mais l’empoisonnement persiste jusqu’à la purge.",
			"type" : "damage",
			"cout" : "4 PM",
			"bonus" : {
			}
		}
		],
		"Rang 4":[
		{
			"label":"Crachat de l’hydre",
			"description":"Contre 6 points de mana, le personnage peut cracher un jet d’acide à une portée de 10 mètres, infligeant 2D8 + {{1}} points de dégâts. Cet acide ignore la RD, mais pas la RM. ",
			"type" : "damage",
			"cout" : "6 PM",
			"bonus" : {

			}
		},
		{
			"label":"Remontée acide",
			"description":"Contre 4 points de mana, le personnage vide rapidement le contenu de sa glande à venin et régurgite une masse vivante semblable à un limon.",
			"type" : "invocation",
			"cout" : "4 PM",
			"bonus" : {

			}
		}
		],
		"Rang 5":[
		{
			"label":"Toxine paralysante",
			"description":"Contre 8 points de mana, le personnage produit une toxine (grave) qui va paralyser une cible pendant {{1}} tours ou jusqu’à la purge de celle-ci. ",
			"type" : "debuff",
			"cout" : "8 PM",
			"bonus" : {

			}
		},
		{
			"label":"Anicroche délétère",
			"description":"Contre 6 points de mana, formule toxique est améliorée et passe en dangerosité grave. En plus du bonus aux dégâts, il empêche les cibles de tenter de purger les poisons pendant {{1}} tours. La toxine agit en inhibant les mécanismes de défense et de purification du corps, rendant toute tentative de se débarrasser des poisons inefficace pendant toute la durée de l’effet.",
			"type" : "debuff",
			"cout" : "6 PM",
			"bonus" : {

			}
		}
		],
		"Rang 6":[
		{
			"label":"Serpent",
			"description":"Contre 8 points de mana, le personnage peut invoquer une pluie d’acide dans une zone de type 1 et sur une portée de 10 mètres. Cette pluie inflige 2D10 + {{1}} dégâts d’acide, ignorant la RD, mais pas la RM. De plus, il gagne un bonus de 1 à la ténacité.",
			"type" : "damage",
			"cout" : "8 PM",
			"bonus" : {
				"tenacite": 1

			}
		}
		]
	},
	"Belluaire":{
		"type": "Autre",
		"Rang 1":[
		{
			"label":"Apprenti dresseur",
			"description":"L’animal (maximum Fp {{1}}) gagne 2 points dans une compétence et son dresseur 1 point dans sa compétence de dressage.",
			"type" : "passif",
			"bonus": {
				"dressage": 1
			}
		},
		{
			"label":"Lien sauvage",
			"description":"Le personnage partage une connexion télépathique avec son familier, lui permettant d’échanger pensées et sensations dans un rayon de 10+{{2}} mètres. ",
			"type" : "passif",
			"bonus" : {

			}
		}
		],
		"Rang 2":[
		{
			"label":"Rugissement bestial",
			"description":"Contre 3 points de mana, le belluaire gagne un bonus à l’intimidation de 4 pour son prochain jet. S’il s’agit d’un animal, ce bonus est doublé.",
			"type" : "buff",
			"cout" : "3 PM",
			"bonus" : {
				"RD": 1
			}
		},
		{
			"label":"Force béotienne",
			"description":"L’animal et le dresseur gagnent un bonus aux dégâts de 2, ainsi qu’un autre bonus de 1 à la RD.",
			"type" : "passif",
			"bonus" : {
				"RD": 1,
				"bonus_dmg" : 2
			}
		}
		],
		"Rang 3":[
		{
			"label":"Union naturelle",
			"description":"Si le familier est de taille supérieure au dresseur, celui-ci peut s’en servir comme monture via la compétence d’équitation. Celle-ci sera considérée comme formée à la monte et au combat monté.",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		},
		{
			"label":"Expert dresseur",
			"description":"Le dresseur peut désormais apprivoiser un animal supplémentaire (il faudra le dresser soi-même. Lors d’une création de personnages, on prendra pour Fp maximum, le niveau du belluaire) et les familiers gagnent un bonus de 1 sur leur marge de critique.",
			"type" : "passif",
			"bonus": {

			}
		}
		],
		"Rang 4":[
		{
			"label":"Capacités partagées",
			"description":"Contre 2 points de mana par tours, le belluaire peut assimiler une capacité, une compétence ou un don de son animal.",
			"type" : "buff",
			"cout" : "2 PM par tour",
			"bonus" : {

			}
		},
		{
			"label":"Omniscience animale",
			"description":"Le personnage a des informations précises sur tous les animaux qu’il rencontre.",
			"type" : "passif",
			"bonus" : {

			}
		}
		],
		"Rang 5":[
		{
			"label":"Contrôle des bêtes",
			"description":"Sans jet, mais contre le Fp de l’animal (domptable) en points de mana, le dresseur peut prendre le contrôle de celui-ci pour un tour (renouvelable pour le même coût en mana). Le belluaire peut alors faire faire ce qu’il souhaite à l’animal, sans jet de dressage.",
			"type" : "autre",
			"cout" : "x PM",
			"bonus" : {

			}
		},
		{
			"label":"Vie rudimentaire",
			"description":"Le belluaire a passé des années à affûter sa survie. Un point dépensé dans une des compétences suivantes en vaut désormais 2 : athlétisme, survie, perception et discrétion. L’effet ne s’applique pas pour les points dépensés avant le rang 5.",
			"type" : "passif",
			"bonus" : { //TODO

			}
		}
		],
		"Rang 6":[
		{
			"label":"Maître dresseur",
			"description":"Le belluaire peut distribuer 2 points de compétences à chaque animal possédé. Ils reçoivent également un bonus de 4 aux dégâts et à l’initiative, une marge de critique augmentée de 4 et un autre bonus de 2 à la RD. Le dresseur est alors capable de s’entourer de 3 animaux au total (mêmes restrictions que pour expert dresseur).",
			"type" : "passif",
			"bonus" : {

			}
		}
		]
	},
		"Change-forme":{
		"type": "Autre",
		"Rang 1":[
		{
			"label":"Don de Morroyak",
			"description":"Le personnage gagne la capacité de se transformer en un animal d’un FP {{1}}. Il débloque une nouvelle transformation à chaque niveau, tout en conservant les précédentes déjà obtenues. L’action prend 1 tour et coûte l’équivalent du FP de l’animal en mana. Le personnage adopte les caractéristiques de l'animal tout en conservant son intelligence, sa sagesse (et les compétences qui y sont liées) et ses points de vie et de mana.",
			"type" : "autre",
			"bonus": {
			}
		},
		{
			"label":"Proie",
			"description":"En action bonus, le personnage marque une cible comme proie pour 1 heure, qui ne peut alors plus se cacher aux yeux du personnage (même en devenant invisible). Une seule proie peut être marquée à chaque fois, la première doit mourir avant de pouvoir en désigner une autre. À noter que seul le personnage peut bénéficier des effets liés à cette technique.",
			"type" : "debuff",
			"bonus" : {

			}
		}
		],
		"Rang 2":[
		{
			"label":"Unguibus et rostro",
			"description":"Action bonus, coûtant 2 points de mana, qui augmente la Pa du personnage de 1. De plus, il bénéficie d’un effet de saignement (sous les critères habituels) pendant {{1}} tour(s).",
			"type" : "buff",
			"cout" : "2 PM",
			"bonus" : {
			}
		},
		{
			"label":"Chaîne alimentaire",
			"description":"Si la proie est vaincue par le personnage, celui-ci regagne instantanément {{2}} points de vie.",
			"type" : "passif_situationnel",
			"bonus" : {
			}
		}
		],
		"Rang 3":[
		{
			"label":"Prédateur",
			"description":"Contre 2 points de mana, le personnage fond sur sa proie à une vitesse incroyable (jusqu’à 10 mètres), contournant obstacles et attaques d'opportunité. S'il frappe la proie avec succès après ce bond, il inflige {{1}} de dégât(s) supplémentaire(s).",
			"type" : "autre",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label":"Formes animales",
			"description":"Le personnage gagne un bonus de 10 points de vie et ses attaques sous formes animales bénéficient de 1D de dégât supplémentaire.",
			"type" : "passif",
			"bonus": {
				"pv": 10
			}
		}
		],
		"Rang 4":[
		{
			"label":"Harmonie des formes",
			"description":"Contre 3 points de mana, le personnage peut invoquer l'esprit des animaux qu'il incarne pour bénéficier d'une capacité d'une forme différente, pendant {{1}} tours, sans avoir à se transformer.",
			"type" : "buff",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		{
			"label":"Férocité",
			"description":"« Unguibus et rostro » est amélioré. La Pa passe à 2 et les effets de saignement se cumulent à chaque attaque. De plus, les dégâts de saignement sont triplés.",
			"type" : "passif",
			"bonus" : {

			}
		}
		],
		"Rang 5":[
		{
			"label":"Force de la meute",
			"description":"Contre 4 points de mana, le personnage partage son instinct de meute. Tant qu'il est transformé, chaque allié dans un rayon de 10 mètres bénéficie d'un bonus de 2 aux jets d'attaque et d’une case de déplacement supplémentaire pour chaque autre allié (y compris le personnage) à proximité. Les invocations ne sont pas considérées comme des alliés et l’effet perdure pendant 1D6 + {{1}} tours.",
			"type" : "buff",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label":"Alpha",
			"description":"Chaque fois que le personnage tue une proie, celui-ci gagne une charge de fureur. Au bout de 3 charges, celui-ci à droit à un coup critique automatique sur sa prochaine attaque du combat en cours.",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		}
		],
		"Rang 6":[
		{
			"label":"Morphe sauvage",
			"description":"Le personnage gagne un bonus de 20 points de vie, ses attaques sous formes animales bénéficient de 1D de dégât supplémentaire, ainsi qu’une classe de dé.",
			"type" : "passif",
			"bonus" : {
			"pv": 20
			}
		}
		]
	},
	"Saltimbanque":{
		"type": "Autre",
		"Rang 1":[
		{
			"label":"Apprenti bateleur",
			"description":"Le personnage gagne un bonus de 2 à ses jets de représentation. L’esquive et le déplacement deviennent autorisés durant la canalisation (relance les effets et le coût, sans refaire de jet). Mais la course sera interdite et les déplacements réduits de moitié.",
			"type" : "passif",
			"bonus": {
				"representation": 2
			}
		},
		{
			"label":"Danse du paon",
			"type" : "heal",
			"cout" : "3 PM par tour",
			"description":"Contre 3 points de mana par tour et un jet de représentation, le personnage <b>canalise</b> une danse qui restitue {{1}}+3 points de vie, à répartir entre les alliés situés dans une zone de 5 mètres autour du danseur.",
			"bonus" : {

			}
		}
		],
		"Rang 2":[
		{
			"label":"Complainte de la pucelle",
			"type" : "debuff",
			"cout" : "3 PM",
			"description":"Contre 3 points de mana, le personnage joue une mélodie qui perturbe ses ennemis dans un rayon de 5 mètres. Les cibles doivent réussir un jet de volonté opposé au jet de représentation du personnage. Il faudra ensuite lancer 1D{{1}}, pour déterminer le malus appliqué jusqu'à la fin du combat.",
			"bonus" : {

			}
		},
		{
			"label":"Chant du papillon",
			"type" : "buff",
			"cout" : "4 PM",
			"description":"Contre 4 points de mana et un jet de représentation, des ailes éthérées se forment dans le dos des alliés leur permettant de se déplacer de 5+{{1}} mètres dans la direction de leur choix et sans provoquer d’attaques d’opportunité.",
			"bonus" : {

			}
		}
		],
		"Rang 3":[
		{
			"label":"Valse de l’eau",
			"type" : "buff",
			"cout" : "4 PM",
			"description":"Contre 4 points de mana et un jet de représentation, le personnage entame une danse créant des bulles protectrices et oxygénées sur ses alliés, dans une zone de 5 mètres autour de lui. La bulle permet d’évoluer librement dans l’eau (sans malus), mais explosera si le des dégâts sont subits.",
			"bonus": {

			}
		},
		{
			"label":"Expert bateleur",
			"description":"Le personnage gagne un bonus de 3 sur la compétence de représentation, ainsi qu’un bonus de 1 à la marge de critique sur celle-ci. De plus, la portée de toutes les prestations augmente de 5 mètres.",
			"bonus": { //"portee" : 5
				"representation": 3,
				"crit_representation": 1
			}
		}
		],
		"Rang 4":[
		{
			"label":"Guige du feu",
			"type" : "damage",
			"cout" : "4 PM",
			"description":"Contre 4 points de mana par tour et un jet de représentation, le personnage <b>canalise</b> une danse qui brûle de 2D6 + {{1}} les adversaires présents dans une zone de 5 mètres autour du danseur. Il faudra réussir un jet d’acrobaties, non opposé, afin d’esquiver la zone.",
			"bonus" : {

			}
		},
		{
			"label":"Mélodie de la terre",
			"description":"Contre 4 points de mana par tour et un jet de représentation, le personnage canalise une ballade qui anime le sol sur 5 mètres autour de lui. Chaque tour, les cibles doivent réussir un jet d’acrobatie opposé pour éviter d’être entravés. En cas d’échec, un jet d’athlétisme opposé est nécessaire pour se libérer.",
			"type" : "buff",
			"cout" : "4 PM par tour",
			"bonus" : {

			}
		}
		],
		"Rang 5":[
		{
			"label":"Air héroïque",
			"description":"Contre 5 points de mana par tour et un jet de représentation, le personnage <b>canalise</b> un hymne encourageant. Les alliés, situés dans une zone de 5 mètres autour du danseur, verront leurs points de mana être restaurés d’une valeur équivalente au niveau du danseur. Le lanceur ne peut en revanche pas bénéficier de cet effet.",
			"type" : "buff",
			"cout" : "5 PM par tour",
			"bonus" : {

			}
		},			
		{
			"label":"Ballet du lotus noir",
			"description":"Contre un jet de représentation et 8 points de mana par tour, le personnage <b>canalise</b> sa performance ultime et choisit 4 effets dans la liste suivante. Il peut les utiliser en bonus aux alliés, ou malus aux ennemis. Les mêmes effets ne peuvent pas se cumuler sur une cible, mais peuvent être modifiés à chaque tour : <br>"+
			"● 4 de RD ou RM<br>"+
			"● 10 de résistance à un élément<br>"+
			"● 2D8 + {{1}}  en soins ou en dégâts<br>"+
			"● 5 dans une compétence<br>"+
			"● 2 au critique sur une compétence<br>"+
			"● 10 mètres de portée des sorts<br>"+
			"● 1D de dégât<br>"+
			"● 5 cases de déplacement<br>"+
			"● 5 à l’initiative<br>"+
			"● 1 classe de dé",
			"type" : "buff",
			"cout" : "8 PM par tour",
			"bonus" : {

			}
		}
		],
		"Rang 6":[
		{
			"label":"Maître bateleur",
			"description":"Le personnage gagne un bonus de 4 à la représentation et un bonus de 1 au critique pour cette compétence.",
			"bonus" : {
				"representation": 4,
				"crit_representation": 1
			}
		}
		]
	},
	"Vaudouisant":{
		"type": "Autre",
		"Rang 1":[
		{
			"label":"Partisan",
			"description":"Les compétences de volonté et de psychologie sont remplacées par la compétence de «perspicacité», basée sur la sagesse. Tous les points qui auraient été dépensés dans ces compétences sont transférés à celle-ci. De plus, le personnage gagne un bonus de 1 dans cette nouvelle compétence. À noter que la portée initiale des sorts est au contact.",
			"type" : "passif",
			"bonus": {
				"perspicacite": 1
			}
		},
		{
			"label":"Dagyde",
			"description":"Contre 2 points de mana, le personnage conjure une poupée unique qui servira de vecteur aux sortilèges. Voir la fiche de l’invocation pour plus de détails.",
			"type" : "invocation",
			"cout" : "2 PM",
			"bonus" : {

			}
		}
		],
		"Rang 2":[
		{
			"label":"Guérison ancestrale",
			"description":"Contre 4 points de mana et un jet de perspicacité réussi, le personnage utilise le pouvoir des ancêtres pour guérir les blessures physiques ou spirituelles récentes de sa cible. En cas de blessures physiques, le sort rend un montant de 1D8 +  {{1}} points de vie.",
			"type" : "heal",
			"cout" : "4 PM",
			"bonus" : {
			}
		},
		{
			"label":"Liaison spirituelle",
			"description":"Contre 4 points de mana et un jet de perspicacité réussi, le personnage créer un lien spirituel entre deux personnes, leur permettant de partager des sensations (inoffensives), des émotions et des pensées.",
			"type" : "autre",
			"cout" : "4 PM",
			"bonus" : {

			}
		}
		],
		"Rang 3":[
		{
			"label":"Pacte équitable",
			"description":"Lors d’un rituel de 10 minutes, le personnage peut pactiser avec un esprit qui prêtera sa force pour les prochaines 24 heures, mais qui demandera une rétribution pour les 24 suivantes. Par exemple : lvl × 10 Pv ou Pm, le niveau en classes de dé, le niveau en bonus sur une compétence, + 1 don ou sort. À noter que le paiement sera toujours pour le vaudouisant, même si ce n’est pas lui qui bénéficie du pacte.",
			"type" : "rituel",
			"bonus" : {

			}
		},
		{
			"label":"Ouanga",
			"description":"Le personnage obtient un bonus de 1 sur la compétence perspicacité, ainsi qu’un bonus de 1 en charisme",
			"type" : "passif",
			"bonus": {
				"perspicacite": 1,
				"charisme": 1
			}
		}
		],
		"Rang 4":[
		{
			"label":"Envoûtement",
			"description":"Contre 6 points de mana et un jet de charisme (compétence à la discrétion du MJ selon la situation), le personnage insuffle un sentiment de respect, d’amour, de peur ou d’amitié à une cible, la faisant agir par conséquent. L’effet perdure durant {{1}} heure(s) et est considéré comme un maléfice. De ce fait, la cible pourra tenter de résister avec un jet de volonté opposé.",
			"type" : "debuff",
			"cout" : "6 PM",
			"bonus" : {

			}
		},
		{
			"label":"Prospérité",
			"description":"Contre 8 points de mana et un jet de perspicacité réussi, le personnage attire les bonnes vibrations jusqu’à sa cible. Celle-ci bénéficie d’un bonus de {{1}}, qui perdurera jusqu’à la fin du combat (n’est cependant pas cumulable et une cible ne peut bénéficier que d’une augmentation).",
			"type" : "buff",
			"cout" : "8 PM",
			"bonus" : {

			}
		}
		],
		"Rang 5":[
		{
			"label":"Amis de l’au-delà",
			"description":"Contre 8 points de mana et un jet de perspicacité réussi, le personnage invite de mauvais esprits qui vont venir assaillir une zone de type 2 autour d’une cible. Les ennemis subiront des coups invisibles, des chatouilles et des pincements. Malmenés, ils auront un malus équivalent au niveau du mage pour réussir leurs actions, en plus de subir 2D6 points de dégâts non esquivables et non réductibles, à chaque tour.",
			"type" : "debuff",
			"cout" : "8 PM",
			"bonus" : {

			}
		},
		{
			"label":"Vengeance karmique",
			"description":"Contre 10 points de mana et un jet de perspicacité réussi, le personnage honore une cible pour le reste du combat. Les esprits se chargeront alors de rétribuer chaque dégât subi par la cible à ses assaillants (la cible conserve les dégâts reçus), assurant ainsi une vengeance proportionnée et équitable. Les dégâts déduits par une armure, un bouclier ou excédentaires ne seront pas comptabilisés.",
			"type" : "damage",
			"cout" : "10 PM",
			"bonus" : {

			}
		}
		],
		"Rang 6":[
		{
			"label":"Bokor",
			"description":"Le personnage gagne un bonus de 2 à la perspicacité et un bonus de 1 au critique pour cette compétence.",
			"type" : "passif",
			"bonus" : {
				"perspicacite": 2,
				"crit_perspicacite": 1
			}
		}
		]
	},	
	"Pris de boissons":{
		"type": "Autre",
		"Rang 1":[
		{
			"label":"Apprenti brasseur",
			"description":"Le personnage obtient un bonus de 3 à ses jets de ténacité face à l’alcool, ainsi qu’un bonus à ses dégâts de 3 lorsqu’il en consomme (dès chaque prise, mais non cumulable).",
			"type" : "passif_situationnel",
			"bonus": {

			}
		},
		{
			"label":"Brasserie magique",
			"description":"Contre 1 point de mana, le personnage peut générer l’équivalent d’une bouteille (3 chopes) en alcool faible.<br>"+
			"Contre 2 points de mana, il produit de l’alcool normal.<br>"+
			"Et contre 3 points de mana, il produit de l’alcool fort.",
			"type" : "autre",
			"cout" : "1, 2, ou 3 PM",
			"bonus" : {

			}
		}
		],
		"Rang 2":[
		{
			"label":"Ki martial",
			"description":"Lorsque le personnage consomme une dose d’alcool, celui-ci gagne des points de ki. Les points de ki sont comme des points de mana, mais temporaires. Plus l’alcool ingéré est fort, plus il générera de points de ki. Un alcool faible ne produira qu’un seul point de ki, un alcool normal 2 et 3 pour un alcool fort. Au maximum, le personnage peut stocker 1 point de ki +2 par niveau. Les points de ki disparaîtront après 6 heures et sont réinitialisés avec la prise d’une autre boisson. Tous les 4 points de ki (non consommés) le personnage gagne un bonus à la RM de 1, cumulable.",
			"type" : "passif_situationnel",
			"bonus" : { // TODO

			}
		},
		{
			"label":"Même pas peur",
			"description":"Dès lors où le personnage possède au moins 1 point de ki, il résiste automatiquement à un effet de peur. Il faudra un minimum de 2 points afin d’être également immunisé face à la terreur (Les points de ki seront alors dépensés automatiquement).",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		}		
		],
		"Rang 3":[
		{
			"label":"Rage de l’ivrogne",
			"description":"Le personnage peut dépenser 1 point de ki pour augmenter ses dégâts physiques de 1D6. Il peut également décider de dépenser plus de points, contre 2 points il infligera 2D6 dégâts en plus, contre 3 points 3D6, etc. L’effet perdure pendant {{1}} tours.",
			"type" : "buff",
			"cout" : "x point(s) de ki",
			"bonus" : {

			}
		},
		{
			"label":"Expert brasseur",
			"description":"Le personnage obtient un bonus de 3 à ses jets de ténacité face à l’alcool, ainsi qu’un bonus de 1D à ses dégâts physiques, lorsqu’il en consomme. De plus, la réserve maximum de ki est doublée et l’action de boire devient gratuite en combat.",
			"type" : "passif_situationnel",
			"bonus": {

			}
		}
		],
		"Rang 4":[
		{
			"label":"Foie robuste",
			"description":"Le personnage à la possibilité de relancer ses jets de ténacité lié à la prise d’alcool. Chaque jour, il sera possible de relancer {{1}} jet(s). De plus, les bonus de résistance à l’alcool s’appliquent désormais pour tout type de poisons.",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		},
		{
			"label":"Souffle alcoolisé",
			"description":"Contre 4 points de ki et un jet de ténacité réussi, le personnage souffle devant lui un cône de feu qui infligera 2D6 + {{1}} points de dégâts de feu (magiques). Afin de l’esquiver, il faudra réussir un jet d’acrobaties, opposé à la réussite du personnage.",
			"type" : "damage",
			"cout" : "4 points de ki",
			"bonus" : {

			}
		},
		],
		"Rang 5":[
		{
			"label":"Pouvoir de l’hydromel",
			"description":"Contre 2 points de ki, le personnage gagne 1 de RD, il peut dépenser plus de points, afin de cumuler les effets (maximum {{0.5}}). Cependant, l’effet perdure pendant {{1}} tours.",
			"type" : "buff",
			"cout" : "2 points de ki",
			"bonus" : {

			}
		},
		{
			"label":"Maîtrise du buveur",
			"description":"Contre 2 points de ki, le personnage gagne un bonus de 1 à sa marge de critique, il peut dépenser plus de points, afin de cumuler les effets (maximum {{0.5}}). Cependant, l’effet perdure pendant {{1}} tours.",
			"type" : "buff",
			"cout" : "2 points de ki",
			"bonus" : {

			}
		}
		],
		"Rang 6":[
		{
			"label":"Maître brasseur",
			"description":"Le personnage obtient un bonus de 2 à ses jets de ténacité, un bonus de 1D de dégâts physiques et de 2 à sa marge de critique lorsqu’il consomme de l’alcool. De plus, brasserie magique devient instantanée.",
			"type" : "passif_situationnel",
			"bonus" : {
				"tenacite": 2
			}
		}
		]
	},
	"Arbalétrier occulte":{
		"type": "Autre",
		"Rang 1":[
		{
			"label":"Apprenti pistolero",
			"description":"L’arbalète de poing est modifiée afin d’extraire la magie de son utilisateur et de former des balles de mana. De plus, le personnage gagne un bonus au toucher de 1, ainsi qu’un bonus de dégâts de 1 au maniement de l’arbalète de poing. À noter que l’arbalétrier peut manier deux armes et que chacune sera comptabilisée pour roquette et shrapnel.",
			"type" : "passif",
			"bonus": {
				"arbalete": 1,
				"bonus_dmg": 1
			}
		},
		{
			"label":"Chargeur personnel",
			"description":"Le personnage peut convertir 2 points de mana pour générer une balle. Celles-ci sont magiques et ignorent la RD, contrairement à des munitions traditionnelles. En dehors de cette spécificité, les balles conservent les mêmes caractéristiques que des munitions classiques.",
			"type" : "buff",
			"cout" : "2 PM par balle",
			"bonus" : {

			}
		}
		],
		"Rang 2":[
		{
			"label":"Shrapnel",
			"description":"Contre 3 points de mana et un jet d’arbalète réussi, le personnage tire une salve de projectiles en l'air qui retombe dans une zone de type 1 pendant 3+{{1}} tours. Chaque projectile inflige des dégâts fixes équivalents à la moitié des dégâts maximums de l’arme (arrondis à l’entier supérieur, sans bonus de roquette). Il est impossible de cumuler plusieurs utilisations dans une même zone, sous peine que les projectiles s’entrechoquent et se brisent avant d’infliger des dégâts.",
			"type" : "damage",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		{
			"label":"Viser juste",
			"description":"L’action de viser, n’octroie plus 2 de bonus au toucher, mais un bonus de 3.",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		}
		],
		"Rang 3":[
		{
			"label":"Brûlure",
			"description":"À chaque coup critique réalisé, la cible subit 4 points de dégât de brûlure par tour, pendant {{1}} tour(s). L’effet est cumulable si plusieurs critiques tombent alors que l’effet est déjà actif.",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		},
		{
			"label":"Expert pistolero",
			"description":"Lorsque le personnage manie une arbalète de poing, il gagne un bonus aux dégâts de 1D, ainsi qu’un bonus à son toucher et à sa marge de critique de 1. De plus, le malus qu’imposait l’ambidextrie est réduit de 1.",
			"type" : "passif",
			"bonus": {
				"arbalete": 1,
				"type_arbalete": {
					"bonus_D_dmg": 1,
					"critique": 1
				}
			}
		}
		],
		"Rang 4":[
		{
			"label":"Récession",
			"description":"L’arbalétrier concentre un seul et unique tir sur une cible. Pour chaque point de mana dépensé, la cible reculera de deux cases opposées au personnage. Celle-ci devra réussir un jet de force brute opposé au toucher du lanceur, afin de tenir bon.",
			"type" : "damage",
			"cout" : "1 PM par tranche de 2 cases",
			"bonus" : {

			}
		},
		{
			"label":"Cow-boy",
			"description":"Lorsqu’il achève un ennemi, le personnage lance {{1}}D3, afin de récupérer du mana en ponctionnant l’énergie vitale de la cible.",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		}
		],
		"Rang 5":[
		{
			"label":"Roquette",
			"description":"Contre 5 points de mana et un jet de tir réussi, le personnage peut tirer une roquette vers ses ennemis, infligeant de gros dégâts. Les dégâts de l’arme gagnent 2 classes de dé et l’attaque viendra frapper dans une zone de type 1. Néanmoins, cette capacité à un temps de recharge de 4 tours (roquette, action, action, action, action, roquette).",
			"type" : "damage",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label":"Inarrêtable",
			"description":"Tant que le personnage dépense 3 points de mana supplémentaires (en plus du coût des balles, ou des capacités) après chaque attaque, celui-ci a le droit d’attaquer de nouveau une cible. Cependant, à chaque attaque réalisée, la prochaine se fera avec un malus de 5.",
			"type" : "damage",
			"cout" : "3 PM supplémentaires, répétable",
			"bonus" : {

			}
		}
		],
		"Rang 6":[
		{
			"label":"Maître pistolero",
			"description":"Lorsque le personnage manie une arbalète de poing, il gagne un bonus aux dégâts de 1D, un bonus sur la marge de critique de 1, ainsi qu’un bonus de 3 au toucher.",
			"type" : "passif",
			"bonus" : {
				"arbalete": 3,
				"type_arbalete" : {
					"bonus_D_dmg": 1,
					"critique": 1
				}
			}
		}
		]
	},
	"Aide de camp":{
		"type": "Autre",
		"Rang 1":[
		{
			"label":"Auxiliaire",
			"description":"Le personnage gagne un bonus de 2 à ses compétences de survie et de dépeçage. De plus, habitué à porter beaucoup de matériel pour ses alliés, sa capacité de charge est doublée.",
			"type" : "passif",
			"bonus" : {
				"survie": 2,
				"multiplicateur_charge": 2,
			}
		},
		{
			"label":"Bivouac préparé",
			"description":"Le personnage sait préparer des repas uniques, qui octroient à tous les membres du groupe un bonus de {{1}}, dans toutes les compétences liées à la force et la constitution, pendant 4 heures.",
			"type" : "autre",
			"bonus" : {
			}
		}
		],
		"Rang 2":[
		{
			"label":"Feu de camp",
			"description":"Contre 2 points de mana, le personnage peut invoquer un feu qui ne faiblira pas durant 8 heures d’affilée, prodiguant lumière et chaleur, même sans combustible.",
			"type" : "autre",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label":"Homme à tout faire",
			"description":"Contre 5 points de mana, le personnage augmente sensiblement sa perception pour l’heure qui va suivre, lui conférant un bonus de {{1}} aux jets de survie, de perception, ainsi que durant les périodes de récolte de ressources.",
			"type" : "buff",
			"cout" : "5 PM",
			"bonus" : {

			}
		}
		],
		"Rang 3":[
		{
			"label":"Assistant",
			"description":"Le personnage gagne un bonus de 2 à sa compétence de médecine. De plus, sur un jet de médecine réussi, il peut prodiguer un massage de quinze minutes à un allié, lui permettant d’avoir un sommeil plus réparateur, doublant sa régénération naturelle au prochain repos.",
			"type" : "autre",
			"bonus" : {
				"medecine": 2

			}
		},
		{
			"label":"Formation médicale avancée",
			"description":"Le personnage obtient le don médecin de terrain et contre 5 points de mana, permet d’améliorer ses soins magiques, ou ceux d’un allié, afin que ceux-ci permettent de stabiliser un allié mourant.",
			"type" : "heal",
			"cout" : "5 PM",
			"bonus": {
            "dons": ["medecin_de_terrain"]
			}
		}
		],
		"Rang 4":[
		{
			"label":"Tactique de groupe",
			"description":"Le personnage a un avantage aux jets d’attaque contre une cible, si ses alliés l’attaquent aussi. Pour chaque allié attaquant la même cible que le personnage, tous auront un bonus de 1 au toucher.",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		},
		{
			"label":"Don de soi",
			"description":"Le personnage peut transférer ses points de vie ou de mana à un allié. Les points offerts ne peuvent être regagnés que par une régénération naturelle et la méditation.",
			"type" : "autre",
			"bonus" : {

			}
		}
		],
		"Rang 5":[
		{
			"label":"Bannière d’inspiration",
			"description":"Contre 6 points de mana, le personnage génère une bannière qui motive le groupe pendant son niveau en tours. Dans une zone de type 2 autour de lui, le groupe recevra un bonus de {{1}} à la volonté, à l’intimidation, à l’esquive et à l’attaque. Tant qu’une bannière est active, il est impossible d’en générer une autre.",
			"type" : "buff",
			"cout" : "6 PM",
			"bonus" : {

			}
		},
		{
			"label":"Pas sans toi",
			"description":"Si un allié (hors invocation) se trouve dans l’état mourant, ou qu’il a succombé à ses blessures, le personnage gagne un bonus de 2D à ses dégâts et sa marge de critique est augmentée de 2 jusqu’à la fin du combat.",
			"type" : "passif_situationnel",
			"bonus" : {
		
			}
		}
		],
		"Rang 6":[
		{
			"label":"Compagnon de route",
			"description":"Tout ce temps passé à aider et observer des aventuriers a permis au personnage de gagner une polyvalence considérable et obtient un bonus de 1 sur tous les domaines d’artisanat. De plus, durant une demi-heure et contre 10 points de mana, le personnage est désormais capable de générer une zone de protection dans une zone de type 3 autour de lui. La zone ne peut pas être traversée, a l’avantage de rentre invisible tout ce qui se trouve à l’intérieur, tout en étant elle-même quasi imperceptible et est totalement insonorisée. Cette protection perdure pendant {{1}} heures et permet d’ignorer totalement les effets météorologiques, y compris les températures externes.",
			"type" : "autre",
			"cout" : "6 PM",
			"bonus": {
				"forgeron": 2,
				"taneur": 2,
				"alchimiste": 2,
				"faconneur": 2,
				"tailleur": 2,
			}
		}
		]
	},
	"Cultiste du bien":{
		"type": "Autre",
		"Rang 1":[
		{
			"label":"Acolyte du bien",
			"description":"<span style=\"font-weight:bold\"><i class=\"fa fa-exclamation-triangle\"></i>Interdit la prise de cultiste du mal. De plus, les sorts choisis ne pourront bénéficier d’améliorations.<i class=\"fa fa-exclamation-triangle\"></i></span><br> Les compétences d’incantation et volonté fusionnent pour devenir la « dévotion », basée sur la sagesse. Le personnage gagne un bonus de 2 à celle-ci, ainsi qu’un bonus de 1 au critique. Il choisit également un sort (de cercle 3 ou moins) pour débuter son grimoire du dévot.",
			"choix" : {
				"rang": 3,
				"type": "Magie"
			},
			"type" : "passif",
			"bonus" : {
				"devotion": 2,
				"crit_devotion": 1
			}
		},
		{
			"label":"Imposition des mains",
			"description":"Contre 3 points de mana, un jet de dévotion réussi et sur une distance de 20 mètres, le personne peut soigner 2D{{2}} points de vie.",
			"type" : "heal",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		],
		"Rang 2":[
		{
			"label":"Démonstration divine",
			"description":"Après un critique, l’inspiration est modifiée. Si le score est inférieur ou égal à (sagesse – 10) × 3, vous soignez tous vos alliés dans une zone de type 2 autour de vous, de {{2}} points de vie.<br>"+
			"De plus, le personnage gagne un sort du cercle 4 ou moins du grimoire choisi.",
			"choix" : {
				"rang": 4,
				"type": "Magie"
			},
			"type" : "passif",
			"bonus" : {

			}
		},
		{
			"label":"Châtiment",
			"description":"Contre 3 points de mana et sur une distance de 20 mètres, le personnage peut infliger 2D{{1}}+4 points de dégâts. De plus, la cible subira un malus à la défense de {{1}} pendant le prochain tour. Il faudra pour cela réussir un jet de dévotion opposé à un jet de volonté de la cible. L’attaque est magique et infligera le double des dégâts sur les créatures démoniaques.",
			"type" : "damage",
			"cout" : "3 PM",
			"bonus" : {

			}
		}
		],
		"Rang 3":[
		{
			"label":"Credo",
			"description":"Contre 8 points de mana, le cultiste gagne un bonus de 4 au critique sur sa prochaine action et rend l’échec critique impossible (transforme l’échec critique en échec simple). Ce sort est instantané et n’utilise donc pas le tour.",
			"type" : "buff",
			"cout" : "8 PM",
			"bonus" : {

			}
		},
		{
			"label":"Prêtre du bien",
			"description":"Le personnage gagne un bonus de 2 sur sa compétence de dévotion, ainsi qu’un bonus de 1 à sa marge de critique sur cette même compétence. De plus le personnage gagne un sort du cercle 4 ou moins du grimoire choisi.",
			"choix" : {
				"rang": 4,
				"type": "Magie"
			},
			"type" : "passif",
			"bonus" : {
				"devotion": 2,
				"crit_devotion": 1
			}
		}
		],
		"Rang 4":[
		{
			"label":"Purge",
			"description":"Contre 3 points de mana, le personnage, au contact de sa cible, retire tout état préjudiciable, ainsi que les malédictions. L’action se fera sur un jet de dévotion (opposé si besoin) à la réussite de l’ennemi ayant causé le mal. Toutefois, il sera impossible de traiter un mal de niveau {{1}}+3.",
			"type" : "heal",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		{
			"label":"Sanctuaire",
			"description":"Lorsque le cultiste se trouve en temps de repos, celui-ci dégage une présence rassurante. Tout personnage se reposant près de lui verra sa régénération être doublée (sommeil, comme méditation). De plus, le personnage gagne un sort du cercle 4 ou moins du grimoire choisi.",
			"choix" : {
				"rang": 4,
				"type": "Magie"
			},
			"type" : "passif_situationnel",
			"bonus" : {

			}
		}
		],
		"Rang 5":[
		{
			"label":"Croisade",
			"description":"Contre 8 points de mana et un jet de dévotion réussi, le personnage bénit un allié pendant 2+{{1}} tours  et lui confère un bonus de 4 en incantation, augmente la portée de ses sorts de 5 mètres, leur ajoute une classe de dés et augmente leur marge de critique de 1.",
			"type" : "buff",
			"cout" : "8 PM",
			"bonus" : {

			}
		},
		{
			"label":"Fureur divine",
			"description":"Les sorts imposition des mains et châtiment gagnent 2D et peuvent désormais être améliorés au même titre que les sorts traditionnels.<br>"+
			"De plus, le personnage gagne un sort du cercle 5 ou moins du grimoire choisi.",
			"choix" : {
				"rang": 5,
				"type": "Magie"
			},
			"type" : "passif",
			"bonus" : {

			}
		}
		],
		"Rang 6":[
		{
			"label":"Archiprêtre du bien",
			"description":"Le personnage gagne un bonus de 2 sur sa compétence de dévotion, un bonus de 1 à sa marge de critique sur cette même compétence et il peut lancer 2 fois chacun de ses jets de soins, afin de garder le meilleur. De plus, le personnage gagne un sort du cercle 6 ou moins du grimoire choisi.",
			"choix" : {
				"rang": 6,
				"type": "Magie"
			},
			"type" : "passif",
			"bonus": {
				"devotion": 2,
				"crit_devotion": 1
			}
		}
		]
	},
	"Cultiste du mal":{
		"type": "Autre",
		"Rang 1":[
		{
			"label":"Acolyte du mal",
			"description":"<span style=\"font-weight:bold\"><i class=\"fa fa-exclamation-triangle\"></i>Interdit la prise de cultiste du bien. De plus, les sorts choisis ne pourront bénéficier d’améliorations.<i class=\"fa fa-exclamation-triangle\"></i></span><br> Acquisition de la compétence «Rituel», basée sur l’intelligence avec un bonus naturel de 2 à celle-ci, ainsi qu’un bonus de 1 au critique. Le personnage choisit une magie qui sera liée et formera son grimoire de rituel. Le rituel combine les compétences d’incantation et Intimidation et s’y substitue pour ces jets.",
			"type" : "passif",
			"choix" : {
				"rang": 3,
				"type": "Magie"
			},
			"bonus" : {
				"rituel": 2,
				"crit_rituel": 1
			}
		},
		{
			"label":"Frappe vicieuse",
			"description":"Contre 3 points de mana, sur une distance de 15 mètres et un jet de rituel réussi, le personnage envoie une onde corrompue qui inflige 2D2 + {{1}} dégâts magiques, pendant 4 tours. Le jet sera opposé à la ténacité de la cible, chaque tour. Si le sort est relancé sur la même cible, la classe dé augmente de 1 et cela réinitialise la durée de l’affliction.",
			"type" : "damage",
			"cout" : "3 PM",
			"bonus" : {

			}
		}
		],
		"Rang 2":[
		{
			"label":"Violence abjecte",
			"description":"Après un critique, l’inspiration est modifiée. Si le score est inférieur ou égal à (intelligence – 10) × 3, vous obtenez un bonus de {{1}} dégâts pour la prochaine attaque qui touchera (durant le combat actuel).<br>"+
			"De plus, le personnage gagne un sort du cercle 4 ou moins du grimoire choisi.",
			"choix" : {
				"rang": 4,
				"type": "Magie"
			},
			"type" : "passif_situationnel",
			"bonus" : {

			}
		},
		{
			"label":"Conjuration satanique",
			"description":"Contre 15 points de mana, un jet de rituel et à la suite d’une incantation de 2 heures, le personnage invoque un diablotin qui lui sera lié. À sa mort physique, il retourne dans le plan démoniaque, mais pourra être réinvoqué. Le diablotin a une personnalité propre. Plus il verra chez le personnage un maître plaisant, plus il prendra des initiatives pour l’aider.",
			"type" : "invocation",
			"cout" : "15 PM",
			"bonus" : {

			}
		}
		],
		"Rang 3":[
		{
			"label":"Aura impie",
			"description":"Contre 6 points de mana et un jet de rituel, le personnage effraie tous les ennemis présents dans une zone de type 3 autour de lui. Chaque tour, les ennemis débutant dans la zone devront réaliser un jet de volonté simple s’ils souhaitent avoir la force d’attaquer le personnage ou son familier. L’effet perdure pendant 1D4 + {{1}} tours, ou jusqu’à ce que le cultiste frappe une cible effrayée.",
			"type" : "heal",
			"cout" : "6 PM",
			"bonus" : {

			}
		},
		{
			"label":"Prêtre du mal",
			"description":"Le personnage gagne un bonus de 2 sur sa compétence de rituel, ainsi qu’un bonus de 1 sur la marge de critique de cette même compétence.<br>"+
			"De plus, le personnage gagne un sort du cercle 5 ou moins du grimoire choisi.",
			"choix" : {
				"rang": 5,
				"type": "Magie"
			},
			"type" : "passif",
			"bonus": {
				"rituel": 2,
				"crit_rituel": 1
			}
		}
		],
		"Rang 4":[
		{
			"label":"Consécration méphistophélique",
			"description":"Une fois par jour, le personnage manifeste son culte par automutilation et offre son sang aux seigneurs démons contre un gain de puissance. Par tranche de 10 points de vie sacrifiés, le personnage gagne un bonus de 1D sur tous ses dégâts. Les points de vie seront restitués en fin de journée, lors du repos.<br>"+
			"De plus, le personnage gagne un sort du cercle 4 ou moins du grimoire choisi.",
			"choix" : {
				"rang": 4,
				"type": "Magie"
			},
			"type" : "passif_situationnel",
			"cout" : "Points de vie",
			"bonus": {

			}
		},
		{
			"label":"Ça a un goût de poulet …",
			"description":"Sur une portée de 10 mètres, le personnage vole les points de vie ou de mana d’un allié (il faudra réussir un jet de rituel opposé à la ténacité de l’allié s’il s’y oppose). Les points volés ne peuvent être regagnés que par une régénération naturelle et la méditation. À noter qu’il est possible de mettre un allié dans le coma avec cette capacité.",
			"type" : "autre",
			"bonus" : {

			}
		}
		],
		"Rang 5":[
		{
			"label":"Flammes infernales",
			"description":"Le sort frappe vicieuse est amélioré et gagne 1D supplémentaire. Le sort peut également être amélioré, au même titre que les sorts traditionnels.<br>"+
			"De plus, le personnage gagne un sort du cercle 5 ou moins du grimoire choisi.",
			"choix" : {
				"rang": 5,
				"type": "Magie"
			},
			"type" : "passif",
			"bonus" : {

			}
		},
		{
			"label":"Fureur destructrice",
			"description":"Contre 6 points de mana et un jet de rituel, le personnage entre dans un état de rage et fait monter sa puissance à son paroxysme. Tous les sorts à effets néfastes sont augmentés de {{1}} classes de dé ou tours, et ce, durant 5 tours.",
			"type" : "buff",
			"cout" : "6 PM",
			"bonus" : {

			}
		}
		],
		"Rang 6":[
		{
			"label":"Archiprêtre du mal",
			"description":"Le personnage gagne un bonus de 2 sur sa compétence de rituel, un bonus de 1 à sa marge de critique sur cette même compétence et il peut lancer 2 fois chacun de ses jets de dégâts, afin de garder le meilleur.<br>"+
			"De plus, le personnage gagne un sort du cercle 6 ou moins du grimoire choisi.",
			"choix" : {
				"rang": 6,
				"type": "Magie"
			},
			"type" : "passif",
			"bonus": {
				"rituel": 2,
				"crit_rituel": 1
			}
		}
		]
	},
	"Chevalier":{
		"type": "Autre",
		"Rang 1":[
		{
			"label":"Apprenti cavalier",
			"description":"Le personnage débute l’aventure avec un cheval de selle et gagne un bonus à l’équitation de 2 et au dressage. Habitué aux dépenses qu’engendre sa monture, le personnage sait flairer les bonnes affaires. Tous les prix concernant les montures, leur alimentation et les équipements sont divisés par deux.",
			"type" : "passif",
			"bonus" : {
				"equitation": 2,
				"dressage": 2
			}
		},
		{
			"label":"Quiétude",
			"description":"Contre 2 points de mana et un jet de dressage réussi, le personnage peut calmer instantanément son animal et ainsi éviter les malus d’une monture agitée. Cette action ne fait pas passer le tour et le personnage peut jouer normalement après l’avoir lancé.",
			"type" : "autre",
			"cout" : "2 PM",
			"bonus" : {

			}
		}
		],
		"Rang 2":[
		{
			"label":"Joute",
			"description":"Lorsque le cavalier lance sa monture au galop, celui-ci peut attaquer toutes les cibles adjacentes aux cases de déplacement traversées par la monture, avec un malus de 4 à chaque attaque suivant la première.",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		},
		{
			"label":"Focalisation",
			"description":"Le personnage peut réduire son malus au toucher lorsque celui-ci chevauche sa monture. Pour un tour, le coût en mana sera équivalent au montant de la réduction du malus.",
			"type" : "autre",
			"cout": "x PM",
			"bonus" : {

			}
		}
		],
		"Rang 3":[
		{
			"label":"À terre, à cheval",
			"description":"Contre 4 points de mana, le personnage peut faire passer son esquive sur la compétence d’équitation, pour toute la durée du combat.",
			"type" : "autre",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label":"Expert cavalier",
			"description":"Les actions et ordres simples d’équitation et de dressage (Pour la monture uniquement) ne nécessitent plus de test. De plus, ces mêmes compétences reçoivent un bonus de 2.",
			"type" : "passif",
			"bonus": {
				"equitation": 2,
				"dressage": 2
			}
		}
		],
		"Rang 4":[
		{
			"label":"Gardien du royaume",
			"description":"Le chevalier est un expert du combat monté et de ce fait, possède une habileté impressionnante sur le champ de bataille. En monte, le personnage ne subit plus d’attaque d’opportunité lorsqu’il passe à côté d’ennemis et il peut désormais se déplacer avant et après avoir attaqué.",
			"type" : "passif",
			"bonus" : {

			}
		},
		{
			"label":"Désarçonnement",
			"description":"Lorsque le personnage touche une cible, celui-ci peut essayer de la renverser contre 4 points de mana. S’il touche plusieurs cibles en un tour, il peut tenter pour chacune d’elle, tant qu’il dépense le mana à chaque fois. Les cibles peuvent se défendre avec un jet d’acrobaties simple. Elles ne doivent cependant pas excéder le score en force du personnage × 10 en kg, afin de pouvoir appliquer l’effet. ",
				"type" : "autre",
			"cout" : "4 PM",
			"bonus" : {

			}
		}
		],
		"Rang 5":[
		{
			"label":"Élan",
			"description":"Lorsque le cavalier lance une attaque au corps-à-corps, alors que sa monture à effectué au moins la moitié de ses déplacements dans le tour, celui-ci double ses dégâts d’arme contre 4 points de mana.",
			"type" : "damage",
			"cout": "4 PM",
			"bonus" : {

			}
		},
		{
			"label":"Harmonie fusionnelle",
			"description":"Contre 6 points de mana et durant {{1}} tours, le personnage peut fusionner avec sa monture, arborant une apparence proche d’un centaure. Le personnage peut toujours utiliser ses compétences et additionne la RD, les points de vie et les cases de déplacement de sa monture à ses attributs. Si celle-ci possède des capacités, le personnage peut désormais les utiliser.",
			"type" : "buff",
			"cout": "6 PM",
			"bonus" : {

			}
		}
		],
		"Rang 6":[
		{
			"label":"Maître cavalier",
			"description":"Le personnage gagne un bonus à l’équitation de 2, ainsi qu’au dressage. De plus, sa marge de critique de son arme augmente de 1 et gagne en plus une classe de dé sur un type d’arme de mêlée.",
			"type" : "passif",
			"bonus": {
				"equitation": 2,
				"dressage": 2,
				"critique": 1
			},
			"choix" : {
				"type": "Type_Arme_Melee",
				"bonus_classe_D": 2
			}
		}
		]
	},
	"Médecin mystique":{
		"type": "Autre",
		"Rang 1":[
		{
			"label":"Infirmier",
			"description":"Le personnage gagne un bonus à la médecine de 1, un bonus de 2 à sa marge de critique sur cette compétence et la médecine gagne une classe de dé.",
			"type" : "passif",
			"bonus" : {
				"medecine": 1,
				"crit_medecine": 2, //TODO médecine classe de dé
			}
		},
		{
			"label":"Serment de Milost",
			"description":"Lorsque le personnage utilise la médecine pour soigner, celui-ci gagne 1D de soins supplémentaire.",
			"type" : "passif",
			"bonus" : {

			}
		}
		],
		"Rang 2":[
		{
			"label":"Sauvetage",
			"description":"Le médecin peut désormais faire léviter son matériel et effectuer ses soins sur une distance de 4 mètres, contre 1 point de mana.",
			"type" : "heal",
			"cout" : "1 PM",
			"portee" : {
				"lance" : 4
			},
			"bonus" : {

			}
		},
		{
			"label":"Polyvalence",
			"description":"Le personnage obtient le don médecin de terrain. Mais, s’il possède déjà le don, la compétence médecin gagne un bonus de 3. De plus, la médecine ne consomme plus de charge de trousse de soins.",
			"type" : "passif",
			"bonus_conditionel" : {
				"condition" : {"don" : "medecin_de_terrain"},
				"oui" : {"medecine": 3},
				"non" : {"dons": ["medecin_de_terrain"]}
			},
			"bonus" : {
			}
		}
		],
		"Rang 3":[
		{
			"label":"Chirurgien",
			"description":"Si le personnage est équipé d’une lame courte, celui-ci peut utiliser sa compétence de médecine pour faire des dégâts. On utilisera alors le jet initialement dédié aux soins pour les dégâts physiques, sans bonus autorisés.",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		},
		{
			"label":"Médecin",
			"description":"Le personnage gagne un bonus à la médecine de 1, un bonus de 2 à sa marge de critique sur cette compétence et les soins gagnent 1D supplémentaire. De plus, le personnage choisit un sort du cercle 3 qu’il lancera sur la compétence médecine",
			"choix" : {
				"rang": 3,
				"type": "Magie"
			},
			"type" : "passif",
			"bonus": {
				"medecine": 1,
				"crit_medecine": 2 //TODO médecine 1D
			}
		}
		],
		"Rang 4":[
		{
			"label":"Connaissances étendues",
			"description":"Contre 4 points de mana, le personnage peut effectuer son prochain jet de médecine dans une zone de type 3. Il faudra néanmoins choisir si l’action sera dans une optique offensive ou de soins.",
			"type" : "buff",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label":"Tu ne tueras point",
			"description":"Si le personnage n’a engagé aucune action offensive durant les 3 derniers tours, les soins de sa médecine augmentent de 1D et l’action de soigner rend automatiquement au médecin {{1}} points de mana.",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		}
		],
		"Rang 5":[
		{
			"label":"Bandages professionnels",
			"description":"Si une cible a reçu un soin de la part du personnage dans la journée, sa régénération naturelle de points de vie et de mana sera doublée lors de sa prochaine nuit de sommeil. De plus, le personnage choisit un sort du cercle 5 qu’il lancera sur la compétence médecine.",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		},
		{
			"label":"Hel",
			"description":"Contre 5 points de mana, le personnage arbore une posture combative ou pacifique :<br>"+
			"La posture combative offre un bonus de 2D à toutes sources de dégâts et inflige passivement {{2}} dégâts non esquivables, dans un rayon de 10 cases autour de lui. En revanche, les soins sont réduits de 1D.<br>"+
			"La posture pacifique offre un bonus de 2D à toutes sources de soins et soigne passivement tout allié présent dans dans un rayon de 10 cases autour de lui de {{2}}. En revanche, les dégâts sont réduits de 1D.",
			"type" : "autre",
			"cout" : "5 PM",
			"bonus" : {

			}
		}
		],
		"Rang 6":[
		{
			"label":"Urgentiste",
			"description":"Le personnage gagne un de 2D sur ses soins de médecine, ainsi qu’un bonus de 3 à sa marge de critique sur cette compétence. De plus, le personnage choisit un sort du cercle 7 qu’il lancera sur la compétence médecine.",
			"type" : "passif_situationnel",
			"bonus": {
				"crit_medecine": 3
			}
		}
		]
	},
	"Artificier":{
		"type": "Autre",
		"Rang 1":[
		{
			"label":"Bombardier",
			"description":"Les explosifs ne sont plus des armes exotiques et sont utilisables, sans malus, avec la compétence d’armes de jet (mais pas avec le don évolutif). De plus, l’artificier gagne un bonus de 2 dans cette capacité.",
			"type" : "passif",
			"bonus" : {
				"jet": 2
			}
		},
		{
			"label":"Bombe de mana",
			"description":"Contre 2 points de mana, le personnage peut doubler la zone d’effet d’un explosif, ajouter 1D de dégât, ou  ajouter une classe de dé, au choix.",
			"type" : "buff",
			"cout" : "2 PM",
			"bonus" : {

			}
		}
		],
		"Rang 2":[
		{
			"label":"Machiniste",
			"description":"Le personnage peut confectionner des explosifs avec l’artisanat façonnage. Le matériel afin de les réaliser coûtera 20 Pb par bombe et nécessitera 1 heure pour créer deux explosifs.",
			"type" : "passif",
			"bonus" : {

			}
		},
		{
			"label":"Bombe de souffle",
			"description":"Le personnage sait confectionner une bombe qui, à son explosion, infligera 3D6 points de dégâts et repoussera de 4 cases toute cible ayant raté un jet d’acrobatie opposé..",
			"type" : "autre",
			"bonus" : {

			}
		}
		],
		"Rang 3":[
		{
			"label":"Bombe d’accroissement",
			"description":"Le personnage peut incorporer un poison, une drogue ou une potion à sa bombe et ainsi faire que l’effet s’applique dans une zone de 4×4. L’effet peut  fonctionner avec une fiole de soins par exemple.",
			"type" : "autre",
			"bonus" : {

			}
		},
		{
			"label":"Artilleur",
			"description":"Le personnage gagne un bonus de 2 sur la compétence d’armes de jet ainsi qu’un bonus au critique de 1 sur celle-ci. De plus, les dégâts des bombes augmentent automatiquement d’une classe de dé.",
			"type" : "passif",			
			"bonus": {
				"jet": 2,
				"type_jet": {
					"critique" : 1,
					"bonus_classe_D": 2
				}
			}
		}
		],
		"Rang 4":[
		{
			"label":"Bombe collante",
			"description":"Contre 4 points de mana, le personnage peut choisir de faire coller son projectile à sa cible. Le jet d’esquive (et pas d’acrobatie) ennemi sera en opposition et s’il échoue, la bombe infligera le double des dégâts de par la proximité de l’explosion.",
			"type" : "autre",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label":"Bombe d’os",
			"description":"Le personnage sait confectionner une bombe à dispersion. Celle-ci infligera 3D8 + 6 points de dégâts et appliquera un saignement durant 4 tours si la cible rate un jet de constitution simple.",
			"type" : "autre",
			"bonus" : {

			}
		}
		],
		"Rang 5":[
		{
			"label":"Bombe instable",
			"description":"Cette bombe est chargée en poudre explosive et s’en trouve être très instable. Le personnage voit ses chances d’échec critique augmentées de 2 (28-29-30). La bombe quant à elle, inflige 6D8 + 2 points de dégâts dans une zone 6×6.",
			"type" : "autre",
			"bonus" : {

			}
		},
		{
			"label":"Pétarade",
			"description":"Dès lors où le personnage réalise un coup critique sur un lancer d’explosifs, il peut choisir entre relancer de suite un projectile, sans malus, ou d’ajouter une classe de dé à ses dégâts. Pour le premier choix, seule la première bombe bénéficiera d’éventuels bonus.",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		}
		],
		"Rang 6":[
		{
			"label":"Pyrobombardier",
			"description":"Le personnage gagne un bonus de 2 sur la compétence d’armes de jet, ainsi qu’un bonus au critique de 1 sur celle-ci. De plus, il peut désormais fabriquer 4 explosifs par heure de travail et le coût des matériaux passe à 10 Pb l’unité.",
			"type" : "passif",
			"bonus": {
				"jet": 2,
				"type_jet": {
					"critique": 1
				}
			}
		}
		]
	},
	"Paladin sacré":{
		"type": "Autre",
		"Rang 1":[
		{
			"label":"Écuyer de la lumière",
			"description":"Apparition de la compétence « foi », basée sur le charisme, avec un bonus de 2 à celle-ci, ainsi qu’un bonus au critique de 1. La foi permet de remplacer l’incantation pour les sorts choisis dans ce don évolutif. Il faudra choisir un sort de rang 3 ou moins pouvant être lancé 3 fois par jour, sans coût en mana.",
			"choix" : {
				"rang": 3,
				"type": "Magie"
			},
			"type" : "passif",			
			"bonus" : {
				"foi": 2,
				"crit_foi": 1			
			}
		},
		{
			"label":"Force de foi",
			"description":"Le personnage, contre x points de mana (x étant le rang actuel du don) et un jet de foi réussi, peut soigner un allié se trouvant à moins de 10 mètres de 1D6 + {{1}} points de vie.",
			"type" : "heal",
			"cout" : "x PM",
			"bonus" : { //TODO faire le x = rang

			}
		}
		],
		"Rang 2":[
		{
			"label":"Assurance divine",
			"description":"Contre 3 points de mana, le personnage peut augmenter de 1 point ses compétences de charisme (hors foi). Il est possible de cumuler l’effet, en dépensant plus de mana. Cette augmentation perdure pendant une demi-heure.",
			"type" : "buff",
			"cout" : "3 PM",			
			"bonus" : {

			}
		},
		{
			"label":"Défense exaltée",
			"description":"Contre 4 points de mana et dans une zone de 20 mètres autour du personnage, tous les membres du groupe se voient octroyer un bonus de 3 à la RM pendant 4 + {{1}} tours.",
			"type" : "buff",
			"cout" : "4 PM",
			"bonus" : {

			}
		}
		],
		"Rang 3":[
		{
			"label":"Vindication",
			"description":"Contre 8 points de mana, et un jet de foi, le personnage imprègne son arme d’une force divine pendant 4+{{1}} heures. Cette action instantanée octroie un bonus aux dégâts de xD4 dégâts magiques supplémentaires (x étant le rang actuel du don). Si le personnage lâche son arme, alors l’effet s’estompe.",
			"type" : "buff",
			"cout" : "8 PM",
			"bonus" : {

			}
		},
		{
			"label":"Chevalier béni",
			"description":"Le personnage gagne un bonus de 2 sur sa compétence de foi, ainsi qu’un bonus de 1 à la marge de critique de cette même compétence. De plus, il pourra choisir un nouveau sort de rang 5 ou moins dans le même grimoire qu’au rang 1 et pourra le lancer 2 fois par jour, gratuitement, avec un jet de foi.",
			"type" : "passif",
			"choix": {
				"type": "Magie",
				"rang": 5
			},			
			"bonus": {
				"foi": 2,
				"crit_foi" : 1
			}
		}
		],
		"Rang 4":[
		{
			"label":"Soutien indéfectible",
			"description":"«Force de foi» gagne un bonus à son effet de 2D et peut désormais être augmenté avec des dons comme un sort classique. Le sort peut également être lancé sur soi à partir de maintenant.",
			"type" : "passif",
			"bonus" : {

			}
		},
		{
			"label":"Impartialité",
			"description":"Contre 8 points de mana et un jet de foi réussi, le paladin enchante son arme (de manière instantanée) pour les 10 prochains tours. À chaque attaque réussie, l’arme ponctionnera la santé de l’ennemi et soignera son propriétaire de la moitié des dégâts infligés.",
			"type" : "buff",
			"cout" : "8 PM",			
			"bonus" : {

			}
		}
		],
		"Rang 5":[
		{
			"label":"Dévotion incorruptible",
			"description":"Le personnage gagne un bonus à la volonté de {{1}}. De plus, l’effet d’inspiration est augmenté. Lorsque le personnage réalise un coup critique, tous les alliés regardants vers le personnage reçoivent un bonus à la volonté de 4 jusqu’à la fin du combat (non cumulable).",
			"type" : "passif_situationnel",				
			"bonus" : {
				"volonte_par_niveaux": 1
			}
		},
		{
			"label":"Persuasion éclatante",
			"description":"Contre 12 points de mana et un jet de foi opposé à la volonté de la cible, le personnage peut forcer une personne à agir selon ses désirs. Une fois un ordre donné, la cible s’efforcera de le réaliser, sans pour autant oublier sa propre survie (manger, boire, dormir). Les instructions autodestructrices ne seront jamais suivies. Une fois l’ordre réalisé, le charme disparaît et la cible n’en gardera aucun souvenir.",
			"type" : "autre",
			"cout" : "12 PM",
			"bonus" : {

			}
		}
		],
		"Rang 6":[
		{
			"label":"Paladin glorieux",
			"description":"Le personnage gagne un bonus de 2 sur sa compétence de foi. De plus, il pourra choisir un nouveau sort de rang 7 ou moins dans le grimoire choisi au préalable et pourra le lancer 1 fois par jour, gratuitement, sur un jet de foi. Le personnage gagne également un bonus de 1 sur toutes les compétences sociales (hors foi).",
			"type" : "passif",			
			"choix": {
				"type": "Magie",
				"rang": 7
			},
			"bonus": {
				"foi": 2,
				"bluff" : 1,
				"representation" : 1,
				"eloquence" : 1,
				"provocation" : 1
			}
		}
		]
	},
		"Officier diplomatique":{
		"type": "Autre",
		"Rang 1":[
		{
			"label":"Capitaine",
			"description":"Les compétences de psychologie et d’éloquence fusionnent pour devenir « l’instigation », basée sur le charisme. Le personnage gagne un bonus de 2 à celle-ci, ainsi qu’un bonus de 1 au critique. Le jet d’instigation est une action bonus de début du tour.",		
			"type" : "passif",
			"bonus" : {
				"instigation": 2,
				"crit_instigation": 1			
			}
		},
		{
			"label":"Repli stratégique",
			"description":"Contre 3 points de mana, un jet d’instigation et pendant 1D4 + {{1}} tours, celui-ci augmente de son niveau la vitesse de déplacement de ses alliés se trouvant dans un rayon de 30 mètres autour de lui.",
			"type" : "buff",
			"cout" : "3 PM",
			"bonus" : {

			}
		}
		],
		"Rang 2":[
		{
			"label":"Ralliement",
			"description":"Lorsque le personnage réussit son jet d’aura, tous les alliés assistant à la scène récupèrent {{2}} en points de vie ou mana. L’effet habituel de l’aura s’applique ensuite.",
			"type" : "passif_situationnel",			
			"bonus" : {

			}
		},
		{
			"label":"Discours motivant",
			"description":"Contre un jet d’instigation réussi et contre 4 points de mana, le personnage peut offrir à un allié sa marge de réussite pour son prochain jet. On ajoute la marge de réussite du jet d’instigation du personnage en tant que bonus. La dépense en mana se fait avant de connaître le résultat du jet.",
			"type" : "buff",
			"cout" : "4 PM",
			"bonus" : {

			}
		}
		],
		"Rang 3":[
		{
			"label":"Commandeur",
			"description":"Le personnage gagne un bonus à l’instigation de 2 et peut choisir un don évolutif d’arme et gagne un rang 3 (avec mention « expert ») de celui-ci.",
			"type" : "passif",
			"choix" : { 
				"rang": 3, 
				"type": "Arme" 
			}, 
			"bonus" : { 
			"instigation": 2
			} 
		}, 
		{ 
			"label":"Insufflation de bravoure",
			"description":"Contre 3 points de mana et un jet d’instigation réussi, le personnage peut insuffler du courage à tous les alliés dans un rayon de 30 mètres. Pendant 1D4 + {{1}} tour, ils deviennent insensibles à la peur. S’ils étaient déjà sous l’emprise d’un effet similaire, cet effet disparaît.",
			"type" : "buff",
			"cout" : "3 PM",
			"bonus" : {
			}
		}
		],
		"Rang 4":[
		{
			"label":"Encensement",
			"description":"Contre 4 points de mana et un jet d’instigation réussi, tous les alliés présents dans un rayon de 30 mètres verront leur prochaine attaque infliger 1D ou une classe de dé supplémentaire.",
			"type" : "buff",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label":"Tactique unificatrice",
			"description":"Contre 6 points de mana et un jet d’instigation, le personnage coordonne ses alliés (situés dans un rayon de 10 mètres autour de lui) de manière à ce que leurs prochaines attaques outrepassent la RD de la cible.",
			"type" : "buff",
			"cout" : "6 PM",			
			"bonus" : {

			}
		}
		],
		"Rang 5":[
		{
			"label":"Manipulation émotionnelle",
			"description":"Contre un jet d’instigation réussi et 8 points de mana, le personnage peut manipuler subtilement les émotions d’une cible. Il peut calmer les tensions, inspire confiance, provoquer la peur ou la colère, etc. Si une cible résiste une première fois à la manipulation, elle devient insensible à celle-ci.",
			"type" : "autre",
			"cout" : "8 PM",				
			"bonus" : {
				
			}
		},
		{
			"label":"Synergie",
			"description":"À chaque fois que le personnage arrive à parer une attaque au corps-à-corps, il aura droit à un jet d’instigation. S’il réussit, chaque allié présent dans un rayon de 10 mètres attaquera automatiquement (à condition de la voir, d’être à portée et en mesure d’attaquer), grâce à une attaque bonus, guidée par le personnage.",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		}
		],
		"Rang 6":[
		{
			"label":"Connétable",
			"description":"Lorsque le personnage effectue un jet (peu importe la compétence) qui est opposé à la volonté d’une cible, cette dernière aura naturellement un malus de {{1}} à sa volonté. De plus, le personnage peut désormais donner deux ordres en un tour. Il faudra dépenser les deux coûts en mana et lancer deux jets de commandement. Il est toutefois impossible de donner deux fois le même ordre.",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		}
		]
	},
	"Archétype guerrier":{
		"type": "Autre",
		"Rang 1":[
		{
			"label":"Bleu",
			"description":"Ne peut être pris qu’après un don évolutif classique. Le personnage gagne 10 points de vie, un bonus naturel de 1 à la RD et un bonus de 2 dégâts sur un type d’arme de mêlée (hors couteaux).",
			"type" : "passif",
			"bonus" : {
				"RD": 1,
				"pv": 10
			},
			"choix" : {
				"type": "Type_Arme_Melee",
				"bonus_dmg": 2
			}
		},
		{
			"label":"Partie de soi",
			"description":"Le personnage dépense {{1}} points de vie et invoque à ses côtés (5 mètres maximum) un esprit guerrier spécifique au don classique choisi avant ce don-ci. Il est considéré comme une invocation, avec toutes les conditions que cela implique.",
			"type" : "invocation",
			"cout" : "x PV",
			"bonus" : {

			}
		}
		],
		"Rang 2":[
		{
			"label":"Contre-coup",
			"description":"Contre 2 points de mana, le personnage réduit de moitié les dégâts subits et résiste aux états préjudiciables pendant tout un tour. En revanche, le tour suivant, tous les dégâts reçus sont augmentés de moitié.",
			"type" : "autre",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label":"Essence guerrière",
			"description":"Contre 6 points de mana, le personnage entoure son arme ou ses poings d’une aura magique, qui perdurera pendant {{1}} tours. Cette énergie peut être tirée sur une distance de 15 mètres, mais fait disparaître l’enchantement.",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		}
		],
		"Rang 3":[
		{
			"label":"Désinvolture",
			"description":"Contre 5 points de mana, il est désormais possible de changer l’attribut principal d’une compétence pour faire passer le jet sur la constitution. Hormis pour l’incantation et la survie, les compétences basées sur l’intelligence et la sagesse ne peuvent être modifiées.",
			"type" : "autre",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label":"Va-t-en-guerre",
			"description":"Le personnage gagne 20 points de vie, un bonus naturel de 1 à la RD et une classe de dé sur un type d’arme de mêlée (hors couteaux).",
			"type" : "passif",			
			"bonus" : {
				"RD": 1,
				"pv": 20,
			},
			"choix" : {
				"type": "Type_Arme_Melee",
				"bonus_classe_D": 2
			}
		}
		],
		"Rang 4":[
		{
			"label":"Sanction",
			"description":"Lorsqu’un ennemi fait perdre des points de vie au personnage, celui-ci peut dépenser 5 points de mana afin de pouvoir contre-attaquer, même si ce n’est pas son tour.",
			"type" : "damage",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label":"Liens du sang",
			"description":"Contre 4 points de vie, sur une distance de 20 mètres et durant toute la durée du combat, le personnage parfait son invocation. Ainsi, la moitié des dégâts qu’elle inflige seront convertis en soins pour le personnage.",
			"type" : "heal",
			"cout" : "4 PV",
			"bonus" : {

			}
		}
		],
		"Rang 5":[
		{
			"label":"Équilibre",
			"description":"Le personnage dépense 10 points de vie afin de drainer le mana de tous les ennemis présents dans une zone de type 2 autour de lui. Le personnage vole {{1}} points de mana à chaque cible et les transforme en points de bouclier. Il est impossible de superposer deux boucliers et les actions consommant des points de vie, puisent dans les points de bouclier.",
			"type" : "autre",
			"cout" : "10 PV",
			"bonus" : {

			}
		},
		{
			"label":"Fureur",
			"description":"Dès que le personnage perd des points de vie, celui-ci gagne 1 point de rage. Quatre points de rage dépensés offrent à un bonus de 1D sur les dégâts de l’arme.",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		}
		],
		"Rang 6":[
		{
			"label":"Maître d’armes",
			"description":"Le personnage gagne 30 points de vie, un bonus naturel de 1 à la RD et 1 dé sur un type d’arme de mêlée (hors couteaux).",
			"type" : "passif",
			"bonus" : {
				"RD": 1,
				"pv": 30
			},
			"choix" : {
				"type": "Type_Arme_Melee",
				"bonus_D_dmg": 1
			}
		}
		]
	},
	"Archétype mage":{
		"type": "Autre",
		"Rang 1":[
		{
			"label":"Magicien",
			"description":"Ne peut être pris qu’après une magie. Le personnage gagne 10 points de mana et chaque don de magie n’octroie plus un bonus de 5 points de mana, mais 7.",
			"type" : "passif",
			"bonus" : {
				"pm": 10,
				"bonus_dons_magique_mana" : 2
			}
		},
		{
			"label":"Concentration payante",
			"description":"La méditation rend désormais 2+{{2}} points de mana par heure. Si le personnage médite sans être dérangé (aucune interaction et pas de déplacement), la régénération passe à 2+{{3}}.",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		}
		],
		"Rang 2":[
		{
			"label":"Examen de l’âme",
			"description":"Contre 4 points de mana, le personnage peut sonder l’aura d’un humanoïde et en déduire différentes informations. Il pourra, entre autres, détecter l’alignement d’une cible en fonction de la couleur de l’aura, ainsi que le niveau de puissance qu’elle dégage. Il sera également possible de savoir si la cible possède un lien plus ou moins fort avec la magie.",
			"type" : "autre",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label":"Prédiction",
			"description":"Contre 5 points de mana, le personnage entame un rituel de 30 minutes et interroge l’espace-temps afin de connaître l’avenir sur le court terme (24 heures). La question doit être précise, sous peine de ne pas recevoir de réponse. Celles-ci seront d’ailleurs concises et courtes.",
			"type" : "rituel",
			"cout" : "5 PM",
			"bonus" : {

			}
		}
		],
		"Rang 3":[
		{
			"label":"Emmagasinage",
			"description":"Le personnage peut stocker de son mana dans une seule arme ou tenue. Il pourra amonceler un maximum de {{5}} points dans l’objet désiré. Le transfert fait perdre des points de mana au personnage qui pourront être récupérés naturellement. Par la suite, le mana contenu dans l’objet pourra être utilisé à tout moment, telle une potion de mana.",
			"type" : "autre",
			"cout" : "X PM",
			"bonus" : {

			}
		},
		{
			"label":"Sorcier",
			"description":"Le personnage gagne 20 points de mana et la portée de tous ses sorts est augmentée de 5 mètres.",
			"type" : "passif",			
			"bonus" : {
				"pm": 20,
				"portee_sort" : 5
			}
		}
		],
		"Rang 4":[
		{
			"label":"Un livre ouvert",
			"description":"Contre 10 points de mana et un jet d’incantation, le personnage est capable de lire dans les pensées d’une cible. L’effet dure 10+{{1}} minutes et peut être contré si la cible réussit un jet de volonté opposé.",
			"type" : "autre",
			"cout" : "10 PM",
			"bonus" : {

			}
		},
		{
			"label":"Bouleversement des sorts",
			"description":"Le mage peut modifier les zones d’effet magiques à sa guise. Il faudra compter 6 points de mana pour passer d’une zone de type 1 à une zone de type 2 et 12 points de mana si l’on veut passer de 1 à 3 (idem pour la réduction). À noter que le genre ne peut pas être modifié, un cône pourra être plus ou moins grand, mais ne pourra devenir une zone de type 1.",
			"type" : "buff",
			"cout" : "6 ou 12PM",
			"bonus" : {

			}
		}
		],
		"Rang 5":[
		{
			"label":"Vide-poches",
			"description":"Contre 6 points de mana, le personnage entame un rituel de 10 minutes et créer une déchirure dans l’espace-temps dans lequel il pourra stocker autant d’objets qu’il le désire. Il sera possible de récupérer (intacts) tous les objets en invoquant de nouveau le trou.",
			"type" : "rituel",
			"cout" : "6 PM",
			"bonus" : {

			}
		},
		{
			"label":"Vol de savoir",
			"description":"Contre 8 points de mana et pendant le niveau du mage en heures, le personnage acquiert toutes les connaissances de sa cible et est capable d’imiter tous ses pouvoirs magiques ou psychiques. Une cible possédant une intelligence d’au moins 15 sera capable de tenter de ne pas laisser le mage entrer dans son esprit, à l’aide d’un jet de volonté opposé.",
			"type" : "buff",
			"cout" : "8 PM",
			"bonus" : {

			}
		}
		],
		"Rang 6":[
		{
			"label":"Archimage",
			"description":"Le personnage gagne 30 points de mana. De plus, il choisit 5 sorts dont le coût en mana baisse de 1 (minimum 1) et dont la marge de critique augmente de 1.",
			"type" : "passif",
			"bonus" : {
				"pm": 30,
				"cinq_sorts" : 1 // TODO, je ne savais pas si c’est possible ou pas alors je mets un todo
			}
		}
		]
	},
	"Archétype voleur":{
		"type": "Autre",
		"Rang 1":[
		{
			"label":"Filou",
			"description":"Le personnage gagne un bonus de 1 dans deux compétences liées à la dextérité et un bonus de 2 dégâts sur un type d’arme à distance, ou couteaux.",
			"type" : "passif",
			"bonus" : {
				"bonus_Pts_Competences": 2
			},
			"choix" : {
				"type": "Type_Arme_Distance",
				"bonus_dmg": 2
			}
		},
		{
			"label":"Aptitude",
			"description":"Contre 3 points de mana et durant 1 heure, le personnage peut faire passer les compétences d’athlétisme et de manœuvres sur la dextérité.",
			"type" : "autre",
			"cout" : "3 PM",
			"bonus" : {

			}
		}
		],
		"Rang 2":[
		{
			"label":"Ouverture",
			"description":"Lorsqu’un ennemi est à portée et qu’il réalise un échec critique sur son attaque, le personnage peut en profiter pour l’attaquer. Si plusieurs échecs critiques sont réalisés, le personnage pourra attaquer s’il à la portée nécessaire et l’angle de vue pour voir l’action.",
			"type" : "passif_situationnel",
			"bonus" : {

			}
		},
		{
			"label":"Opportunités",
			"description":"Le personnage ne provoque plus d’attaque d’opportunité lorsqu’il se déplace près d’ennemis ou se désengage. De plus, s’il manie une arme à distance et qu’un ennemi sort de son premier incrément de portée, cela provoque une attaque d’opportunité.",
			"type" : "passif_situationnel",
			"bonus" : {
			}
		}
		],
		"Rang 3":[
		{
			"label":"Piper les dés",
			"description":"Le personnage sait mettre toutes les chances de son côté pour les compétences de discrétion, acrobaties, sabotage, commerce et bluff. Il est possible de convertir 2 points de mana contre 1 point de compétence, pour le prochain jet concerné.",
			"type" : "buff",
			"cout" : "2 PM pour 1 point",
			"bonus" : {

			}
		},
		{
			"label":"Gredin",
			"description":"Le personnage gagne un bonus de 1 point en dextérité, une case de déplacement supplémentaire et un bonus d’une classe de dé sur un type d’arme à distance, ou couteaux.",
			"type" : "passif",			
			"bonus" : {
				"dexterite": 1,
				"deplacement": 1
			},
			"choix" : {
				"type": "Type_Arme_Distance",
				"bonus_classe_D": 2
			}
		}
		],
		"Rang 4":[
		{
			"label":"Bassesse",
			"description":"Contre 5 points de mana, le personnage génère un fumigène laissant s’échapper une nappe de fumée opaque, d’une zone de type 3. Toute personne dans la zone souffre d’une toux qui inflige un malus de 4 au toucher et un autre malus de 6 à la défense. La zone perdure pendant {{1}} tours et possède un temps de recharge de 3 tours.",
			"type" : "debuff",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label":"Perception implacable",
			"description":"Contre 8 points de mana, le personnage déploie une vigilance prodigieuse dans un rayon de 10 mètres autour de lui. Cette capacité révèle immédiatement toutes les créatures dissimulées ou invisibles à proximité. Elle permet également de détecter les pièges et de mettre en lumière les passages secrets.",
			"type" : "buff",
			"cout" : "8 PM",
			"bonus" : {

			}
		}
		],
		"Rang 5":[
		{
			"label":"Incognito",
			"description":"Hors combat, si le jet de discrétion est réussi, il devient impossible pour les PNJ de détecter le personnage (hors critique de leur part). Contre 5 points de mana, la capacité est utilisable également en combat et permet d’effectuer une attaque de manière totalement furtive.",
			"type" : "buff",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label":"Mesquinerie",
			"description":"Contre 5 points de mana, le personnage augmente sa célérité et exploite les failles des mouvements d’une cible. Toutes ses attaques contre elle sont considérées comme des attaques sournoises, avec tous les bonus applicables à ce type d’attaque.",
			"type" : "buff",
			"cout" : "5 PM",
			"bonus" : {

			}
		}
		],
		"Rang 6":[
		{
			"label":"Maître escroc", // A voir
			"description":"Le personnage gagne un bonus de 2 dans quatre compétences liées à la dextérité, ainsi qu’un bonus à la marge de critique de 1 sur ces mêmes compétences. Il gagne également un bonus d’un dé sur un type d’arme à distance, ou couteaux.",
			"type" : "passif",
			"bonus" : {
				"bonus_Pts_Competences": 8
			},
			"choix" : {
				"type": "Type_Arme_Distance",
				"bonus_D_dmg": 1
			}
		}
		]
	},
	"Aéromancie" : {
		"magie" : true,
		"style" : {
			"background" : "aeromancie.jpg",
			"color" : "#000000",
			"shadow": "#CCFFFF",
		},
		"type": "Magie",
		"Rang 1": [
		{
			"label": "Apnée",
			"portee" : {
				"personnel" : true
			},
			"description" : "Ce sort oxygène directement le sang du mage et augmente le temps pendant lequel le magicien peut maintenir sa respiration, la faisant passer à 1+{{1}} heures.",
			"type" : "buff",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label": "Disque de vent",
			"portee" : {
				"lance" : 10
			},
			"defense" : "opposée – esquive ou bouclier",
			"description" : "Le mage génère un disque de vent dans la paume de ses mains et le lance en direction d’un ennemi. Le disque inflige 1D4 + {{{1}}, déchirant la chair par la puissance de rotation.",
			"type" : "damage",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label": "Bourrasque",
			"portee" : {
				"description" : "cône en face du lanceur"
			},
			"defense" : "opposée – ténacité",
			"description" : "PLa rafale permet de repousser une ou des personnes/objets en face du lanceur, la distance du repoussement sera égale à {{1}} cases. Si une personne se trouve en face du lanceur, elle protégera involontairement les autres cibles se trouvant derrière. La réussite du mage sera divisée par deux pour les faire chuter. La première cible devra réussir un jet de ténacité pour ne pas se faire éjecter, si elle le rate, elle percutera les personnes derrière elle, leur appliquant un malus de 3 pour leur propre jet de ténacité.",
			"type" : "autre",
			"cout" : "2 PM",
			"bonus" : {

			}
		}
		],
		"Rang 2": [
		{
			"label": "Bulle d’air",
			"portee" : {
				"lance" : 5,
				"description" : "5 mètres ou personnelle"
			},
			"description" : "Le mage créer une bulle d’air autour de la tête d’une cible ou de la sienne pendant {{1}} heures. La bulle ne peut pas être percée et possède les mêmes caractéristiques que le sort apnée. La différence vient du fait que la bulle permet de parler sous l’eau ou de cibler un petit objet pour le protéger des dégâts de l’eau.",
			"type" : "buff",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		{
			"label": "Pieds lestes",
			"portee" : {
				"lance" : 10,	
				"personnel" : true
			},
			"description" : "La personne visée se sent plus légère, son poids ne l’entrave plus comme à l’habituel. Sa vitesse de déplacement est doublée pour son prochain tour.",
			"type" : "buff",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		{
			"label": "Création venteuse mineure",
			"portee" : {
				"lance" : 5
			},
			"description" : "L’aéromancien rassemble les vents environnants pour forger un petit humanoïde élémentaire. Se référer à fiche des invocations de l’aéromancie.",
			"type" : "invocation",
			"cout" : "3 PM",
			"bonus" : {

			}
		}
		],
		"Rang 3": [
		{
			"label": "Lévitation",
			"portee" : {
				"lance" : 5,	
				"personnel" : true
			},
			"defense" : "aucune",
			"description" : "Le mage se fait léviter ou porte une cible de moins de 100 kilos. Le sort doit être canalisé, sous peine de faire chuter la cible, lui infligeant les dégâts de chute en conséquence. Le mage peut déplacer la cible de 2+{{1}} mètres à chaque tour, dans n’importe quelle direction. Pendant qu’elle lévite, la cible ne peut subir de dégâts.",
			"type" : "autre",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label": "Sculpture de vent",
			"portee" : {
				"lance" : 15,
				"description" : "sur trois cases verticales ou horizontales"
			},
			"description" : "Le magicien fait surgir un mur venteux de 3 mètres de large pour 2+{{1}} mètres de haut. Le mage à la possibilité de <b>canaliser</b> le sort contre 1 point de mana par tour, ce qui aura pour effet d’allonger la sculpture de 2 mètres à chaque fois. Le mur restera actif 5 minutes après l’arrêt de la canalisation. Traverser le mur prendra un tour complet à une unité, arrêtant ses déplacements. Elle pourra retrouver une mobilité normale au prochain tour. Aucun projectile physique ne peut traverser ce mur, ils seront automatiquement déviés.",
			"type" : "autre",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label": "Création de monture élémentaire",
			"portee" : {
				"lance" : 3
			},
			"description" : "Le mage façonne un destrier de vent, dont l’apparence sera fixe et sera définie à la création par le joueur et le MJ. Se référer à fiche des invocations de l’aéromancie.",
			"type" : "invocation",
			"cout" : "4 PM",
			"bonus" : {

			}
		}
		],
		"Rang 4": [
		{
			"label": "Souffle",
			"portee" : {
				"description" : "En cône devant le lanceur"
			},
			"defense" : "opposée – athlétisme ou acrobatie",
			"description" : "Ce sort permet au lanceur de remplir ses poumons d’air et d’exhaler un vent puissant entrant en collision avec un groupe d’ennemis, infligeant 2D6 + {{1}} dégâts. Si une personne se trouve en face du lanceur, elle protégera involontairement les autres cibles se trouvant derrière, les dégâts subits seront alors divisés par deux.",
			"type" : "damage",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": " Asphyxie",
			"portee" : {
				"lance" : 10,
				"description" : "dans une zone de type 1"
			},
			"defense" : "aucune",
			"description" : "Sort à <b>canaliser</b> qui videra la zone de tout air, même celui contenu dans les poumons des victimes. Toute personne emprisonnée à l’intérieur de la zone perdra des points de souffle en condition de combat. Lorsque ceux-ci tombent à 0, les cibles tombent dans le coma. Elles perdent des points de vie chaque tour, puis des points de vie temporaires, jusqu’à la mort par asphyxie. Les cibles qui ne sont pas considérées comme immobilisées ou entravées et peuvent sortir de la zone. Cependant, leur vitesse de déplacement divisée par deux à cause du manque d’air.",
			"type" : "autre",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Chute",
			"portee" : {
				"description" : "zone de type 1 autour du lanceur"
			},
			"description" : "Dans une faible zone autour du lanceur, toute personne se trouvant à l’intérieur verra sa vitesse de chute réduite, évitant les dégâts de chute pendant 1+{{1}} minutes ",
			"type" : "buff",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Création venteuse",
			"portee" : {
				"lance" : 5
			},
			"description" : "Le mage rassemble les vents environnants pour forger un être élémentaire. Se référer à fiche des invocations de l’aéromancie.",
			"type" : "invocation",
			"cout" : "5 PM",
			"bonus" : {

			}
		}
		],
		"Rang 5": [
		{
			"label": "Supersonique",
			"portee" : {
				"lance" : 1,	
				"personnel" : true
			},
			"description" : "Le mage ou une cible peut se déplacer à la vitesse du son, en contrepartie d’un bruit énorme interdisant tout jet lié à la discrétion. De ce fait, la cible peut se déplacer où elle veut ce tour-ci dans un rayon de 50 mètres.",
			"type" : "buff",
			"cout" : "6 PM",
			"bonus" : {

			}
		},
		{
			"label": "Vol céleste",
			"portee" : {
				"personnel" : true
			},
			"description" : "Dote le magicien d’une paire d’ailes éthérées vertes, qui lui permettent de voler pendant 10+{{1}} minutes . Le vol sera magique et soumis aux malus de déplacement et toucher liés au vol.",
			"type" : "buff",
			"cout" : "6 PM",
			"bonus" : {

			}
		}
		],
		"Rang 6": [
		{
			"label": "Création venteuse majeure",
			"portee" : {
				"lance" : 5
			},
			"description" : "Le mage réunit les vents à proximité pour forger un grand élémentaire. Se référer à fiche des invocations de l’aéromancie.",
			"type" : "invocation",
			"cout" : "7 PM",
			"bonus" : {

			}
		},
		{
			"label": "Purification de l’air",
			"portee" : {
				"description" : "zone de type 3, autour du lanceur"
			},
			"description" : "Ce sort permet de rendre l’air respirable dans une zone, éliminant gaz et autres substances nocives à inhaler. Le sort dure 1D4 + {{1}} tours et peut être relancé alors qu’une zone est déjà active.",
			"type" : "autre",
			"cout" : "7 PM",
			"bonus" : {

			}
		}
		],
		"Rang 7": [
		{
			"label": "Décollage!",
			"portee" : {
				"lance" : 15,
				"description" : "dans une zone de type 2"
			},
			"defense" : "opposée – ténacité",
			"description" : "D’un geste de la main, le mage créer une forte rafale de vent venant souffler les ennemis et les envoyant valser dans les airs sur 3+{{1}} mètres de haut, provoquant des dégâts de chute en fonction de la distance.",
			"type" : "damage",
			"cout" : "8 PM",
			"bonus" : {

			}
		},
		{
			"label": "Lame de vent",
			"portee" : {
				"lance" : 15
			},
			"defense" : "opposée – esquive ou bouclier",
			"description" : "L’aéromancien invoque une épée éthérée, aussi légère qu’une plume. Elle se dirigera automatiquement vers la cible et infligera 2D12 + {{1}} dégâts . Une fois le coup porté, la lame disparaîtra instantanément.",
			"type" : "damage",
			"cout" : "8 PM",
			"bonus" : {

			}
		}
		],
		"Rang 8": [
		{
			"label": "Tornade",
			"portee" : {
				"lance" : 15,
				"description" : "dans une zone de type 3"
			},
			"defense" : "opposée – acrobatie ou athlétisme (si la distance à sauter est de 2 cases ou moins)",
			"description" : "Le mage invoque une tornade à partir de vents violents qui infligent 6D6 dégâts + 1 par niveau à toute entité. Jusqu’au prochain tour du mage, la zone de la tornade empêche toutes les attaques à distance non magiques. De plus, à l’intérieur, chaque mètre nécessite d’utiliser deux cases de déplacement.",
			"type" : "damage",
			"cout" : "9 PM",
			"bonus" : {

			}
		},
		{
			"label": "Souffle divin",
			"portee" : {
				"personnel" : true
			},
			"defense" : "aucune",
			"description" : "Le mage gagne 20 points en esquive pendant 1D4+{{1}} tours . Dès qu’il subit des dégâts au contact, il renvoie 2D6 dégâts automatiquement à son assaillant, ne subissant pas ces mêmes dégâts.",
			"type" : "buff",
			"cout" : "9 PM",
			"bonus" : {

			}
		}
		]
	},
	"Chronomancie" : {
		"magie" : true,
		"style" : {
			"background" : "chronomancie.jpg",
			"color" : "#9291f3",
			"shadow": "black",
		},
		"type": "Magie",
		"Rang 1": [
		{
			"label": "Ralentissement",
			"portee" : {
				"lance" : 10
			},
			"defense" : "opposée – volonté",
			"description" : "Ce sort influence la vitesse d’écoulement du temps et diminue la vitesse de déplacement de la cible, lui retirant la moitié de sa vitesse de déplacement pendant 2+{{1}} tours.",
			"type" : "debuff",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label": "Conservation",
			"portee" : {
				"lance" : 1,
				"description" : "contact sur un objet"
			},
			"description" : "Le mage cible un conteneur et toute denrée périssable à l’intérieur se fige dans le temps. Cette manipulation offre une conservation et une fraîcheur parfaite de ceux-ci. L’effet perdure pendant 1D4 + {{1}} jours.",
			"type" : "autre",
			"cout" : "2 PM ",
			"bonus" : {

			}
		},
		{
			"label": "Double",
			"portee" : {
				"lance" : 10
			},
			"description" : "Le chronomancien invoque un double de lui-même venant d’un futur ou passé de quelques secondes d’intervalle, en le positionnant aux côtés d’un ennemi. Le double infligera les dégâts de l’arme du mage, et s’il n’en possède pas, donnera un coup de poing. Les chances de toucher ainsi que les dégâts sont les mêmes que ceux du mage. Le double disparaît après l’attaque, qu’elle soit réussie ou ratée. Pour esquiver cette attaque, un jet d’esquive standard, opposé à la réussite du double sera nécessaire.",
			"type" : "invocation",
			"cout" : "2 PM",
			"bonus" : {

			}
		}
		],
		"Rang 2": [
		{
			"label": "Vol de temps",
			"portee" : {
				"lance" : 15
			},
			"defense" : "opposée – ténacité",
			"description" : "Le lanceur de sorts agit comme une éponge et absorbe l’essence vitale de sa cible pour se fortifier. Il inflige 1D6 + {{1}} dégâts qu’il convertit ensuite en soins personnels.",
			"type" : "damage",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		{
			"label": "Croissance contrôlée",
			"portee" : {
				"lance" : 1,
				"description" : "zone de type 1"
			},
			"defense" : "opposée – ténacité",
			"description" : "Le mage <b>canalise</b> un sort qui fera vieillir les êtres vivants présents dans la zone de 1+{{{1}} mois, par tour. Cela peut être aussi appliqué sur une personne que sur une plante ou un animal.",
			"type" : "autre",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		{
			"label": "Contretemps",
			"portee" : {
				"lance" : 15
			},
			"defense" : "opposée – esquive si le renvoi est réussi",
			"description" : "Le mage fige le temps devant lui et le remonte, pouvant ainsi renvoyer un projectile d’où il est venu. Ce sort est à utiliser à la demande du joueur, en cas d’attaque contre lui. Il faudra en informer le MJ avant que celui-ci n’indique si l’attaque touchait ou non. Si le mage n’a plus assez de mana pour lancer le sort, rien ne se passera.",
			"type" : "reaction",
			"cout" : "3 PM",
			"bonus" : {

			}
		}
		],
		"Rang 3": [
		{
			"label": "Vision temporelle",
			"portee" : {
				"personnel" : true
			},
			"description" : "Avec ce sort, le mage gagne la capacité d’espionner des personnes ou un lieu dans une zone bien précise (comme une ville) du passé. Le sort dure 10+{{1}} minutes  et le lieu doit forcément être en endroit que le mage a déjà visité. Le déplacement restera possible, mais pas les interactions. Le mage ne sera pas perceptible par les entités qui évoluent dans la zone et n’aura aucun impact sur le présent, il n’est là qu’en spectateur. Le mage peut mettre fin au sort plus tôt s’il le souhaite.",
			"type" : "autre",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label": "Un coup d’avance",
			"portee" : {
				"lance" : 5
			},
			"description" : "Le mage accélère la vitesse d’attaque d’un allié, permettant d’effectuer une action ou une attaque supplémentaire lors de son prochain tour. Si la cible ne profite pas de l’effet durant son tour, le sort se dissipera et l’effet sera perdu. Augmenter sa cadence d’attaque n’infligera pas de malus, mais un jet pour toucher la cible sera nécessaire à chacune des attaques.",
			"type" : "buff",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label": "Soins accélérés",
			"portee" : {
				"lance" : 1,	
				"personnel" : true
			},
			"description" : "Ce sort permet de créer une faille temporelle au niveau d’une blessure et d’accélérer la régénération naturelle de celle-ci, vieillissant la plaie et rendant 2D4 + {{1}} points de vie.",
			"type" : "heal",
			"cout" : "4 PM",
			"bonus" : {

			}
		}
		],
		"Rang 4": [
		{
			"label": "Téléportation",
			"portee" : {
				"lance" : 20,
				"description" : "personnelle"
			},
			"description" : "Le lanceur marche à une vitesse phénoménale durant quelques secondes, ce qui lui permet de rejoindre une zone sans que l’œil humain ne puisse le suivre, donnant l’impression d’une téléportation. Il est impossible d’attaquer après ce sort ou d’effectuer une action, seule l’utilisation des cases de déplacement est autorisée.",
			"type" : "autre",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Espace psyché",
			"portee" : {
				"description" : "zone de type 1, autour du lanceur"
			},
			"description" : "Le chronomancien <b>canalise</b> et forme un dôme solide dans un espace circulaire autour de lui. Le dôme est indestructible de l’extérieur et les personnes situées à l’intérieur sont enfermées dans une dimension parallèle où le temps n’a pas d’effet et/ou la magie est inutilisable. Aucune attaque ne peut être lancée de l’intérieur, en revanche, les alliés enfermés à l’intérieur peuvent se reposer et régénérer leurs points de vie ou de mana, si le mage maintient le sort suffisamment longtemps. Aucune intrusion, même d’un allié n’est possible tant que le sort est gardé canalisé. Le coût de la canalisation sera de 2 points de mana par tranches de 10 minutes, si le sort est effectué en combat, tous les membres présents dans le dôme seront considérés comme hors combat.",
			"type" : "autre",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Horloge",
			"portee" : {
				"lance" : 15
			},
			"defense" : "opposée – trois jets d’esquive ou de bouclier (un malus de 2 pour le deuxième et un malus de 4 pour le troisième)",
			"description" : "Le mage invoque une horloge éthérée, dont les aiguilles se disloquent et foncent sur une seule et même cible (trois aiguilles: heures, minutes et secondes). Le chronomancien doit lancer un jet de toucher, ainsi qu’un jet de dégâts par aiguille. La plus grande faisant 1D8 + {{0.5}} dégâts, 1D6 + {{1.5}} dégâts pour la moyenne et 1D4 + {{0.5}} dégâts pour la petite. Cependant, seule la première aiguille pourra bénéficier d’améliorations magiques.",
			"type" : "damage",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Bouclier temporel",
			"portee" : {
				"lance" : 1,	
				"personnel" : true
			},
			"description" : "Le chronomancien canalise une image d’une autre boucle temporelle sur un allié. Cette image, presque invisible à l'œil nu, absorbe la totalité des dégâts infligés à la cible. Cependant, le mage devra dépenser 2 points de mana par tranche de 5 points de dégâts (arrondi à l'entier supérieur) infligés à la cible. Si le mage n’a plus assez de mana pour absorber un coup, le bouclier disparaît immédiatement et la cible subit les dégâts restants.",
			"type" : "buff",
			"cout" : "5 PM",
			"bonus" : {

			}
		}
		],
		"Rang 5": [
		{
			"label": "Bond dans le passé",
			"portee" : {
				"personnel" : true
			},
			"description" : "Le mage se projette littéralement dans le passé et peut modifier un élément qui ne doit pas dater de plus de 24 heures. Cela doit obligatoirement être quelque chose qui a été personnellement accompli par le mage ou qu’il aurait pu influencer de manière conséquente. Le mage peut voyager pendant 1D10 + {{1}} minutes et peut décider de mettre fin au sort plus tôt s’il le désire. Il sera impossible d’utiliser un sort de déplacement dans le temps plus d’une fois par tranche de 24 heures, au risque que le mage voie son corps ne pas supporter le trop-plein de changements de lignes temporelles.",
			"type" : "autre",
			"cout" : "6 PM",
			"bonus" : {

			}
		},
		{
			"label": "Retour en arrière",
			"portee" : {
				"description" : "-"
			},
			"description" : "Le Chronomancien manipule l’espace-temps et toutes les personnes de la carte se voient revenir en arrière de 1 tour, annulant tous les dégâts, soins ou quelconques actions comme si rien n’avait eu lieu. Le mage n’est pas concerné par cet effet, et garde sa perte de mana ainsi que ses blessures. Bien que très puissant, ce sort possède un contrecoup non négligeable. Il requiert beaucoup de puissance de la part du mage et le fait directement entrer dans l’état épuisé. S’il relance le sort en étant déjà épuisé, il tombera inconscient pendant 3D5 minutes, et ceci, peu importe que le sort ait fonctionné ou non.",
			"type" : "autre",
			"cout" : "6 PM",
			"bonus" : {

			}
		}
		],
		"Rang 6": [
		{
			"label": "Saut dans le temps",
			"portee" : {
				"personnel" : true
			},
			"description" : "Le mage se projette dans le futur et peut modifier un élément qui ne doit pas dépasser 24 h à compter de la période à laquelle le sort est lancé. Le lieu sera obligatoirement le même que celui d’où le mage a lancé le sort. Le mage peut voyager pendant 1D10 + {{1}} minutes et peut décider de mettre fin au sort plus tôt s’il le désire. Si le mage décide de ne pas être un simple spectateur de la scène, les actions qu’il réalisera ou provoquera se réaliseront forcément en temps voulu. Il sera impossible d’utiliser un sort de déplacement dans le temps plus d’une fois par tranche de 24 heures, au risque que le mage voie son corps ne pas supporter le trop-plein de changements de lignes temporelles.",
			"type" : "autre",
			"cout" : "7 PM",
			"bonus" : {

			}
		},
		{
			"label": "Bulle intemporelle",
			"portee" : {
				"lance" : 10,
				"description" : "dans un Zone de type 1"
			},
			"defense" : "opposée –  acrobatie ou d’athlétisme (si la distance à franchir est de moins de 2 cases)",
			"description" : "Le mage créer une zone qui permet d’isoler une zone et d’y arrêter le temps pendant 1D4 + {{1}} tours. Seul un jet d’acrobatie ou d’athlétisme, opposé à la réussite du mage, permettra d’esquiver la zone et de ne pas se retrouver piéger. Tant qu’une bulle temporelle est active, il est impossible d’en relancer une autre. Lorsqu’une cible est piégée à l’intérieur, elle est considérée comme étant hors combat et ne pourra rien faire pour se défaire du sort. En revanche, la totalité du corps de la cible doit se trouver dans la zone pour fonctionner.",
			"type" : "autre",
			"cout" : "7 PM",
			"bonus" : {

			}
		}
		],
		"Rang 7": [
		{
			"label": "Voyage temporel",
			"portee" : {
				"description" : "zone de type 1 autour du lanceur"
			},
			"description" : "Grâce à ce sort, le mage peut téléporter un groupe à l’époque souhaitée pendant 1+{{1}} heures . Le lieu doit forcément être en endroit que le mage a déjà visité. Le déplacement restera possible, mais pas les interactions. Le mage et ses compères ne seront pas perceptibles par les entités évoluant dans la zone et cela n’aura aucun impact sur le présent, ils ne sont là qu’en spectateurs. Le mage peut mettre fin au sort plus tôt s’il le souhaite.",
			"type" : "autre",
			"cout" : "8 PM",
			"bonus" : {

			}
		},
		{
			"label": "Bond dans le passé II",
			"portee" : {
				"personnel" : true
			},
			"description" : "Le mage se projette littéralement dans le passé et peut modifier un élément qui ne doit pas dater de plus d’un an. Cela doit obligatoirement être quelque chose qui a été personnellement accompli par le mage ou qu’il aurait pu influencer de manière conséquente. Le mage peut voyager pendant 1D10 + {{1}} minutes et peut décider de mettre fin au sort plus tôt s’il le désire. Il sera impossible d’utiliser un sort de déplacement dans le temps plus d’une fois par tranche de 24 heures, au risque que le mage voie son corps ne pas supporter le trop-plein de changements de lignes temporelles.",
			"type" : "autre",
			"cout" : "8 PM",
			"bonus" : {

			}
		}
		],
		"Rang 8": [
		{
			"label": "Poids des âges",
			"portee" : {
				"lance" : 5
			},
			"defense" : "opposée – ténacité à chaque tour",
			"description" : "Le mage <b>canalise</b> le sort, vieillissant ou rajeunissant la cible à toute vitesse, allant même potentiellement jusqu’à disparaître en poussières. Chaque tour, le mage diminue ou augmente la cible d’un incrément d’âge, qui sont : très jeune, jeune, adolescent, adulte, vieux. Si la cible est davantage rajeunie alors qu’elle est au stade « très jeune », elle redeviendra embryon et mourra. Si la cible est davantage vieillie alors qu’elle est au stade « vieux », son corps deviendra poussière et mourra.",
			"type" : "autre",
			"cout" : "9 PM",
			"bonus" : {

			}
		},
		{
			"label": "Odyssée temporelle",
			"portee" : {
				"personnel" : true
			},
			"description" : "Sort ultime de voyage temporel, il permet de téléporter le mage à l’époque souhaitée, sans contraintes de temps ni d’action. Les actions effectuées auront toujours une répercussion sur le présent ou le futur. Il sera impossible d’utiliser un sort de déplacement dans le temps plus d’une fois par tranche de 24 heures, au risque que le mage voie son corps ne pas supporter le trop-plein de changements de lignes temporelles.",
			"type" : "autre",
			"cout" : "9 PM",
			"bonus" : {

			}
		}
		]
	},
	"Échomancie" : {
		"magie" : true,
		"style" : {
			"background" : "echomancie.jpg",
			"color" : "#000000",
			"shadow": "white",
		},
		"type": "Magie",
		"Rang 1": [
		{
			"label": "Silence",
			"portee" : {
				"lance" : 10
			},
			"defense" : "opposée - ténacité",
			"description" : "Le mage empêche l’air de vibrer autour de la tête de la cible, empêchant tout son de sortir de sa bouche, la rendant muette pendant 4+{{1}} tours .",
			"type" : "debuff",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label": "Mélodie",
			"portee" : {
				"lance" : 10
			},
			"description" : "Une symphonie mystique envoûte la cible, elle est comme hypnotisée, sous le contrôle du jeteur de sorts. Le personnage se retrouve alors dans l’état fasciné et y restera jusqu’à la réussite d’un jet de volonté ou en recevant une source de dégâts.",
			"type" : "debuff",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label": "Timbre de voix",
			"portee" : {
				"lance" : 1,	
				"personnel" : true
			},
			"description" : "La cible change de voix pendant 1+{{1}} heure ou jusqu’à ce que le mage rompt le sortilège. Seules les voix connues ou imaginées par le mage sont autorisées. En effet, le mage ne pourra pas prendre la voix du maire de la ville, s’il ne l’a jamais entendue.",
			"type" : "autre",
			"cout" : "2 PM",
			"bonus" : {

			}
		}
		],
		"Rang 2": [
		{
			"label": "Rugir",
			"portee" : {
				"lance" : 10,	
				"personnel" : true
			},
			"description" : "La voix de la cible devient plus menaçante que jamais, augmentant les jets liés à l’intimidation de 3, pendant 1D4 + {{1}} tours.",
			"type" : "buff",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		{
			"label": "Décibel",
			"portee" : {
				"lance" : 15,
				"description" : "zone de type 2"
			},
			"description" : "Le mage réduit ou augmente l’intensité d’un son. Le sort sera plus ou moins difficile à réussir selon l’intensité du son ainsi que de la modification souhaitée (sachant que celles-ci vont de 1 à 12). Le MJ définira avec le joueur l’intensité du son à modifier et indiquera le nombre de paliers exigés pour la transformation. Les incréments d’intensité sont indiqués ci-dessous, avec des exemples, pour une meilleure visualisation.<br>"+ 
			"Pour calculer les malus liés à la difficulté de l’opération, on calculera de combien on souhaite réduire ou augmenter le son, multiplié par deux. Par exemple, passer d’un rire gras (intensité 7) à un chuchotement (intensité 1) donnera un écart de 6, que l’on multipliera par 2, ce qui donnera un malus de 12 pour l’action. Nullifier le bruit d’une explosion, donnera une différence de 12 (de 12 à 0) ce qui fera un malus de 24 au jet du mage pour que celle-ci ne produise aucun bruit. De plus, le sort peut être utilisé pendant un déplacement lent du mage:<br>"+  
			"1-3: léger (chuchotement, crépitement du feu, bruit de pas) 3-6: normal (parler, ronflement,bruit d’armure) 6-9: bruyant (combat, pleurs, rires, instrument de musique) 9-12: très bruyant (hurlement, rugissement, explosion)",
			"type" : "autre",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		{
			"label": "Illusion sonore",
			"portee" : {
				"lance" : 20,
				"description" : "zone de type 1"
			},
			"description" : "Ce sort permet de générer un son (un bruit, une mélodie, un chant) à l’oreille d’une ou plusieurs cibles. Le sort est à <b>canaliser</b> pour que le son perdure. Auquel cas, le coût sera de 2 points de mana par tranches de 10 minutes.",
			"type" : "autre",
			"cout" : "3 PM",
			"bonus" : {

			}
		}
		],
		"Rang 3": [
		{
			"label": "Ode",
			"portee" : {
				"lance" : 5,	
				"personnel" : true
			},
			"description" : "Les paroles d’encouragement du mage permettent de vaincre la douleur et l’épuisement d’un allié ou de lui-même. Ces mots augmentent la constitution ou la force de 3, pendant 1D4 + {{1}} tours.",
			"type" : "buff",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label": "Hurlement",
			"portee" : {
				"description" : "cône en face du lanceur"
			},
			"defense" : "opposée – athlétisme ou acrobaties",
			"description" : "Le lanceur fait monter le son de sa voix ou de son instrument, attaquant directement les tympans de ses ennemis et infligeant 2D4 + {{1}} dégâts. Si quelqu’un se trouve en face du lanceur, les autres cibles derrière celle-ci subiront la moitié des dégâts, car protégées par la personne devant elles.",
			"type" : "damage",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label": "Parole",
			"portee" : {
				"lance" : 1
			},
			"description" : "La cible acquiert la capacité de s’exprimer dans la langue choisie par le jeteur de sorts, et ce, même si la cible ne peut normalement pas parler (pour des raisons de santé ou à cause de l’environnement). L’effet dure 1+{{1}} heures ou jusqu’à ce que le mage y mette un terme.",
			"type" : "autre",
			"cout" : "4 PM",
			"bonus" : {

			}
		}
		],
		"Rang 4": [
		{
			"label": "Idylle",
			"portee" : {
				"lance" : 10
			},
			"defense" : "opposée – volonté",
			"description" : "L’échomancien provoque un état d’euphorie hypnotique par le seul son de sa voix, la cible étant irrésistiblement attirée. Ses paroles sont bues et offrent un bonus à l'éloquence de {{{1}}, pendant toute la durée de la scène. Charmer un même personnage plusieurs fois entraînera des malus à la réussite du mage (malus de 6 par tentative réussie). Si la cible réussit un jet de volonté opposé à la réussite du mage, le sort ne fera pas effet.",
			"type" : "debuff",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Cacophonie",
			"portee" : {
				"lance" : 10
			},
			"defense" : "opposée – volonté",
			"description" : "La cible ne peut plus compter sur son sens de l’audition, elle n’entend plus qu’un babillage incompréhensible. Le sort lui fait perdre ses repères et la fait devenir confuse pendant 1D4 + {{1}} tours.",
			"type" : "debuff",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Mots véritables",
			"portee" : {
				"lance" : 5
			},
			"defense" : "opposée – volonté",
			"description" : "La cible ne peut plus mentir, même si elle essaye de faire croire à un mensonge, seule la vérité sortira de sa bouche, sans exception. L’effet dure 1+ {{1}} heures ou jusqu’à ce que le mage y mette un terme.",
			"type" : "autre",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Berceuse",
			"portee" : {
				"lance" : 10
			},
			"defense" : "opposée – volonté",
			"description" : "D’une douce mélodie, le mage fait plonger la cible dans un profond sommeil pendant 1D4 + {{1}} tours. Durant ce laps de temps, elle est incapable de faire quoi que ce soit. Seul le fait de subir des dégâts la fera se réveiller avant la fin de l’effet.",
			"type" : "debuff",
			"cout" : "5 PM",
			"bonus" : {

			}
		}
		],
		"Rang 5": [
		{
			"label": "Prise de parole",
			"portee" : {
				"lance" : 15
			},
			"defense" : "opposée – volonté",
			"description" : "Le mage fait dire ce qu’il veut, à qui il veut. Le sort n’est pas détectable en apparence, car le mage n’a pas besoin de bouger les lèvres ou de faire de mouvement spécifique après avoir jeté le sort. En revanche, faire dire des choses incohérentes à la cible pourra mettre la puce à l’oreille à ses interlocuteurs. La cible reste cependant maîtresse de son corps et de ses actions, seuls sa bouche et le son qui en sort sont sous l’influence du mage.",
			"type" : "autre",
			"cout" : "6 PM",
			"bonus" : {

			}
		},
		{
			"label": "Onde de choc",
			"portee" : {
				"lance" : 1
			},
			"defense" : "opposée – ténacité",
			"description" : "De la paume des mains du mage, naît une explosion sonique frappant la cible, la blessant de l’intérieur, infligeant 2D8 + {{1}} dégâts.",
			"type" : "damage",
			"cout" : "6 PM",
			"bonus" : {

			}
		}
		],
		"Rang 6": [
		{
			"label": "Cri de guerre",
			"portee" : {
				"description" : "zone de type 2 à partir du lanceur"
			},
			"description" : "Le mage ainsi que tous ses alliés présents dans la zone se voient animés d’une rage de vaincre hors norme. Cette motivation leur permet d’attaquer une fois de plus durant {{1}} tour(s).",
			"type" : "buff",
			"cout" : "7 PM",
			"bonus" : {

			}
		},
		{
			"label": "Vibrations",
			"portee" : {
				"lance" : 10
			},
			"defense" : "opposée – ténacité à chaque tour",
			"description" : "La cible est bombardée d’ondes sonores sur toutes les fréquences, jusqu’à ce qu’elle atteigne son point de rupture. Le sort est à <b>canaliser</b> et inflige 2D10 + {{1}} dégâts chaque tour où le sort est maintenu.",
			"type" : "damage",
			"cout" : "7 PM",
			"bonus" : {

			}
		}
		],
		"Rang 7": [
		{
			"label": "Symphonie de portée",
			"portee" : {
				"lance" : 1,
				"description" : "zone de type 2 autour du mage"
			},
			"description" : "Le mage crée une mélodie harmonieuse qui résonne avec ses alliés, amplifiant leur capacité à projeter leur énergie magique. La mélodie et le mage agissent comme un vecteur, transportant les effets des sorts sur une plus grande distance. Durant 2D4 + {{1}} tours, les alliés (mais pas le mage) touchés voient la portée de leurs sorts augmentés (même ceux de contact) de {{2}} cases. En revanche, un allié muet ne pourra bénéficier de cet effet.",
			"type" : "buff",
			"cout" : "8 PM",
			"bonus" : {

			}
		},
		{
			"label": "Orchestra",
			"portee" : {
				"lance" : 10
			},
			"defense" : "opposée – esquive ou bouclier, puis un jet de ténacité",
			"description" : "De multiples orbes de son sont projetés sur la cible. Chaque sphère qui touche inflige 1D5 + {{0.5}} dégâts et à une chance d’étourdir l’ennemi pendant 1 tour. Le lanceur peut lancer 1D5 orbes, dont les dégâts sont cumulables, mais pas l’effet d’étourdissement. Plus la cible doit tenter d’esquiver les orbes ou de résister à l’étourdissement et plus elle aura de mal à réussir (malus de 2 pour la deuxième, 4 pour la troisième, etc.). Cependant, seul le premier orbe pourra bénéficier de bonus de dégât (comme surcharge, sort puissant, etc.).",
			"type" : "damage",
			"cout" : "8 PM",
			"bonus" : {

			}
		}
		],
		"Rang 8": [
		{
			"label": "Sourdine",
			"portee" : {
				"lance" : 15,
				"description" : "zone de type 2"
			},
			"defense" : "opposée – ténacité",
			"description" : "De sa bouche, le mage émet un son tellement aigu, puissant et vif, qu’il rend sourd toute personne (alliés compris) se trouvant dans la zone pendant 2D6 + {{1}} tours.",
			"type" : "debuff",
			"cout" : "9 PM",
			"bonus" : {

			}
		},
		{
			"label": "Vacarme Strident",
			"portee" : {
				"lance" : 20,
				"description" : "zone de type 3"
			},
			"defense" : "opposée – acrobatie ou athlétisme (si la distance à sauter est de 2 cases ou moins), puis ténacité",
			"description" : "Une explosion assourdissante d’énergie sonique frappe les personnages de la zone, les rendant sourds pendant 2D6 jours et infligeant 5D6 + {{1}} dégâts.<br>"+ 
			"La zone étant grande, les malus liés à la distance de saut seront appliqués en fonction de la position du personnage, et de la zone à atteindre. (1 case = malus de 0, 2 cases = malus de 3, 3 cases = malus de 6, 4 cases = malus de 9).<br>"+
			"Un jet de ténacité, sera également nécessaire pour éviter d’être assourdit.",
			"type" : "damage",
			"cout" : "9 PM",
			"bonus" : {

			}
		}
		]
	},
	"Électromancie" : {
		"magie" : true,
		"style" : {
			"background" : "electromancie.jpg",
			"color" : "#66ffff",
			"shadow": "black",
		},
		"type": "Magie",
		"Rang 1": [
		{
			"label": "Éclair lumineux",
			"portee" : {
				"description" : "zone de type 1, autour du lanceur"
			},
			"defense" : "opposée – ténacité",
			"description" : "Le mage créer un éclair extrêmement lumineux qui aveugle toute personne présente dans la zone d’effet, qui regarde vers le mage. L’effet d’aveuglement persiste pendant 1D4 + {{1}} tours.",
			"type" : "debuff",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label": "Électrochoc",
			"portee" : {
				"lance" : 1
			},
			"defense" : "opposée – esquive",
			"description" : "Le mage touche sa victime du plat de la main pour l’électrocuter et la repousser d’une case. Le choc électrique inflige 1D6 + {{1}} dégâts.",
			"type" : "damage",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label": "Accélération",
			"portee" : {
				"lance" : 5,	
				"personnel" : true
			},
			"description" : "Le mage envoie des stimulus électriques dans son corps ou dans celui d’un allié, décuplant la réactivité de la cible. Ceci a pour effet d’octroyer une grande vélocité, offrant un bonus de 1 en esquive et en dextérité pendant 1D3 + {{1}} tours.",
			"type" : "buff",
			"cout" : "2 PM",
			"bonus" : {

			}
		}
		],
		"Rang 2": [
		{
			"label": "Champs de force",
			"portee" : {
				"description" : "zone de type 1, autour du lanceur."
			},
			"description" : "L’électromancien génère un champ de force à <b>canaliser</b>, qui entoure toute personne se trouvant à l’intérieur et qui protège des petits projectiles non magiques (munitions, explosifs et armes de jet). Le champ de force est indestructible, mais il est toutefois possible de le traverser ou d’attaquer au travers en mêlée, faisant subir 1D6 + {{1}} dégâts. Les projectiles physiques lancés de l’intérieur rentreront en collision avec la barrière, et seront détruits. Dans le cas d’explosifs, ceux-ci se déclencheront à l’intérieur, à l’impact de la barrière. Le champ de force possède une puissance limitée et ne pourra protéger de gros débris ou autres objets plus imposants.",
			"type" : "autre",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		{
			"label": "Charge paralysante",
			"portee" : {
				"lance" : 10
			},
			"defense" : "opposée – ténacité",
			"description" : "Le jeteur de sorts libère de ses mains un éclair paralysant sur un ennemi. Cette décharge aura pour effet d’appliquer l’état paralysé pendant 1D2 + {{1}} tours.",
			"type" : "debuff",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		{
			"label": "Paratonnerre",
			"portee" : {
				"description" : "zone de type 1, autour du lanceur"
			},
			"defense" : "opposée – athlétisme ou acrobatie",
			"description" : "La foudre vient directement frapper le mage, électrocutant toutes les cibles se trouvant aux alentours, sans distinction. Le lanceur ne subit pas les dégâts de son propre sortilège, qui inflige 1D8 + {{1}} dégâts.",
			"type" : "damage",
			"cout" : "3 PM",
			"bonus" : {

			}
		}
		],
		"Rang 3": [
		{
			"label": "Étincelle",
			"portee" : {
				"lance" : 5,	
				"personnel" : true
			},
			"description" : "À l’aide d’un petit coup de jus sans gravité, la vitesse des connexions neuronales du mage ou d’un allié est augmentée. Ceci a pour effet d’offrir un bonus à l’intelligence de 3 pendant 1D4 + {{1}} tours.",
			"type" : "buff",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label": "Électrisation des chakras",
			"portee" : {
				"description" : "zone de type 2, autour du lanceur."
			},
			"description" : "Le mage utilise sa maîtrise de la foudre afin d’accélérer les flux d’énergie naturels du corps humain, appelés chakras. Ainsi, la circulation énergétique est fluidifiée et la récupération du corps et de l’esprit est améliorée. Le sort n’est efficace que sur un temps de repos, mais permet de doubler les résultats des jets de régénération de points de vie ou de mana via le sommeil ou la méditation.",
			"type" : "buff",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label": "Électronimbus",
			"portee" : {
				"lance" : 5
			},
			"defense" : "opposée – esquive",
			"description" : "L’électromancien invoque un nuage gris, déversant la foudre sur l’ennemi avant de disparaître. Celui-ci inflige 2D4 + {{1}} dégâts, avant de disparaître.",
			"type" : "damage",
			"cout" : "4 PM",
			"bonus" : {

			}
		}
		],
		"Rang 4": [
		{
			"label": "Boule de foudre ",
			"portee" : {
				"lance" : 10,
				"description" : "(lancer le sort) 10 mètres (distance maximum réalisé par la boule)"
			},
			"defense" : "opposée – esquive ou bouclier",
			"description" : "Le mage génère une sphère d’électricité de 90 cm de diamètre qu’il projette sur un ennemi, infligeant 2D6 + {{1}} dégâts. L’orbe se déplace en ligne droite, jusqu’à entrer en contact avec un obstacle ou disparaître si elle n’a rien touché sur 10 mètres",
			"type" : "damage",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Électricité statique",
			"portee" : {
				"lance" : 10
			},
			"defense" : "aucune",
			"description" : "L’électricité présente dans l’air se condense et forme un arc électrique entre les 2 cibles choisies par le jeteur de sorts. De cet arc pourra jaillir la foudre, liant les deux êtres. Si l’un prend des dégâts électriques, l’autre subira ces mêmes dégâts. Le lien ne peut supporter une distance entre les deux cibles de plus de dix mètres. Si cette distance maximum est dépassée, le sort prendra fin et le lien qui unit les deux cibles se brisera. Le sort perdure naturellement 1D4 + {{{1}} tours, jusqu’à rupture du lien par une distance trop importante ou la mort d’une des deux entités liées.",
			"type" : "debuff",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Fulguration",
			"portee" : {
				"lance" : 10,
				"description" : "Téléportation personnelle"
			},
			"defense" : "esquive opposée, puis ténacité opposée si le jet d’esquive a échoué",
			"description" : "Le lanceur se transforme en foudre pure et peut se déplacer rapidement de 10 mètres en ligne droite. Toute cible se faisant traverser par le mage peut se retrouver paralysée et subit instantanément 4D2 + {{1}} dégâts.",
			"type" : "damage",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Bombe magnétique",
			"portee" : {
				"lance" : 10,
				"description" : "zone de type 1"
			},
			"defense" : "simple - ténacité",
			"description" : "Le mage lève le bras vers le ciel et fait tomber une pluie d’éclairs sur une zone, paralysant les cibles durant {{1}} tours. À chaque début de tour d’un ennemi paralysé, celui-ci aura droit à un nouveau jet de ténacité non opposé pour tenter de sortir de cet état.",
			"type" : "debuff",
			"cout" : "5 PM",
			"bonus" : {

			}
		}
		],
		"Rang 5": [
		{
			"label": "Électron libre",
			"portee" : {
				"description" : "personnelle"
			},
			"description" : "Le corps du mage se change entièrement en électricité, gardant une forme humanoïde grossière. L’électromancien décide de lui-même quand arrêter le sort, si cela s’effectue en combat, reprendre son état normal demandera un tour. Sous cette forme, le mage garde ses caractéristiques, mais ne pourra faire que se déplacer (mais sa vitesse de déplacement est triplée). Il devient insensible aux attaques physiques, les attaques magiques continuent cependant de lui infliger des dégâts.",
			"type" : "autre",
			"cout" : "6 PM",
			"bonus" : {

			}
		},
		{
			"label": "Lance foudroyante",
			"portee" : {
				"lance" : 15
			},
			"defense" : "opposée – esquive ou bouclier",
			"description" : "Le mage crée une lance de foudre crépitante qui fonce sur la cible désignée. La lance inflige 2D8 + {{1}} dégâts.",
			"type" : "damage",
			"cout" : "6 PM",
			"bonus" : {

			}
		}
		],
		"Rang 6": [
		{
			"label": "Flèches éclairs",
			"portee" : {
				"lance" : 15
			},
			"defense" : "opposée – esquive ou bouclier (malus de 2 pour la deuxième, 4 pour la troisième et 6 pour la dernière)",
			"description" : "Un arc géant se génère dans les mains du mage, invoquant 4 flèches élémentaires de foudre. Chaque flèche agit sous la volonté du mage et peut se concentrer sur une ou plusieurs victimes. Chaque flèche inflige 2D4 + {{1}} dégâts, cependant, seule la première flèche pourra bénéficier d’améliorations de sorts.",
			"type" : "damage",
			"cout" : "7 PM",
			"bonus" : {

			}
		},
		{
			"label": "Volt toile",
			"portee" : {
				"lance" : 15,
				"description" : "zone de type 2"
			},
			"defense" : "opposée – ténacité",
			"description" : "Des lignes directrices se forment à vitesse prodigieuse, formant une toile qui s’électrifiera afin de paralyser tout ennemi se trouvant à l’intérieur. Le sort est à garder canalisé, un succès critique (en plus des effets habituels) sur le lancer du sort octroiera une chance d’étourdir les cibles plutôt que de les paralyser. Un ennemi paralysé le restera pendant un tour, mais recevra un malus de 4 sur son prochain jet de ténacité, s’il a été paralysé le tour précédent.",
			"type" : "debuff",
			"cout" : "7 PM",
			"bonus" : {

			}
		}
		],
		"Rang 7": [
		{
			"label": "Oiseau-tonnerre",
			"portee" : {
				"lance" : 3,
				"description" : "zone d’explosion de type 1"
			},
			"defense" : "simple – athlétisme ou acrobatie pour éviter l’explosion",
			"description" : "le mage créer une monture volante rapide comme l’éclair, un aigle déclenchant le tonnerre à chacun de ses battements d’ailes. Celle-ci à la capacité de se changer intégralement en un gigantesque rayon de foudre qui fondra sur les ennemis à la demande de l’invocateur, infligeant 2D12 dégâts dans la case d’impact et 2D8 autour. La monture ne peut être montée que par son invocateur et ne nécessite aucun jet de dressage pour la contrôler. La monture peut devenir intangible et n’est pas sujette au divers malus concernant le vol.",
			"type" : "invocation",
			"cout" : "8 PM",
			"bonus" : {

			}
		},
		{
			"label": "MJollnir",
			"portee" : {
				"lance" : 15
			},
			"defense" : "opposée – athlétisme ou acrobatie, puis ténacité si raté",
			"description" : "Un énorme marteau de foudre descend du ciel et frappe la cible, lui infligeant 5D6 + {{1}} dégâts et risquant de l’étourdir pendant 1D4 tours.",
			"type" : "damage",
			"cout" : "8 PM",
			"bonus" : {

			}
		}
		],
		"Rang 8": [
		{
			"label": "Magnétosphère",
			"portee" : {
				"lance" : 15,
				"description" : "zone de type 2"
			},
			"defense" : "opposée – athlétisme chaque tour",
			"description" : "Le mage génère une sphère magnétique intense centrée sur un point, qui persiste durant 1D4 + {{1}} tours. Cette force écrasante perturbe gravement les objets métalliques dans son rayon d’action. Les armes métalliques deviennent inutilisables, irrésistiblement attirées vers le sol et clouées sur place. Les créatures portant des armures principalement métalliques subissent une immobilisation complète, incapables de se mouvoir tant qu’elles restent dans la zone. Les projectiles métalliques, tels que flèches ou carreaux, sont stoppés dès qu’ils entrent dans la sphère, empêchant leur utilisation.",
			"type" : "debuff",
			"cout" : "9 PM",
			"bonus" : {

			}
		},
		{
			"label": "Orage",
			"portee" : {
				"lance" : 15,
				"description" : "zone de type 3"
			},
			"defense" : "opposée – acrobatie ou athlétisme (si la distance à sauter est de 2 cases ou moins)",
			"description" : "Le mage <b>canalise</b> une tempête de belle dimension et fait pleuvoir les éclairs sur l’endroit de son choix. Les cibles se trouvant à l’intérieur subiront 6D6 + {{1}} dégâts par tour passé à l’intérieur.<br>"+ 
			"La zone étant grande, les malus liés à la distance de saut seront appliqués en fonction de la position du personnage et de la zone à atteindre pour esquiver (1 case = malus de 0, 2 cases = malus de 3, 3 cases = malus de 6, 4 cases = malus de 9).",
			"type" : "damage",
			"cout" : "9 PM",
			"bonus" : {

			}
		}
		]
	},
	"Hématomancie" : {
		"magie" : true,
		"style" : {
			"background" : "hematomancie.jpg",
			"color" : "#f56262",
			"shadow": "black",
		},
		"type": "Magie",
		"Rang 1": [
		{
			"label": "Palpitations",
			"portee" : {
				"lance" : 10
			},
			"defense" : "opposée – ténacité",
			"description" : "Sort est à <b>canaliser</b> qui accélère ou ralentit le rythme cardiaque de la cible. Le MJ définira avec le joueur l’intensité de la modification et indiquera le nombre de paliers exigés pour la transformation. Les incréments d’intensité sont indiqués ci-dessous, avec des exemples, pour une meilleure visualisation. Pour calculer les malus liés à la difficulté de l’opération, on calculera de combien on souhaite réduire ou augmenter le rythme cardiaque, multiplié par deux. Par exemple, passer d’un pouls normal (intensité 2) à un pouls rapide (intensité 7) donnera un écart de 5, que l’on multipliera par 2, ce qui donnera un malus de 10 pour l’action. Il est impossible via ce sort de faire complémentent s’arrêter le cœur ou de le faire accélérer à un point inhumain. En revanche, baisser ou augmenter le pouls d’un individu aura des répercussions directes sur ses actions et son attitude. Un pouls trop bas pourra entraîner une perte de conscience, un pouls trop élevé favorisera l’apparition de stress. La cible pourra essayer de se calmer avec un jet de volonté opposé réussi. Chaque palier, demandera un tour entier d’incantation: 1-3: pouls bas, 3-6: pouls normal, 6-9: pouls rapide",
			"type" : "autre",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label": "Suée rouge",
			"portee" : {
				"lance" : 15
			},
			"defense" : "opposée – ténacité",
			"description" : "La cible se vide de son sang par la transpiration, passant dans l’état ensanglanté pour 1D4 + {{1}} tours. Il n’est pas possible de stopper le saignement par la médecine, comme pour une blessure standard, étant donné que la perte est généralisée sur tout le corps.",
			"type" : "damage",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label": "Coagulation",
			"portee" : {
				"lance" : 1,	
				"personnel" : true
			},
			"description" : "Les plaies de la cible se régénèrent bien plus rapidement que la normale. Ce sort permet une coagulation rapide, reformant les tissus et arrêtant les saignements localisés. Ce sort soignera du 1D4 + {{1}} points de vie.",
			"type" : "heal",
			"cout" : "2 PM",
			"bonus" : {

			}
		}
		],
		"Rang 2": [
		{
			"label": "Ecchymose",
			"portee" : {
				"lance" : 10
			},
			"defense" : "opposée – ténacité",
			"description" : "L’hématomancien provoque instantanément des bleus douloureux, sans même avoir besoin d’un contact rapproché. La douleur des contusions infligera 1D6 + {{1}} dégâts.",
			"type" : "damage",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		{
			"label": "Anticorps",
			"portee" : {
				"lance" : 1,	
				"personnel" : true
			},
			"description" : "Ce sort de guérison permet de soigner les infections ou les poisons qui se sont propagés dans le sang. Pour cela, tout comme pour les jets de médecine, plus l’infection sera virulente et plus elle sera difficile à soigner. Le MJ donnera au joueur le malus adéquat en temps voulu.",
			"type" : "heal",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		{
			"label": "Garrot",
			"portee" : {
				"lance" : 1,	
				"personnel" : true
			},
			"defense" : "opposée – corps-à-corps, si besoin",
			"description" : "Le mage <b>canalise</b> son sort afin de bloquer l’arrivée de sang dans une partie précise du corps. Cela peut servir à éviter une perte de sang trop importante, d’empêcher la propagation d’un poison ou alors à des fins moins louables, comme couper la circulation d’un membre afin de l’engourdir. Il n’y aura pas de jet pour éviter ce sort, si la cible n’est pas consentante elle devra se dégager. Si en revanche, le mage refuse de lâcher prise, un jet de corps-à-corps sera demandé à chacun.",
			"type" : "autre",
			"cout" : "3 PM",
			"bonus" : {

			}
		}
		],
		"Rang 3": [
		{
			"label": "Septicémie",
			"portee" : {
				"lance" : 1
			},
			"defense" : "opposée – ténacité",
			"description" : "Le mage empoisonne le sang de sa victime, atteignant le système nerveux de manière extrêmement rapide. La cible se retrouvera alors dans l’état Nauséeux pendant 1D4 + {{1}} tours.",
			"type" : "debuff",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label": "Commotion",
			"portee" : {
				"lance" : 5
			},
			"defense" : "opposée – ténacité",
			"description" : "D’une impulsion de la main, le mage créer une secousse sanguine dont résulte un choc violent à l’intérieur du corps de la cible, provoquant des lésions internes et infligeant 2D4 + {{1}} dégâts.",
			"type" : "damage",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label": "Sang-froid",
			"portee" : {
				"lance" : 5,	
				"personnel" : true
			},
			"description" : "La cible se sent extrêmement calme et capable de maîtriser ses émotions les plus virulentes. Elle reçoit alors un bonus de 4 à ses jets de volonté pendant 1D4 + {{1}} tours.",
			"type" : "buff",
			"cout" : "4 PM",
			"bonus" : {

			}
		}
		],
		"Rang 4": [
		{
			"label": "Liens du sang",
			"portee" : {
				"lance" : 10
			},
			"defense" : "opposée – acrobatie",
			"description" : "Le jeteur de sorts matérialise des tentacules de sang qu’il fait jaillir du corps de la victime. Sous son contrôle, les appendices sanguinolents sont capables de saisir la cible et de l’immobiliser.",
			"type" : "debuff",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Transfusion",
			"portee" : {
				"lance" : 1,
				"description" : "(pour les deux cibles)"
			},
			"defense" : "opposée – ténacité si l’une des cibles n’est pas consentante",
			"description" : "Le mage créer une entaille sur son corps, ou sur celui d’une autre cible, ouvrant les veines à cet endroit et y laissant s’écouler le fluide vital. La perte de sang infligera {{1}} points de dégâts, qui seront convertis en soins pour autrui. Le mage a également la possibilité de <b>canaliser<b> son sort, afin de transfuser une plus grosse quantité de sang. Après la transfusion la plaie se refermera, comme-ci aucune blessure n’avait été infligée.",
			"type" : "autre",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Absorption vampirique",
			"portee" : {
				"lance" : 5,
				"description" : "soin personnel"
			},
			"description" : "Le mage se délecte de l’hémoglobine de ses ennemis, aspirant le sang de leur corps comme s’il lévitait jusqu’au mage. L’hématomancien s’en nourrira pour régénérer ses propres blessures, soignant 2D6 + {{1}} points de vie. La quantité de sang absorbé est trop infime pour que la cible subisse des dégâts. Il sera impossible de se régénérer avec le sang d’une cible morte, même si la mort est très récente, le sang aura perdu toute propriété curative.",
			"type" : "heal",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Sang chaud",
			"portee" : {
				"lance" : 5,	
				"personnel" : true
			},
			"description" : "La cible se voit envahie par une rage intérieure, sa peau rougit légèrement et son rythme cardiaque augmente. Ceci ayant pour effet de prodiguer un bonus aux dégâts de 3 pendant 1D4 + {{1}} tours.",
			"type" : "buff",
			"cout" : "5 PM",
			"bonus" : {

			}
		}
		],
		"Rang 5": [
		{
			"label": "Barrière sanguinolente",
			"portee" : {
				"lance" : 5,	
				"personnel" : true
			},
			"description" : "Le mage utilise le sang d’un individu fraîchement décédé, oubliant les limites physiques et faisant sortir celui-ci du corps. Le sang viendra alors recouvrir le mage ou une cible et coagulera rapidement à l’air libre, afin de former une barrière protectrice pendant 1D4 + {{1}} tours, et augmentera son armure de 2. Il est également possible de faire prendre une forme au sang, comme une sculpture qui durcira au bout de quelques minutes. La taille de la sculpture, dépendra de la quantité de sang disponible.",
			"type" : "autre",
			"cout" : "6 PM",
			"bonus" : {

			}
		},
		{
			"label": "Hémorragie interne",
			"portee" : {
				"lance" : 10
			},
			"defense" : "opposée – ténacité",
			"description" : "La cible est prise de fortes douleurs dans le corps et ses veines se vident de leur contenu depuis l’intérieur. Le sort inflige 2D10 + {{1}} dégâts.",
			"type" : "damage",
			"cout" : "6 PM",
			"bonus" : {

			}
		}
		],
		"Rang 6": [
		{
			"label": "Sangsue",
			"portee" : {
				"lance" : 10
			},
			"defense" : "opposée – esquive",
			"description" : "L’hématomancien génère une sangsue de chair qui va venir s’accrocher à la jugulaire de la cible. La sangsue est considérée comme étant une invocation (mais qui ne sera active que lorsque le sort touchera). Si la cible manque son esquive, la sangsue le mordra et elle ne pourra la retirer au risque de se déchirer la gorge. Chaque tour, la sangsue ponctionnera 4+{{1}} points de vie à la cible et se gorgera de son sang. La sangsue ne se détachera que sur ordre du mage ou à la mort de la cible et reviendra ensuite jusqu’à lui ou un allié, afin de transvaser le sang récupéré. Ainsi, la totalité des dégâts que la sangsue aura infligés sera convertie en soins. Chaque tour, la sangsue infligera automatiquement des dégâts fixes, ceux-ci sont considérés comme étant de type physique, mais la sangsue possède une PA de 4 et ignore donc les armures (Ce qui reflète le fait qu’elle se faufile sans mal dans les interstices d’écailles ou de pièces d’armure).",
			"type" : "invocation",
			"cout" : "7 PM",
			"bonus" : {

			}
		},
		{
			"label": "Hématémèse",
			"portee" : {
				"lance" : 10
			},
			"defense" : "opposée – ténacité",
			"description" : "La cible, blessée de l’intérieur, se met à vomir du sang et perd 2D4 + {{1}} dégâts à chaque tour, et ce, durant 1D4 + {{1}} tours.",
			"type" : "damage",
			"cout" : "7 PM",
			"bonus" : {

			}
		}
		],
		"Rang 7": [
		{
			"label": "Régénération",
			"portee" : {
				"lance" : 1,	
				"personnel" : true,
				"description" : "(sauf si le membre est un bras)"
			},
			"description" : "Ce sort de guérison permet de reconstituer un membre perdu, régénérant les tissus cellulaires afin de redonner au membre son apparence originelle et toutes ses facultés motrices. Le sort est à <b>canaliser</b> pendant plusieurs minutes. Il faudra compter 10 minutes pour une petite zone comme un doigt ou une oreille, 20 minutes pour un avant-bras ou une main entière, 30 minutes pour un bras entier, 40 pour une jambe, etc. Ce sort ne peut être utilisé que sur une cible vivante, il sera donc impossible au mage de rendre la vie à un personnage qui aurait été décapité.",
			"type" : "heal",
			"cout" : "8 PM",
			"bonus" : {

			}
		},
		{
			"label": "Exsangue",
			"portee" : {
				"lance" : 15
			},
			"defense" : "opposée – ténacité",
			"description" : "La cible se vide de son sang à vitesse grand V, à tel point que l’on peut voir sa couleur de peau virer au blanc au fil des secondes qui défilent. La perte abondante de sang inflige 5D6 + {{1}} dégâts.",
			"type" : "damage",
			"cout" : "8 PM",
			"bonus" : {

			}
		}
		],
		"Rang 8": [
		{
			"label": "Stagnation",
			"portee" : {
				"lance" : 10
			},
			"defense" : "opposée – ténacité, puis volonté si le premier jet est raté",
			"description" : "Ce sort à <b>canaliser</b> fige le sang dans les veines de la victime et le fait coaguler instantanément. La cible est alors aux bords de la mort (ses organes n’étant plus alimentés) et va devoir réagir très vite sous peine de tomber dans le coma, puis mourir. Elle devra tout d’abord réussir un jet de ténacité. S’il est réussi, alors il ne se passera rien. S’il le jet est raté, elle devra alors réussir un jet de volonté qui s’effectua avec un malus de 4. Si le second jet réussit, la cible aura réussi à contrer le mage, mais sera contraint pendant 2 tours de ne rien faire, le temps que sa circulation redevienne normale. En revanche, si ce deuxième jet est également raté, la cible tombera dans le coma, son sort sera alors laissé entre les mains du mage qui pourra le laisser en vie ou continuer la canalisation pendant 4 tours et ainsi provoquer un dysfonctionnement des organes et tuer sa cible. Le mage subira un malus de 6, s’il tente de relancer le sort sur une cible ayant contré les effets de celui-ci.",
			"type" : "autre",
			"cout" : "9 PM",
			"bonus" : {

			}
		},
		{
			"label": "Sang pour sang",
			"portee" : {
				"lance" : 1,	
				"personnel" : true
			},
			"description" : "Sort de soins grandiose qui régénère les blessures du corps de la cible, rendant du 7D6 + {{1}} points de vie.",
			"type" : "heal",
			"cout" : "9 PM",
			"bonus" : {

			}
		}
		]
	},
	"Hydromancie" : {
		"magie" : true,
		"style" : {
			"background" : "hydromancie.jpg",
			"color" : "#93c4f5",
			"shadow": "black",
		},
		"type": "Magie",
		"Rang 1": [
		{
			"label": "Branchies",
			"portee" : {
				"lance" : 1,	
				"personnel" : true
			},
			"description" : "Des branchies se forment sur le cou du mage, ou d’une autre cible et permettent de respirer sous l’eau pour une durée de 1+{{1}} heures.",
			"type" : "buff",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label": "Marcher sur l’eau",
			"portee" : {
				"lance" : 1,	
				"personnel" : true
			},
			"description" : "En solidifiant l’eau sous les pas, le mage permet de marcher sur l’eau pendant 10+{{1}} minutes.",
			"type" : "buff",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label": "Vision des profondeurs",
			"portee" : {
				"description" : "personnelle"
			},
			"description" : "Le lanceur peut voir à travers l’eau jusqu’à une distance de plusieurs centaines de mètres, ne prenant pas en compte la noirceur des abysses pendant 10+{{1}} minutes.",
			"type" : "buff",
			"cout" : "2 PM",
			"bonus" : {

			}
		}
		],
		"Rang 2": [
		{
			"label": "Geyser",
			"portee" : {
				"lance" : 10
			},
			"defense" : "opposée – athlétisme ou acrobaties pour esquiver, puis un jet d’acrobaties simple pour éviter le renversement",
			"description" : "Un jet d’eau et de vapeur jaillit du sol et s’élève avec quiconque se tiendrait au-dessus. La cible encaisse 1D2 + {{1}} dégâts et est projetée à 1D5 + {{1}} mètres, entraînant alors des dégâts de chute en conséquence. La cible peut retomber sur ses jambes afin de ne pas être renversée, mais les dégâts de chute seront tout de même comptabilisés.",
			"type" : "damage",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		{
			"label": "Gel",
			"portee" : {
				"lance" : 10
			},
			"defense" : "opposée – acrobatie ou force brute (malus de 4)",
			"description" : "Le mage enchevêtre une cible en formant rapidement un amas de glace au sol juste au niveau de ses pieds pendant 1D4 + {{1}} tours ou jusqu’à ce que celle-ci réussisse à se dégager.",
			"type" : "debuff",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		{
			"label": "Création liquide mineure",
			"portee" : {
				"lance" : 5
			},
			"description" : "L’hydromancien assemble et rigidifie des particules d’eau afin de former un petit humanoïde élémentaire. Se référer à la fiche des invocations de l’hydromancie.",
			"type" : "invocation",
			"cout" : "3 PM",
			"bonus" : {

			}
		}
		],
		"Rang 3": [
		{
			"label": "Sculpture de glace",
			"portee" : {
				"lance" : 15,
				"description" : " sur trois cases verticales ou horizontales"
			},
			"description" : "Le magicien fait surgir un mur impossible à escalader sans équipement de 3 mètres de large pour 2+{{1}} mètres de haut. Le mage à la possibilité de <b>canaliser</b> le sort contre 1 point de mana par tour, ce qui aura pour effet d’allonger la sculpture de 2 mètres à chaque fois. Il est impossible qu’un quelconque projectile le traverse, ou de voir au travers. Le mur possède 35+{{5}} points de vie avec une RD de 1 et restera actif 5 minutes après l’arrêt de la canalisation.",
			"type" : "autre",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label": "Prison aqueuse",
			"portee" : {
				"lance" : 5
			},
			"defense" : "aucune",
			"description" : "Le mage <b>canalise</b> une bulle d’eau qui enveloppe la tête de la cible et la prive de tout oxygène. Toute personne emprisonnée à l’intérieur entamera ses points de souffle. Lorsque ceux-ci tombent à 0, la cible tombe dans le coma et commence à perdre des points de vie temporaires jusqu’à la mort par asphyxie. La bulle peut être percée, mais se reformera toujours tant que le mage continue sa canalisation. En revanche, si quelque chose perturbe l’attention du mage (blessure, chute, etc.), le sort ne sera plus actif.",
			"type" : "autre",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label": "Étreinte glaciale",
			"portee" : {
				"lance" : 5,	
				"personnel" : true
			},
			"description" : "La blessure se recouvre d’une légère couche de gel, soignant les blessures en régénérant de 1D8 + {{1}} les tissus.",
			"type" : "heal",
			"cout" : "4 PM",
			"bonus" : {

			}
		}
		],
		"Rang 4": [
		{
			"label": "Manteau du froid",
			"portee" : {
				"lance" : 5,	
				"personnel" : true
			},
			"description" : "L’hydromancien créer une couche de givre sur son corps ou celui d’une cible, ce qui augmente sa RD de 2 pendant 1D4 + {{1}} tours.",
			"type" : "buff",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": " Régénération aqueuse",
			"portee" : {
				"lance" : 10,	
				"personnel" : true
			},
			"description" : "Le corps étant grandement composé d’eau, l’hydromancien peut facilement régénérer les tissus d’une blessure. Ce sort soigne du 2D6 + {{1}}.",
			"type" : "heal",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Blizzard",
			"portee" : {
				"lance" : 10,
				"description" : "dans une zone de type 2"
			},
			"defense" : "simple – ténacité pour les dégâts de froid",
			"description" : "Le mage créer un épais brouillard où les chutes de neige font rage pendant 1D4 + {{1}} tours. La tempête obstrue la vision de tout individu pris à l’intérieur (en dehors du mage) et impose un malus de 2 au toucher et à la défense. La température y chute brutalement, faisant subir 1D4 de dégâts de froid si à toute personne n’étant pas chaudement couverte. Au moindre dégât de froid, la cible entre dans l’état fatigué.",
			"type" : "debuff",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Création liquide",
			"portee" : {
				"lance" : 5
			},
			"description" : "L’hydromancien assemble et rigidifie des particules d’eau afin de former un être élémentaire. Se référer à la fiche des invocations de l’hydromancie.",
			"type" : "invocation",
			"cout" : "5 PM",
			"bonus" : {

			}
		}
		],
		"Rang 5": [
		{
			"label": "Patinoire",
			"portee" : {
				"lance" : 10,
				"description" : "dans une zone de type 2"
			},
			"description" : "Le mage génère une épaisse couche de glace sur le sol, rendant acrobatique toute manœuvre de déplacement pendant 1D4 + {{1}} tours. Se déplacer normalement demandera de réussir un jet d’acrobatie sous peine de finir renversé. En revanche, se déplacer lentement (la moitié des cases de déplacement) ne nécessitera pas de jet. Certaines actions incluant des mouvements rapides ou amples (comme l’attaque au corps-à-corps ou l’esquive) de la part du personnage demanderont également un jet d’acrobatie, mais avec un malus de 7.",
			"type" : "autre",
			"cout" : "6 PM",
			"bonus" : {

			}
		},
		{
			"label": "Liquéfaction",
			"portee" : {
				"description" : "personnelle"
			},
			"description" : "Le corps du mage se change entièrement en eau, gardant une forme humanoïde grossière. L’hydromancien décide de lui-même quand arrêter le sort. Si cela s’effectue en combat, reprendre son état normal demandera un tour. Sous cette forme, le mage garde ses caractéristiques, mais ne pourra faire que se déplacer (mais sa vitesse de déplacement est triplée). Il devient insensible aux attaques physiques, mais les attaques magiques continuent de lui infliger des dégâts.",
			"type" : "autre",
			"cout" : "6 PM",
			"bonus" : {

			}
		}
		],
		"Rang 6": [
		{
			"label": "Création liquide majeure",
			"portee" : {
				"lance" : 5
			},
			"description" : "L’hydromancien masse et rigidifie des particules d’eau afin de former un grand élémentaire. Se référer à la fiche des invocations de l’hydromancie.",
			"type" : "invocation",
			"cout" : "7 PM",
			"bonus" : {

			}
		},
		{
			"label": "Épieu de givre",
			"portee" : {
				"lance" : 10
			},
			"defense" : "opposée – esquive ou bouclier",
			"description" : "Le lanceur de sorts forge un épieu de glace qui fond sur la cible désignée. L’arme gelée infligera 2D10 + {{1}} dégâts.",
			"type" : "damage",
			"cout" : "7 PM",
			"bonus" : {

			}
		}
		],
		"Rang 7": [
		{
			"label": "Assèchement",
			"portee" : {
				"lance" : 10
			},
			"defense" : "opposée – ténacité",
			"description" : "Ce sort fait s’évaporer l’humidité du corps de la cible sous forme d’une vapeur très légère. La peau se ride et se tend, infligeant 5D6 + {{1}} dégâts.",
			"type" : "damage",
			"cout" : "8 PM",
			"bonus" : {

			}
		},
		{
			"label": "Pluie mortelle",
			"portee" : {
				"lance" : 15,
				"description" : "dans une zone de type 2"
			},
			"defense" : "opposée – acrobaties ou athlétisme (si la distance à sauter est de 2 cases ou moins)",
			"description" : "Une pluie fine commence à tomber dans une zone, les gouttelettes formant de petites aiguilles acérées de glace. Le mage doit <b>canaliser</b> pour faire perdurer son sort autant de tours que désiré et infliger 4D6 dégât par tour à toute entité présente de la zone. Un jet d’acrobatie ou d’athlétisme (dans le cas où la distance à sauter est de 2 cases ou moins) opposé à la réussite du mage sera nécessaire afin de sortir de la zone rapidement et ainsi éviter l’attaque.",
			"type" : "damage",
			"cout" : "8 PM",
			"bonus" : {

			}
		}
		],
		"Rang 8": [
		{
			"label": "Tsunami",
			"portee" : {
				"lance" : 15,
				"description" : "dans une zone de type 2"
			},
			"defense" : "opposée – acrobaties ou athlétisme (si la distance à sauter est de 2 cases ou moins), puis athlétisme (malus de 8) si le premier jet est raté, afin de sortir de la masse d’eau",
			"description" : "Le personnage créer une énorme vague qui se déplace en ligne droite, sur terre ou sur mer. Toute personne prise dans les eaux tumultueuses subira 5D6 dégâts + {{1}}.  Le mage peut </b>canaliser</b> la masse d’eau et ainsi la faire se déplace de 4 cases par tour, entraînant toute créature dans son avancée. Le tsunami disparaîtra si le mage arrête de canaliser ou qu’il perd la vision sur sa création.",
			"type" : "damage",
			"cout" : "9 PM",
			"bonus" : {

			}
		},
		{
			"label": "Rayon polaire",
			"portee" : {
				"lance" : 15
			},
			"defense" : "opposée – esquive ou bouclier, puis ténacité si le premier jet rate, pour les dégâts de froid",
			"description" : "Un rayon bleuté et glacé part de la main du mage pour infliger 6D6 + {{1}} points de dégâts, ainsi que 1D4 de dégâts de gel. Au moindre dégât de froid, la cible entre dans l’état fatigué.",
			"type" : "damage",
			"cout" : "9 PM",
			"bonus" : {

			}
		}
		]
	},
	"Luciomancie" : {
		"magie" : true,
		"style" : {
			"background" : "luciomancie.jpg",
			"color" : "#5d0000",
			"shadow": "#FFFFCC",
		},
		"type": "Magie",
		"Rang 1": [
		{
			"label": "Dissipation lumineuse",
			"portee" : {
				"lance" : 10,
				"description" : "zone de type 1"
			},
			"description" : "Une lueur sort de la paume de la main du mage, dissout les ténèbres naturelles ou magiques et éclaircit la zone pendant 1D4 + {{1}} tours.",
			"type" : "autre",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label": "Faisceau",
			"portee" : {
				"lance" : 10
			},
			"defense" : "opposée – esquive ou bouclier",
			"description" : "Le jeteur de sorts décoche du bout du doigt un rayon capable de brûler la matière. Celui-ci peut permettre de déclencher un feu ou infliger une brûlure légère (1+{{1}} dégât, au début de chaque tour de la cible). Si la cible n’arrive pas à esquiver le sort, elle subira automatiquement les dégâts au début de chaque tour, l’effet durera pendant 1D4 + {{1}} tours.",
			"type" : "damage",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label": "Lumière curative",
			"portee" : {
				"lance" : 5,	
				"personnel" : true
			},
			"description" : "Le mage génère un sort libérant une lumière miraculeuse qui soigne les blessures de 1D6 + {{1}}.",
			"type" : "heal",
			"cout" : "2 PM",
			"bonus" : {

			}
		}
		],
		"Rang 2": [
		{
			"label": "Fils radieux",
			"portee" : {
				"lance" : 10
			},
			"defense" : "opposée – esquive, puis un jet d'acrobatie ou de force brute (malus de 4) si le premier jet rate, jusqu’à libération",
			"description" : "Des fils d’énergie blanche jaillissent du bout des doigts du lanceur et s’enroulent autour de la cible afin de l’enchevêtrer jusqu’à ce qu’elle parvienne à se dégager. ",
			"type" : "debuff",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		{
			"label": "Aveuglement",
			"portee" : {
				"description" : "cône devant le lanceur"
			},
			"description" : "Le luciomancien créer un puissant flash lumineux en face de lui qui aveugle pendant 1D4 + {{1}} tours toute personne présente dans la zone d’effet et qui regarderait vers le mage. Le sort sera inefficace si la victime réussit un jet de ténacité, en opposition à la réussite du mage pour éviter l’éblouissement. ",
			"type" : "debuff",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		{
			"label": "Bouclier rutilant",
			"portee" : {
				"lance" : 10
			},
			"description" : "Le mage créer une barrière de lumière autour d’un allié ou de lui-même, qui absorbera une quantité de dégâts définis. Le bouclier pourra absorber 10+{{5}} dégâts physiques ou magiques, avant de voler en éclats. Le bouclier perdurera jusqu’au prochain tour du mage. À noter que si une attaque détruit le bouclier, le surplus des dégâts sera appliqué à la cible.",
			"type" : "buff",
			"cout" : "3 PM",
			"bonus" : {

			}
		}
		],
		"Rang 3": [
		{
			"label": "Rayon luminescent",
			"portee" : {
				"lance" : 15
			},
			"description" : "Le mage créer un trait lumineux qui va venir traverser jusqu’à 3 cibles alignées (sur 3 cases) de manière linéaire. La première cible subit 1D6 + {{0.5}} points de dégâts, mais le projectile perd en intensité à chaque cible touchée. La deuxième cible ne subira que 1D4 + {{0.5}} dégâts et la dernière 1D2 + {{0.5}} dégâts. Le projectile peut bénéficier d’améliorations, mais seul la première cible subira le surplus de puissance, son corps absorbant l’impact.",
			"type" : "damage",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label": "Aire thérapeutique",
			"portee" : {
				"lance" : 10,
				"description" : "dans une zone de type 2"
			},
			"description" : "Le mage invoque un orbe qui va venir ricocher entre 4 cibles (maximum) présentes dans la zone. Le mage peut bénéficier du soin, puisqu’il décide de la trajectoire de l’orbe. Le projectile ne peut rebondir deux fois sur une même personne et soigne du 1D6 + {{1}}.",
			"type" : "heal",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label": "Soins supérieurs",
			"portee" : {
				"lance" : 10,	
				"personnel" : true
			},
			"description" : "Une lumière chaleureuse vient guérir les blessures, refermant les plaies et faisant regagner 2D6 + {{1}} points de vie.",
			"type" : "heal",
			"cout" : "4 PM",
			"bonus" : {

			}
		}
		],
		"Rang 4": [
		{
			"label": "Baumes de soin",
			"portee" : {
				"description" : "-"
			},
			"description" : "Le mage créer 1D3 boule(s) de lumière qui nécessitent d’être conservées dans un récipient. Tous les baumes doivent être consommés avant de pouvoir en générer d’autres. Ces baumes peuvent être utilisés par n’importe qui et sont impérissables. À leur ouverture, ils rendront 4+{{1}} points de vie au personnage sur lequel un baume est appliqué. Les baumes ne peuvent bénéficier d’améliorations magiques.",
			"type" : "heal",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Soigner les maux",
			"portee" : {
				"lance" : 1,	
				"personnel" : true
			},
			"description" : "Permet de soigner une maladie, un venin ou un poison. Plus la gravité du mal est élevée, plus il sera difficile à soigner. Les malus liés à la puissance du poison ou de la maladie sont les mêmes que pour la médecine traditionnelle et seront donnés par le MJ en temps voulu. À noter que ce sort ne permet pas de soigner les maladies mentales, les maladies congénitales ou génétiques. Ce sort sert uniquement à détruit un élément intrusif au corps et ne permet pas, par exemple, de soigner une cécité visuelle ou une narcolepsie.",
			"type" : "heal",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Bénédiction",
			"portee" : {
				"lance" : 10,	
				"personnel" : true,
				"description" : "(dans le cas où le mage en a la possibilité mentale et physique (possibles malus))"
			},
			"description" : "Le luciomancien place une aura éclatante sur une cible. À chaque début de tour et pendant 1D4 + {{1}} tours, la cible régénérera automatiquement 2+{{1}} points de vie.",
			"type" : "buff",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Purification",
			"portee" : {
				"lance" : 1,	
				"personnel" : true,
				"description" : "(dans le cas où le mage en a la possibilité mentale et physique (possibles malus))"
			},
			"defense" : "opposée – incantation initiale qui a causé la malédiction",
			"description" : "Permet d’annuler une malédiction ou un mauvais sort, il faudra réussir un jet d’incantation en opposition à la réussite du sort qui a causé la malédiction. ",
			"type" : "heal",
			"cout" : "5 PM",
			"bonus" : {

			}
		}
		],
		"Rang 5": [
		{
			"label": "Prisme",
			"portee" : {
				"lance" : 5
			},
			"description" : "Le mage invoque un prisme de plusieurs centaines de kilos. Le prisme est considéré comme suffisamment imposant pour représenter un obstacle. Se référer à la fiche de l’invocation de la luciomancie.",
			"type" : "invocation",
			"cout" : "6 PM",
			"bonus" : {

			}
		},
		{
			"label": "Éclats tranchants",
			"portee" : {
				"lance" : 15
			},
			"defense" : "opposée – esquive ou bouclier",
			"description" : "Des éclats ressemblants à des morceaux de verre étincelants plongent sur la cible et la transpercent, infligeant 2D8 + {{1}} dégâts.",
			"type" : "damage",
			"cout" : "6 PM",
			"bonus" : {

			}
		}
		],
		"Rang 6": [
		{
			"label": "Réparation des os",
			"portee" : {
				"lance" : 1,	
				"personnel" : true,
				"description" : "(tant que cela n’affecte pas un bras du mage)"
			},
			"description" : "Ce sort permet la reconstitution des parties manquantes ou fragilisées d’un os. Le soin n’est pas instantané et doit être canalisé pendant 4 tours. Un malus sera imposé en fonction de la gravité de la fracture et sera annoncé au moment voulu par le MJ.",
			"type" : "heal",
			"cout" : "7 PM",
			"bonus" : {

			}
		},
		{
			"label": "Prothèse diaphane",
			"portee" : {
				"lance" : 1,	
				"personnel" : true,
				"description" : "(sauf si le membre est un bras)"
			},
			"description" : " Ce sort de guérison permet de reconstituer un membre perdu, afin de lui restituer ses facultés motrices. Le sort est à <b>canaliser</b> pendant plusieurs minutes (il faudra compter 10 minutes pour une petite zone comme un doigt ou une oreille, 20 minutes pour un avant-bras ou une main entière, 30 minutes pour un bras entier, 40 pour une jambe, etc.). Ce sort ne peut être utilisé que sur une cible vivante, il sera donc impossible au mage de rendre la vie à un personnage qui a été décapité ou dont le cœur a été arraché. À noter que l’apparence du membre régénéré sera altérée. La partie sera entièrement fonctionnelle, mais apparaîtra entièrement blanche, avec une texture entièrement lisse et produisant une faible luminescence.",
			"type" : "heal",
			"cout" : "7 PM",
			"bonus" : {

			}
		}
		],
		"Rang 7": [
		{
			"label": "Fureur lumineuse",
			"portee" : {
				"lance" : 15
			},
			"defense" : "opposée – esquive ou bouclier",
			"description" : "Le mage joint ses mains et condense sa magie dans celles-ci, la libérant dans un puissant rayon de lumière qui inflige 4D6 + {{1}} dégâts.",
			"type" : "damage",
			"cout" : "8 PM",
			"bonus" : {

			}
		},
		{
			"label": "Soins divins",
			"portee" : {
				"lance" : 15,	
				"personnel" : true
			},
			"description" : "Le mage illumine les blessures de la cible d’une pure lumière blanche, soignant instantanément 5D6 + {{1}} points de vie.",
			"type" : "heal",
			"cout" : "8 PM",
			"bonus" : {

			}
		}
		],
		"Rang 8": [
		{
			"label": "Protection d’Ilum",
			"portee" : {
				"lance" : 15,
				"description" : "zone de type 2"
			},
			"description" : "Des halos de lumières descendent du ciel, éclairant le mage et ses alliés d’une douce et chaleureuse lumière. Les alliés se trouvant dans la zone seront tous soignés de 4D6 + {{{1}} points de vie, le mage est automatiquement ciblé par l’effet.",
			"type" : "heal",
			"cout" : "9 PM",
			"bonus" : {

			}
		},
		{
			"label": "Lumière de vie",
			"portee" : {
				"lance" : 1
			},
			"description" : "Une aura lumineuse vient englober la cible décédée, régénérant ses blessures et lui restaurant son âme. La cible sera ramenée à la vie avec la totalité de ses attributs chiffrés. Ce sort ne fonctionne que sur des cibles mortes depuis peu et dont le corps est encore en bon état.",
			"type" : "heal",
			"cout" : "9 PM",
			"bonus" : {

			}
		}
		]
	},
	"Magie générale" : {
		"magie" : true,
		"style" : {
			"background" : "magie_general.jpg",
			"color" : "#e6f3fb",
			"shadow": "black",
		},
		"type": "Magie",
		"Rang 1": [
		{
			"label": "Feu follet",
			"portee" : {
				"description" : "personnelle"
			},
			"description" : "L’arcaniste fait apparaître au-dessus de lui une petite flamme bleutée qui le suivra lors de ses déplacements, éclairant la zone telle une torche. La flamme ne produit aucune chaleur et perdurera pendant 15+{{5}} minutes.",
			"type" : "autre",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label": "Siphon de magie",
			"portee" : {
				"lance" : 10
			},
			"defense" : "opposée – volonté",
			"description" : "L’arcaniste peut extirper l’essence magique d’un autre être, se l’appropriant tout naturellement. Le sort ne pourra être utilisé que sur une créature possédant des points de mana. Dans le cas contraire, le mana sera utilisé, mais le mage n’en récupérera pas. Au même titre que le mage ne peut prélever plus de magie que la cible n’en a (On prendra en compte les points de magie restants et non le total). Le sort permet de voler 1D4 + {{1}} points de mana.",
			"type" : "autre",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label": "Décharge arcanique",
			"portee" : {
				"lance" : 15
			},
			"defense" : "opposée – esquive ou bouclier",
			"description" : "Une étincelle bleue jaillit de la main de l’arcaniste, fonçant directement sur l’ennemi ciblé. Le sort inflige 1D4 + {{1}} dégâts.",
			"type" : "damage",
			"cout" : "2 PM",
			"bonus" : {

			}
		}
		],
		"Rang 2": [
		{
			"label": "Nourriture éthérée",
			"portee" : {
				"description" : "-"
			},
			"description" : "La mage fait apparaître 2D2 + {{1}} plats, qui disparaîtront dans l’heure. Ces aliments n’étant pas tout à fait habituels, ils auront la capacité d’hydrater et rassasier les individus qui les ingéreront. De plus, de par la magie qui les compose, un plat offrira également un bonus de {{1}} à la prochaine action du personnage.",
			"type" : "buff",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		{
			"label": "Transfert",
			"portee" : {
				"lance" : 10
			},
			"description" : "À l’inverse du siphon, ce sort permet à l’arcaniste de donner de son mana à une cible. Le mage peut transférer {{5}} points de mana, mais ne pourra jamais donner plus que ce qui lui reste de sa propre énergie magique.",
			"type" : "autre",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		{
			"label": "Vision du passé",
			"portee" : {
				"lance" : 5
			},
			"description" : "L’arcaniste, de par sa simple volonté, est capable de transmettre un de ses souvenirs à une cible. Un lien mental s’établira alors entre les deux cibles, permettant le partage d’une scène visualisée par le mage.",
			"type" : "autre",
			"cout" : "3 PM",
			"bonus" : {

			}
		}
		],
		"Rang 3": [
		{
			"label": "Bouclier des arcanes",
			"portee" : {
				"lance" : 1,	
				"personnel" : true
			},
			"description" : "Des flux magiques entourent une cible et lui offre de meilleures chances de parer les attaques. La cible gagne alors un bonus à l’esquive de 1+{{1}} pendant une durée de 1 + {{1}} tours.",
			"type" : "buff",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label": "Flèche de l’éther",
			"portee" : {
				"lance" : 20
			},
			"defense" : "opposée – esquive ou bouclier",
			"description" : "Une flèche de magie pure se forme et vient se planter dans le corps de la cible. Cette flèche inflige 1D5 + {{0.5}} dégâts. Les dégâts du sort sont peu élevés, mais la marge de critique de ce sort est augmentée de 2 (critique sur 1, 2 et 3).",
			"type" : "damage",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label": "Focus",
			"portee" : {
				"description" : "-"
			},
			"description" : "Le mage forme un petit familier volant, d’un diamètre équivalent à un œil. L’invocateur devra ensuite <b>canaliser</b> son sort, afin de prendre le contrôle de sa créature qui deviendra alors les yeux du mage, le privant de son propre sens de la vue durant l’utilisation. L’œil est incapable d’entendre, laissant donc au mage son audition personnelle. Se référer à la fiche des invocations de la magie générale.",
			"type" : "invocation",
			"cout" : "4 PM",
			"bonus" : {

			}
		}
		],
		"Rang 4": [
		{
			"label": "Néant distordu",
			"portee" : {
				"lance" : 15,
				"description" : "zone de type 2"
			},
			"defense" : "opposée – acrobatie ou athlétisme (si la distance à sauter est de 2 cases ou moins)",
			"description" : "Une force arcanique détruit les lois matérielles fondamentales dans une zone. Le corps des victimes est déformé par de violentes pulsations qui infligent 2D4 + {{1}} dégâts à toutes les créatures se trouvant à l’intérieur.",
			"type" : "damage",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": " Régénération du vide",
			"portee" : {
				"lance" : 5,	
				"personnel" : true
			},
			"description" : "Sur lui-même ou sur un allié, l’arcaniste utilise l’essence de sa magie pour soigner les blessures au fil du temps. Ce sort rend 2+{{1}} points de vie tous les tours, pendant 3 tours. Il est cependant impossible de cumuler plusieurs régénérations sur une même cible.",
			"type" : "heal",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Barrière dimensionnelle",
			"portee" : {
				"description" : "zone de type 2 autour du mage"
			},
			"description" : "L’arcaniste forme un dôme éthéré sur un espace circulaire autour de lui. Le dôme possède 40+{{10}} points de vie et est insensible aux attaques magiques. Étant donné que la barrière bloque les attaques physiques, il ne sera pas possible de recevoir d’attaques extérieures tant que celle-ci sera active. Néanmoins, les personnages à l’intérieur pourront continuer leur assaut à condition que le mage <b>canalise</b> son sort afin d’abaisser la barrière le temps de quelques secondes, pour que ses alliés puissent attaquer. Si des dégâts infligés excèdent les points de vie restants du bouclier, le surplus sera infligé à la cible initiale.",
			"type" : "autre",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Union",
			"portee" : {
				"lance" : 30
			},
			"description" : "Les esprits du membre du groupe sont connectés, toutes les informations circulent entre eux sans aucune retenue. La connexion mentale dure {{4}} heures, mais sera interrompue si jamais un personnage s’éloigne trop du mage (plus de {{1}} kilomètre).",
			"type" : "autre",
			"cout" : "5 PM",
			"bonus" : {

			}
		}
		],
		"Rang 5": [
		{
			"label": "Peau magique",
			"portee" : {
				"lance" : 5,	
				"personnel" : true
			},
			"description" : "L’arcaniste, de par son lien spécifique avec le mana pur, peut octroyer à ses alliés ou à lui-même un bonus à la RM de 1D4 + {{0.5}}, pendant 1D4 + {{1}} tours.",
			"type" : "buff",
			"cout" : "6 PM",
			"bonus" : {

			}
		},
		{
			"label": "Lien",
			"portee" : {
				"lance" : 5
			},
			"description" : "Le mage se lie avec une autre personne pendant 1D4 + {{1}} tours et partage ainsi tous les effets néfastes comme bénéfiques. Lorsqu’une cible reçoit un soin, un regain de points de mana, ou un buff, l’autre recevra également le double (si jamais le mage se soigne de 4 points de vie, la personne liée sera soignée de 8 points de vie). En revanche, cela sera également valable pour les dégâts et les debuff. Si l’un des deux personnages se fait toucher, l’autre subira le double des dégâts. Dans ce cas-ci, la personne initialement touchée par l’attaque réduira normalement les dégâts par sa RD ou sa RM. La deuxième personne subira alors le double dégâts, après réduction de l’armure de son comparse. De son côté, la deuxième personne ne pourra réduire les dégâts que s’il possède de la RM. Dans le cas d’une attaque de zone (ou d’un soin) et que les deux personnes sont touchées en même temps, chacun subira ses propres dégâts (ou soins) et fera ensuite subir le double à l’autre comme expliqué plus haut.",
			"type" : "autre",
			"cout" : "6 PM",
			"bonus" : {

			}
		}
		],
		"Rang 6": [
		{
			"label": "Blink",
			"portee" : {
				"description" : "personnelle, téléportation"
			},
			"description" : "Le mage se téléporte sur une distance de 10+{{5}} mètres. Ce sort peut également être lancé instantanément durant une attaque ennemie, cependant, l’arcaniste devra d’abord réussir un jet de dextérité (avant de réussir un jet d’incantation), opposé à celle de l’assaillant et payer le double du coût en mana.",
			"type" : "autre",
			"cout" : "7 PM",
			"bonus" : {

			}
		},
		{
			"label": "Lance de l’éther",
			"portee" : {
				"lance" : 30
			},
			"description" : "Une lance de magie pure se forme et fonce à vitesse folle vers la cible et inflige 2D8 + {{1}} dégâts. Les dégâts du sort sont peu élevés, mais la marge de critique de ce sort est augmentée de 2 (critique sur 1, 2 et 3) ou (au choix), l’attaque peut transpercer les ennemis en ligne droite, sans perdre de dégâts, mais garde son critique à 1. Il faudra réussir un jet d’esquive opposé à la réussite du mage pour éviter l’attaque.",
			"type" : "damage",
			"cout" : "7 PM",
			"bonus" : {

			}
		}
		],
		"Rang 7": [
		{
			"label": "Daïdaïdolon",
			"portee" : {
				"lance" : 5
			},
			"description" : "L’arcaniste invoque un serviteur du vide, qui combattra à ses côtés. Se référer à la fiche des invocations de la magie générale.",
			"type" : "invocation",
			"cout" : "8 PM",
			"bonus" : {

			}
		},
		{
			"label": "Vol arcanique",
			"portee" : {
				"description" : "personnelle"
			},
			"description" : "Le mage fait apparaître une paire d’ailes bleutées et éthérées dans son dos, lui procurant la capacité de vol. Le mage peut également choisir de rendre tout son corps intangible et pourra activer ou désactiver cet état à sa guise, mais le changement d’état prendra un tour. L’effet s’estompera de lui-même après une heure ou que le mage décide de mettre fin au sort.",
			"type" : "buff",
			"cout" : "8 PM",
			"bonus" : {

			}
		}
		],
		"Rang 8": [
		{
			"label": "Implosion du vide",
			"portee" : {
				"lance" : 15,
				"description" : "dans une zone de type 1"
			},
			"description" : "Alors qu’il déchaîne toute la puissance des arcanes, le mage créer une violente explosion dans une zone, infligeant 8D6 + {{1}} dégâts à toutes les créature se trouvant à l’intérieur. Il faudra réussir un jet d’acrobatie ou d’athlétisme (dans le cas ou la distance à sauter est de 2 cases ou moins) opposé à la réussite du mage afin de sortir de la zone rapidement et ainsi éviter les dégâts.",
			"type" : "damage",
			"cout" : "9 PM",
			"bonus" : {

			}
		},
		{
			"label": "Fusion prismatique",
			"portee" : {
				"lance" : 10
			},
			"defense" : "opposée – esquive ou bouclier",
			"description" : "Un rayon violacé part des mains du mage, partant en ligne droite. Le rayon aura pour effet de soigner les alliés de 3D6 + {{1}} points de vie et d’infliger 3D6 + {{1}} dégâts aux ennemis. Le sort traversera les entités vivantes (PJ et PNJ) mais pas les éléments extérieurs comme de gros rochers, des arbres ou des bâtiments. De plus, le sort peut être à <b>canaliser</b>, si le mage le désir.  Concernant les augmentations magiques, le sort pourra bénéficier de renforcements pour les soins et les dégâts, mais de façon dissociée (on pourra par exemple utiliser le don magie altruiste et soigner de 4D6, en infligeant toujours du 3D6).",
			"type" : "autre",
			"cout" : "9 PM",
			"bonus" : {

			}
		}
		]
	},
	"Magie runique" : {
		"magie" : true,
		"style" : {
			"background" : "magie_runique.jpg",
			"color" : "#8486e0",
			"shadow": "black",
		},
		"description" : "● Un objet ne peut accueillir plus d’une rune à la fois, ne pouvant supporter trop de puissance.<br>"+

"● Un personnage ne peut porter sur lui (équipement, sur son corps ou objet d’inventaire) plus de 3 enchantements, pour les mêmes raisons que pour les objets.<br>"+

"● Les effets d’une rune ne sont cependant pas cumulables<br>"+

"● Il est possible de réaliser 2 types d’enchantements : unique ou persistant. L’enchantement unique ne sera sujet à aucune restriction, alors que pour un enchantement persistant, il faudra réaliser un rituel d’une heure par niveau de la rune, dépenser le double du mana et le sort se lancera avec un malus de 10. Au bout de 3 échecs sur un même objet, celui-ci sera détruit. e plus, la durée d’un enchantement persistant dépendra du niveau du mage :<br>"+

"Niveau 1 : pendant 12 heures<br>"+
"Niveau 2 : pendant 24 heures<br>"+
"Niveau 3 : pendant 3 jours<br>"+
"Niveau 4 : pendant 1 semaine<br>"+
"Niveau 5 : pendant 2 semaines<br>"+
"Niveau 6 : pendant 1 mois<br>"+
"Niveau 7 : pendant 1 an<br>"+
"Niveau 8 et plus : définitf<br>"+

"● Dans le cas d’un enchantement unique, la rune s’efface après utilisation. Si l’enchantement est persistant, l’effet perdurera dans un laps de temps indiqué ci-dessus et s’activera au besoin et sans coût ou jet. À noter qu’une rune persistante peut être désactivée et activée à volonté tant qu’elle est active. Mais l’effet est tout de même consommé lorsque la rune est désactivée.<br>"+

"● Une rune peut être rechargée (peu importe le type d’enchantement) avec un nouveau jet du mage (avec la possibilité d’un échec), le même coût en mana et 1 tour par niveau de la rune, le temps d’une canalisation.<br>"+

"● Certains effets, même en enchantements persistants, ne seront actifs qu’une fois par jour et devront se recharger durant 24 heures. Cela sera alors précisé dans la description du sort.",
		"type": "Magie",
		"Rang 1": [
		{
			"label": "Iconoclaste",
			"portee" : {
				"lance" : 10
			},
			"description" : "La cible est débarrassée de toutes runes ou tous symboles magiques éventuels. Cependant, le mage ne pourra détruire que les runes de son niveau ou celles inférieures.",
			"type" : "autre",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label": "Rune aqueuse",
			"cible(s)" : "un récipient",
			"description" : "Cette rune peut être inscrite sur une outre, une gourde ou tout autre récipient destiné à transporter du liquide. Lorsque la rune est utilisée, elle offrira {{500}} ml d’eau potable à l’activation dans le cas d’un enchantement unique. Pour un enchantement persistant, la quantité sera offerte à chaque activation.",
			"type" : "autre",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label": "Rune lumineuse",
			"cible(s)" : "un objet",
			"description" : "Cette rune va émettre une lumière qui va luire avec l’intensité d’une torche pendant 15+{{5}} minutes dans le cas d’un enchantement unique.",
			"type" : "autre",
			"cout" : "2 PM",
			"bonus" : {

			}
		}
		],
		"Rang 2": [
		{
			"label": "Rune de vitesse",
			"cible(s)" : "équipement",
			"description" : "Cette rune ajoute un bonus de {{2}} à l’initiative le temps d’un combat et s’active par la volonté du porteur. Dans le cas d’un enchantement persistant, l’effet perdure au-delà d’un combat.",
			"type" : "buff",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		{
			"label": "Rune informative",
			"cible(s)" : "objet ou équipement",
			"description" : "Lors de la création de la rune, le mage devra énoncer oralement les circonstances pour lesquelles la rune devra s’activer. Les informations pourront être vagues, comme « Déclenche-toi lorsqu’un humanoïde sera dans les environs », ou au contraire très précises, comme « Déclenche-toi lorsqu’un serpent venimeux rôde dans les parages », selon le besoin. En revanche, afin de localiser un individu spécifique, il faudra que le mage ait déjà vu cette personne (un visuel suffit, pas besoin de connaître la cible sur le bout des doigts) et de connaître son nom ou un statut qui le définit clairement, pour éviter toute ambiguïté. L’effet de la rune porte sur un rayon de 50 mètres autour d’elle et celle-ci se mettra à émettre un son mental pour indiquer que la cible désirée se trouve dans le périmètre, puis s’éteindra.",
			"type" : "autre",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		{
			"label": "Rune de communication",
			"cible(s)" : "objets ou équipements",
			"description" : "Cette rune est gravée sur deux objets distincts qui permettront à deux individus de communiquer. Tant que les deux moitiés de la rune sont actives, les deux porteurs peuvent savoir approximativement où se situe l’autre. La communication ainsi que la détection durent pendant {{1}} heure(s) (enchantement unique) et prennent effet dans un rayon de 30+{{5}} mètres. Au-delà, tout contact sera alors coupé.",
			"type" : "autre",
			"cout" : "3 PM",
			"bonus" : {

			}
		}
		],
		"Rang 3": [
		{
			"label": "Rune arcanique",
			"cible(s)" : "armes",
			"description" : "Cette rune peut être inscrite sur n’importe quelle arme de contact pour l’enchanter, permettant ainsi d’infliger des dégâts magiques et ainsi ignorer l’armure ou toucher des créatures éthérées. L’effet perdurera pour {{1}} attaque(s) dans le cas d’un enchantement unique.",
			"type" : "buff",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label": "Rune de stockage",
			"cible(s)" : "objet ou équipement ",
			"description" : "Cette rune permet au détenteur de l’objet sur lequel elle est gravée de lancer le sort qu’elle renferme, une fois par jour, sans dépense de points de magie et sans risque d’échec. En revanche, il est impossible de stocker un sort runique dans une rune et chaque rune ne peut contenir qu’un seul sort. Mais il peut y voir plusieurs runes de ce type sur un même objet. Les sorts pourront alors être lancés de manière indépendante par le porteur. À noter que d’autres mages, avec l’aide du créateur de runes, pourront stocker leurs propres sorts. Dans le cas d’un enchantement persistant, la rune se désactivera après utilisation, mais sera de nouveau disponible dans les 24 heures, le temps de se recharger. Il n’y aura pas besoin de stocker le sort de nouveau, celui-ci reviendra automatiquement.",
			"type" : "autre",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label": "Rune de légèreté",
			"cible(s)" : "objet ou équipement",
			"description" : "Cette rune peut être gravée sur une pièce d’armure et ainsi diminuer le malus lié à sa charge de {{0.5}}. Ce sort permet également d’alléger des armes afin de retirer la restriction de force pour pouvoir les manier. Ou plus simplement, d’alléger un objet quelconque. Le poids d’un objet pourra être diminué de {{10}} kilos, pour une durée de 15+{{5}} minutes dans le cas d’un enchantement unique.",
			"type" : "autre",
			"cout" : "4 PM",
			"bonus" : {

			}
		}
		],
		"Rang 4": [
		{
			"label": "Rune fortifiante",
			"cible(s)" : "armes",
			"description" : "Les attaques d’une arme portant cette rune infligent 1D{{1}} points de dégâts supplémentaires. Par exemple, un mage de niveau 4, gagnera un bonus de 1D4 dégâts. La rune sera active le temps d’un combat et s’active lorsque le porteur le décide. Dans le cas d’un enchantement persistant, l’effet perdure au-delà d’un combat.",
			"type" : "buff",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Rune de serrure",
			"cible(s)" : "objets comportant un verrou",
			"description" : "Quand elle est gravée sur une porte ou sur un objet verrouillable, cette rune permet de bloquer ou débloquer la serrure. Dans le cas d’un enchantement unique, l’effet perdure pendant {{1}} jour(s). Elle fonctionne avec toutes les serrures physiques, les verrous magiques ou runiques. Le lanceur du sort, ainsi que d’éventuelles personnes définies à la création de la rune, pourront déverrouiller la serrure à leur guise, sans restriction d’utilisation. Il sera impossible d’ouvrir la serrure par une autre personne, hormis un autre runomancien qui opposera son sort à celui du personnage.",
			"type" : "autre",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Rune de protection",
			"cible(s)" : "objet ou équipement",
			"description" : "Lorsque cette rune est apposée, elle offre au personnage au bonus de {{1}} à la RD ou la RM pendant une heure. Dans le cas d’un enchantement persistant, l’effet perdure au-delà d’une heure.",
			"type" : "buff",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Rune régénératrice",
			"cible(s)" : "objet ou équipement",
			"description" : "Cette rune, sertie sur un objet, permet de récupérer 2D4 + {{1}} points de vie à l’activation. Dans le cas d’un enchantement persistant, la rune se désactivera après utilisation, mais sera de nouveau disponible dans les 24 heures, le temps de se recharger.",
			"type" : "heal",
			"cout" : "5 PM",
			"bonus" : {

			}
		}
		],
		"Rang 5": [
		{
			"label": "Rune anti-magie",
			"cible(s)" : "objet ou équipement",
			"description" : "Cette rune annule automatiquement un sort lancé sur le porteur. Si le sort en question est un sort de zone et que l’ennemi vise la case où se trouve le personnage, celui-ci sera bloqué et le porteur, ainsi que les autres personnages de la zone seront protégés. Mais s’il le lance sur une case annexe au porteur, la rune ne se déclenchera pas. À l’instar, elle n’annulera pas des sorts non dirigés sur le porteur, comme une invocation qui ira l’attaquer après sa création. Attention, tant que la rune est active, elle bloquera aussi tout sort bénéfique (il est donc préférable d’enchanter un objet, pour pouvoir l’enlever au besoin). Dans le cas d’un enchantement persistant, la rune se désactivera après utilisation automatique, mais sera de nouveau disponible dans les 24 heures, le temps de se recharger.",
			"type" : "autre",
			"cout" : "6 PM",
			"bonus" : {

			}
		},
		{
			"label": "Rune chercheuse",
			"cible(s)" : "projectiles physiques",
			"description" : "Cette rune est gravée sur tous types de projectiles (armes de jet, flèches ou carreaux) et permet de toucher automatiquement une cible se trouvant à portée, sans avoir à faire de jet. Un enchantement ne sera valide que sur un projectile à la fois, il faudra réitérer le processus pour enchanter plusieurs flèches, par exemple. L’effet ne perdurera que pour un seul coup et sur une seule et même munition, ce qui fait qu’une attaque multiple (comme la double encoche) n’aura qu’une seule attaque qui touchera de façon automatique. Dans le cas d’un enchantement persistant, la rune se désactivera après utilisation, mais sera de nouveau disponible dans les 24 heures, le temps de se recharger.",
			"type" : "buff",
			"cout" : "6 PM",
			"bonus" : {

			}
		}
		],
		"Rang 6": [
		{
			"label": "Rune contre-coup",
			"cible(s)" : "armure",
			"description" : "La rune renvoie un coup d’un assaillant sur lui-même. L’effet de la rune est automatique, dès que le porteur subira des dégâts au corps-à-corps, l’ennemi se verra retourner ses dégâts contre lui (il aura cependant le droit à un jet d’esquive, avec un malus de 4, à cause de la surprise). Dans le cas d’un enchantement persistant, la rune se désactivera après utilisation automatique, mais sera de nouveau disponible dans les 24 heures, le temps de se recharger.",
			"type" : "damage",
			"cout" : "7 PM",
			"bonus" : {

			}
		},
		{
			"label": "Rune de guérison",
			"cible(s)" : "objet ou équipement",
			"description" : "Chaque fois qu’un coup inflige des dommages au porteur (après réduction d’armure), la rune s’active et le soigne automatiquement de {{2}} points de vie, au début du tour suivant. Si jamais le porteur à reçu plusieurs attaques, il recevra autant de fois le soin, qu’il a reçu d’attaques. Dans le cas d’un enchantement unique, l’effet perdure durant tout un combat.et pour un enchantement persistant, l’effet perdure au-delà.",
			"type" : "heal",			"cout" : "7 PM",
			"bonus" : {

			}
		}
		],
		"Rang 7": [
		{
			"label": "Rune affaiblissante",
			"cible(s)" : "objet ou équipement",
			"description" : "Hormis le porteur, quiconque est touché par l’objet runique voit une de ses caractéristiques (choisie au moment de l’inscription) baisser de {{2}}, pendant 1D5 + {{1}} tours. Dans le cas d’un enchantement persistant, l’effet perdure au-delà de ce laps de temps, il faudra alors être vigilent lorsqu’un allié aura besoin de toucher le porteur.",
			"type" : "debuff",
			"cout" : "8 PM",
			"bonus" : {

			}
		},
		{
			"label": "Rune d’invisibilité",
			"cible(s)" : "objet ou équipement",
			"description" : "Lorsque son porteur l’active, il est ensuite impossible de le voir par des procédés habituels (une détection magique le rendrait visible). La rune ne le rend toutefois pas silencieux, et il sera possible de repérer le personnage par d’autres moyens que la vue. La durée de l’invisibilité sera de {{2}} minutes. L’effet peut cependant être désactivé avant si besoin, auquel cas, la charge de la rune sera consommée. Il est également possible de rendre invisible un objet en inscrivant la rune directement dessus. Durant l’invisibilité, s’il s’agit d’un personnage, celui-ci pourra effectuer toutes les actions voulues, même attaquer, sans interrompre l’invisibilité (il sera alors beaucoup plus facile de le repérer). Dans le cas d’un enchantement persistant, le porteur deviendra invisible dès qu’il porte l’objet enchanté et que l’effet est activé.",
			"type" : "buff",
			"cout" : "8 PM",
			"bonus" : {

			}
		}
		],
		"Rang 8": [
		{
			"label": "Rune de mort",
			"cible(s)" : "armes",
			"description" : "Une rune de mort ne peut être inscrite que sur une arme. Cela aura pour effet de tuer toute créature d’un type spécifique (si celle-ci rate un jet de ténacité simple) si l’arme lui inflige le moindre dommage en combat. Chaque rune fonctionne sur une créature précise (un monstre ou un humanoïde précis du bestiaire), durant {{1}} tour(s). Le mage créant la rune doit avoir déjà combattu une créature du type en question au cours de son existence pour que la création de celle-ci soit possible. À rappeler qu’une fois le coup fatal infligé, l’effet se dissipe. Dans le cas d’un enchantement persistant, la rune se désactivera après utilisation, mais sera de nouveau disponible dans les 24 heures, le temps de se recharger.",
			"type" : "autre",
			"cout" : "9 PM",
			"bonus" : {

			}
		},
		{
			"label": "Rune explosive",
			"cible(s)" : "objet",
			"description" : "Le mage peut décider d’activer cette rune après un laps de temps prédéfini, ou simplement lorsqu’une créature vient à passer un peu trop près. Dans tous les cas, l’esquive est impossible et l’explosion infligera les dégâts suivants en fonction de son positionnement vis-à-vis de la rune :<br>"+
				"1 case : Le personnage subit 8D6 dégâts<br>"+
				"2 cases : Le personnage subit 6D6 dégâts<br>"+
				"3 cases : Le personnage subit 4D6 dégâts<br>"+
				"5 cases : Le personnage subit 2D6 dégâts<br>"+
				"6 cases : Le personnage subit 1D6 dégâts<br>"+
				"Chaque personnage qui subit plus de 15 points de dégâts de l’explosion devra réussir un jet d’acrobatie sous peine d’être renversé. Tout support où se trouve la rune subira bien entendu l’explosion et peut être amené à se briser si les dégâts subits sont trop importants. Dans le cas d’un enchantement unique, la rune explosera d’elle-même dans l’heure si elle n’a pas été activée. Pour un enchantement persistant, le mage sera libre de l’activer quand il le souhaite, dans le laps de temps qui lui est fourni par son niveau.",
			"type" : "damage",
			"cout" : "9 PM",
			"bonus" : {

			}
		}
		]
	},
	"Magie zooanthropique" : {
		"magie" : true,
		"style" : {
			"background" : "magie_zooanthropique.jpg",
			"color" : "#c8faf9",
			"shadow": "black",
		},
		"description" : "La transformation partielle (d’une seule partie du corps) n’épargne pas les vêtements et dure pendant {{10}} minutes. Il est possible de cumuler les transformations, tant qu’une partie du corps n’est pas sollicité deux fois. Par exemple, on pourra avoir une langue de caméléon en plus de griffes, mais il sera impossible d’avoir et des griffes et des pinces.<br>"+
		"Il existe aussi des altérations éphémères, qui englobent la totalité du corps. La transformation permet de dépasser les limites du corps et draine rapidement l’énergie du mage. Tant que l’effet sera actif, le mage devra dépenser le coût en mana du sort à chaque tour. C’est pour cette même raison qu’il est impossible de cumuler plusieurs altérations. En activer une, arrête immédiatement l’autre.<br>"+
		"Les sorts de transformation sont considérés comme des actions bonus et permettent donc au mage de jouer normalement après leur jet d’incantation (pour une seule transformation par tour). Les jets d’attaque se feront sur la compétence corps-à-corps. Le bonus de dégâts lié à la force n’est pas compté pour cette magie, car remplacé par le gain de dégâts à chaque niveau du mage.<br>"+
		"Il sera possible d’utiliser les dons de pugilat et de combat, mais pas les augmentations magiques de sorts.",
		"type": "Magie",
		"Rang 1": [
		{
			"label": "Adhérence",
			"portee" : {
				"description" : "personnelle"
			},
			"description" : "Le mage couvre la paume de ses mains de spatules, qui créent des forces d’adhérence moléculaire permettant au mage de coller à des surfaces variées, même très lisses. Ces lamelles spatiales permettent également d’empêcher le désarmement ou de lâcher prise sur un objet.",
			"type" : "buff",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label": "Régulation thermique",
			"portee" : {
				"description" : "personnelle"
			},
			"description" : "Le mage développe des follicules thermorégulateurs et recouvre son corps d’un pelage adaptatif. En présence de chaleur, la densité des poils est réduite, ils deviennent plus courts et abordent une couleur plus claire afin de réfléchir la lumière. En cas de temps froids, la densité des poils augmente pour créer une couche isolante supplémentaire, les poils deviennent plus longs en formant une barrière thermique et la couleur du poil s’assombrit pour absorber davantage de lumière. De ce fait, le mage devient insensible aux rudesses du climat et n’a pas besoin de faire de jet de ténacité.",
			"type" : "buff",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label": "Dard",
			"portee" : {
				"description" : "personnelle"
			},
			"defense" : "opposée – esquive ou bouclier et un jet de ténacité simple si raté",
			"description" : "Une queue de scorpion se forme dans le dos du mage, infligeant 1D6 + {{1}} dégâts. En plus des dégâts, chaque attaque à une chance d’empoissonner la cible si elle rate un jet de constitution non opposé. Le poison inflige 1D4 dégâts non réductibles par tour et perdurera jusqu’aux soins (les effets ne sont pas cumulables). La queue reste sur le corps du mage jusqu’à ce que celui-ci décide de la faire disparaître.",
			"type" : "damage",
			"cout" : "2 PM",
			"bonus" : {

			}
		}
		],
		"Rang 2": [
		{
			"label": "Ouïes",
			"portee" : {
				"description" : "personnelle"
			},
			"description" : "Le mage peut développer des branchies magiques, lui permettant de respirer sous l’eau et d’explorer les profondeurs sans avoir besoin d’oxygène.",
			"type" : "buff",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		{
			"label": "Saut de sauterelle",
			"portee" : {
				"description" : "personnelle"
			},
			"description" : "Les jambes du mage se métamorphosent en longues pattes de sauterelle, augmentant drastiquement la capacité de saut. Le mage pourra désormais sauter de 10 mètres en hauteur ou longueur sans aucun malus. On ajoutera ensuite, un malus de 2 pour chaque mètre supplémentaire pour un maximum de 15. Puis, le score de malus sera ensuite multiplié par 2 pour chaque mètre supplémentaire.",
			"type" : "buff",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		{
			"label": "Propensions animales",
			"portee" : {
				"description" : "personnelle"
			},
			"description" : "Le mage se dote de capacités communes et courantes chez les animaux. Il aura le choix de se doter d’une truffe, d’yeux, de rapace ou de grandes oreilles afin d’augmenter sa perception visuelle, olfactive ou auditive de 5. Il est également possible d’acquérir une vision nocturne ou thermique. L’effet se dissipe au bout d’une heure ou lorsque le mage le décide.",
			"type" : "buff",
			"cout" : "3 PM",
			"bonus" : {

			}
		}
		],
		"Rang 3": [
		{
			"label": "Épines",
			"portee" : {
				"description" : "personnelle"
			},
			"defense" : "aucune - esquive ou bouclier opposé en cas d’attaque à distance",
			"description" : "Le corps du personnage se recouvre d’épines semblables à celles d’un porc-épic, infligeant son niveau en dégâts (inesquivables) à chaque fois que le personnage est la cible d’attaques de contact. Le mage a également la possibilité de toucher à distance (5 mètres) des cibles placées dans son dos, en envoyant directement des piques sur elles. L’action se fera alors avec un malus de 4 et rendra le sort inactif.",
			"type" : "damage",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label": "Rugissement",
			"portee" : {
				"description" : "personnelle"
			},
			"defense" : "opposée – volonté",
			"description" : "Le mage transforme ses cordes vocales afin d’en décupler ses capacités. Il lui est alors possible de rugir, provoquant la peur chez ses ennemis et les faisant fuir. Le mage reçoit un bonus à l’intimidation de 20 sur les animaux habituellement considérés comme des proies par les autres animaux (ils fuiront de manière naturelle, sauf cas spécifique) et un bonus de 10 sur les animaux prédateurs ainsi que les humains. Il leur faudra réussir un jet de volonté opposé pour ne pas entrer dans l’état effrayé.",
			"type" : "buff",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label": "Morsure venimeuse",
			"portee" : {
				"description" : "personnelle"
			},
			"defense" : "opposée – esquive ou bouclier et un jet de ténacité simple si raté",
			"description" : "Des crochets semblables à ceux d’une araignée poussent sur le visage du mage, permettant à une attaque naturelle de morsure qui inflige 1D6 + {{1}} dégâts. De plus, l’ennemi devra réussir un jet de ténacité simple, sous peine de se retrouver paralysé durant 1D2 + {{1}} tours.",
			"type" : "damage",
			"cout" : "4 PM",
			"bonus" : {

			}
		}
		],
		"Rang 4": [
		{
			"label": "Régénération cellulaire",
			"portee" : {
				"description" : "personnelle"
			},
			"description" : "Tel un axolotl, le mage déclenche une régénération cellulaire accélérée. Les cellules endommagées se transforment en cellules souches, orchestrant la reconstruction rapide des tissus. Le mage pourra se soigner d’un montant égal à sa caractéristique de constitution en points de vie.",
			"type" : "heal",
			"cout" : "5 PM par tour",
			"bonus" : {

			}
		},
		{
			"label": "Griffes",
			"portee" : {
				"description" : "personnelle"
			},
			"defense" : "opposée – esquive ou bouclier",
			"description" : "Les ongles du mage s’allongent et durcissent, prenant au passage une coloration noire. Des griffes longues de plusieurs centimètres se forment, augmentant les dégâts au corps-à-corps. Chaque coup de griffe inflige 3D6+ {{1}} dégâts et possède une Pa de 3",
			"type" : "damage",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Peau caméléon",
			"portee" : {
				"description" : "personnelle"
			},
			"description" : "La peau du mage s’adapte aux couleurs qui l’entourent, augmentant drastiquement sa discrétion. Pour que l’effet soit parfait, il faudra au préalable retirer ses vêtements. S’il ne se déplace pas et qu’il ne réalise pas d’action bruyante, le mage sera considéré comme invisible. S’il se déplace de 3 cases par tour ou moins, il est alors considéré comme dissimulé. Le sort augmente la compétence de discrétion de 25 dans l’état invisible et de 15 en étant dissimulé.",
			"type" : "buff",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Ailes",
			"portee" : {
				"description" : "personnelle"
			},
			"description" : "Ce sort confère au mage une paire d’ailes d’une envergure magnifique, semblables à celles d’un oiseau. Le vol n’est pas considéré comme magique et fonctionne de manière classique.",
			"type" : "buff",
			"cout" : "5 PM",
			"bonus" : {

			}
		}
		],
		"Rang 5": [
		{
			"label": "Cobra cracheur",
			"portee" : {
				"description" : "personnelle, puis 5 mètres"
			},
			"defense" : "opposée – esquive ou bouclier",
			"description" : "Des crochets se forment dans la bouche du mage, lui permettant de cracher du venin dans les yeux de ses ennemis. Le venin n’inflige pas de dégâts, sauf s’il touche la muqueuse des yeux. Viser les yeux n’est pas sujet au malus, l’appendice permettant une grande précision de tir. Le jet peut être tiré sur 5 mètres en ligne droite et infligera 1D4 dégâts + {{1}}. L’intérêt principal du sort étant la cécité qu’il provoque. Selon la marge de réussite, le venin aura les effets suivants :<br>"+

				"1 à 6 : L’ennemi est aveuglé pendant 2 tours<br>"+
				"7 à 15 : L’ennemi est aveuglé pendant 5 tours<br>"+
				"16 à 20 : La cécité perdure pendant le niveau du mage en jour<br>"+
				"Coup critique : Le jet rend aveugle définitivement",
			"type" : "debuff",
			"cout" : "6 PM",
			"bonus" : {

			}
		},
		{
			"label": "Écailles",
			"portee" : {
				"description" : "personnelle"
			},
			"description" : "Le corps du mage se recouvre d’une épaisse couche d’écailles, difficile à transpercer. Le personnage augmente ainsi sa RD de {{1}} (l’effet ne se cumule pas).",
			"type" : "buff",
			"cout" : "6 PM",
			"bonus" : {

			}
		}
		],
		"Rang 6": [
		{
			"label": "Langue fléau",
			"portee" : {
				"description" : "personnelle, puis 5 mètres"
			},
			"description" : "La langue du mage s’allonge et change de forme. Celle-ci prend l’apparence d’une langue de caméléon, avec les attributs qui vont avec. La langue offre une portée d’attaque de 5 mètres et inflige 2D12 dégâts + {{1}}. La langue peut également servir pour deux manœuvres : assommer et agripper.<br>"+
				"En effet, la vitesse de projection de la langue fait qu’elle peut servir d’assommoir. La langue ne fera alors pas de dégâts, et l’on se référera aux règles habituelles de manœuvres.<br>"+
				"La langue peut également agripper un ennemi, et s’en servir pour ramener la cible vers le mage (pour un maximum de 90 kilos).",
			"type" : "damage",
			"cout" : "7 PM",
			"bonus" : {

			}
		},
		{
			"label": "Constriction",
			"portee" : {
				"description" : "personnelle"
			},
			"defense" : "opposée – corps-à-corps, puis acrobatie chaque tour si raté, afin de se libérer",
			"description" : "Le bas du corps du mage disparaît et laisse place à un corps de serpent, telle une Méduse et, sur un jet d'acrobatie, le mage peut désormais effectuer une étreinte. Le mage va s’enrouler autour d’une cible et la serrer afin de l’immobiliser. Pendant la constriction, le mage est libre de se servir d’autres sorts ou attaquer une fois la cible immobilisée. S’il le désire, il peut infliger 1D8 dégâts de constriction supplémentaire par tour. Le mage peut garder cette forme à volonté et peut la combiner avec une autre transformation partielle tant qu’elle ne concerne pas le bas du corps.",
			"type" : "debuff",
			"cout" : "7 PM",
			"bonus" : {

			}
		}
		],
		"Rang 7": [
		{
			"label": "Somatotropine",
			"portee" : {
				"description" : "personnelle"
			},
			"description" : "Instantanément, les muscles du mage se développent de manière impressionnante. Le mage bénéficie d’un bonus de 1D à tous ses jets de dégât et les bonus de dégât liés au niveau du mage sont doublés.",
			"type" : "buff",
			"cout" : "8 PM par tour",
			"bonus" : {

			}
		},
		{
			"label": "Pinces",
			"portee" : {
				"description" : "personnelle"
			},
			"description" : "Le mage voit ses mains devenir deux énormes pinces de crabe, sectionnant avec force tout ce qui passera à sa portée. Chaque pince infligera 5D6 + {{1}} dégâts.",
			"type" : "damage",
			"cout" : "8 PM",
			"bonus" : {

			}
		}
		],
		"Rang 8": [
		{
			"label": "Électrocytes",
			"portee" : {
				"description" : "personnelle"
			},
			"description" : "Le mage génère et concentre des électrocytes où il le souhaite dans son corps et peut déclencher des impulsions électriques infligeant 7D6 + {{1}} points de dégâts.",
			"type" : "damage",
			"cout" : "9 PM",
			"bonus" : {

			}
		},
		{
			"label": "Ruée colibri",
			"portee" : {
				"description" : "personnelle"
			},
			"description" : "Le mage déclenche une augmentation rapide de son métabolisme et de son activité musculaire. Pendant cette période, le mage peut effectuer une attaque supplémentaire à chaque tour de jeu. L’effet n’impose pas de malus supplémentaire au toucher et se cumule avec d’autres sources d’attaques multiples.",
			"type" : "buff",
			"cout" : "9 PM par tour",
			"bonus" : {

			}
		}
		]
	},
	"Nécromancie" : {
		"magie" : true,
		"style" : {
			"background" : "necromancie.jpg",
			"color" : "#f6f6f6",
			"shadow": "black",
		},
		"type": "Magie",
		"Rang 1": [
		{
			"label": "Pourrissement",
			"portee" : {
				"lance" : 15
			},
			"defense" : "opposée – ténacité",
			"description" : "La chair et les plantes se mettent à pourrir par endroits, infligeant 1D6 + {{1}} dégâts.",
			"type" : "damage",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label": "Parler aux morts",
			"portee" : {
				"lance" : 1
			},
			"description" : "Le mage à la morbide capacité de parler aux morts, à la condition que le cadavre ne soit pas trop abîmé. L’esprit du mort reviendra alors dans son corps pour une courte période. L’esprit aura les mêmes connaissances et informations que lors de son vivant, tout comme son caractère. L’effet se dissipe dès que la conversation prend fin.",
			"type" : "autre",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label": "Disparition nécrotique",
			"portee" : {
				"lance" : 1,
				"description" : "puis 20 mètres autour du lanceur au niveau 3"
			},
			"description" : "De par sa simple volonté, le mage peut faire disparaître un cadavre en un tour, celui-ci se faisant dévorer par la magie à une vitesse folle. Lorsque le nécromancien atteint le niveau 3, la capacité devient multicibles et il sera alors possible de cibler plusieurs cadavres, qui disparaîtront eux aussi en un tour seulement.",
			"type" : "autre",
			"cout" : "2 PM",
			"bonus" : {

			}
		}
		],
		"Rang 2": [
		{
			"label": "Nécrophage",
			"portee" : {
				"lance" : 1,
				"description" : "régénération personnelle"
			},
			"description" : "Le nécromancien peut ponctionner le mana d’un cadavre, tant que celui-ci est décédé depuis moins d’une heure. Ce sort rendra alors au mage 1D4 + {{1}} points de mana. Le cadavre possède une «charge d’énergie» encore stockée dans son corps pour une heure. Cette charge peut être utilisée par le nécromancien de diverses façons. Une fois la charge utilisée, le cadavre ne disparaît pas, mais il est vidé de son énergie spirituelle et celle-ci est consommée.",
			"type" : "autre",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		{
			"label": "Énergie vitale",
			"portee" : {
				"lance" : 15,
				"description" : "soins personnels"
			},
			"description" : "mage 1D4 + {{1}} points de vie. Le cadavre possède une «charge d’énergie» encore stockée dans son corps pour une heure. Cette charge peut être utilisée par le nécromancien de diverses façons. Une fois la charge utilisée, le cadavre ne disparaît pas, mais il est vidé de son énergie spirituelle et celle-ci est consommée.",
			"type" : "heal",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		{
			"label": "Cri des damnés",
			"portee" : {
				"description" : "personnelle"
			},
			"description" : "Le mage est entouré de voix et spectres de l’outre-tombe. Celui-ci gagne un bonus de 1D4 + {{1}} en intimidation pendant 1D4 + {{1}} tour.",
			"type" : "buff",
			"cout" : "3 PM",
			"bonus" : {

			}
		}
		],
		"Rang 3": [
		{
			"label": "Serviteur squelette",
			"portee" : {
				"lance" : 5
			},
			"description" : "Un squelette sort des entrailles de la terre, prêt à servir son maître, il faudra se référer à la liste des invocations de la nécromancie.",
			"type" : "invocation",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label": "Pourrissement de zone",
			"portee" : {
				"lance" : 15,
				"description" : "dans une zone de type 2"
			},
			"defense" : "opposée – ténacité",
			"description" : "Les corps des vivants deviennent presque semblables à ceux des morts autour du nécromancien, celui-ci accélère le pourrissement de la chair et des végétaux et inflige 1D6 + {{1}} dégâts, mais en zone.",
			"type" : "damage",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label": "Horde",
			"portee" : {
				"description" : "personnelle"
			},
			"description" : "Le nécromancien aime se voir entouré par la mort et la personnification de celle-ci au travers de ses diverses invocations. Maître des cadavres, le mage peut augmenter sa limite d’invocations maximales (mais uniquement pour les invocations nécrotiques) qui est normalement à 1, sauf don spécifique, et ce, pour toute la durée du combat."+
			" Au fils des niveaux, le mage pourra invoquer plus de serviteurs :<br>"+
			" De niveau 1 à 2 = 1 Invocation supplémentaire.<br>"+
			" De niveau 3 à 4 = 2 Invocations supplémentaires.<br>"+
			" De niveau 5 à 6 = 3 Invocations supplémentaires.<br>"+
			" De niveau 7 à 8 = 4 Invocations supplémentaires.<br>"+
			" De niveau 9 à 10 = 5 Invocations supplémentaires.<br>"+
			" Au-delà de 4 invocations au total (nécrotiques ou autres) le mage subira un malus de 4 à ses actions pour chaque invocation supplémentaire sur le terrain, car il devient difficile d’avoir un contrôle mental sur toutes et d’incanter en même temps.",
			"type" : "buff",
			"cout" : "4 PM",
			"bonus" : {

			}
		}
		],
		"Rang 4": [
		{
			"label": "Serviteur zombie",
			"portee" : {
				"lance" : 5
			},
			"description" : "Un mort-vivant se forme depuis la terre et vient aider son maître dans la bataille, il faudra se référer à la liste des invocations de la nécromancie.",
			"type" : "invocation",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Emprise cadavérique",
			"portee" : {
				"lance" : 15,
				"description" : "dans une zone de type 2"
			},
			"defense" : "opposée – acrobatie ou athlétisme (si la distance à sauter est de 2 cases ou moins), puis acrobatie si échoué",
			"description" : "Des bras squelettiques sortent du sol et bloquent toutes les créatures ennemies en attrapant leurs jambes. Chaque personnage ou créature présente dans cette zone doit réussir un jet d’esquive pour ne pas être affecté. Si le jet échoue, ils subiront 1D6 + {{1}} dégâts et entreront dans l’état enchevêtré",
			"type" : "debuff",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Vortex de vie",
			"portee" : {
				"lance" : 15,
				"description" : "dans une zone de type 3"
			},
			"defense" : "opposée – ténacité (jet unique, pas besoin à chaque tour)",
			"description" : "Le mage <b>canalise</b> son sort, touchant chaque créature présente dans la zone, dont ses alliés. Mais le nécromancien, ainsi que toutes les créatures de types «mort vivant» ou «fantomatique» (comme ses invocations), ne pourront être attaqués par ce sort qui n’affecte que les vivants. Les cibles subiront {{2}} dégâts, pour chaque tour passé dans la zone. Les dégâts totaux infligés soigneront le mage de moitié (arrondit à l’entier inférieur).",
			"type" : "damage",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Arme nécrotique",
			"portee" : {
				"lance" : 5
			},
			"defense" : "opposée – ténacité",
			"description" : "Le nécromancien enchante une arme (ne peut enchanter qu’une arme à la fois et ne fonctionne pas sur une attaque à mains nues ou naturelle) qui infligera 1D6 dégâts putrides (magiques) supplémentaires aux dégâts de base. Lorsque l’arme inflige au moins 1 dégât (il faudra donc passer la RD et la RM d’un ennemi), la cible aura une chance de passer dans l’état nauséeux durant 1D4 + {{1}} tours. Une cible ayant résisté à l’effet nauséeux ou qui a déjà été affectée au moins une fois ne pourra plus subir cette affliction de nouveau. Le sort a une durée de 10+{{5}} minutes.",
			"type" : "buff",
			"cout" : "5 PM",
			"bonus" : {

			}
		}
		],
		"Rang 5": [
		{
			"label": "Destrier de la mort",
			"portee" : {
				"lance" : 5
			},
			"description" : "Le nécromantien invoque un cheval squelette, animé par une énergie bleutée d’outre-tombe. Il faudra se référer à la liste des invocations de la nécromancie.",
			"type" : "invocation",
			"cout" : "6 PM",
			"bonus" : {

			}
		},
		{
			"label": "Nécrose",
			"portee" : {
				"lance" : 20
			},
			"defense" : "opposée – ténacité",
			"description" : "Le mage cible un opposant, se concentrant pour faire pourrir la chair de celui-ci, infligeant 2D6 + {{1}} dégâts.",
			"type" : "damage",
			"cout" : "6 PM",
			"bonus" : {

			}
		}
		],
		"Rang 6": [
		{
			"label": "Exécuteur squelette",
			"portee" : {
				"lance" : 5
			},
			"description" : "Un guerrier squelette imposant, prend forme prêt de son maître, il faudra se référer à la liste des invocations de la nécromancie.",
			"type" : "invocation",
			"cout" : "7 PM",
			"bonus" : {

			}
		},
		{
			"label": "Pavillon de mort",
			"portee" : {
				"lance" : 20,
				"description" : "dans une zone de type 3"
			},
			"defense" : "opposée – acrobatie ou athlétisme (si la distance à sauter est de 2 cases ou moins)",
			"description" : "Un étendard apparaît au centre de la zone et chaque créature présente dans cette zone, doit réussir un jet afin de sortir de celle-ci.<br>"+
			"Étant grande, les malus liés à la distance de saut seront appliqués en fonction de la position du personnage, et de la zone à atteindre (1 case = malus de 0, 2 cases = malus de 3, 3 cases = malus de 6, 4 cases = malus de 9), pour ne pas être affecté.<br>"+
			"Si le jet échoue, le personnage subira un malus de 3 à sa vitesse de déplacement ainsi qu’un malus de 4 à toutes ses compétences, jusqu’à ce qu’il sorte de la zone. Ce sort ne peut affecter le nécromancien ou ses invocations, mais est actif pour absolument toutes les autres entités.",
			"type" : "debuff",
			"cout" : "7 PM",
			"bonus" : {

			}
		}
		],
		"Rang 7": [
		{
			"label": "Nécrose de zone",
			"portee" : {
				"lance" : 20,
				"description" : "dans une zone de type 2"
			},
			"defense" : "opposée – ténacité",
			"description" : "Toutes les créatures vivantes présentes dans la zone voient leur corps noircir et se décomposer lentement, subissant 2D6 + {{1}} dégâts.",
			"type" : "damage",
			"cout" : "8 PM",
			"bonus" : {

			}
		},
		{
			"label": "Explosion cadavérique",
			"portee" : {
				"lance" : 20,
				"description" : "explosion dans une zone de type 1, 2 et 3"
			},
			"defense" : "opposée – acrobaties ou athlétisme (si la distance à sauter est de 2 cases ou moins)",
			"description" : "Le mage utilise la charge d’un cadavre et le transforme en une bombe, explosant sur le champ. L’explosion infligera 6D6 + {{1}} dégâts dans une zone de type 1 autour du cadavre, 3D6 + {{1}} dégâts dans une zone de type 2 autour du cadavre et 1D6 + {{1}} dégâts dans une zone de type 3 autour du cadavre. L’explosion détruira alors complètement le corps, n’en laissant que des lambeaux inutilisables. Afin d’éviter les dégâts de l’explosion, on appliquera les mêmes malus que ceux cités pour le sort «Pavillon de mort». À noter que l’exposition ne peut bénéficier d’augmentations magiques.",
			"type" : "damage",
			"cout" : "8 PM",
			"bonus" : {

			}
		}
		],
		"Rang 8": [
		{
			"label": "Phylactère",
			"portee" : {
				"description" : "personnelle"
			},
			"description" : "Le mage crée un phylactère, un objet magique destiné à contenir son âme, ses souvenirs, ses caractéristiques, ses compétences et ses pouvoirs. Une fois activé, l'objet inscrit automatiquement l’intégralité de son essence. À sa mort, la résurrection prend une semaine pour restaurer son âme dans une nouvelle enveloppe corporelle, dont le mage choisit l'âge. Une fois ce délai écoulé, il renaît à l’endroit du phylactère, ressuscité avec 1 point de vie et de mana, dans une forme similaire à sa précédente. Si le phylactère est endommagé ou détruit avant la fin du processus, l’âme est perdue et la résurrection échoue. Si le phylactère est détruit alors que le mage est encore vivant, il ne se passe rien pour ce dernier, mais un nouveau phylactère devra être créé.",
			"type" : "autres",
			"cout" : "9 PM",
			"bonus" : {

			}
		},
		{
			"label": "Réanimation",
			"portee" : {
				"lance" : 1
			},
			"description" : "Par une lugubre incantation, le mage fait se relever le corps d’une créature décédée. L’individu n’aura pas de conscience propre et obéira aux désirs du mage. Le sort à une durée limitée d’effet de 1D6 tours + 1 par niveau. Aucun jet supplémentaire ne sera demandé, car les cibles seront sous l’emprise totale du lanceur de sort. Celles-ci seront capables des mêmes actions et capacités que de leur vivant.",
			"type" : "autres",
			"cout" : "9 PM",
			"bonus" : {

			}
		}
		]
	},
	"Occultomancie" : {
		"magie" : true,
		"style" : {
			"background" : "occultomancie.jpg",
			"color" : "#9361c4",
			"shadow": "black",
		},
		"type": "Magie",
		"Rang 1": [
		{
			"label": "Vision",
			"portee" : {
				"description" : "personnelle"
			},
			"description" : "Le mage n’est plus du tout indisposé par l’obscurité naturelle, il voit parfaitement bien de jour comme de nuit pendant 10+{{1}} minutes.",
			"type" : "buff",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label": "Oppression",
			"portee" : {
				"lance" : 10
			},
			"defense" : "opposée – volonté, jusqu’à réussite",
			"description" : "La cible est entourée par des brumes aux volutes trompeuses et se retrouve prise d’un sentiment de peur envahissant, se retrouvant dans l’état effrayé.",
			"type" : "debuff",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label": "Rayon obscure",
			"portee" : {
				"lance" : 15
			},
			"defense" : "opposée – esquive ou bouclier",
			"description" : "De l’énergie concentrée noire jaillit des mains du lanceur, frappant une cible et infligeant 1D6 + {{1}} dégâts.",
			"type" : "damage",
			"cout" : "2 PM",
			"bonus" : {

			}
		}
		],
		"Rang 2": [
		{
			"label": "Ténèbres magiques",
			"portee" : {
				"lance" : 10,
				"description" : "dans une zone de type 1"
			},
			"defense" : "aucune",
			"description" : "Le mage fait apparaître une zone d’ombre pendant 1D4 + {{{1}}, qui rendra aveugle toute personne ne pouvant voir dans les ténèbres magiques. Il est également impossible de voir au travers et la zone sera considérée comme un obstacle visuel. L’utilisation d’une torche, d’une lampe ou toute autre source de lumière naturelle sera inefficace.",
			"type" : "debuff",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		{
			"label": "Gemini",
			"portee" : {
				"lance" : 5
			},
			"description" : "Le mage façonne un double d’ombre qui trompe le regard de ses adversaires, qui ne voient non plus un, mais deux magiciens en face d’eux. La copie est physique, possède une corpulence identique à l’original et peut se mouvoir, mais il lui est cependant impossible d’attaquer ou de parler. Le double possède la moitié des points de vie du lanceur et aucune RD ou RM et disparaît lorsque ceux-ci atteignent 0.",
			"type" : "invocation",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		{
			"label": "Spectre",
			"portee" : {
				"description" : "personnelle"
			},
			"description" : "Le corps du mage se change en un nuage de fumée noire, gardant une forme humanoïde grossière. L’occultomancien décide de lui-même quand arrêter le sort, si cela s’effectue en combat, reprendre son état normal demandera un tour. Sous cette forme, le mage garde ses caractéristiques, mais ne pourra faire que se déplacer (mais sa vitesse de déplacement est triplée). Il devient insensible aux attaques physiques, les attaques magiques continuent cependant de lui infliger des dégâts.",
			"type" : "autre",
			"cout" : "3 PM",
			"bonus" : {

			}
		}
		],
		"Rang 3": [
		{
			"label": "Nuée",
			"portee" : {
				"lance" : 15
			},
			"defense" : "opposée – esquive ou bouclier",
			"description" : "Le mage fait apparaître une nuée de chauves-souris ombreuses, qui frappent la cible et lui infligent 2D4 + {{1}} points de dégâts.",
			"type" : "damage",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label": "Affliction",
			"portee" : {
				"lance" : 15,
				"description" : "dans une zone de type 1"
			},
			"defense" : "opposée – volonté",
			"description" : "Pendant 4+{{1}} tours, une aura noire recouvre les corps des victimes et les empêche de recevoir une quelconque source de soins, qu’elle soit naturelle ou magique. Le sort peut toutefois être dissipé avec un contre-sort annihilant les malédictions.",
			"type" : "debuff",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label": "Chat noir",
			"portee" : {
				"description" : "personnelle"
			},
			"description" : "Pendant un tour, le mage prend la forme d’un félin ténébreux, ce qui accroît sa vitesse et double ses déplacements pendant 1D4 + {{1}} tours.",
			"type" : "buff",
			"cout" : "4 PM",
			"bonus" : {

			}
		}
		],
		"Rang 4": [
		{
			"label": "Éclipse",
			"portee" : {
				"description" : "-"
			},
			"description" : "Le mage force les astres à se croiser durant 4+{{1}} tours, créant une éclipse où le monde sera plongé complètement dans les ténèbres durant quelques secondes. Une source de lumière sera impérative pour y voir, même devant soi. Les personnes possédant la capacité de voir dans les ténèbres naturelles auront tout de même leur vision réduite de moitié.",
			"type" : "autre",
			"cout" : "6 PM",
			"bonus" : {

			}
		},
		{
			"label": " Téléportation",
			"portee" : {
				"lance" : 20,
				"description" : "téléportation"
			},
			"description" : "Le mage disparaît dans sa propre ombre et choisit de réapparaître où il veut dans les 20+{{1}} mètres. Cela lui prend un tour complet et permet par exemple, de se téléporter sur une hauteur ou encore traverser un fossé.",
			"type" : "autre",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Ombre chinoise",
			"portee" : {
				"lance" : 10
			},
			"defense" : "opposée – acrobatie tous les tours jusqu’à libération",
			"description" : "Le mage créer une forme brumeuse, de l’apparence de son choix, qui va s’élancer et s’écraser sur sa cible, ne pouvant être esquivée. Une fois la cible touchée, la brume va envelopper le corps de la victime et l’enchevêtrera.",
			"type" : "debuff",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Hantise",
			"portee" : {
				"lance" : 10
			},
			"defense" : "opposée – volonté, puis volonté avec malus de 4 pour sortir de l’état si le premier rate",
			"description" : "Permet au magicien de jouer sur les peurs de sa victime, il apparaît aux yeux de sa cible comme la chose la plus menaçante que la victime peut concevoir. La vision de l’ennemi est ainsi troublée, voyant le mage comme un terrible danger, le faisant entrer dans l’état terrifié.",
			"type" : "debuff",
			"cout" : "5 PM",
			"bonus" : {

			}
		}
		],
		"Rang 5": [
		{
			"label": "Ficelles vaudoues",
			"portee" : {
				"lance" : 10
			},
			"defense" : "opposée – volonté",
			"description" : "Le mage tisse un lien magique entre lui et une cible et la force à en attaquer une autre. L’attaque ne fait pas perdre son tour à la cible puisqu’elle est exécutée durant celui du mage. Par ailleurs, que la cible ait déjà attaqué ou non, aucun malus ne lui sera appliqué pour cette attaque supplémentaire. L’attaque devra être une attaque basique, à moins que le mage n’en sache plus sur les compétences de sa cible ou qu’il l’ait vu les réaliser. Dans tous les cas, l’attaque se fera toujours avec les caractéristiques de la cible et non celles du mage.",
			"type" : "damage",
			"cout" : "6 PM",
			"bonus" : {

			}
		},
		{
			"label": "Dans les ombres",
			"portee" : {
				"description" : "personnelle"
			},
			"description" : "Le lanceur de sorts se camoufle parmi les ombres et devient entièrement invisible. Ses jets de discrétion sont automatiquement réussis, à condition qu’il reste dans l’ombre et qu’il ne fasse pas de bruit. L’effet perdure pendant 2+{{1}} minutes, mais si une attaque est lancée le sort prend immédiatement fin.",
			"type" : "buff",
			"cout" : "6 PM",
			"bonus" : {

			}
		}
		],
		"Rang 6": [
		{
			"label": "Gale noire",
			"portee" : {
				"lance" : 10
			},
			"defense" : "opposée – ténacité pour esquiver l’effet, puis ténacité chaque tour si le premier est raté, afin d’éviter les dégâts",
			"description" : "Le mage fait naître une tache noire brumeuse qui croît et s’étend jusqu’à recouvrir entièrement son hôte, drainant son énergie vitale de 2D6 dégâts, pendant {{1}} tour(s). Ce sort peut être dissipé avec un contre-sort annihilant les malédictions",
			"type" : "damage",
			"cout" : "7 PM",
			"bonus" : {

			}
		},
		{
			"label": "Possession",
			"portee" : {
				"lance" : 5
			},
			"defense" : "opposée – volonté, et si raté, volonté à chaque tour avec un malus de 4 pour reprendre le contrôle",
			"description" : "Une fumée obscure va sortir des mains du lanceur et se diriger vers sa cible. Celle-ci va s’introduire dans le corps de la victime, en passant par le nez, la bouche ou les oreilles et doucement le mage prendra possession du corps de sa cible et l’immobilisera entièrement. À chaque tour, la cible pourra essayer de lutter contre la possession et enter de posséder un même personnage plusieurs fois entraînera des malus à la réussite du mage (malus de 6 par tentative). Le mage ne pourra posséder qu’une seule cible à la fois, mais sera libre de ses propres mouvements même lorsque le sort est actif.",
			"type" : "debuff",
			"cout" : "7 PM",
			"bonus" : {

			}
		}
		],
		"Rang 7": [
		{
			"label": "Fléau d’ombre",
			"portee" : {
				"lance" : 15
			},
			"defense" : "opposée – esquive ou bouclier",
			"description" : "L’ombre de la cible se déforme et s’étend, formant rapidement une imposante masse. Celle-ci va venir frapper sa cible d’une force impressionnante en infligeant 4D6 + {{1}} dégâts, avant de disparaître d’où elle est venue.",
			"type" : "damage",
			"cout" : "8 PM",
			"bonus" : {

			}
		},
		{
			"label": "Clones",
			"portee" : {
				"description" : "zone de type 1 autour du lanceur"
			},
			"description" : "Le mage génère 1D4 clones qui vont s’élancer sur la ou les cibles que le lanceur de sorts aura désignées, et ce durant 1D4 + {{1}} tours. Chaque clone inflige les dégâts de l’arme du mage et doit réussir un jet de toucher sur les mêmes jets que le lanceur. Ils ne peuvent pas lancer de sort et possèdent la moitié des points de vie du mage. Au-delà de 4 clones, le mage subira un malus de 4 à ses actions pour chaque clone supplémentaire sur le terrain, car il devient difficile d’avoir un contrôle mental sur tous et de se battre en même temps.",
			"type" : "damage",
			"cout" : "8 PM",
			"bonus" : {

			}
		}
		],
		"Rang 8": [
		{
			"label": "Effroi",
			"portee" : {
				"lance" : 10
			},
			"defense" : "opposée – volonté, si le jet est raté il faudra réaliser un jet de ténacité simple. S’il rate également, la cible meurt. Dans le cas où le jet de volonté serait raté, mais celui de ténacité réussi, il faudrait réaliser un jet de volonté chaque tour avec un malus de 4, jusqu’à réussir à se calmer.",
			"description" : "Le mage ensorcelle la cible qui devient sous l’emprise d’une frayeur irrépressible et irrationnelle, la faisant entrer dans l’état terrifié, ce qui pourra aller jusqu’à entraîner une crise cardiaque foudroyante.",
			"type" : "autre",
			"cout" : "9 PM",
			"bonus" : {

			}
		},
		{
			"label": "Trou noir",
			"portee" : {
				"lance" : 10,
				"description" : "dans une zone de type 2"
			},
			"defense" : "opposée – acrobatie ou athlétisme (si la distance à sauter est de 2 cases ou moins)",
			"description" : "Le mage <b>canalise</b> et fait apparaître un vortex ténébreux qui aspire tout ce qui passe à sa portée. Les victimes qui restent à l’intérieur subiront 6D6 + {{1}} dégâts par tour.",
			"type" : "damage",
			"cout" : "9 PM",
			"bonus" : {

			}
		}
		]
	},
	"Phytomancie" : {
		"magie" : true,
		"style" : {
			"background" : "phytomancie.jpg",
			"color" : "#83dc2c",
			"shadow": "black",
		},
		"type": "Magie",
		"Rang 1": [
		{
			"label": "Enchevêtrement",
			"portee" : {
				"lance" : 15
			},
			"defense" : "opposée – acrobatie ou athlétisme, puis acrobatie ou force brute (avec malus de 4) jusqu’à libération",
			"description" : "Des racines et des lianes sortent du sol et bloquent l’ennemi en s’enroulant autour de ses jambes. La cible se retrouve bloquée dans ses mouvements et sera considérée comme entravée.",
			"type" : "debuff",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label": "Main verte",
			"portee" : {
				"lance" : 10
			},
			"description" : "De la petite graine au grand arbre, il n’y a qu’un pas pour le phytomancien qui peut faire croître les végétaux à une vitesse folle. Le sort est à <b>canaliser</b> et le mage devra maintenir son sort jusqu’à atteindre le développement souhaité. Chaque tour la plante va pousser, passant par différents stades de croissance.<br>"+
			"Pour les petites et moyennes plantes : crosse, levée, développement, formation des feuilles et enfin la floraison.<br>"+
			"Pour les arbres le temps de canalisation est doublé, donnant les stades suivants : germe, gaulis, perchis, jeune futaie, futaie adulte, fleuraison puis germination.",
			"type" : "autre",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label": "Appel botanique",
			"portee" : {
				"lance" : 5
			},
			"description" : "La mage fait pousser une plante de la taille d’un enfant, armée de trois lianes se terminant de masses épineuses.",
			"type" : "invocation",
			"cout" : "2 PM",
			"bonus" : {

			}
		}
		],
		"Rang 2": [
		{
			"label": "Cataplasme",
			"portee" : {
				"lance" : 1,	
				"personnel" : true
			},
			"description" : "Le phytomancien fait pousser dans la paume de sa main une petite plante médicinale qui se désintégrera et formera une crème épaisse verdâtre, qui pourra être appliquée sur une plaie et ainsi prodiguer 1D8 + {{1}} de soins.",
			"type" : "heal",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		{
			"label": "Poison insidieux",
			"portee" : {
				"lance" : 10
			},
			"defense" : "opposée – ténacité jusqu’à la purge du poison",
			"description" : "Des vapeurs violacées s’échappent de petites fleurs invoquées autour de la cible, qui empoisonnent le sang par inhalation. Chaque tour la cible doit réussir un jet de ténacité. S’il échoue, la charge augmente de 1 et s’il réussit, la charge diminue de 1. Si la charge de poison atteint 0, le corps de la cible purge complètement le poison. Si la charge atteint 5, elle est au maximum et ne peut plus augmenter..<br>"+
			"1 charge = 1D1 + {{1}}<br>"+
			"2 charges = 1D2 + {{1}}<br>"+
			"3 charges = 1D3 + {{1}}<br>"+
			"4 charges = 1D4 + {{1}}<br>"+
			"5 charges = 1D5 + {{1}}",
			"type" : "damage",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		{
			"label": "Rafflesia",
			"portee" : {
				"lance" : 15,
				"description" : "(zone de type 2, à partir de la fleur)"
			},
			"defense" : "opposée – ténacité",
			"description" : "Le mage invoque une grosse fleur rougeâtre de 10 kilos et d’un mètre de diamètre, dont s’échappe une effluve pestilentielle. Dans une zone autour de la fleur, l’air sera saturé d’une odeur de cadavre en putréfaction et tous les personnages présents dans la zone risquent de se retrouver dans l’état nauséeux. Les animaux auront plutôt tendance à fuir face à l’odeur, bien qu’il soit possible qu’à l’inverse de les repousser, l’odeur attire des insectes ou charognards. La plante sera détectable par le sens de l’odorat dans un rayon bien plus grand que la zone d’effet, mais ne sera pas assez prononcée pour affecter les créatures environnantes.",
			"type" : "debuff",
			"cout" : "3 PM",
			"bonus" : {

			}
		}
		],
		"Rang 3": [
		{
			"label": "Mur de ronces",
			"portee" : {
				"lance" : 15,
				"description" : "sur trois cases verticales ou horizontales"
			},
			"description" : "Le magicien fait surgir un mur rigide de 3 mètres de large pour 2+{{1}} mètres de haut. Le mage à la possibilité de <b>canaliser</b> le sort contre 1 point de mana par tour, ce qui aura pour effet d’allonger la sculpture de 2 mètres à chaque fois. Le mur possède 35+ {5}} points de vie et restera actif 5 minutes après l’arrêt de la canalisation.",
			"type" : "autre",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label": "Caféine",
			"portee" : {
				"lance" : 5,	
				"personnel" : true
			},
			"description" : "Le phytomancien fait pousser une tige jusqu’à hauteur de la bouche et permet l’ingestion d’une graine ayant à la capacité d’amplifier le métabolise, comme s’il avait reçu un stimulant. Le corps se régénère plus rapidement et pendant 1D4 + {{1}} tours la cible regagnera 1+{{1}} points de vie à chaque début de tour.",
			"type" : "buff",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label": "Éclosion",
			"portee" : {
				"lance" : 15,
				"description" : "dans une zone de type 1"
			},
			"defense" : "opposée – esquive avec un malus de 2, si raté, acrobatie pour retomber correctement",
			"description" : "De gros bourgeons sortent du sol, sous les pieds des créatures présentes dans la zone et infligent 1D8 dégâts + {{1}}. Les cibles touchées seront projetées en l’air, à condition qu’elles pèsent moins de 90 kilos. Une créature éjectée pourra tenter de retomber proprement sur ses jambes, au risque de se retrouver renversée. Le mage peut également choisir d’une direction et pousser un ennemi, imposant alors un malus de 2.",
			"type" : "damage",
			"cout" : "4 PM",
			"bonus" : {

			}
		}
		],
		"Rang 4": [
		{
			"label": "Écorce",
			"portee" : {
				"lance" : 5,	
				"personnel" : true
			},
			"description" : "La cible est en peu de temps recouverte de bois, le type de ce dernier dépendant de la volonté du jeteur de sorts. L’écorce forme une cuirasse protectrice, qui augmente de 2 la RD de la cible pendant 1D4 + {{1}} tours.",
			"type" : "buff",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Entremêlements",
			"portee" : {
				"lance" : 15,
				"description" : "dans une zone de type 2"
			},
			"defense" : "opposée – acrobatie ou athlétisme, puis acrobatie ou force brute (avec malus de 4) jusqu’à libération",
			"description" : "Des racines et des lianes sortent du sol et s’enrouent autour des jambes des unités ennemies présentes dans la zone. Les cibles se retrouvent bloquées dans leurs mouvements et seront considérées comme entravées.",
			"type" : "debuff",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Dialogue végétal",
			"portee" : {
				"description" : "personnelle"
			},
			"description" : "Le phytomancien à la douce capacité à communiquer avec le monde végétal. Durant 10+{{5}} minutes, le mage peut converser avec la flore, dans un langage que lui seul peut comprendre, à moins de posséder le don dialogue végéta",
			"type" : "buff",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Épines",
			"portee" : {
				"lance" : 10
			},
			"defense" : "opposée – esquive ou bouclier",
			"description" : "Une grosse ronce surgit aux pieds du mage, projetant une salve d’épines dans la direction désirée. La cible subira alors 2D6 + {{1}} points de dégâts.",
			"type" : "damage",
			"cout" : "5 PM",
			"bonus" : {

			}
		}
		],
		"Rang 5": [
		{
			"label": "Distorsion du bois",
			"portee" : {
				"lance" : 5
			},
			"description" : "Le mage peut modeler le bois, la façonner au gré de ses envies. Le sort est à <b>canaliser</b> et permet de façonner du bois vivant, aussi bien que du bois mort ou du bois traité. Le sort sera plus ou moins difficile à réussir selon la modification souhaitée (sachant que celles-ci vont de 1 à 6). Le MJ définira avec le joueur l’intensité de la modification demandée et indiquera le nombre de paliers exigés pour la transformation. Les incréments d’intensité sont indiqués ci-dessous, avec un exemple, pour une meilleure visualisation.<br>"+
			"Pour calculer les malus liés à la difficulté de l’opération, on regardera quel palier est demandé, que l’on multipliera par deux. Ce calcul représentera également le nombre de tours nécessaires à la réalisation de la transformation. Par exemple, changer un bâton en cuillère en bois ne sera pas trop difficile (difficulté 3, que l’on multipliera par 2, ce qui donnera un malus de 6 pour l’action, ainsi que 6 tours de canalisation.) La seule restriction, étant que l’incrément de taille initiale de la pièce de bois ne peut être modifié (Impossible de faire une barque à partir d’un seau d’eau).<br>"+
			"1: changements mineurs<br>"+
			"2: petits changements<br>"+
			"3: changements visibles<br>"+
			"4: changements conséquents<br>"+
			"5: gros changements<br>"+
			"6: dénaturalisation de l’objet initial, travail de grande précision, etc",
			"type" : "autre",
			"cout" : "6 PM",
			"bonus" : {

			}
		},
		{
			"label": "Phytomorphe",
			"portee" : {
				"description" : "personnelle"
			},
			"description" : "Le mage peut transformer son corps en plante, avec tous les changements que cela englobe. Il lui sera impossible de réaliser une d’action (autre que de mettre fin au sort), mais bénéficiera d’un camouflage parfait et indétectable à l’œil nu, tant qu’il se transforme en plante vivant dans le milieu adéquat. Si cette condition est respectée, la seule façon de détecter le mage, sera d’être doté d’une perception magique.",
			"type" : "autre",
			"cout" : "6 PM",
			"bonus" : {

			}
		}
		],
		"Rang 6": [
		{
			"label": "Fruit revigorant",
			"portee" : {
				"description" : "personnelle"
			},
			"description" : "Le mage peut créer 1D2 + {{1}} fruits tout à fait comestibles et très riches en apports caloriques. Un fruit permet de se sustenter en eau et nourriture pour la journée, ainsi que d’octroyer un bonus de à la RM de 4 pendant 2 heures et de retirer tous les états préjudiciables.",
			"type" : "buff",
			"cout" : "7 PM",
			"bonus" : {

			}
		},
		{
			"label": "Plante carnivore géante",
			"portee" : {
				"lance" : 5
			},
			"description" : "Le mage invoque une gigantesque plante d’infliger de sérieuses morsures ou d’entraver ses ennemis avec ses lianes.",
			"type" : "invocation",
			"cout" : "7 PM",
			"bonus" : {

			}
		}
		],
		"Rang 7": [
		{
			"label": "Spores soporifiques",
			"portee" : {
				"lance" : 15
			},
			"defense" : "opposée – ténacité",
			"description" : "Une fine pellicule de poudre se génère au-dessus de la cible, entrant dans son corps par inhalation et provoquant chez elle l’effet d’un somnifère pendant 1D2 + {{1}} tours. Une cible endormie ne peut pas effectuer d’action, elle devient alors une cible facile, incapable de se défendre et sera en proie à recevoir un coup de grâce si l’attaque s’effectue hors combat. Si la cible se fait attaquer alors qu’elle dort, elle aura une chance de se réveiller et aura alors droit à un autre jet, avec un bonus de 8.",
			"type" : "debuff",
			"cout" : "8 PM",
			"bonus" : {

			}
		},
		{
			"label": "Déluge de fouets",
			"portee" : {
				"lance" : 15
			},
			"defense" : "opposée – esquive ou bouclier",
			"description" : "Une multitude de lianes, de racines, de ronces et de branches s’abat sur une cible, la lacérant de toutes parts et infligent 4D6 + {{1}} points de dégâts.",
			"type" : "damage",
			"cout" : "8 PM",
			"bonus" : {

			}
		}
		],
		"Rang 8": [
		{
			"label": "Yggdrasil",
			"portee" : {
				"lance" : 1
			},
			"description" : "Le phytomancien puise dans les ressources profondes d’Hunaleros et dans sa propre énergie pour donner naissance à l’arbre légendaire reliant les mondes. Ce frêne gigantesque, d’un vert émeraude, n’est pas physiquement présent. Il ne s’agit que d’une image, un reflet dans cet univers. Cette image est en réalité un portail, une entrée menant à divers endroits du monde, même les plus inaccessibles. Il sera alors possible pour le mage et ses alliés de se déplacer librement dans le monde. Générer un tel arbre aux pouvoirs extraordinaires demande du temps. Et le mage devra effectuer un rituel de 120 heures (pas nécessairement de façon continue) durant lequel il devra rester à <b>canaliser</b> la formation de l’arbre. Chaque heure passée dans le rituel, fera perdre 10 points de mana, l’arbre se servant de cette énergie pour croître.",
			"type" : "invocation",
			"cout" : "9 PM",
			"bonus" : {

			}
		},
		{
			"label": "Efflorescence",
			"portee" : {
				"description" : "zone de type 2 autour du lanceur"
			},
			"description" : "De multiples pétales colorés semblent tomber tout autour du mage, recouvrant les cibles telle une magnifique pluie. Lorsqu’un pétale touche un allié, celui-ci est guérit de tout les changements d’état qui l’accablaient (Hormis maladie, malédiction, affliction ou cas spécifique) et rend instantanément 5D6 + {{1}} points de vie.",
			"type" : "heal",
			"cout" : "9 PM",
			"bonus" : {

			}
		}
		]
	},
	"Pyromancie" : {
		"magie" : true,
		"style" : {
			"background" : "pyromancie.jpg",
			"color" : "#fff97f",
			"shadow": "#500000",
		},
		"type": "Magie",
		"Rang 1": [
		{
			"label": "Sphère de feu",
			"portee" : {
				"lance" : 10
			},
			"defense" : "opposée – esquive ou bouclier",
			"description" : "Le pyromancien matérialise une sphère de feu de 90 cm de diamètre qu’il projette sur un ennemi, lui infligeant 1D4 + {{1}} dégâts.",
			"type" : "damage",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label": "Lumières dansantes",
			"portee" : {
				"description" : "personnelle"
			},
			"description" : "Le mage génère une petite flamme dans le creux de sa main qui éclaire la zone telle une torche durant 15+{{5}} minutes avant de s’éteindre.",
			"type" : "autre",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label": "Contrôle du feu",
			"portee" : {
				"lance" : 20
			},
			"description" : "Sort permettant de diminuer ou d’augmenter l’intensité des flammes d’un feu naturel. Le sort est à <b>canaliser</b> et sera plus ou moins long selon l’état initial du feu ainsi que de la modification souhaitée. Le MJ définira avec le joueur le nombre de paliers exigés pour la transformation.<br>"+
			"Par exemple, passer d’une flamme d’un briquet (1) à un feu de camp (5) prendra : 30sc + 30sc + 1 m + 1m = 3 minutes,car l’incrément de puissance aura été augmenté de 4. Les exemples ci-dessous aident à se représenter la taille des flammes pour les campagnes, mais la liste est tout à fait exhaustive pour chaque palier.<br>"+
			"1-3: superficielle (briquet, torche, torche huilée) = 30 secondes<br>"+ 
			"4-6: mportante (brasero, feu de camp, grand feu) = 1 minute<br>"+ 
			"7-9: brûlure grave (bûcher, feu de joie, incendie) = 5 minutes<br>"+ 
			"10-12: mortelle (feu immense, souffle de dragon) = 10 minutes",
			"type" : "autre",
			"cout" : "2 PM",
			"bonus" : {

			}
		}
		],
		"Rang 2": [
		{
			"label": "Boule de feu",
			"portee" : {
				"lance" : 5,
				"description" : "(lancer le sort) 10 mètres (déplacement maximum de la boule)"
			},
			"defense" : "Défense: opposée – esquive ou bouclier et acrobatie ou athlétisme pour éviter l’explosion",
			"description" : "Le mage créer une boule de feu qui se déplace en ligne droite et inflige 1D6 + {{1}} de dégâts. Lorsqu’elle rencontre un obstacle (peu importe lequel), elle explose et inflige 1D4 dégâts dans une zone de type 1. Pour éviter la sphère, la première cible doit réussir un jet d’esquive, la sphère continuera alors sa route jusqu’au prochain obstacle. La cible ou l’obstacle avec lequel la boule entre en collision ne pourra esquiver les dégâts d’explosion. Pour les cibles adjacentes, il faudra réussir un jet d’acrobatie ou d’athlétisme, pour sauter hors de la zone. Si la boule de feu ne rentre en collision avec aucun obstacle sous 10 mètres, elle se dissipera d’elle-même.",
			"type" : "damage",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		{
			"label": "Pyrotechnie",
			"portee" : {
				"description" : "cône devant le lanceur"
			},
			"defense" : "opposée – volonté",
			"description" : "Le mage créer une explosion devant lui qui aveugle pendant 1D4 + {{1}} tours toute personne le regardant et présente dans la zone d’effet.",
			"type" : "debuff",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		{
			"label": "Création enflammée mineure",
			"portee" : {
				"lance" : 5
			},
			"description" : "Le pyromancien amène les flammes à se rassembler pour former un petit humanoïde élémentaire. Se référer à la fiche des invocations de la pyromancie.",
			"type" : "invocation",
			"cout" : "3 PM",
			"bonus" : {

			}
		}
		],
		"Rang 3": [
		{
			"label": "Projectiles de feu",
			"portee" : {
				"lance" : 10
			},
			"defense" : "opposée – esquive ou bouclier, avec malus de 2 et 4 pour la deuxième et troisième si destinées à une seule cible",
			"description" : "Le mage génère trois boules de feu qu’il va ensuite lancer sur une ou plusieurs cibles, chaque projectile infligeant 1D6 + {{0.5}} dégâts. Seul le premier projectile pourra bénéficier d’améliorations magiques.",
			"type" : "damage",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label": "Maîtrise des flammes",
			"portee" : {
				"description" : "personnelle"
			},
			"description" : "Le pyromancien puise sa force dans une source de feu environnante, augmentant les dégâts de ses deux prochains sorts selon la catégorie du feu à disposition. Si le combat prend fin avant que le mage n’utilise ses deux attaques, l’effet se dissipe de lui-même. Le MJ donnera au joueur la valeur du bonus en fonction du feu qui sera utilisé. On peut cependant prendre le repère suivant : 1D1 à 1D3 pour un feu superficiel, 1D4 à 1D6 pour un feu important, 1D7 à 1D9 pour un grand feu et 1D10 à 1D12 pour un feu gigantesque.",
			"type" : "buff",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label": "Feu intérieur",
			"portee" : {
				"lance" : 5,	
				"personnel" : true
			},
			"description" : "Le sang de la cible boue dans ses veines, accentuant l’ardeur avec laquelle le mage ou un allié se bat. Cette énergie nouvelle procure un bonus à la force de 3 pendant 1D4 + {{1}} tours.",
			"type" : "buff",
			"cout" : "4 PM",
			"bonus" : {

			}
		}
		],
		"Rang 4": [
		{
			"label": "Glyphe explosive",
			"portee" : {
				"lance" : 1,
				"description" : "dans une zone de type 1"
			},
			"defense" : "aucune",
			"description" : "Le mage <b>canalise</b> pendant 5 minutes un tracé au sol ou sur un objet, le temps d’effectuer le dessin. Une fois l’incantation terminée, toute créature entrant dans la zone déclenchera une explosion, provoquant 2D8 + {{1}} dégâts au contact et 2D4 + {{1}} à 1 mètre de distance. Le glyphe perdure pendant 1D4 + {{1}} heures et reste détectable sur un jet de perception passive, ou active si la victime s’y attend. Une fois le piège déclenché, il est impossible à esquiver. À noter que le glyphe ne peut bénéficier d’augmentations magiques.",
			"type" : "damage",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Bénédiction des flammes",
			"portee" : {
				"lance" : 1
			},
			"description" : "Le jeteur de sorts habille une arme ou une armure d’une aura enflammée pendant 1D4 + {{1}} tours.  Au choix, le mage peut octroyer un bonus de résistance aux dégâts de feu de 4 sur une armure, ou un bonus 4 dégâts de feu sur une arme.",
			"type" : "buff",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Sculpture de flammes",
			"portee" : {
				"lance" : 20,
				"description" : "sur trois cases verticales ou horizontales"
			},
			"description" : "Le magicien fait surgir un rideau flamboyant de 3 mètres de large pour 2+{{1}} mètres de haut. Le mage à la possibilité de <b>canaliser</b> le sort contre 1 point de mana par tour, ce qui aura pour effet d’allonger la sculpture de 2 mètres à chaque fois. Le mur inflige 2D6 points de dégâts à quiconque le traverse et restera actif 5 minutes après l’arrêt de la canalisation.",
			"type" : "autre",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Création enflammée",
			"portee" : {
				"lance" : 5
			},
			"description" : "Le pyromancien fait se dessiner un être de flammes qui se battra à ses côtés. Se référer à la fiche des invocations de la pyromancie.",
			"type" : "invocation",
			"cout" : "5 PM",
			"bonus" : {

			}
		}
		],
		"Rang 5": [
		{
			"label": "Cône de feu",
			"portee" : {
				"description" : "cône devant le lanceur"
			},
			"defense" : "opposée – athlétisme ou acrobatie",
			"description" : "Le mage produit un cône de flammes, brûlant tout dans une petite zone devant lui et infligeant 4D4 + {{1}} dégâts. Si la case devant le mage est occupée, les unités derrière cette cible ne subiront que la moitié des dégâts.",
			"type" : "damage",
			"cout" : "6 PM",
			"bonus" : {

			}
		},
		{
			"label": "Flammerole",
			"portee" : {
				"description" : "personnelle"
			},
			"description" : "Une petite flamme apparaît au-dessus de la tête du mage, absorbant les dégâts de feu reçus et les convertissant en soins durant {{1}} tour(s). La source des dégâts peut être naturelle ou magique, les sorts de dégâts habituels pouvant désormais cibler le mage afin qu’il se soigne de lui-même.",
			"type" : "buff",
			"cout" : "6 PM",
			"bonus" : {

			}
		}
		],
		"Rang 6": [
		{
			"label": "Création enflammée majeure",
			"portee" : {
				"lance" : 5
			},
			"description" : "Le pyromancien forme un brasier géant qui prend vie, donnant naissance à un grand élémentaire. Se référer à la fiche des invocations de la pyromancie.",
			"type" : "invocation",
			"cout" : "7 PM",
			"bonus" : {

			}
		},
		{
			"label": "Aura élémentaire",
			"portee" : {
				"lance" : 1,	
				"personnel" : true
			},
			"description" : "Un halo incandescent recouvre la cible, la protégeant des effets des flammes naturelles, ainsi que des fortes chaleurs pendant 10+{{10}} minutes.",
			"type" : "buff",
			"cout" : "8 PM",
			"bonus" : {

			}
		}
		],
		"Rang 7": [
		
		{
			"label": "Passion ardente",
			"portee" : {
				"lance" : 1,	
				"personnel" : true
			},
			"description" : "Lorsque ce sort est activé, le mage s'enflamme littéralement et renforce ses pouvoirs à chaque sort de feu qu’il lance. Lorsqu’un sort de feu est généré, le mage bénéficie de bonus progressifs de {{1}} dégâts et d’une case de déplacement (ces bonus sont limités à un par tour, même en cas d’attaques multiples) supplémentaires. Si le mage parvient à maintenir cet état pendant 10 tours consécutifs, il gagne une attaque additionnelle. Toutefois, si le mage ne lance pas de sort de feu pendant 1 tour, le sort se dissipe automatiquement, annulant tous les bonus accumulés. Chaque tour pendant lequel le sort reste actif coûte 10 points de vie au mage, qui subit le contre-coup des brûlures (Les boucliers et la RM permettent de réduire ces dégâts).",
			"type" : "buff",
			"cout" : "8 PM",
			"bonus" : {
	

			}
		},
		{
			"label": "Fournaise",
			"portee" : {
				"lance" : 1,	
				"personnel" : true
			},
			"defense" : "opposée – acrobatie ou athlétisme (si la distance à sauter est de 2 cases ou moins) pour éviter la zone. Un jet de ténacité opposé sera demandé pour toute entité dans la zone (hormis la cible) afin d’éviter les dégâts de chaleur",
			"description" : "Le mage <b>canalise</b> une rafale de vents brûlants qui s’abat sur la zone, rendant l’air irrespirable et infligeant 2D6 + {{1}} points de dégâts de chaleur pendant 4 tours. Lorsqu’une créature subit des dégâts de cette attaque, elle passe automatiquement dans l’état épuisé pendant 1D6 tours. L’état est non cumulable et ne peut être esquivé dès le moment où le sort inflige des dégâts à la cible.",
			"type" : "damage",
			"cout" : "8 PM",
			"bonus" : {

			}
		}
		],
		"Rang 8": [
		{
			"label": "Météorites",
			"portee" : {
				"lance" : 20,
				"description" : "explosion dans une zone de type 1"
			},
			"defense" : "opposée – esquive ou bouclier (avec malus de 2 et 4 pour la deuxième et troisième si destinées à une seule cible) et acrobatie ou athlétisme pour éviter l’explosion",
			"description" : "Le mage fait apparaître 4 météorites d’une soixantaine de centimètres chacune, qu’il peut lancer sur une ou plusieurs cibles. Toute cible frappée par l’une des sphères subit 2D6 + {{{1}} points de dégâts, la sphère explose ensuite et inflige 1D8 points de dégâts de feu (fixes) aux unités situées dans cette zone. Si une cible ne parvient pas à esquiver une météorite, elle subira automatiquement les dégâts d’explosion. Cependant, seule la première météorite pourra bénéficier des augmentations de dégâts (surcharge, magie destructive, etc.) et les dégâts d’explosion ne pourront faire l’objet d’augmentation.",
			"type" : "damage",
			"cout" : "9 PM",
			"bonus" : {

			}
		},
		{
			"label": "Bouclier incandescent",
			"portee" : {
				"description" : "zone de type 1 autour du mage"
			},
			"description" : "Un écran ovale possédant 60+{{10}} points de vie se forme autour du mage et permet de bloquer les projectiles physiques et magiques. Les attaques à distances provenant de l’intérieur du dôme sont vaines et il est impossible d’être en prise avec un adversaire au corps-à-corps si l’un est à l’intérieur et l’autre à l’extérieur. Le bouclier est ardent et le traverser inflige 1D8 points de dégâts en plus de causer l’inflammation de la personne. Le bouclier se brisera et disparaîtra lorsque ses points de vie atteindront 0, mais si une cible est enflammée, l’effet perdurera.",
			"type" : "buff",
			"cout" : "9 PM",
			"bonus" : {

			}
		}
		]
	},
	"Telluromancie" : {
		"magie" : true,
		"style" : {
			"background" : "telluromancie.jpg",
			"color" : "#0f0d0a",
			"shadow": "#FFFFCC",
		},
		"type": "Magie",
		"Rang 1": [
		{
			"label": "Danse du sable",
			"portee" : {
				"lance" : 15,
				"description" : "dans une zone de type 2"
			},
			"defense" : "opposée – acrobaties ou athlétisme (si la distance à sauter est de 2 cases ou moins) et ténacité chaque tour tant qu’un personnage est dans la zone",
			"description" : "Le mage forme une tempête de sable, réduisant la visibilité dans une zone. Les cibles prises dans la zone entrent dans l’état aveuglé pendant 1D4 + {{1}} tours.",
			"type" : "debuff",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label": "Secousse",
			"portee" : {
				"description" : "cône en face du lanceur"
			},
			"defense" : "opposée – acrobaties ou athlétisme",
			"description" : "Le mage frappe et fait trembler le sol, déstabilisant toute personne se trouvant en face de lui. Toute unité, hormis le mage, risque de se retrouver renversée. Les individus quadrupèdes auront un bonus naturel de 4 pour tenter de ne pas chuter.",
			"type" : "debuff",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label": "Fracture lithique",
			"portee" : {
				"lance" : 5,
				"description" : "la pierre explose dans zone de type 1"
			},
			"description" : "Le telluromancien génère une pierre vibrante d’une énergie tellurique concentrée, depuis les profondeurs de la terre. En refermant son poing, le mage fait exploser l’énergie contenue dans la pierre, la réduisant en une multitude de petits éclats tranchants. Les éclats sont alors projetés dans tous les sens et infligent 1D4 + {{1}} points de dégâts.",
			"type" : "damage",
			"cout" : "2 PM",
			"bonus" : {

			}
		}
		],
		"Rang 2": [
		{
			"label": "Crevasse",
			"portee" : {
				"lance" : 10
			},
			"defense" : "opposée – acrobaties",
			"description" : "Le mage joint ses mains et ouvre le sol en deux, créant une fissure de 2 mètres de large et de 5+{{1}} mètres de profondeur. L’effet sur le sol est définitif et toute personne tombant au fond de la crevasse subira des dégâts appropriés. Le sort à la possibilité d’être canalisé pour 1 point de mana par tour, afin d’augmenter de 2+{{1}} mètres la largeur de la crevasse.",
			"type" : "autre",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		{
			"label": "Poings de pierre",
			"portee" : {
				"lance" : 5,	
				"personnel" : true
			},
			"description" : "Les poings du personnage ciblé se recouvrent de pierre durant 10+{{2}} minutes, gagnant la même dureté que de la roche et augmentant ses dégâts de pugiliste de 2+{{1}}.",
			"type" : "buff",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		{
			"label": "Création rocailleuse mineure",
			"portee" : {
				"lance" : 5
			},
			"description" : "Le telluromancien façonne la roche afin de créer un petit humanoïde élémentaire. Se référer à la fiche des invocations de la telluromancie.",
			"type" : "invocation",
			"cout" : "3 PM",
			"bonus" : {

			}
		}
		],
		"Rang 3": [
		{
			"label": "Sculpture de pierre",
			"portee" : {
				"lance" : 15,
				"description" : "sur trois cases verticales ou horizontales"
			},
			"description" : "Le magicien fait surgir un solide mur de 3 mètres de large pour 2+{{1}} mètres de haut. Le mage à la possibilité de <b>canaliser</b> le sort contre 1 point de mana par tour, ce qui aura pour effet d’allonger la sculpture de 2 mètres à chaque fois. Il est impossible qu’un quelconque projectile le traverse, ou de voir au travers. Le mur possède 35+{{10}} points de vie avec une RD de 2 et restera actif 5 minutes après l’arrêt de la canalisation.",
			"type" : "autre",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label": "Résistance",
			"portee" : {
				"lance" : 5,	
				"personnel" : true
			},
			"description" : "Le mage augmente la résistance de la peau et des organes, agissant comme un plâtre souple et prodiguant un bonus à la constitution de 3 pendant 1D4 + {{1}} tours.",
			"type" : "buff",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label": "Piliers de terre",
			"portee" : {
				"lance" : 10
			},
			"defense" : "opposée – esquive",
			"description" : "Le mage lève les bras, faisant sortir des rochers pointus du sol et empalent la cible en infligeant 1D8 + {{1}} dégâts.",
			"type" : "damage",
			"cout" : "4 PM",
			"bonus" : {

			}
		}
		],
		"Rang 4": [
		{
			"label": "Peau de pierre",
			"portee" : {
				"lance" : 5,	
				"personnel" : true
			},
			"description" : "La peau du lanceur ou celle d’un allié devient dure comme la roche, augmentant son armure de 2 pendant 1D4 + {{1}} tours.",
			"type" : "buff",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Sables mouvants",
			"portee" : {
				"lance" : 10,
				"description" : "sur une zone de type 2"
			},
			"defense" : "opposée – athlétisme",
			"description" : "Le lanceur de sorts fait onduler la terre pendant 1D4 + {{1}} tours, afin d’emprisonner toutes les personnes se trouvant dans la zone. Elles seront ralenties et devront réussir un jet d’athlétisme pour pouvoir avancer à vitesse normale, mais sans pouvoir courir pour autant.",
			"type" : "debuff",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Ancrage",
			"portee" : {
				"description" : "personnelle"
			},
			"description" : "Le lanceur peut s’enraciner dans le sol, ne pouvant plus être renversé ou subir de malus dû à l’équilibre. Cependant, il se retrouve dans l’état enchevêtré jusqu’à l’arrêt de son sort. Le mage choisit quand mettre fin à son sortilège, mais revenir à son état normal prendra un tour complet.",
			"type" : "buff",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Création rocailleuse",
			"portee" : {
				"lance" : 5
			},
			"description" : "Le tulluromancien combine terre et pierres afin de créer un être élémentaire. Se référer à la fiche des invocations de la telluromancie..",
			"type" : "invocation",
			"cout" : "5 PM",
			"bonus" : {

			}
		}
		],
		"Rang 5": [
		{
			"label": "Rocher",
			"portee" : {
				"lance" : 10
			},
			"defense" : "opposée – esquive ou bouclier",
			"description" : "D’un mouvement de la main, un rocher sort de terre et vient frapper une cible et lui administre 2D8 + {{1}} dégâts.",
			"type" : "damage",
			"cout" : "6 PM",
			"bonus" : {

			}
		},
		{
			"label": "Régénération",
			"portee" : {
				"description" : "personnelle"
			},
			"description" : "Le mage se transforme en rocher d’une dureté impressionnante. Durant cinq tours maximum, le personnage sera insensible à toute forme de dégâts, soins extérieurs, BUFFS, ou états préjudiciables. Il ne pourra également pas être déplacé, car fermement ancré au sol. De plus, le mage sera dans l’impossibilité de faire la moindre action, même celle de communiquer. En contrepartie, à chaque tour celui-ci régénérera automatiquement {{2}} points de vie. Le mage peut néanmoins choisir d’interrompre le sort avant la fin des cinq tours.",
			"type" : "heal",
			"cout" : "6 PM",
			"bonus" : {

			}
		}
		],
		"Rang 6": [
		{
			"label": "Création rocailleuse majeure",
			"portee" : {
				"lance" : 5
			},
			"description" : "Le telluromancien amasse de multiples rochers afin de créer un grand élémentaire. Se référer à la fiche des invocations de la telluromancie.",
			"type" : "invocation",
			"cout" : "7 PM",
			"bonus" : {

			}
		},
		{
			"label": "Transmutation",
			"portee" : {
				"lance" : 1
			},
			"description" : "Le mage à la capacité de retirer les effets du sort pétrification, redonnant son apparence normale à une créature pétrifiée, la ramenant aussitôt à la vie. Le sort permet également de transformer une masse de pierre en une substance ayant la texture de la chair. La masse qui en résulte est inerte, à moins qu’elle ne soit animée par de l’énergie vitale ou magique. Le changement de matière est définitif et le sort est à <b>canaliser</b>. Le temps de canalisation dépendra de la taille de la cible à transformer. Chaque incrément de taille sera multiplié par deux, donnant un nombre de tours nécessaires auquel on soustraira {{0.5}}. Le MJ donnera la taille de l’objet au joueur avant toute tentative du sort, sachant qu’il faudra toujours au minimum 1 tour.<br>"+
			"minuscule = 1<br>"+
			"très petite = 2<br>"+
			"petite = 3<br>"+
			"moyenne = 4<br>"+
			"grande = 5<br>"+
			"très grande = 6<br>"+
			"gigantesque = 7<br>"+
			"colossale = 8",
			"type" : "heal",
			"cout" : "7 PM",
			"bonus" : {

			}
		}
		],
		"Rang 7": [
		{
			"label": "Corps de diamant",
			"portee" : {
				"lance" : 5,	
				"personnel" : true
			},
			"description" : "En plus de se durcir, la peau de la cible se recouvre de superbes plaques formées de diamants bruts qui lui accordent un bonus à la RM de 3D6 + {{1}} pendant 1D4 + {{1}} tours, cumulable avec peau de pierre.",
			"type" : "buff",
			"cout" : "8 PM",
			"bonus" : {

			}
		},
		{
			"label": "Pétrification",
			"portee" : {
				"lance" : 10
			},
			"defense" : "esquive pour éviter la boue, si le jet est raté, un jet de ténacité toujours opposé pour ne pas finir pétrifié",
			"description" : "Par <b>canalisation<b>, de la boue recouvre une cible et durcie rapidement, l’immobilisant entièrement. La cible n’est pas morte, mais le changement de matière est définitif à moins qu’un telluromancien ne l’en délivre. Le temps de canalisation dépendra de la taille de la cible à transformer. Chaque incrément de taille sera multiplié par deux, donnant un nombre de tours nécessaires auquel on soustraira le niveau du mage divisé par 2. Le MJ donnera la taille de l’objet au joueur avant toute tentative du sort, sachant qu’il faudra toujours au minimum 1 tour. Le mage peut décider de stopper la canalisation à tout moment, enchevêtrant ou immobilisant simplement la cible, de manière plus ou moins importante.<br>"+
			"minuscule = 1<br>"+
			"très petite = 2<br>"+
			"petite = 3<br>"+
			"moyenne = 4<br>"+
			"grande = 5<br>"+
			"très grande = 6<br>"+
			"gigantesque = 7<br>"+
			"colossale = 8",
			"type" : "debuff",
			"cout" : "8 PM",
			"bonus" : {

			}
		}
		],
		"Rang 8": [
		{
			"label": "Écrasement tellurique",
			"portee" : {
				"lance" : 15
			},
			"defense" : "opposée – esquive",
			"description" : "Des rochers s’élèvent, des piques se forment, la terre se réveille et frappe un ennemi, l’écrasant entre deux gigantesques masses. La cible qui n’arrive pas à esquiver recevra 8D6 + {{1}} dégâts.",
			"type" : "damage",
			"cout" : "9 PM",
			"bonus" : {

			}
		},
		{
			"label": "Golem",
			"portee" : {
				"lance" : 5
			},
			"description" : "Le telluromancien réunit toutes sortes de roches, afin de former un être humanoïde gigantesque et docile. Se référer à la fiche des invocations de la telluromancie.",
			"type" : "invocation",
			"cout" : "9 PM",
			"bonus" : {

			}
		}
		]
	},
	"Iconomancie" : {
		"magie" : true,
		"style" : {
			"background" : "iconomancie.png",
			"color" : "#000000",
			"shadow": "white",
		},
		"description" : "Cette magie nécessite du matériel de dessin, de peinture ou d’écriture et s’utilise avec la compétence représentation.",
		"type": "Magie",
		"Rang 1": [
		{
			"label": "Empreintes",
			"portee" : {
				"lance" : 10,
				"description" : "si le sort est lancé à 5 mètres, les empreintes ne pourront s’étendre que sur 5 mètres de plus"
			},
			"defense" : "ténacité opposée si la cible n’est pas consentante",
			"description" : "Le jeteur de sorts fait émerger toutes sortes de marques de griffes, de coups ou autre sur un corps. Il peut tout aussi bien utiliser ce sort sur un support non vivant et y faire apparaître des trous, bosses, empreintes de pas ou d’animaux, etc.",
			"type" : "autre",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label": "Cliché",
			"portee" : {
				"lance" : 10
			},
			"description" : "Le mage analyse l’entité ou la zone ciblée et retranscrit son image mentale sur un support, telle une photographie. Le processus ne prend qu’une trentaine de secondes.",
			"type" : "autre",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label": "Multi-supports",
			"portee" : {
				"description" : "visuelle (entre les deux objets)"
			},
			"description" : "L’iconomancien utilise une image (gravure, dessin, tatouage, etc) apposée sur une surface, puis, la fera se déplacer sur n’importe quel autre support que celui d’origine.",
			"type" : "autre",
			"cout" : "2 PM",
			"bonus" : {

			}
		}
		],
		"Rang 2": [
		{
			"label": "Encre invisible",
			"portee" : {
				"lance" : 1
			},
			"description" : "Le mage peut rendre visible une encre qu’il serait initialement invisible et inversement. Une encre que le mage a rendue invisible ne peut cependant être révélée avec des méthodes classiques.",
			"type" : "autre",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		{
			"label": "Coloriage",
			"portee" : {
				"lance" : 15
			},
			"defense" : "ténacité opposée si la cible n’est pas consentante",
			"description" : "Le personnage cible un individu, un animal ou même un objet et peut modifier l’entièreté ou partiellement les couleurs de celui-ci. Le mage peut également décider de retirer toutes couleurs de la cible en question, laissant la cible dans des dégradés de gris. L’effet est considéré comme une malédiction et ne pourra pas guérir de façon naturelle (le mage peut annuler lui-même son œuvre ou celle d’un autre iconomancien).",
			"type" : "autre",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		{
			"label": "Art mûre",
			"portee" : {
				"lance" : 10,	
				"personnel" : true
			},
			"description" : "La cible est entourée d’encre virevoltante, parant une partie des dégâts subis (si les dégâts excèdent la résistance, la cible subira le surplus de ceux-ci) pendant {{1}} tours. Le sort bloquera {{2}} dégâts, pour chaque attaque reçue.",
			"type" : "buff",
			"cout" : "3 PM",
			"bonus" : {

			}
		}
		],
		"Rang 3": [
		{
			"label": "Esquisse",
			"portee" : {
				"lance" : 5
			},
			"description" : "Le mage peut faire apparaître un objet (Et pas un ensemble d’objets comme les kits) après l’avoir dessiné sur le support de son choix. Les objets disparaissent après {{1}} heure(s) et sont considérés comme des objets normaux, en trois dimensions. De plus, les objets apparaîtrons toujours de la façon la plus commune possible (Une flûte sera toujours en bois, une épée en fer, une robe en coton, etc.) tout en gardant un aspect crayonné très caractéristique, montrant une facture purement magique et les rendant invendables et inconsommables. Il est d’ailleurs impossible d’insuffler des bonus aux objets comme les améliorations.",
			"type" : "invocation",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label": "Marques cabalistiques",
			"portee" : {
				"lance" : 20,
				"description" : "(ne peut se viser soi-même)"
			},
			"defense" : "ténacité opposée, pour une cible non consentante",
			"description" : "Le mage peut faire apparaître deux sortes de symboles magiques sur le corps de sa cible: Mort: Une icône de tête de mort apparaît sur le front de la cible et inflige 1D8 + {{1}} dégâts, Vie: Une icône de feuille apparaît sur le front de la cible et soigne 1D8 + {{1}} points de vie.",
			"type" : "autre",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label": "Croquis",
			"portee" : {
				"lance" : 10
			},
			"description" : "Le mage choisit 2 types d’animaux qu’il connaît par cœur (d’un Fp compris entre ½ et 3, avec accord du MJ) qu’il pourra dessiner sur le support de son choix et invoquer en combat. La limite d’une invocation sur le terrain reste inchangée et il sera également impossible de changer d’animaux par la suite, choisissez bien. Les invocations apparaissent en trois dimensions et perdurent pendant {{1}} heure(s). Cela dit, elles sont initialement incapables de faire autre chose que se déplacer, sauf à l’aide du sort mise en abyme.",
			"type" : "invocation",
			"cout" : "4 PM",
			"bonus" : {

			}
		}
		],
		"Rang 4": [
		{
			"label": "Nature morte",
			"portee" : {
				"lance" : 10
			},
			"defense" : "opposée – volonté, et si raté, volonté à chaque tour avec un malus de 4 pour reprendre le contrôle",
			"description" : "Le mage se sert d’un support (cela peut aller du sol, à un rocher, une feuille ou un mur) pour y emprisonner une cible ou un objet. Celle-ci voit son corps s’aplatir et onduler telle une feuille, pour finalement ne faire qu’un avec son support. La cible sera enfermée et ne pourra effectuer aucune action, jusqu’à ce que le mage décide de la libérer en arrêtant sa <b>canalisation<b>, ou qu’elle se libère. Le support doit être assez large pour contenir la cible, car son image ne changera pas de taille. De plus, la cible retrouvera son apparence normale en un tour, si le support est détruit ou que la canalisation prend fin. Abîmer le support n’infligera aucun dégâts à la cible et tenter de l’effacer n’aura aucun effet. À noter que la cible a le droit à un jet par tour de volonté (malussé de 4 et toujours en opposition) pour se libérer de cet état.",
			"type" : "debuff",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Autoportrait",
			"portee" : {
				"description" : "personnelle"
			},
			"description" : "Le mage se dessine, projetant alors son œuvre dans le monde en trois dimensions, créant une réplique de lui-même. La réplique peut parler et agir comme le ferait l’original, mais ne peut pas attaquer ou lancer de sorts. Le double possède les mêmes caractéristiques que l’original, mais disparaît dès lors où le mage n’a plus la vision sur son double.",
			"type" : "invocation",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Mise en abyme",
			"portee" : {
				"description" : "personnelle"
			},
			"description" : "Le mage se change en masse d’encre et prend possession de ce qui était encore il y a peu une simple image. Il arbore alors l’apparence de celle-ci et peut se déplacer sur le support initial ou l’en faire sortir. Ce sort peut se combiner avec croquis, réplique, plus vrai que nature et caricature, afin de prendre le contrôle de ses propres œuvres. Il sera alors possible pour le mage de faire attaquer sa créature, tout en gagnant ses caractéristiques et capacités (uniquement pour les œuvres du mage). L’iconomancien peut à tout moment retrouver son apparence normale, faisant retourner la créature qu’il incarnait à son état et sa place d’origine. En revanche, si l’image qu’il incarne est détruite, le mage reviendra à sa position initiale, avec ses points de vie et mana qu’il avait au moment de la transformation. Pendant qu’il change de forme, sauf dans le cas d’une créature dotée de mains, le mage n’aura plus accès à ses sorts et objets.",
			"type" : "autre",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Tache",
			"portee" : {
				"lance" : 20
			},
			"defense" : "opposée – esquive ou bouclier",
			"description" : "Le mange donne un grand coup de son outil, d’où en sort une quantité incroyable d’encre ou de peinture. La masse liquide va alors s’abattre sur une cible, lui infligeant 2D6 + {{1}} dégâts, suite à la violence du choc.",
			"type" : "damage",
			"cout" : "5 PM",
			"bonus" : {

			}
		}
		],
		"Rang 5": [
		{
			"label": "Réplique",
			"portee" : {
				"lance" : 15
			},
			"description" : "Le mage choisit 2 autres types d’animaux qu’il connaît par cœur (d’un Fp compris entre ½ et 4, avec accord du MJ) et les ajoute à sa liste commencée avec le sort croquis.",
			"type" : "invocation",
			"cout" : "6 PM",
			"bonus" : {

			}
		},
		{
			"label": "Encre",
			"portee" : {
				"lance" : 30,
				"description" : "mais nécessite une ligne de vue"
			},
			"description" : "L’iconomancien devient liquide comme de l’encre, son corps se laissant absorber dans une mélasse noire. Celui-ci ressortira comme il est venu, mais plus loin. Ce sort ne permet pas de traverser un mur ou la matière, car nécessite de voir le point d’arrivée depuis le lieu où le sort est lancé. Effectuer le déplacement du point A au point B consomme le tour.",
			"type" : "autre",
			"cout" : "6 PM",
			"bonus" : {

			}
		}
		],
		"Rang 6": [
		{
			"label": "Plus vrai que nature",
			"portee" : {
				"lance" : 20
			},
			"description" : "Le mage choisit 2 autres types d’animaux qu’il connaît par cœur (d’un Fp compris entre ½ et 6, avec accord du MJ) et les ajoute à sa liste commencée avec les sorts croquis et réplique.",
			"type" : "invocation",
			"cout" : "7 PM",
			"bonus" : {

			}
		},
		{
			"label": "Art mada",
			"portee" : {
				"lance" : 25
			},
			"defense" : "opposée – esquive ou bouclier (malus de 2 pour la deuxième, 4 pour la troisième et 6 pour la dernière)",
			"description" : "Quatre grandes plumes d’écriture se forme au-dessus de la tête du mage et plongent sur une ou plusieurs cibles (Une cible attaquée par plusieurs plûmes aura plus de mal à se défendre). Chaque plume inflige 1D5 + {{1}} dégâts, cependant seule la première plume pourra bénéficier des bonus aux dégâts (Surcharge, magie destructive etc).",
			"type" : "damage",
			"cout" : "7 PM",
			"bonus" : {

			}
		}
		],
		"Rang 7": [
		{
			"label": "Entrée des artistes",
			"portee" : {
				"lance" : 1
			},
			"description" : "Le mage dessine une porte magique durant 1D6 tours sur un support suffisamment grand et stable. Si le joueur a défini un point de destination avec le MJ avant le scénario, la porte mènera à cet endroit. Sinon, la porte créée fixe un point de départ, et une autre porte devra être dessinée pour relier cet endroit au suivant. Chaque nouvelle porte dessinée se connecte directement à la dernière créée, annulant les liens avec les précédentes. Si un support est détruit ou effacé, le passage devient inutilisable.",
			"type" : "autres",
			"cout" : "8 PM",
			"bonus" : {

			}
		},
		{
			"label": "Gomme",
			"portee" : {
				"lance" : 20
			},
			"description" : "Le mage peut dessiner un humanoïde en particulier, ou inventer un personnage et le faire apparaître en 3 dimensions. Pour dessiner une personne précise, il faudra avoir eu l’occasion de l’observer au moins une heure et d’assez près (5 mètres) pour pouvoir retranscrire les traits de son visage avec précision. Lorsque le dessin prend vie, celui-ci perdurera pendant le niveau du mage en heures, ne sera capable que de se déplacer et à condition que le mage le garde dans son champs de vision. Couplé avec le sort mise en abyme, l’icônomancien pourra entrer dans la peau de son dessin et agir librement à sa place. Cela modifiera son apparence, ses caractéristiques et attributs, mais pas ses compétences, ses sorts, sa voix, ni même ses connaissances. Le mage fera alors regagner 4D6 + {{1}} points de vie. La gomme ne peut réparer un membre sectionné, manquant ou brisé, tout comme le sort ne cible que les blessures visibles.",
			"type" : "heal",
			"cout" : "8 PM",
			"bonus" : {

			}
		}
		],
		"Rang 8": [
		{
			"label": "Caricature",
			"portee" : {
				"lance" : 10
			},
			"description" : "Le mage peut dessiner un humanoïde en particulier, ou inventer un personnage et le faire apparaître en 3 dimensions. Pour dessiner une personne précise, il faudra avoir eu l’occasion de l’observer au moins une heure et d’assez près (5 mètres) pour pouvoir retranscrire les traits de son visage avec précision. Lorsque le dessin prend vie, celui-ci perdurera pendant {{1}} heure(s), ne sera capable que de se déplacer et à condition que le mage le garde dans son champ de vision. Couplé avec le sort mise en abyme, l’iconomancien pourra entrer dans la peau de son dessin et agir librement à sa place. Cela modifiera son apparence, ses caractéristiques et attributs, mais pas ses compétences, ses sorts, sa voix, ni même ses connaissances.",
			"type" : "invocation",
			"cout" : "9 PM",
			"bonus" : {

			}
		},
		{
			"label": "Roman",
			"portee" : {
				"lance" : 30
			},
			"defense" : "opposée – volonté, et si raté, volonté à chaque tour avec un malus de 4 pour reprendre le contrôle",
			"description" : "Le mage prend le contrôle d’une personne, décrivant ses actions et ses paroles. Pour cela, il lui suffit de connaître le nom et le visage de la personne qu’il souhaite contrôler, ainsi que de l’avoir en vue au moment de l’enchantement. Chaque tour, la cible aura le droit à un nouveau jet pour tenter de reprendre ses esprits. À noter que de donner un ordre suicidaire, autodestructeur (Sauf si cela est dans ses habitudes), infaisable ou contre nature, annulera automatiquement l’effet. En combat, le mage peut écrire une action par tour (Ce qui consommera le sien) et la cible se cantonnera alors à cet ordre, jusqu’à ce qu’il soit réalisé. Si elle y parvient et qu’elle ne reçoit aucune directive pour la suite, elle passera simplement son tour. Dans une situation hors combat, le mage peut rédiger autant qu’il le souhaite et la cible réalisera les ordres un par un. Tant que le premier ordre n’est pas réalisé ou annulé (rayé sur le support) par le mage, la cible ne passera pas au second. De plus, la cible devra elle aussi réussir des jets pour réussir ses actions.",
			"type" : "debuff",
			"cout" : "9 PM",
			"bonus" : {

			}
		}
		]
	},
	"Négatomancie" : {
		"magie" : true,
		"style" : {
			"background" : "negatomancie.jpg",
			"color" : "#7E0021",
			"shadow": "white",
		},
		"type": "Magie",
		"Rang 1": [
		{
			"label": "Naturellement négatif",
			"portee" : {
				"description" : "personnelle"
			},
			"description" : "Comme si de rien n’était, le mage annule 1+{{1}} de malus liés à l’environnement pendant {{2}} tours. Cela peut être les malus liés au combat dans l’eau, à un terrain encombré ou glissant.",
			"type" : "autre",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label": "Chut",
			"portee" : {
				"lance" : 10
			},
			"defense" : "opposée – volonté",
			"description" : "Le négatomancien vient perturber les dires de la cible, la faisait dicter ses mots à l’envers et empêchant toute cohérence dans ses propos. De ce fait, il sera aisé d’empêcher une cible de s’exprimer, de la faire paraître folle ou de la gêner dans l’incantation de sorts (malus de 4). L’effet perdure pendant {{1}} minute(s)",
			"type" : "debuff",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label": "Cassation",
			"portee" : {
				"lance" : 15
			},
			"defense" : "opposée – ténacité",
			"description" : "Le mage cible un ennemi et par sa simple volonté, oblige sa cible à relancer une fois un jet d’initiative ou un jet de dégâts (ne fonctionne qu’en réaction de l’un de ces jets. Ce qui signifie que le mage peut lancer ce sort même si ce n’est pas son tour).",
			"type" : "reaction",
			"cout" : "2 PM",
			"bonus" : {

			}
		}
		],
		"Rang 2": [
		{
			"label": "Abolition",
			"portee" : {
				"lance" : 10
			},
			"defense" : "opposée – volonté",
			"description" : "Le personnage annule, pendant 2+{{1}} tours, l’entièreté des bonus de soutien magiques actifs (buff et hot) sur la cible. Les effets bonus ne sont pas mis en « pause » et peuvent ne plus être actifs à la fin de ce sort. Celui-ci est considéré comme une malédiction qui peut être levée à l’aide d’un sort adéquat.",
			"type" : "autre",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		{
			"label": "Intolérable",
			"portee" : {
				"description" : "soi-même, mais le renvoi est sur 10 mètres"
			},
			"defense" : "opposée – esquive",
			"description" : "Le négatomancien vient de subir des dégâts et impossible pour lui de ne pas rendre la monnaie de sa pièce à celui qui vient de lui infliger ça. Ce sort n’annule pas les blessures, mais permet de renvoyer la même quantité de dégâts reçus à la cible qui les a infligés. Le mage renvoi la source de dégâts initiale sans prendre en compte la réduction effectuée par sa propre RD ou RM. À noter que le sort ne se lance pas en réaction directe, mais en début de tour, pour une blessure subie durant le dernier tour.",
			"type" : "damage",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		{
			"label": "Retour",
			"portee" : {
				"lance" : 15
			},
			"description" : "Le mage appose une croix sous les pieds d’un allié ou d’un ennemi, qui aura pour effet de ramener la cible à son point de départ une fois que celle-ci aura fait son action (la marque disparaît ensuite). Le mage peut décider de <b>canaliser</b> le sort pour que l’effet de retour ne s’active que bien plus tard.",
			"type" : "autre",
			"cout" : "4 PM",
			"bonus" : {

			}
		}
		],
		"Rang 3": [
		{
			"label": "Effacement",
			"portee" : {
				"lance" : 5
			},
			"description" : "Le mage restaure 2D4 + {{1}} points de mana perdus d’un allié qui en a utilisé lors de son dernier tour. Le regain ne peut en aucun cas excéder le coût du sort ou de la capacité, puisqu’il s’agit d’une annulation de coût.",
			"type" : "autre",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label": "Incapables",
			"portee" : {
				"lance" : 10
			},
			"description" : "Le mage a bien compris que l’on ne pouvait compter que sur soi-même. Celui-ci peut octroyer une relance (D30, initiative, dé mannequin ou régénération) à un allié, sur un jet que celui-ci aurait raté ou jugé insuffisant (Hors échec critique).",
			"type" : "reaction",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label": "Annulation",
			"portee" : {
				"lance" : 10,	
				"personnel" : true
			},
			"description" : "Le mage dissipe 2D4 + {{1}} points de dégâts d’une attaque qui a été infligée depuis son dernier tour. Annulation ne peut s’appliquer pour des dégâts reçus sur un bouclier et le regain ne peut en aucun cas excéder les derniers dégâts subis, puisqu’il s’agit d’une annulation.",
			"type" : "autre",
			"cout" : "4 PM",
			"bonus" : {

			}
		}
		],
		"Rang 4": [
		{
			"label": "Renvoi",
			"portee" : {
				"lance" : 10
			},
			"defense" : "opposée – incantation (jet qu’à lancé la cible pour l’invocation)",
			"description" : "Le mage concentre son énergie sur la créature fraîchement invoquée et la renvoie d’où elle vient. De plus, si le mage souhaite empêcher toute nouvelle invocation de ce type pendant {{1}} tour(s), il devra en plus, dépenser le double du mana qu’a consommé l’invocateur.",
			"type" : "autre",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Abrogation",
			"portee" : {
				"lance" : 10,	
				"personnel" : true
			},
			"defense" : "opposée – incantation (jet qu’à lancé la cible pour la malédiction)",
			"description" : "Le négatomancien annule une malédiction ou un état préjudiciable magique qui aurait touché une cible durant le dernier tour.",
			"type" : "heal",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Suppression",
			"portee" : {
				"lance" : 5
			},
			"defense" : "opposée – esquive",
			"description" : "Le négatomancien neutralise entièrement la pénétration d’armure d’une cible et ce, pendant 2+{{1}} tours.",
			"type" : "debuff",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Recommencement",
			"portee" : {
				"description" : "zone de type 2 autour du mage"
			},
			"defense" : "opposée – volonté",
			"description" : "Tous les ennemis situés autour du mage devront relancer une fois leurs jets réussis (hors coups critiques) pendant 1+{{1}} tours.",
			"type" : "debuff",
			"cout" : "5 PM",
			"bonus" : {

			}
		}
		],
		"Rang 5": [
		{
			"label": "Rogner",
			"portee" : {
				"lance" : 10
			},
			"description" : "Le mage retire son niveau divisé par 3 (Arrondit à l’entier inférieur, pour un minimum de 1) en classe de dé sur les attaques d’une cibles, pendant {{1}} tour(s).<br>"+
			"Pour plus de clarté, cela donne :<br>"+ 
			"1 classe de dés du niveau 1 à 5<br>"+
			"2 classes pour les niveaux 6 à 8<br>"+
			"et 3 classes pour les niveaux 9 et 10.",
			"type" : "debuff",
			"cout" : "6 PM",
			"bonus" : {

			}
		},
		{
			"label": "Amputation",
			"portee" : {
				"lance" : 15
			},
			"description" : "Le mage dérobe une capacité ou un sort de nature offensive à un ennemi ou un allié pendant {{1}} tour(s). Le personnage interdit également l’utilisation de la capacité à sa cible pendant ce laps de temps. Afin de dérober une capacité, il faut que le mage ait déjà eut l’occasion de la voir à l’œuvre au moins une fois et pourra ensuite la lancer sur la compétence incantation, tout en gardant les dégâts initiaux de celle-ci. Les attaques naturelles et physiques simples ne peuvent pas être dérobées.",
			"type" : "autre",
			"cout" : "6 PM",
			"bonus" : {

			}
		}
		],
		"Rang 6": [
		{
			"label": "Sanction",
			"portee" : {
				"lance" : 1
			},
			"defense" : "opposée – esquive",
			"description" : "Le négatomancien brise les défenses ennemies et annule {{1}} de la RD ou la RM de sa cible, durant 1D12 + {{1}} tours. L’effet n’est pas cumulable sur une même cible",
			"type" : "buff",
			"cout" : "7 PM",
			"bonus" : {

			}
		},
		{
			"label": "Non",
			"portee" : {
				"description" : "zone de type 2 autour du mage"
			},
			"defense" : "opposée – volonté",
			"description" : "Pendant 2D4 + {{1}} tours, le mage décide si les jets de compétences de ses adversaires doivent être relancés avec un malus de {{1}}. Ne se combine pas avec «recommencement» qui force systématiquement les ennemis à relancer.",
			"type" : "debuff",
			"cout" : "7 PM",
			"bonus" : {

			}
		}
		],
		"Rang 7": [
		{
			"label": "De justesse",
			"portee" : {
				"lance" : 10
			},
			"description" : "Le mage rattrape de peu une grosse erreur de la part d’un de ses collègues. Le négatomancien est désormais capable d’octroyer une relance sur un échec critique (mais pas l’un des siens). Ce sort inflige un fort contre-coup au mage et le fait directement entrer dans l’état épuisé (Impossible de courir et la vitesse de déplacement est réduite de moitié). S’il relance le sort en étant déjà épuisé, il tombera inconscient pendant 2D4 tours, et ceci, peu importe que le sort ait fonctionné ou non. À noter que le sort s’utilise en réponse à un jet, même si ce n’est pas le tour du mage.",
			"type" : "debuff",
			"cout" : "8 PM",
			"bonus" : {

			}
		},
		{
			"label": "Déchronologie",
			"portee" : {
				"lance" : 1
			},
			"description" : "Une croix apparaît sur le front de la cible. Si ses points de vie tombent à 0, la marque se détruit et la cible regagne immédiatement 10 % de ses points de vie totaux et gagne l’état fatigué (malus de 2 à tous ses jets de compétence ainsi qu’à ses jets de dégâts). Une fois la marque apposée, celle-ci perdure pendant 2D8 + {{1}} tours. Il est impossible de superposer les marques ou d’en avoir plusieurs actives en même temps. Lorsque l’effet est consommé ou disparaît faute d’utilisation, il sera impossible de la réutiliser sur la même cible avant une heure sous peine d’épuiser son corps et la rapprocher de la mort plutôt que de l’en éloigner.",
			"type" : "heal",
			"cout" : "8 PM",
			"bonus" : {

			}
		}
		],
		"Rang 8": [
		{
			"label": "Éponge",
			"portee" : {
				"description" : "soi-même, mais le renvoi est sur 15 mètres autour du lanceur"
			},
			"defense" : "esquive opposée à un jet d’incantation au début de chaque tour",
			"description" : "Le mage va rediriger les dégâts reçus par ses alliés sur lui-même durant tout le prochain tour. Le sort est à <b>canaliser</b> et permet au début de chaque tour du mage, de renvoyer la même quantité de dégâts qu’ont subis ses alliés (mais pas lui directement) sur un ou plusieurs ennemis. Si le mage ne souhaite pas continuer à <b>canaliser</b>, il peut jouer normalement après la répartition des dégâts. Les dégâts qu’absorbe et subis le mage sont comptés après réduction de la RD, la RM et d’éventuels boucliers. «Annulation» peut se combiner avec les dégâts reçus après l’arrêt d’une canalisation du sort.",
			"type" : "damage",
			"cout" : "9 PM",
			"bonus" : {

			}
		},
		{
			"label": "Faussaire",
			"portee" : {
				"lance" : 10
			},
			"defense" : "opposée – volonté",
			"description" : "Le mage peut, lorsque ce n’est pas son tour, décider qu’un ennemi ratera systématiquement son action (Hors succès critique de la part de l’ennemi). Le joueur doit obligatoirement se manifester avant le lancer de dé du MJ et le sort sera prit en compte même si le jet est finalement raté, le jet ne servant qu’à vérifier la présence d’un critique. La négatomancie est une magie puissante, mais dont le contre-coup est proportionnel. À chaque fois que le mage utilise ce sort, il subit un état de fatigue:<br>"+
			"1: Vitesse de déplacement réduite de 2 cases<br>"+
			"2: Malus de 2 aux compétences de force et de dextérité<br>"+
			"3: Malus de 4 aux compétences de force et de dextérité, le personne ne peut plus courir et sa vitesse de déplacement est réduite de moitié (Arrondit à l’entier inférieur).<br>"+
			"4: Malus de 6 aux compétences de force et de dextérité. Au-delà, le personnage tombe dans les pommes.",
			"type" : "autre",
			"cout" : "9 PM",
			"bonus" : {

			}
		}
		]
	},
	"Magie chamanique" : {
		"magie" : true,
		"style" : {
			"background" : "chamane.png",
			"color" : "#000000",
			"shadow": "white",
		},
		"type": "Magie",
		"Rang 1": [
		{
			"label": "Totem grigri",
			"portee" : {
				"lance" : 10
			},
			"description" : "Le chaman créer un petit totem insensible aux dégâts, qui perdure pendant 1D2 + {{1}} tours. Le totem n’est pas vivant et ne possède aucune caractéristique. De ce fait, il n’est pas considéré comme une invocation et le mage peut en activer autant qu’il le souhaite. Chaque totem, peut directement être utilisé comme catalyseur d’un sort chamanique. Cette manipulation aura pour effet de lancer le sort dans une zone de type 1 autour du totem, pendant toute la durée où celui-ci est actif sur le terrain. Si un nouveau sort est lancé sur le totem, le premier effet n’est plus actif.",
			"type" : "invocation",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label": "Exorcisme",
			"portee" : {
				"lance" : 1
			},
			"defense" : "opposée – volonté de l’occupant",
			"description" : "Le mage appose ses mains sur la cible et connecte son propre esprit à celui du corps visé. De ce fait, il est capable de percevoir une entité qui serait de trop dans ce corps et de la repousser hors de celui-ci, l’empêchant par la même occasion, de revenir posséder la cible.",
			"type" : "heal",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label": "Lueur",
			"portee" : {
				"lance" : 10,
				"personnel" : true,
				"description" : "peut se lancer sur les totems"
			},
			"description" : "Le chaman commande à l’élément de lumière de venir embrasser un allié et lui rendre 1D4 + {{1}} points de vie. Si le sort est lancé sur un totem, l’effet se produit chaque tour jusqu’à disparition du totem, mais soigne du 1D2 + {{1}}, en plus d’éclairer la zone.",
			"type" : "heal",
			"cout" : "2 PM",
			"bonus" : {

			}
		}
		],
		"Rang 2": [
		{
			"label": "Vague",
			"portee" : {
				"lance" : 5,
				"description" : "peut se lancer sur les totems"
			},
			"defense" : "opposée – athlétisme ou acrobatie pour esquiver",
			"description" : "Le mage appelle l’eau à se former au bout de ses doigts et projette une vague qui inflige 1D6 dégâts + {{1}}. Si le sort est lancé sur un totem, l’effet se produit chaque tour jusqu’à disparition du totem, mais inflige du 1D4 + {{1}} et repousse les ennemis de {{1}} cases. Les cibles touchées seront toujours repoussées à l’opposée du totem.",
			"type" : "damage",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		{
			"label": "Osmose élémentaire",
			"portee" : {
				"lance" : 10,
				"personnel" : true,
				"description" : "se lance uniquement sur les totems"
			},
			"description" : "Le chaman concentre ses forces et enchante son totem afin que celui-ci puisse s’infuser de plusieurs sorts élémentaires. Le mage pourra ajouter son niveau divisé par 3 (arrondi à l’entier inférieur) nouveaux sorts sur son totem. Pour plus de clarté, cela donne 1 sort du niveau 1 à 5, 2 sorts pour les niveaux 6 à 8 et 3 sorts pour les niveaux 9 et 10.",
			"type" : "autre",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		{
			"label": "Rafales",
			"portee" : {
				"lance" : 15,
				"description" : "peut se lancer sur les totems"
			},
			"defense" : "opposée – acrobaties, puis éventuellement ténacité non opposée",
			"description" : "Le mage invoque l’élément de l’air et forme des bourrasques autour de la cible, qui a bien du mal à se concentrer sur ce qu’elle fait, gênée autant visuellement que dans son équilibre. Les compétences basées sur la dextérité et la compétence de perception, sont diminuées de {{1}} durant 1D2 + {{1}} tours. Si le sort est lancé sur un totem, le malus passe à {{0.5} mais les cibles perdent désormais 1 point de souffle par tour si elles ratent un jet de ténacité (chaque tour). L’effet perdure jusqu’à disparition du totem (Ne plus prendre en compte la durée indiquée plus haut, mais celle du totem).",
			"type" : "debuff",
			"cout" : "4 PM",
			"bonus" : {

			}
		}
		],
		"Rang 3": [
		{
			"label": "Fraîcheur",
			"portee" : {
				"description" : "cône devant le lanceur. Peut se lancer sur les totems"
			},
			"defense" : "opposée – ténacité",
			"description" : "La chamane lie les éléments de l’eau et du vent, afin de former de la glace. Toutes les unités touchées par ce voile de froid, voient leur vitesse de déplacement réduite de moitié (arrondi à l’entier inférieur). Si le sort est lancé sur un totem, la vitesse de déplacement est réduite du tiers, mais les cibles qui ratent leur jet de ténacité subissent également 1D4 + {{1}} dégâts de froid. L’effet se produit chaque tour jusqu’à disparition du totem.",
			"type" : "debuff",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label": "Magnétisation",
			"portee" : {
				"lance" : 1,	
				"personnel" : true,	
				"description" : "peut se lancer sur les totems"
			},
			"defense" : "opposée – athlétisme",
			"description" : "Le mage conjure l’élément de la terre, afin de magnétiser une cible sur n’importe quel type de pierre pendant 1D4 + {{1}} tours. De ce fait, la cible ne peut plus chuter ou s’éloigner du sol. Ainsi, il est également possible de se déplacer sur une surface verticale sans problème. Si le sort est lancé sur un totem, l’objet va attirer vers lui toute entité présente dans la zone, à défaut de {{1}} mètres. Le totem étant considéré comme l’élément magnétiseur, il faudra réussir un jet d’athlétisme afin de résister à l’attirance et un autre afin de briser le magnétisme et sortir de la zone d’effet. L’effet se produit chaque tour jusqu’à disparition du totem.",
			"type" : "autre",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label": "Haie",
			"portee" : {
				"description" : "ligne horizontale de 6 cases sur une portée de 5 mètres. Peut se lancer sur les totems"
			},
			"defense" : "opposée – esquive et éventuellement acrobatie opposée",
			"description" : "Le chaman unit les éléments de l’eau et de la terre afin de former celui du bois. Six arbres dont les troncs, racines et branches s’entremêlent et poussent à la vitesse de l’éclair, formant un mur infranchissable (hors escalade ou contournement) et pérenne. Si un arbre s’apprête à pousser sur une case déjà occupée, la vitesse de pousse fera que l’objet sera probablement détruit et toute entité sera projetée (1D6 mètres de haut + 1D6 mètres de large) dans une direction aléatoire. Si le sort est lancé sur un totem, des racines sortiront du sol et viendront saisir les entités présentent dans la zone d’effet. L’effet s’estompera naturellement à la disparition du totem ou si la cible arrive à se dégager des racines.",
			"type" : "autre",
			"cout" : "4 PM",
			"bonus" : {

			}
		}
		],
		"Rang 4": [
		{
			"label": "Imprégnation",
			"portee" : {
				"personnel" : true
			},
			"description" : "Le jeteur de sorts assimile l’élément de ce qu’il touche et son corps se modifie en fonction (Une flamme, un rocher, une tornade, une flaque, etc) durant {{1}} heure(s). Le chaman possède désormais une résistance totale à son élément (ainsi qu’aux effets éventuels environnants, comme la chaleur sous forme de feu). La mage réussit automatiquement (hors critique pour le repérer) ses jets de discrétion s’il tente de se fondre dans un paysage adapté et qu’il ne se déplace pas. De plus, s’imprégner d’un élément ajoute un bonus d’un dé de dégâts ou soins quand celui-ci est utilisé (directement ou via les totems) ou d’une classe de dé lors d’une combinaison avec cet élément. Par exemple, en forme de lumière, là où le sort soigne normalement du 1D4, il passerait à 2D4. En forme d’eau, le sort fraîcheur (eau + air) infligerait 1D6 au lieu de 1D4.",
			"type" : "buff",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Monde des esprits",
			"portee" : {
				"lance" : 1,	
				"personnel" : true
			},
			"defense" : "opposée – volonté si la cible n’est pas consentante",
			"description" : "Le mage débute un rituel de dix minutes, durant lesquelles il devra se tenir assis et au calme. À la fin de celui-ci, le mage sera mentalement capable de dissocier son esprit de son corps, ou de faire de même avec une cible qui a participé au rituel. Voyager dans le monde des esprits permet de passer dans l’état intangible, ainsi que de gagner la vision éthérique (permet de voir les êtres habituellement invisibles). L’esprit est désormais capable de voyager à une vitesse de {{10}} km/h. Durant ce temps, l’enveloppe charnelle reste vulnérable et «vide», le corps garde ses fonctions vitales primaires, mais c’est bien tout. Après avoir retrouvé son corps, toute personne passée dans l’autre monde sera dans l’état fatigué 2, le mage, lui sera en fatigue 3.",
			"type" : "autre",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Flambeau",
			"portee" : {
				"lance" : 10,
				"description" : "peut se lancer sur les totems"
			},
			"defense" : "opposée – esquive",
			"description" : "Le mage adjure à l’élément du feu de lui prêter sa force. Sous les pieds de sa cible jaillit une colonne de feu infligeant 1D8 + {{1}} dégâts. Si le sort est lancé sur un totem, les dégâts infligés seront de 1D6 + {{1}, mais une cible touchée 3 fois par les flammes prendra obligatoirement feu infligeant instantanément 1D6 points de dégâts et de nouveau 1D6 pour chaque tour passé en feu. Un ennemi en feu fera tout pour éteindre les flammes, quitte à oublier tout le reste. Le fait de prendre feu pendant plus de 3 tours, abîmera de manière irréversible les objets en bois et en tissu. L’effet se produit chaque tour jusqu’à disparition du totem.",
			"type" : "damage",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
		{
			"label": "Totem fétiche",
			"portee" : {
				"lance" : 10
			},
			"description" : "Le chaman invoque un totem insensible aux dégâts pendant 1D4 + {{1}} tours. Le totem n’est pas vivant et ne possède aucune caractéristique. De ce fait, il n’est pas considéré comme une invocation et le mage peut en activer autant qu’il le souhaite. Chaque totem peut directement être utilisé comme catalyseur d’un sort chamanique. Cette manipulation aura pour effet de lancer le sort dans une zone de type 2 autour du totem, pendant toute la durée où celui-ci est actif sur le terrain. Si un nouveau sort est lancé sur le totem, le premier effet n’est plus actif.",
			"type" : "invocation",
			"cout" : "5 PM",
			"bonus" : {

			}
		}
		],
		"Rang 5": [
		{
			"label": "Force des esprits",
			"portee" : {
				"lance" : 5,
				"description" : "peut se lancer sur les totems"
			},
			"description" : "Le chaman fait appel aux esprits totémiques qui l’entourent. Celui-ci peut choisir d’invoquer directement un des animaux cités ci-dessous ou de lancer ce sort sur un totem. Les esprits sont considérés comme des invocations et possèdent des caractéristiques identiques à leurs homonymes physiques, hormis les points de vie qui seront divisés par 2. Les esprits sont également insensibles aux dégâts physiques et infligent eux-mêmes des dégâts magiques. Si le sort est lancé sur un totem, les esprits octroieront leurs forces à tous ceux présents dans la zone d’effet:<br>"+
							"Tigre: Une classe de dé supplémentaire<br>"+
							"Loup: 1 au critique sur une compétence<br>"+
							"Orque: 5 mètres de portée supplémentaire sur les sorts<br>"+
							"Hirondelle: 2 cases de déplacements supplémentaires<br>"+
							"Bison: 2 de RD supplémentaire<br>"+
							"Raie: Deux dés de soins supplémentaires<br>"+
							"Ours: Un dé de dégâts supplémentaire",
			"type" : "autre",
			"cout" : "6 PM",
			"bonus" : {

			}
		},
		{
			"label": "Tonnerre",
			"portee" : {
				"lance" : 10,
				"description" : "peut se lancer sur les totems"
			},
			"defense" : "opposée – esquive et éventuellement ténacité non opposée",
			"description" : "Le mage fait appel à la foudre et décharge des éclairs sur sa cible, infligeant 2D8 + {{1}} dégâts. Si le sort est lancé sur un totem, les dégâts infligés seront de 2D6 + {{1}} et toute créature située dans la zone d’effet devra réussir un jet de ténacité, sous peine de se retrouver paralysé (ne peut réaliser que des actions purement mentales).",
			"type" : "damage",
			"cout" : "6 PM",
			"bonus" : {

			}
		}
		],
		"Rang 6": [
		{
			"label": "Totem ancestral",
			"portee" : {
				"lance" : 5
			},
			"description" : "Le chaman invoque un grand totem insensible aux dégâts pendant 1D6 + {{1}} tours. Le totem n’est pas vivant et ne possède aucune caractéristique. De ce fait, il n’est pas considéré comme une invocation et le mage peut en activer autant qu’il le souhaite. Chaque totem peut directement être utilisé comme catalyseur d’un sort chamanique. Cette manipulation aura pour effet de lancer le sort dans une zone de type 3 autour du totem, pendant toute la durée où celui-ci est actif sur le terrain. Si un nouveau sort est lancé sur le totem, le premier effet n’est plus actif.",
			"type" : "invocation",
			"cout" : "7 PM",
			"bonus" : {

			}
		},
		{
			"label": "Émanation",
			"portee" : {
				"description" : "cône en face du lanceur. Peut se lancer sur les totems"
			},
			"defense" : "opposée – acrobaties et éventuellement ténacité non opposée",
			"description" : "Le mage associe les éléments du feu et de l’air, afin de créer un cône de vapeur brûlante qui infligera 2D10 + {{1}}. Si le sort est lancé sur un totem, les dégâts infligés seront de 2D8 + {{1}} et toute personne se trouvant dans la zone de totem qui rate un jet de ténacité, se retrouvera dans un état de fatigue. Les états de fatigue peuvent augmenter à chaque jet raté, jusqu’à ce que la cible tombe dans l’épuisement.",
			"type" : "damage",
			"cout" : "7 PM",
			"bonus" : {

			}
		}
		],
		"Rang 7": [
		{
			"label": "Acide",
			"portee" : {
				"lance" : 10,
				"description" : "peut se lancer sur les totems"
			},
			"defense" : "opposée – esquive",
			"description" : "Le mage coalise les éléments du feu et de l’eau, afin de créer un jet caustique qui infligera 4D6 + {{1}}. Si le sort est lancé sur un totem, les dégâts infligés seront de 4D4 + {{1}} et toute personne présente dans la zone d’effet devra faire un jet de résistance de ses équipements (à chaque tour) afin de voir si l’acide endommage le matériel et le rend inutilisable. Les armes et armures naturelles comme les griffes, les crocs ou le cuir d’un animal ne sont pas concernés par les altérations de cet acide.",
			"type" : "damage",
			"cout" : "8 PM",
			"bonus" : {

			}
		},
		{
			"label": "Obscurité",
			"portee" : {
				"lance" : 5,
				"personnel" : true,
				"description" : "peut se lancer sur les totems"
			},
			"description" : "Le chaman invite l’élément des ténèbres et fait se fondre un allié dans les ombres jusqu’à ce que celui-ci en sorte par une action complexe ou offensive. Obscurité fait que la cible ne peut pas être détectée à moins de se trouver dans un lieu entièrement éclairé, sans une parcelle d’ombre ou d’assister au lancement du sort. De ce fait, l’allié gagne un bonus de 1D6 en cas d’attaque et la cible aura un malus de 6 à sa défense. De plus, une attaque réussie alors que le personnage était sous l’effet du sort, obscurcira la vision de la cible pendant 1D2 + {{1}} tours, la considérant comme aveugle. Si le sort est lancé sur un totem, toute la zone d’effet se verra se remplir de volutes d’un noir profond et l’emplacement deviendra rapidement complètement sombre, bloquant toute source de lumière (intérieure comme extérieur). Toute personne (hormis le mage) ne voyant pas dans les ténèbres magiques, sera automatiquement considéré comme étant aveugle (malus de 6 au toucher.",
			"type" : "autre",
			"cout" : "8 PM",
			"bonus" : {

			}
		}
		],
		"Rang 8": [
		{
			"label": "Magma",
			"portee" : {
				"lance" : 15,
				"description" : "peut se lancer sur les totems"
			},
			"defense" : "opposée – esquive",
			"description" : "Le mage fusionne les éléments du feu et de la terre, afin de former un geyser de lave bouillonnante sous les pieds d’un ennemi, qui occasionnera 6D8 + {{1}}. Si le sort est lancé sur un totem, la zone sera frappée aléatoirement par des gerbes de lave qui infligeront de 6D4 + {{1}}. Chaque tour, une entité présente dans la zone d’effet a 20% de chance qu’une éruption se produise sous ses pieds et chaque case occupée ajoute 10 % supplémentaire (par exemple, un centaure se tenant sur 2 cases aura 30% de chance de faire surgir un geyser, 20 % pour la case «principale» et 10% pour l’annexe).",
			"type" : "damage",
			"cout" : "9 PM",
			"bonus" : {

			}
		},
		{
			"label": "Transfert d’âme",
			"portee" : {
				"lance" : 1
			},
			"defense" : "opposée – volonté si la cible n’est pas consentante",
			"description" : "Le mage transfère une âme dans l’enveloppe charnelle ciblée. Dans le cas où le corps réceptacle en possède déjà une, le mage devra alors au préalable utiliser le sort monde des esprits et y plonger tous les participants (une cible inconsciente pourra tout de même tenter de résister) afin d’échanger les places des deux âmes. Il peut s’agir de deux âmes dans deux corps ou d’une entité souhaitant entrer dans un corps vide ou déjà occupé. Dans le dernier cas, l’âme échangée se retrouvera à errer dans le monde des esprits. L’âme porte avec elle les souvenirs plus ou moins marquants de sa ou ses vies passée(s) et également la personnalité de l’individu. Faire changer une âme de corps ne fera pas oublier d’événement et n’altérera jamais un caractère.",
			"type" : "autre",
			"cout" : "9 PM",
			"bonus" : {

			}
		}
		]
	},
	"Pool astromancie" : {
		"magie" : true,
		"style" : {
			"background" : "astromancie.jpg",
			"color" : "#0f2032",
			"shadow": "#f3c672",
		},
		"type": "Magie",
		"Rang 1": [
		{
			"label": "La girafe",
			"portee" : {
				"description" : "personnelle"
			},
			"description" : "Le mage peut étendre son cou de manière extraordinaire, ajoutant environ 6 mètres à sa portée physique. Ce cou prolongé permet au mage de réaliser diverses actions qui dépassent les capacités physiques humaines. Il n’est plus rigide, mais devient souple et malléable, lui permettant de s’enrouler autour de surfaces, de se plier et de s’adapter à des situations variées. Il peut l’utiliser pour atteindre des objets en hauteur sans effort, observer son environnement depuis une position éloignée et sûre, explorer des zones inaccessibles, ou même réaliser des manœuvres furtives en gardant son corps hors de vue.",
			"type" : "buff",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label": "L’épouvantail",
			"portee" : {
				"lance" : 10
			},
			"defense" : "opposée – volonté",
			"description" : "La constellation de l’épouvantail s’anime soudainement et arbore un regard glacial et terrifiant, effrayant les ennemis et les faisant fuir.Le cygne",
			"type" : "debuff",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label": "Le cygne",
			"portee" : {
				"description" : "personnelle"
			},
			"description" : "Une plume de cygne se détache délicatement de la constellation, permettant au mage d’écrire ou de dessiner des messages invisibles qui ne seront révélés qu’aux personnes spécifiquement choisies au préalable par le mage.",
			"type" : "autre",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label": "La grue",
			"portee" : {
				"lance" : 10
			},
			"defense" : "opposée – esquive ou bouclier",
			"description" : "Le mage concentre l’énergie de la constellation pour lancer des attaques d’une grande précision, évoquant la grâce des coups de bec d’une grue. Ces attaques infligent 1D4 + {{1}} points de dégâts à leurs cibles. ",
			"type" : "damage",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label": "Le lièvre",
			"portee" : {
				"lance" : 5,
				"personnel" : true,
			},
			"description" : "Le mage peut exploiter la vitesse incroyable de la constellation, lui permettant de fuir rapidement une situation dangereuse ou de rattraper des adversaires. Il est possible d’augmenter la vitesse de déplacement de {{1}} case, et ce pendant {{1}} tour(s).",
			"type" : "buff",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label": "La règle",
			"portee" : {
				"lance" : 5
			},
			"description" : "Le mage a la capacité d’utiliser ce sort pour créer un pont solide et équilibré entre deux points, même en l’absence de matériaux physiques. Ce pont perdure pendant la durée du niveau du mage en minutes. Il est stable, indestructible (à moins que le mage ne rompe le sort) et offre un passage sûr pour toutes les personnes qui le traversent.",
			"type" : "debuff",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label": "Le poisson",
			"portee" : {
				"lance" : 5,
				"personnel" : true,
			},
			"description" : "Le mage acquiert la capacité de nager avec la fluidité d’un poisson, se déplaçant rapidement dans l’eau sans jamais manquer d’oxygène. Cette capacité reste active pendant {{1}} minute(s).",
			"type" : "buff",
			"cout" : "2 PM",
			"bonus" : {

			}
		},
		{
			"label": "La lyre",
			"portee" : {
				"lance" : 10,
				"personnel" : true,
			},
			"description" : "Le mage réalise un accord curatif en utilisant les cordes astrales de la lyre, ce qui a pour effet de guérir les blessures physiques et d’apaiser les souffrances. La mélodie restaure 1D4 + {{1}} points de vie.",
			"type" : "heal",
			"cout" : "2 PM",
			"bonus" : {

			}
		}
		],
		"Rang 2": [
		{
			"label": "La colombe",
			"portee" : {
				"lance" : 1,
			},
			"description" : "Le mage a la faculté d’envoyer un message à une personne ou à un lieu précis en faisant appel à une colombe messagère magique pour transmettre des informations. La vitesse de vol de cette colombe est semblable à celle d’un oiseau ordinaire, cependant, elle ne peut pas être blessée et n’a pas besoin de satisfaire des besoins physiologiques.",
			"type" : "invocation",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		{
			"label": "Le capricorne",
			"portee" : {
				"lance" : 15,
				"description" : "dans une zone de type 2"
			},
			"defense" : "simple – athlétisme pour bouger normalement dans la zone",
			"description" : "Le mage génère une zone de boue épaisse et visqueuse, considérée comme un terrain difficile pour toute créature se trouvant à l’intérieur.",
			"type" : "debuff",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
		{
			"label": "L’aigle",
			"portee" : {
				"lance" : 5
			},
			"description" : "Le mage invoque un aigle astral et créer un lien télépathique avec celui-ci. Ce lien offre au mage la possibilité de voir à travers les yeux de l’aigle, étendant ainsi sa vision dans un périmètre éloigné, ce qui facilite la surveillance à distance et l’exploration de zones inaccessibles.",
			"type" : "invocation",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
				{
			"label": "Le burin",
			"portee" : {
				"lance" : 1,
			},
			"description" : "Le sort offre au mage la capacité de laisser sa marque magique sur un objet (et pas un individu ou une surface). Cette marque est un symbole qui améliore les propriétés de l’objet de manière définitive. La marque peut apporter des avantages tels que :<br>"+
			"• L’objet devient plus résistant et obtient un bonus de + 1 par niveau du mage lors des jets de résistance<br>"+
			"• Les objets de type contenant, peuvent accueillir le double de matériaux ou de liquides<br>"+
			"• Les objets qui ont une durée de vie limitée (torches par exemple) peuvent servir le double du temps habituel<br>"+
			"• Tous les textes inscrits sur l’objet deviennent lisibles dans une langue connue par le mage<br>"+
			"• L’objet devient imperméable aux éléments, comme l’eau, le feu ou le froid<br>"+
			"Un objet peut recevoir différentes marques, mais pas cumuler plusieurs fois le même effet.",
			"type" : "buff",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
				{
			"label": "L’oiseau de paradis",
			"portee" : {
				"lance" : 1,
			},
			"defense" : "opposée – ténacité (possible à chaque tour jusqu’à réussite ou fin de l’effet)",
			"description" : "Un magnifique oiseau de paradis surgit, déployant ses ailes éblouissantes et aveuglant les ennemis pendant {{1}} tour(s).",
			"type" : "debuff",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
				{
			"label": "Le fourneau",
			"portee" : {
				"lance" : 5,
			},
			"description" : "Le mage peut invoquer un fourneau et une enclume magiques qui lui permettent de forger des armes et des armures. De ce fait, il n’a pas besoin de transporter de matériel avec lui. De plus, grâce à ces outils enchantés, il bénéficie d’un bonus de {{1}} pour l’artisanat de forge.",
			"type" : "invocation",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
				{
			"label": "La vierge",
			"portee" : {
				"lance" : 5,
				"personnel" : true,
			},
			"description" : "Le mage peut utiliser la magie de la constellation pour accélérer le processus de guérison naturelle, favorisant la récupération des blessures mineures. Cela se traduit par une régénération de 1D6 + {{1}} points de vie regagnés.",
			"type" : "heal",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
				{
			"label": "Le triangle",
			"portee" : {
				"lance" : 10,
				"description" : "en ligne droite"
			},
			"defense" : "opposée – esquive ou bouclier et un jet de ténacité simple si raté",
			"description" : "Ce sort confère au mage la capacité de générer un rayon d’énergie concentrée en formant un triangle avec ses mains. Dirigé vers une cible, ce rayon inflige 1D6 + {{1}} points de dégât.",
			"type" : "damage",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
				{
			"label": "Le sculpteur",
			"portee" : {
				"lance" : 1,
			},
			"description" : "Ce sort permet au mage de réparer des objets (en bois, pierre ou glace) avec une grande habileté, rétablissant leur état d’origine, ou de renforcer temporairement la structure d’un objet ou d’une arme, augmentant sa durabilité et sa résistance. Pendant le niveau du mage en heures × 2, l’objet devient alors incassable, peut importe la situation.",
			"type" : "buff",
			"cout" : "3 PM",
			"bonus" : {

			}
		},
				{
			"label": "Le lézard",
			"portee" : {
				"personnel" : true,
			},
			"description" : "En utilisant ce sort, le mage peut absorber l’énergie solaire, lui permettant de restaurer progressivement ses forces. Tant qu’il se trouve sous une source de lumière solaire assez forte pour générer un peu de chaleur, celui-ci pourra bénéficier d’une régénération de points de vie ou de mana équivalente à la méditation.",
			"type" : "heal",
			"cout" : "3 PM",
			"bonus" : {

			}
		}
		],
		"Rang 3": [
		{
			"label": "Le cocher",
			"portee" : {
				"lance" : 5,
			},
			"description" : "Le mage fait appel à un cheval astral afin de lui servir de monture.",
			"type" : "invocation",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label": "Le centaure",
			"portee" : {
				"lance" : 20,
				"personnel" : true,
			},
			"description" : "Le mage investit l’énergie de la constellation pour accorder à un allié la capacité de se déplacer plus loin, augmentant de {{2}} le nombre de cases qu’il peut parcourir.",
			"type" : "buff",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
		{
			"label": "Le dauphin",
			"portee" : {
				"personnel" : true,
				"description" : "sur une distance de 15 mètres"
			},
			"defense" : "aucune",
			"description" : "Le mage génère des ondes énergétiques uniques qui ricochent sur les objets et les êtres vivants, lui permettant de détecter leur présence et d’obtenir une vision de leur emplacement, même dans l’obscurité totale ou face à des créatures invisibles.",
			"type" : "autres",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
				{
			"label": "Le chien",
			"portee" : {
				"lance" : 5,
			},
			"description" : "Le mage invoque un acolyte canin capable de pister des cibles, de trouver des objets perdus et de détecter des présences invisibles. Ce chien reste auprès du mage jusqu’à ce qu’il soit révoqué, subisse des dégâts ou s’éloigne à plus de 30 mètres de distance. Le chien est un traqueur naturel, n’a pas besoin de jet de survie pour suivre des pistes, mais il doit avoir une odeur de référence pour commencer à pister.",
			"type" : "invocation",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
				{
			"label": "Le paon",
			"portee" : {
				"personnel" : true,
				"description" : "Tout ennemi regardant en direction du mage"
			},
			"defense" : "opposée – volonté (possible à chaque tour jusqu’à réussite ou fin de l’effet)",
			"description" : "Le mage déploie un éventail de plumes colorées semblables à celles d’un paon, pour captiver et impressionner ses adversaires. Cette démonstration combinée à la beauté des plumes les fait entrer dans l’état fasciné durant {{1}} tour(s).",
			"type" : "debuff",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
				{
			"label": "Le loup",
			"portee" : {
				"lance" : 5,
			},
			"defense" : "opposée – esquive ou bouclier",
			"description" : "Ce sort permet au mage d’incarner la férocité du loup en exécutant une morsure redoutable. Le mage peut infliger 1D8 + {{1}} points de dégâts à sa cible.",
			"type" : "damage",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
				{
			"label": "L’alouette",
			"portee" : {
				"lance" : 10,
			},
			"description" : "Le mage peut utiliser le chant de l’alouette pour inspirer la confiance et la détermination dans le cœur d’un de ses compagnons. Deux compétences choisies par le mage reçoivent alors un bonus de {{1}}, perdurant pendant 2+{{1}} tours.",
			"type" : "buff",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
				{
			"label": "Le cancer",
			"portee" : {
				"lance" : 10,
				"personnel" : true,
			},
			"description" : "Ce sort permet au mage de renforcer temporairement sa peau ou celle d’un allié, la transformant en une armure similaire à une carapace. Cela confère un bonus de {{1}} à la RD pendant 2+{{1}} tours.",
			"type" : "buff",
			"cout" : "4 PM",
			"bonus" : {

			}
		},
				{
			"label": "Le bouvier",
			"portee" : {
				"lance" : 15,
			},
			"defense" : "opposée – volonté",
			"description" : "Le mage établit un lien magique avec les animaux, gagnant leur confiance et leur assistance pendant {{0.5}} heures. Ce lien permet de communiquer avec les animaux, de les apaiser, ou même de les solliciter en cas de combat. Ils garderont néanmoins leurs instincts, ainsi que leur « libre arbitre ». Un animal ayant déjà un maître ne pourra être influencé.",
			"type" : "autres",
			"cout" : "4 PM",
			"bonus" : {

			}
		}
		],
		"Rang 4": [
				{
			"label": "Le renard",
			"portee" : {
				"lance" : 1,
				"personnel" : true,
			},
			"description" : "Le mage peut établir une connexion avec la constellation, s’appropriant sa ruse et sa sagesse. Elle lui confère un bonus de {{0.5}} aux caractéristiques d’agilité, d’intelligence et de sagesse, et ce durant 10 minutes.",
			"type" : "buff",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
				{
			"label": "Le scorpion",
			"portee" : {
				"lance" : 5,
			},
			"defense" : "Défense : opposée – ténacité (à chaque tour, jusqu’à réussite)",
			"description" : "Ce sort permet d’invoquer un dard empoisonné, semblable à celui d’un scorpion et capable de paralyser temporairement un ennemi. L’effet de paralysie persiste jusqu’à ce que la cible réussisse un jet de ténacité.",
			"type" : "debuff",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
				{
			"label": "Le serpent",
			"portee" : {
				"lance" : 10,
			},
			"defense" : "opposée – ténacité (à chaque tour, jusqu’à réussite)",
			"description" : "Par le biais de ce sort, le mage lance un sort de poison qui inflige 4+{{1}} points de dégâts par tour à la cible touchée. Les dégâts se répètent à chaque tour tant que la cible ne réussit pas un jet de ténacité pour mettre fin à l’effet. Une fois le poison purgé, une cible ne peut plus être empoisonnée par le mage durant le combat.",
			"type" : "damage",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
				{
			"label": "Le verseau",
			"portee" : {
				"personnel" : true,
			},
			"description" : "Ce sort permet au mage de convertir de l’eau en points de mana. Cependant, pour que la conversion ait lieu, l’eau doit être bue. D’un point de vue biologique, le mage ne peut pas absorber plus de 1 litre toutes les 2 heures, au risque de se rendre malade.",
			"type" : "autres",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
				{
			"label": "Le serpentaire",
			"portee" : {
				"lance" : 10,
				"personnel" : true,
			},
			"description" : "Ce sort de soin puise directement dans la constellation du serpentaire, activant son pouvoir de guérison afin de restaurer 2D6 + {{1}} points de vie.",
			"type" : "heal",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
				{
			"label": "Indartsua",
			"portee" : {
				"lance" : 1,
				"personnel" : true,
			},
			"description" : "En invoquant la puissance d’Indartsua, le mage renforce les muscles de manière prodigieuse, augmentant la caractéristique de force d’un montant de {{1}}.",
			"type" : "buff",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
				{
			"label": "La balance",
			"portee" : {
				"lance" : 10,
			},
			"defense" : "opposée – ténacité si la cible n’est pas consentante",
			"description" : "La balance permet au mage de transférer soit du mana, soit des points de vie entre lui et une cible. Ce transfert peut être volontaire ou forcé lorsqu’il est utilisé contre la volonté d’une cible. La quantité transvasée sera de maximum {{5}} points par tentative. La quantité ne peut excéder le nombre de points minimum ou maximum des deux partis. Auquel cas le sort s’interrompra de lui-même.",
			"type" : "autres",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
				{
			"label": "Le cheval",
			"portee" : {
				"description" : "cône en face du mage"
			},
			"defense" : "aucune",
			"description" : "Le mage peut déclencher une ruade invisible qui projette violemment les ennemis (de taille moyenne au maximum), les repoussant de {{1}} cases, sans possibilité d’esquive. Cette force est suffisante pour désarçonner automatiquement une unité montée. Si le sort est dirigé contre une seule cible, une créature de grande taille peut également être repoussée, et pour une cible plus petite, la distance de projection est doublée.",
			"type" : "autres",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
				{
			"label": "L’écu",
			"portee" : {
				"lance" : 5,
				"personnel" : true,
			},
			"description" : "Le mage invoque le bouclier de l’Écu, un puissant rempart astral de 10+{{5}} points de vie, qui pivote automatiquement pour protéger le mage. Le bouclier s’efface lorsque ses points de vie sont épuisés ou que toute menace soit écartée.",
			"type" : "buff",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
				{
			"label": "L’autel",
			"portee" : {
				"lance" : 5,
			},
			"description" : "Ce sort permet de créer un autel astral où le mage insuffle son mana en guise d’offrande. En échange, l’autel fournit un court moment de protection, offrant un bonus de {{1}} à la RM pour l’entièreté du groupe, et ce pour {{1}} tours.",
			"type" : "buff",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
				{
			"label": "La carène",
			"portee" : {
				"lance" : 1,
			},
			"description" : "Le mage <b>canaliser</b> son sort le temps désiré et protège une embarcation contre les tempêtes, les vagues déferlantes et les courants dangereux. Une brise céleste remplit les voiles du navire, doublant sa vitesse et automatisant la navigation.",
			"type" : "buff",
			"cout" : "5 PM",
			"bonus" : {

			}
		},
				{
			"label": "La flèche",
			"portee" : {
				"lance" : 5,
				"personnel" : true,
			},
			"description" : "Ce sort confère une bénédiction aux projectiles physiques et magiques, améliorant leur précision et leur portée. Le bonus au toucher et à la distance est de {{1}}, au même titre que la durée de l’effet en tours.",
			"type" : "buff",
			"cout" : "5 PM",
			"bonus" : {

			}
		}
		],
		"Rang 5": [
				{
			"label": "La baleine",
			"portee" : {
				"lance" : 10,
				"personnel" : true,
			},
			"description" : "Le mage vocalise le chant de la baleine, une mélodie apaisante qui offre une régénération de la vitalité à ses alliés. La guérison s’élève à 2D8 + {{1}} points de vie.",
			"type" : "heal",
			"cout" : "6 PM",
			"bonus" : {

			}
		},
				{
			"label": "Le caméléon",
			"portee" : {
				"personnel" : true,
			},
			"description" : "Le mage peut se fondre dans son environnement tel un caméléon, devenant invisible aux yeux des autres. L’effet perdure jusqu’à ce que le mage interrompt le sort, subit des dégâts ou en reçoit.",
			"type" : "autres",
			"cout" : "6 PM",
			"bonus" : {

			}
		},
				{
			"label": "L’horloge",
			"portee" : {
				"description" : "zone de type 2 autour du mage"
			},
			"defense" : "opposée – ténacité si la cible veut résister",
			"description" : "Ce sort permet au mage de manipuler brièvement le flux du temps, lui offrant la possibilité de revenir en arrière pour corriger des actions antérieures ou réparer des erreurs commises. Toutes les entités autour du mage reviennent en arrière d’un tour, annulant ainsi tous les dégâts, soins, ou toute autre action effectuée. Le mage lui-même demeure indemne, préservant ses pertes de mana et ses blessures antérieures. En revanche, l’utilisation de ce sort inflige au mage un état de fatigue inévitable, qui va se cumuler à chaque usage.",
			"type" : "autres",
			"cout" : "6 PM",
			"bonus" : {

			}
		},
				{
			"label": "Le pégase",
			"portee" : {
				"personnel" : true,
				"description" : "zone de type 3 autour du lanceur"
			},
			"description" : "Le mage puise dans le pouvoir de la constellation et octroie à son équipe la capacité de s’élever dans les cieux avec des ailes magiques. Les membres de l’équipe peuvent alors voler librement sur une période de 10 minutes.",
			"type" : "buff",
			"cout" : "6 PM",
			"bonus" : {

			}
		},
				{
			"label": "La table",
			"portee" : {
				"lance" : 5,
			},
			"description" : "Le mage utilise la magie de la constellation pour créer un banquet royal. Ce festin abonde en délices et en boissons, apportant une réelle satiété à tous ceux qui s’y attablent. Les convives se voient rassasiés pour une durée de 24 heures, éliminant la faim et la soif de leur quotidien. De plus, la magie bienfaisante du repas efface tous les états négatifs, revitalisant les invités et leur permets de doubler leurs prochains jets de régénération de points de vie et de mana.",
			"type" : "invocation",
			"cout" : "6 PM",
			"bonus" : {

			}
		},
				{
			"label": "Le bélier",
			"portee" : {
				"personnel" : true,
				"description" : "personnelle, puis sur une distance de 10 mètres maximum"
			},
			"defense" : "opposée – esquive ou bouclier",
			"description" : "Le mage invoque la puissance astrale du bélier et se précipite vers l’ennemi, créant une charge dévastatrice. Cette charge astrale inflige 2D8 + {{1}} points de dégâts et peut être lancée sans provoquer d’attaque d’opportunité si le mage est en prise avec un ennemi.",
			"type" : "damage",
			"cout" : "6 PM",
			"bonus" : {

			}
		}
		],
		"Rang 6": [
				{
			"label": "Les voiles",
			"portee" : {
				"lance" : 5,
				"personnel" : true,
			},	
			"description" : "Ce sort confère à un individu une apparence fugitive et insaisissable. Pendant sa durée, la cible devient invisible avant chaque attaque, lui permettant ainsi d’effectuer des attaques sournoises à chaque coup. L’invisibilité dure quelques instants après chaque action, offrant à la cible une fenêtre pour préparer une nouvelle attaque tout en restant dissimulée. Cependant, l’invisibilité ne persiste que pour une seule personne à la fois, et ce pendant {{1}} tours.",
			"type" : "buff",
			"cout" : "7 PM",
			"bonus" : {

			}
		},
				{
			"label": "Le taureau",
			"portee" : {
				"lance" : 10,
				"personnel" : true,
			},
			"description" : "Le mage invoque la furie du taureau, déchaînant une colère bestiale chez la cible. Pendant sa durée, la cible double son bonus de dégâts liés à la force, augmente ses dégâts physiques de 1D et devient également immunisée contre les états préjudiciables. Cependant, la cible perd temporairement le contrôle de ses actions, attaquant tout ce qui se trouve à portée, qu’il s’agisse d’amis ou d’ennemis. La fureur est irrépressible et ne s’arrête qu’à la demande du mage qui doit alors réutiliser le sort afin de forcer l’esprit du taureau à sortir de la cible.",
			"type" : "buff",
			"cout" : "7 PM",
			"bonus" : {

			}
		},
						{
			"label": "La coupe",
			"portee" : {
				"lance" : 1,
			},
			"description" : "Le mage remplit une coupe magique d’un liquide pétillant aux propriétés énergisante et curative, qui revitalise le buveur d’un montant de 12D12 {{1}}. Lorsque la cible boit ce breuvage, elle ressent instantanément une régénération qui rétablit ses points de vie et de mana. Les points de guérison peuvent être répartis entre les points de vie et les points de mana en fonction de leurs besoins.",
			"type" : "heal",
			"cout" : "7 PM",
			"bonus" : {

			}
		},
						{
			"label": "Le corbeau",
			"portee" : {
				"lance" : 10,
			},
			"defense" : "opposée – volonté",
			"description" : "Le mage de lance une malédiction redoutable sur une cible. Une aura sombre, associée au mystérieux oiseau, entoure la personne visée, apportant malheur et malchance. La malédiction se traduit par un malus de {{1}} sur les actions de la cible, entravant sa capacité à réussir ses entreprises durant les 5 prochains tours.",
			"type" : "debuff",
			"cout" : "7 PM",
			"bonus" : {

			}
		},
						{
			"label": "Le lynx",
			"portee" : {
				"lance" : 5,
			},
			"defense" : "opposée – esquive ou bouclier. Si raté, jet de ténacité simple pour éviter ou arrêter le saignement",
			"description" : "Le lynx déchire le voile astral et matérialise ses griffes acérées, prêtes à l’attaque. Sa lacération inflige 4D6 + {{1}} points de dégâts. De plus, chaque griffure à une chance d’infliger un état de saignement à la cible.",
			"type" : "damage",
			"cout" : "7 PM",
			"bonus" : {

			}
		},
						{
			"label": "Le sextant",
			"portee" : {
				"lance" : 1,
			},
			"description" : "Lorsque le mage lance ce sort, il peut choisir deux points dans Hunaleros et rapprocher ces points temporairement, réduisant ainsi la distance réelle à parcourir. Pour une personne qui n’emprunte pas le chemin réduit, la distorsion spatiale peut apparaître comme une sorte de mirage, de flou visuel, ou de phénomène mystérieux dans l’espace environnant. La magie opère principalement à l’intérieur de cette distorsion spatiale, elle ne verrait pas de réduction réelle des distances, mais plutôt une altération de la perception de l’espace à cet endroit précis. On considère que chaque niveau du mage divise le temps de trajet par le même montant.",
			"type" : "autres",
			"cout" : "7 PM",
			"bonus" : {

			}
		}
		],
		"Rang 7": [
						{
			"label": "Le sagittaire",
			"portee" : {
				"lance" : 15,
			},
			"defense" : "opposée – esquive ou bouclier",
			"description" : "Le mage invoque la puissance du sagittaire afin de lancer une flèche astrale qui traverse les cieux avec précision. Cette flèche magique inflige 4D6 + {{1}} points de dégât.",
			"type" : "damage",
			"cout" : "8 PM",
			"bonus" : {

			}
		},
								{
			"label": "La licorne",
			"portee" : {
				"lance" : 10,
				"personnel" : true,
			},
			"description" : "Le mage fait appel à la bienveillance de la licorne pour prodiguer des soins à une cible. Cette magie curative rétablie 4D6 + {{1}} points de vie.",
			"type" : "heal",
			"cout" : "8 PM",
			"bonus" : {

			}
		},
								{
			"label": "Le peintre",
			"portee" : {
				"lance" : 1,
			},
			"description" : "Le mage utilise une image mentale précise d’un lieu qu’il connaît et créer un portail magique en peignant une représentation détaillée de cet endroit vers lequel il souhaite se téléporter. Une fois le tableau achevé, il peut s’y rendre instantanément et le portail se refermera dans la minute.",
			"type" : "autres",
			"cout" : "8 PM",
			"bonus" : {

			}
		},
								{
			"label": "Les gémeaux",
			"portee" : {
				"lance" : 5,
			},
			"description" : "Le mage peut créer une réplique parfaite de lui-même ou d’un allié, reproduisant fidèlement son apparence et ses compétences pour semer la confusion parmi les ennemis. En revanche, celle-ci ne possède que la moitié des points de vie et de mana de l’original et ne peut posséder aucune RD ou RM. La réplique est considérée comme une invocation, avec toutes les règles applicables à cette condition.",
			"type" : "invocation",
			"cout" : "8 PM",
			"bonus" : {

			}
		},
								{
			"label": "Le lion",
			"portee" : {
				"lance" : 1,
				"personnel" : true,
			},
			"description" : "Le mage appose le pouvoir de la constellation du lion pour conférer à un individu un charisme remarquable. L’aura magnétique qui émane de cette personne influence favorablement ceux qui l’entourent. Le personnage gagne un bonus de en charisme de {{0.5}}, et ce pour les 10 minutes à venir.",
			"type" : "buff",
			"cout" : "8 PM",
			"bonus" : {

			}
		},
								{
			"label": "L’ours",
			"portee" : {
				"lance" : 10,
			},
			"defense" : "opposée – esquive ou bouclier et un jet de ténacité simple si raté",
			"description" : "Un déluge de griffes et de morsures émane du mage, déferlant sur un ennemi avec une force destructrice. Chaque attaque inflige 4D6 + {{1}} points de dégâts.",
			"type" : "damage",
			"cout" : "8 PM",
			"bonus" : {

			}
		}
		],
		"Rang 8": [
		{
			"label": "Le dragon",
			"portee" : {
				"description" : "en cône devant le lanceur"
			},
			"defense" : "opposée – athlétisme ou acrobatie",
			"description" : "Le mage invoque la puissance du dragon ancestral, soufflant un cône de feu dévastateur sur ses ennemis. Les flammes engloutissent tout sur leur passage, infligeant 5D6 + {{1}} points de dégâts.",
			"type" : "damage",
			"cout" : "9 PM",
			"bonus" : {

			}
		},
		{
			"label": "La mouche",
			"portee" : {
				"lance" : 10,
			},
			"defense" : "opposée – ténacité et un jet de ténacité simple chaque tour si raté",
			"description" : "Ce sort invoque un essaim de mouches qui propagent la redoutable <b>Peste des Mouches</b>. Les mouches convergent rapidement vers leur cible et injectent une substance magique qui plonge la cible dans un sommeil profond. La victime est incapable de se réveiller par des moyens conventionnels, seul un jet de ténacité peut briser ce sommeil ensorcelé, laissant la victime vulnérable face aux attaques.",
			"type" : "debuff",
			"cout" : "9 PM",
			"bonus" : {

			}
		},
		{
			"label": "La couronne",
			"portee" : {
				"lance" : 1,
			},
			"defense" : "opposée – volonté, puis volonté simple pour chaque ordre supplémentaire",
			"description" : "Grâce à la puissance de la couronne, le mage peut influencer les actions et les décisions de ses cibles, les amenant à se plier à sa volonté pour une durée limitée. Cependant, le mage ne peut pas contraindre les cibles à agir de manière totalement contraire à leur nature, à enfreindre des principes moraux fondamentaux, ou à mettre leur propre vie en danger. Le mage ne peut donner qu’un ordre à la fois et chaque nouvel ordre sera une occasion pour la cible de tenter de se défaire du sort à l’aide d’un jet de volonté simple. La cible ne perd pas la mémoire et se souviendra de tout (hormis d’avoir été envoûtée).",
			"type" : "autres",
			"cout" : "9 PM",
			"bonus" : {

			}
		},
		{
			"label": "La croix",
			"portee" : {
				"lance" : 10,
				"description" : "dans une zone cruciforme de 6x6x6x6 cases"
			},
			"description" : "Le mage bénit une zone sous le signe de la croix pour prodiguer des soins à ses alliés. Une douce lumière se propage dans une zone de forme cruciforme et les alliés récupèrent 6D6 + {{1}} points de vie.",
			"type" : "heal",
			"cout" : "9 PM",
			"bonus" : {

			}
		},
		{
			"label": "L’hydre",
			"portee" : {
				"lance" : 1,
				"personnel" : true,
			},
			"description" : "Ce sort permet au mage de régénérer un membre amputé, à l’exception de la tête ou d’un organe vital. La durée de régénération dépend de la taille du membre manquant. Un doigt, par exemple, ne prendra qu’une heure à repousser, alors qu’une jambe prendra 2 bons jours.",
			"type" : "heal",
			"cout" : "9 PM",
			"bonus" : {

			}
		},
		{
			"label": "Le phénix",
			"portee" : {
				"lance" : 1,
				"personnel" : true,
			},
			"description" : "Tel l’oiseau mythique, le mage peut tromper la mort automatiquement dès que ses points de vie temporaires atteignent 0, tant qu’il possède le mana nécessaire. Son corps se soulève légèrement du sol et une aura rougeâtre, formant des ailes rouges éthérées, l’enveloppe. Il ne pourra rien faire pendant 6 tours, durant lesquels ses points de vie temporaires remonteront à une valeur égale au niveau du mage. S’ils atteignent de nouveau 0 avant la fin des 6 tours, il mourra définitivement. Sinon, il reprendra connaissance avec l’entièreté de ses attributs chiffrés, hormis le mana. Le mage peut recevoir une source de soins pendant les 6 tours, cela n’interrompra cependant pas le sort.",
			"type" : "autres",
			"cout" : "9 PM",
			"bonus" : {

			}
		}
		]
	}
,
	"Astromancie" : {
		"magie" : true,
		"style" : {
			"background" : "astromancie.jpg",
			"color" : "#FFFFA6",
			"shadow": "white",
		},
		"type": "Magie",
		"Rang 1": [
		{
			"choix" : {
				"rang": 1,
				"type": "Magie"
			},
		},
		{
			"choix" : {
				"rang": 1,
				"type": "Magie"
			},
		},
		{
			"choix" : {
				"rang": 1,
				"type": "Magie"
			},
		}
		],
		"Rang 2": [
		{
			"choix" : {
				"rang": 2,
				"type": "Magie"
			},
		},
		{
			"choix" : {
				"rang": 2,
				"type": "Magie"
			},
		},
		{
			"choix" : {
				"rang": 2,
				"type": "Magie"
			},
		}
		],
		"Rang 3": [
		{
			"choix" : {
				"rang": 3,
				"type": "Magie"
			},
		},
		{
			"choix" : {
				"rang": 3,
				"type": "Magie"
			},
		},
		{
			"choix" : {
				"rang": 3,
				"type": "Magie"
			},
		}
		],
		"Rang 4": [
		{
			"choix" : {
				"rang": 4,
				"type": "Magie"
			},
		},
		{
			"choix" : {
				"rang": 4,
				"type": "Magie"
			},
		},
		{
			"choix" : {
				"rang": 4,
				"type": "Magie"
			},
		},
		{
			"choix" : {
				"rang": 4,
				"type": "Magie"
			},
		}
		],
		"Rang 5": [
		{
			"choix" : {
				"rang": 5,
				"type": "Magie"
			},
		},
		{
			"choix" : {
				"rang": 5,
				"type": "Magie"
			},
		}
		],
		"Rang 6": [
		{
			"choix" : {
				"rang": 6,
				"type": "Magie"
			},
		},
		{
			"choix" : {
				"rang": 6,
				"type": "Magie"
			},
		}
		],
		"Rang 7": [
		{
			"choix" : {
				"rang": 7,
				"type": "Magie"
			},
		},
		{
			"choix" : {
				"rang": 7,
				"type": "Magie"
			},
		}
		],
		"Rang 8": [
		{
			"choix" : {
				"rang": 8,
				"type": "Magie"
			},
		},
		{
			"choix" : {
				"rang": 8,
				"type": "Magie"
			},
		}
		]
	},
	"Cartomancie" : {
		"magie" : true,
		"style" : {
			"background" : "cartomancie.jpg",
			"color" : "#000000",
			"shadow": "white",
		},
		"type": "Autre",
		"Rang 1":[
		{
			"label":"Premiers pas",
			"description":"Le personnage génère magiquement des cartes sur une distance de 15 mètres, à l’aide d’un jet d’incantation et contre 2 points de mana. Il a le choix entre les cartes de pique qui infligent des dégâts (et seront opposés à l’esquive ennemie) et de cœur qui prodiguent des soins. Il est capable d’utiliser les cartes 1 à 5 d’un paquet, soit 1D5 + son niveau en points de dégâts ou de soins. Les cartes ne peuvent bénéficier d’améliorations magiques.",
			"type" : "autre",
			"bonus": {

			}
		},
		{
			"label":"Coup de pouce",
			"description":"Le don «repioche» est ajouté à la section dons de la fiche du personnage.",
			"type" : "passif",
        "bonus": {
            "dons": ["repioche"]
        
			}
		}
		],
		"Rang 2":[
		{
			"label":"Apprentissage",
			"description":"Le personnage maîtrise désormais les cartes 6 à 8. À partir de maintenant, on lancera 1D8 + {{1}} pour définir le montant des dégâts ou des soins.",
			"type" : "autre",
			"bonus" : {

			}
		},
		{
			"label":"Nouvelle couleur",
					"description":"Le magicien peut manier les cartes de carreau. Le coût en mana et le fonctionnement restent identiques, hormis que les cibles feront leur jet d’opposition sur la compétence de ténacité. Les cartes de carreau permettent au cartomancien de repousser une cible d’un nombre de cases égal à la valeur tirée, d’infliger un malus aux dégâts, à l’esquive ou à l’attaque, pendant 1D4 tour(s).",
			"type" : "autre",
			"bonus" : {

			}
		}
		],
		"Rang 3":[
		{
			"label":"Cartomancien aguerri",
			"description":"Le personnage connaît désormais les cartes 9 et 10. À partir de maintenant, on lancera 1D10 + {{1}}. De plus, il peut maintenant manier les cartes de trèfle, il peut désormais offrir des bonus à un allié pendant un tour. Il aura le choix entre un bonus aux dégâts magiques ou physiques, un bonus dans une compétence, ou un bonus à la RD et la RM.",
			"type" : "autre",
			"bonus" : {

			}
		},
		{
			"label":"Valet",
			"description":"Le cartomancien obtient une première carte de pouvoir (qui doit être précisé avant la génération de carte) : le valet. Les cartes pouvoir ne sont pas aléatoires et le cartomancien peut en utiliser une par tour, en complément de la génération de carte initiale. Le valet permet, contre 3 points de mana, de faire rebondir l’effet (peu importe la couleur) et d’en faire profiter une cible adjacente. Cette dernière recevra la moitié des dégâts ou des soins de la cible initiale et les effets dureront moitié moins de temps.",
			"type" : "autre",
			"cout" : "3 PM",
			"bonus": {
				
			}
		}
		],
		"Rang 4":[
		{
			"label":"Dame",
			"description":"Le cartomancien connaît une deuxième carte de pouvoir (qui doit être précisé avant la génération de carte). Elle permet, contre 4 points de mana, d’ajouter un DOT ou un HOT sur une cible, durant le niveau du cartomancien en tour(s). Pour les cartes de carreau et de trèfle, elle permet d’ajouter le niveau du mage à la durée des effets.",
			"type" : "autre",
			"cout" : "4 PM",
			"bonus": {

			}
		},
		{
			"label":"Personnification",
			"description":"Contre 5 points de mana et sur une distance de 5 mètres, le cartomancien utilise son tour pour invoquer le valet, la dame ou le roi sur le champ de bataille. Tant que l’invocation est sur le terrain, elle ne peut plus être utilisée comme carte de pouvoir. Les caractéristiques de ces invocations sont données plus bas.",
			"type" : "invocation",
			"cout" : "5 PM",
			"bonus": {

			}
		}
		],
		"Rang 5":[
		{
			"label":"Roi",
			"description":"Le cartomancien connaît une troisième carte de pouvoir, coûtant 5 points de mana à l’utilisation : le roi. Cette carte permet de lancer les effets (peu importe la couleur) en zone de type 2 qui touchent toutes les unités présentes à l’intérieur.",
			"type" : "autre",
			"cout" : "5 PM",
			"bonus": {

			}
		},
		{
			"label":"Tricher",
			"description":"Contre un malus de 4 à son jet de volonté et la dépense en mana de deux tirages, le personnage peut tirer et utiliser deux cartes lors de son tour. Une carte ne peut pas être tirée deux fois.",
			"type" : "autre",
			"bonus": {

			}
		}
		],
		"Rang 6":[
		{
			"label":"Expert cartomancien",
			"description":"Le cartomancien connaît une dernière carte de pouvoir, coûtant 6 points de mana à l’utilisation : l’as. Cette carte permet de doubler le résultat d’une carte générée ou d’une durée.",
			"type" : "buff",
			"cout" : "6 PM",
			"bonus": {

			}
		},
		{
			"label":"Joker",
			"description":"La carte joker est ajoutée au paquet du cartomancien. On lancera désormais 1D11 + {{1}}. Le score du joker sera toujours de 10, mais aura en plus un effet incertain de sa propre table aléatoire.<br>"+
			"1• Le tour du personnage ne coûte aucun point de mana<br>"+
			"2• Les bonus par niveau sont doublés<br>"+
			"3• Le personnage reçoit également les soins ou bonus qu’il offre<br>"+
			"4• Une invocation (valet, dame ou roi) vient sur le terrain<br>"+
			"5• Cette carte ignore la RM de l’ennemi",
			"type" : "autre",
			"bonus": {

			}
		}
		]
	},
};

export default dons_evolutif;