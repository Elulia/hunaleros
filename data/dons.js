const dons = {
	//dons basique de combat
	"acharnement": {
		"label": "Acharnement",
		"description": "À chaque attaque réussie (lorsque le jet de toucher opposé est réussi) sur une même cible, les dégâts du personnage augmentent de 2. Dans le cas d’attaques multiples, l’effet s’applique pour chaque coup porté.",
		"optgroup": "Dons de combat",
		"bonus": {
		}
	},
	"adversaire_polyvalent": {
		"label": "Adversaire polyvalent",
		"description": "Le personnage obtient un bonus de 2 sur ses jets de manœuvres.",
		"optgroup": "Dons de combat",
		"bonus": {
		"manoeuvres" : 2
		}
	},
	"ambidextrie": {
		"label": "Ambidextrie",
		"description": "<span style=\"font-weight:bold\"><i class=\"fa fa-exclamation-triangle\"></i> Pour prendre ce don, il faudra au préalable être niveau 2 et remplir les prérequis de l’arme. <i class=\"fa fa-exclamation-triangle\"></i></span><br> Permet d’infliger une attaque de plus, en maniant une arme dans chaque main. En contrepartie, le jet de toucher s’effectuera avec un malus de 6, un malus de 3 à la défense et une chance d’échec critique augmentée de 2 (28-29-30). Les divers bonus comme les dégâts liés à la force, les dons ou les dons évolutifs ne sont comptés que pour la main directrice. En cas d’ambidextrie avec deux armes différentes, il faudra bien différencier les deux sources de dégâts. De plus, si les scores de compétences ne sont pas identiques, il faudra vérifier que les deux armes touchent avec le jet, sinon une seule des deux infligera les dégâts.",
		"optgroup": "Dons de combat",
		"bonus": {
		}
	},
	"ambidextrie_ii": {
		"label": "Ambidextrie II",
		"description": "Réduit de 1 les malus au toucher et à la défense.",
		"optgroup": "Dons de combat",
		"bonus": {
		}
	},
		"ambidextrie_iii": {
		"label": "Ambidextrie III",
		"description": "Tous les bonus s'appliquent désormais sur les deux armes au lieu d'une seule. Cependant la marge d'échec critique augmente de 1.",
		"optgroup": "Dons de combat",
		"bonus": { // TODO à voir
		}
	},
	"anticipation": {
		"label": "Anticipation",
		"description": "Grâce à sa fine analyse des mouvements adverses, le personnage peut deviner les intentions de son ennemi et réagir en conséquence. Lorsqu’il est ciblé par une attaque, il peut effectuer un jet de psychologie, remplaçant la compétence d’esquive tout en respectant les mêmes conditions de réussite.",
		"optgroup": "Dons de combat",
		"bonus": {
		}
	},
	"assommoir": {
		"label": "Assommoir",
		"description": "Chaque fois que le personnage arrive à infliger des dégâts contondants à sa cible, il gagne un bonus de 2 pour assommer la cible. L’effet est cumulable et reste actif sur la cible jusqu’à ce que la manœuvre soit effectuée.",
		"optgroup": "Dons de combat",
		"bonus": {
		}
	},	
	"assurance": {
		"label": "Assurance",
		"description": "Lorsque le joueur lance son jet d’initiative, il peut en relancer un et choisir le jet qui lui convient le plus. De plus, le personnage gagne un bonus à l’initiative de 2.",
		"optgroup": "Dons de combat",
		"bonus": {
		"initiative" : 2
		}
	},
	"attaque_eclair": {
		"label": "Attaque éclair",
		"description": "<span style=\"font-weight:bold\"><i class=\"fa fa-exclamation-triangle\"></i>Ce don interdit le port d’armures lourdes et complètes.<i class=\"fa fa-exclamation-triangle\"></i></span><br> Le personnage peut désormais se déplacer avant et après avoir frappé, à condition que son mouvement total reste dans les limites de sa vitesse de déplacement. L’ensemble de ce déplacement ne peut provoquer une attaque d’opportunité.",
		"optgroup": "Dons de combat",
		"bonus": {
		}
	},
	"berserk": {
		"label": "Berserk",
		"description": "<b>Une fois par combat et contre 2 points de mana</b>, le personnage entre dans un état de rage, infligeant systématiquement les dégâts maximaux. Cependant, il ne pourra pas esquiver et attaquera une cible aléatoire (alliée ou ennemie), dans la limite de sa portée et s'il n'est pas déjà engagé. Il perd le contrôle de ses actions et doit utiliser toutes les capacités, dons ou sorts qui maximisent ses dégâts. Il continue d'attaquer cette cible jusqu'à ce qu'elle soit hors combat, puis choisit une nouvelle cible aléatoire. L’état dure 4 tours + le niveau du personnage, et peut être annulé par un jet de volonté. Ce jet commence avec un malus de 4, et augmente de 1 à chaque tour passé enragé. Passer dans l’état ou en sortir prend 1 tour.",
		"optgroup": "Dons de combat",
		"bonus": {
		}
	},
	"charge_puissante": {
		"label": "Charge puissante",
		"description": "Lorsqu’une charge est réussie, les dégâts de l’arme de mêlée sont doublés (après prise en compte du bonus naturel de la charge).",
		"optgroup": "Dons de combat",
		"bonus": {
		}
	},
	"choc": {
		"label": "Choc",
		"description": "Le personnage obtient une nouvelle technique pour son arme de corps-à-corps. S’il manie une arme contondante, la cible touchée (de taille moyenne ou moins) est repoussée d’une case. Si elle rencontre un obstacle, les dégâts liés à la force seront doublés.<br>"+
		"S’il manie une arme tranchante, le personnage pourra toucher deux ennemis, de taille moyenne ou moins, alignés l’un derrière l’autre. Chacun aura le droit à un jet d’esquive opposé, la cible se trouvant derrière sera prise au dépourvu. Si un ennemi est seul, mais proche d’une surface où l’arme peut se planter, le personnage peut choisir de ne plus pouvoir utiliser cette arme, afin d’empaler sa cible et la rendre immobile. Pour se défaire, la cible devra subir une fois les dégâts de l’arme sans réduction d’armure et entrera forcément dans l’état ensanglanté.",
		"optgroup": "Dons de combat",
		"bonus": {
		}
	},
	"combat_monte": {
		"label": "Combat monté",
		"description": "Le personnage ne subit plus de malus lors de combat monté, que ce soit lors d’un trot ou d’un galop.",
		"optgroup": "Dons de combat",
		"bonus": {
		}
	},	
	"double_tranchant": {
		"label": "Double tranchant",
		"description": "Le personnage donne toute sa force dans une surpuissante attaque et inflige systématiquement le double des dégâts, critiques compris. Cependant, le contrecoup est violent et le personnage subit les dégâts maximums de son arme. Les dégâts qu’il s’auto-inflige ne sont pas réductibles par l’armure ou un bouclier magique, puisqu’il s’agît du contrecoup direct sur le corps. Le don peut se combiner avec d’autres techniques, notamment multicibles. Auquel cas, le personnage ne subira tout de même qu’une seule fois son jet de dégâts.",
		"optgroup": "Dons de combat",
		"bonus": {
		}
	},
		"echange": {
		"label": "Échange",
		"description": "Échange le nombre voulu de points de compétence d’une arme, pour les transformer en dégâts. Peut être utilisé avant chaque jet d’attaque.",
		"optgroup": "Dons de combat",
		"bonus": {
		}
	},
	"embuscade": {
		"label": "Embuscade",
		"description": "Le personnage sait surprendre ses victimes et les attaquer discrètement. Il gagne alors un bonus de 1D de dégâts à ses attaques sournoises (en plus de 1D6 habituel).",
		"optgroup": "Dons de combat",
		"bonus": { // TODO bonus attaque sournoise
		}
	},
	"entrainement": {
		"label": "Entraînement",
		"description": "Le personnage obtient un bonus à l’initiative égal à son niveau.",
		"optgroup": "Dons de combat",
		"bonus": {
			"bonus_initiative_par_niveaux": 1
		}
	},
	"entrainement_ii": {
		"label": "Entraînement II",
		"description": "Le personnage gagne le double de son niveau en bonus à l’initiative (en plus).",
		"optgroup": "Dons de combat",
		"bonus": {
			"bonus_initiative_par_niveaux": 2
		}
	},
	"execution": {
	"label": "Exécution",
	"description": "Le personnage obtient une nouvelle manœuvre. S’il cible un adversaire possédant 10 % de ses points de vie ou moins, il le tuera sans calculer les dégâts. Cette action peut être utilisée plusieurs fois en un tour, tant que le personnage peut atteindre ses cibles. Si lors d’un enchaînement de cibles, une manœuvre rate ou est parée, le tour se termine. Ce don ne peut se cumuler avec d’autres dons de combat ou de mêlée, puisqu’il s’agit d’une technique spéciale.",
	"optgroup": "Dons de combat",
	"bonus": {
		}
	},
	"feroce": {
		"label": "Féroce",
		"description": "Lorsqu’un adversaire rate une attaque contre le personnage, celui-ci peut répliquer instantanément. Si son jet d’attaque est réussi (opposé à l’esquive de l’ennemi), il infligera la moitié de ses dégâts habituels (arrondis au supérieur).",
		"optgroup": "Dons de combat",
		"bonus": {
		}
	},
	"forcer_la_chance": {
		"label": "Forcer la chance",
		"description": "<b>Contre 3 points de mana</b>, le personnage gagne un bonus au critique sur sa prochaine action de 2. Ladite action peut être active, comme passive (une esquive).",
		"optgroup": "Dons de combat",
		"bonus": {
		}
	},
	"glissade": {
		"label": "Glissade",
		"description": "Le personnage effectue une manœuvre opposée à la psychologie ennemie, passe entre les jambes de la cible et peut ainsi effectuer une attaque dans le dos. Pour ce faire, le personnage doit au moins parcourir 3 cases de déplacement. Si la manœuvre échoue, le personnage se retrouvera dans l’état renversé.",
		"optgroup": "Dons de combat",
		"bonus": {
		}
	},
	"get_down_mr_president": {
		"label": "Get down Mr. President",
		"description": "Le personnage peut intercepter autant de coups destinés à ses alliés qu’il ne le souhaite, sa seule limite étant ses cases de déplacements de base. Il faudra obligatoirement se positionner entre l’ennemi et l’allié, ce qui peut sous-entendre un jet de manœuvres pour déplacer l’allié une case plus loin. Le personnage ne peut pas parer les attaques, à moins de posséder un bouclier. La parade imposera un malus de 6 à la défense, malus qui augmentera si le personnage intercepte plusieurs coups.",
		"optgroup": "Dons de combat",
		"bonus": {
		}
	},
	"hero": {
		"label": "Héro",
		"description": "<span style=\"font-weight:bold\"><i class=\"fa fa-exclamation-triangle\"></i> Pour prendre ce don, il faudra au préalable être niveau 8. <i class=\"fa fa-exclamation-triangle\"></i></span><br> Ce don augmente de 1D les dégâts d’un type d’arme.",
		"optgroup": "Dons de combat",
		"bonus": {
		},
		"choix": {
			"type": "armes",
			"bonus": {
				"bonus_D_dmg": 1
			}
		}
	},
	"inversion": {
		"label": "Inversion",
		"description": "Le calcul du bonus de dégâts du personnage ne se fera plus sur la caractéristique de force, mais sur la dextérité.",
		"optgroup": "Dons de combat",
		"bonus": { //TODO
		}
	},
	"maitre_darme": {
		"label": "Maître d'arme",
		"description": "<span style=\"font-weight:bold\"><i class=\"fa fa-exclamation-triangle\"></i> Pour prendre ce don, il faudra au préalable être niveau 4 <i class=\"fa fa-exclamation-triangle\"></i></span><br>. Ce don augmente d’une classe de dé les dégâts d’un type d’arme.",
		"optgroup": "Dons de combat",
		"bonus": {
		},
		"choix": {
			"type": "armes",
			"bonus": {
				"bonus_classe_D": 2
			}
		}
	},		
	"pas_de_cote": {
		"label": "Pas de côté",
		"description": "Chaque fois qu’un adversaire rate le personnage avec une attaque au corps-à-corps, celui-ci peut se déplacer d’une case (même en diagonale) sans provoquer d’attaque d’opportunité.",
		"optgroup": "Dons de combat",
		"bonus": {
		}
	},
	"prevoyance": {
		"label": "Prévoyance",
		"description": "Durant un combat, tant que le personnage n’a pas attaqué, riposté, ou infligé un debuff, celui-ci gagne un bonus à l’esquive de {{1}}.",
		"optgroup": "Dons de combat",
		"bonus": { // TODO bonus annexes

		}
	},
	"ravageur": {
		"label": "Ravageur",
		"description": "<span style=\"font-weight:bold\"><i class=\"fa fa-exclamation-triangle\"></i> Pour prendre ce don, il faudra au préalable être niveau 4 <i class=\"fa fa-exclamation-triangle\"></i></span><br>. Ce don augmente de 1D les dégâts d’un type d’arme.",
		"optgroup": "Dons de combat",
		"bonus": {
		},
		"choix": {
			"type": "armes",
			"bonus": {
				"bonus_D_dmg": 1
			}
		}
	},
	"rempart": {
		"label": "Rempart",
		"description": "Le personnage dépense le nombre de points de mana qu’il désire et les convertit en points d’esquive. L’effet sera actif jusqu’à son prochain tour.",
		"optgroup": "Dons de combat",
		"bonus": {
		}
	},
		"rush": {
		"label": "Rush",
		"description": "Lorsque le personnage court, sa vitesse de déplacement n’est plus doublée, mais triplée.",
		"optgroup": "Dons de combat",
		"bonus": {
		}
	},
	"tout_donner": {
			"label": "Tout donner",
			"description": "Effectuer un coup critique sur l’initiative permet de jouer deux fois lors de son tour sans contrepartie. De plus, le critique sur l’initiative active l’aura du personnage et lui permet de motiver ses alliés.",
			"optgroup": "Dons de combat",
			"bonus": {
			}	
	},
	//MELE
	
	"attaque_laterale": {
		"label": "Attaque latérale",
		"description": "<b>Manœuvre</b> utilisant <b>2 points de mana</b>. Le personnage est capable de toucher toutes les cibles autour lui, dans une zone de type 1 (même pour les armes à allonge). Durant la capacité, le personnage décrit un cercle et est capable de s’arrêter quand il le souhaite au cours de sa rotation. À chaque adversaire, le suivant obtient un bonus de 2 à l’esquive.",
		"optgroup": "Dons de mélé",
		"bonus": {
		}
	},
	"cible_facile": {
		"label": "Cible facile",
		"description": "Le personnage est un expert du corps-à-corps et gagne un bonus de 2 au toucher sur un type d’arme de mêlée.",
		"optgroup": "Dons de mélé",
		"bonus": {
		},
		"choix": {
			"type": "mélé",
			"bonus": {
				"toucher" : 2
			}
		}
	},
	"coup_cible": {
		"label": "Coups ciblés",
		"description": "Le personnage gagne 1 de pénétration d’armure sur un type d’armes (hors pugilat et attaques naturelles).",
		"optgroup": "Dons de mélé",
		"bonus": { //TODO faire choix comme pour ravageur
			
		},
		"choix": {
			"type": "armes",
			"bonus": {
				"PA": 1
			}
		}
	},
	"coup_destructeur": {
		"label": "Coup destructeur",
		"description": "<span style=\"font-weight:bold\"><i class=\"fa fa-exclamation-triangle\"></i>Nécessite d’être niveau 5 et de posséder une force minimale de 22.<i class=\"fa fa-exclamation-triangle\"></i></span><br> Chaque fois que le personnage porte un coup critique avec une attaque au corps-à-corps, il peut détruire l’arme de son adversaire, en plus d’infliger des dégâts à celui-ci. L’ennemi lancera alors un jet pour savoir si son arme se brise.",
		"optgroup": "Dons de mélé",
		"bonus": {
		}
	},
	"garde": {
		"label": "Garde",
		"description": "<span style=\"font-weight:bold\"><i class=\"fa fa-exclamation-triangle\"></i> Pour prendre ce don, il faudra manier une arme à deux mains. <i class=\"fa fa-exclamation-triangle\"></i></span><br> Le personnage est habitué à parer avec son arme imposante et gagne un bonus à la défense de 2.",
		"optgroup": "Dons de mélé",
		"bonus": {
			"esquive": 2
		}
	},
	"parade": {
		"label": "Parade",
		"description": "Lorsqu’un adversaire effectue une attaque non magique contre le personnage, celui-ci peut tenter de parer ou de dévier un projectile en utilisant une arme (légère ou à deux mains). Le personnage effectue alors un jet de maniement d’arme à la place d’un jet d’esquive, tout en appliquant les mêmes conditions de réussite.",
		"optgroup": "Dons de mélé",
		"bonus": {
		}
	},
	"tranchoir": {
		"label": "Tranchoir",
		"description": "Chaque fois qu’il arrive à infliger des dégâts à sa cible à l’aide d’armes tranchantes, les dégâts de saignement augmentent de 1 (l’effet reste doublé si la cible bouge). Le don peut être utilisé avec d’autres améliorations du saignement.",
		"optgroup": "Dons de mélé",
		"bonus": {
		}
	},
	//DISTANCE
	"acuite_accentuee": {
		"label": "Acuité accentuée",
		"description": "Le personnage ajoute son score dans la compétence de perception (les points, pas le total) et les additionne à ses incréments de portée.",
		"optgroup": "Dons de distance",
		"bonus": {
			//TODO
		}
	},
	"adresse": {
		"label": "Adresse",
		"description": "Le personnage à l’œil pour déceler les failles dans la défense adverse. Ses attaques ont une chance d’outrepasser l’armure de ses adversaires. Toutes les deux attaques réussies (opposition réussie), le personnage annule 1 point de RD de sa cible. L’effet peut se cumuler autant de fois que nécessaire, mais ne concerne que ses propres attaques.",
		"optgroup": "Dons de distance",
		"bonus": {
		}
	},	
	"double_encoche": {
		"label": "Double encoche",
		"description": "<span style=\"font-weight:bold\"><i class=\"fa fa-exclamation-triangle\"></i> Pour prendre ce don, il faudra au préalable être niveau 2. <i class=\"fa fa-exclamation-triangle\"></i></span><br> Le personnage peut tirer deux projectiles durant son tour. En contrepartie, le jet de toucher s’effectuera avec un malus de 6, un malus de 3 à la défense et une chance d’échec critique augmentée de 2 (28-29-30).",
		"optgroup": "Dons de distance",
		"bonus": {
		}
	},
	"double_encoche_ii": {
		"label": "Double encoche II",
		"description": "Réduit de 1 les malus à l'échec critique et à la défense.",
		"optgroup": "Dons de distance",
		"bonus": {
		}
	},
	"double_encoche_iii": {
		"label": "Double encoche III",
		"description": "Réduit de 1 les malus à l'échec critique et à la défense et augmente le score au critique de 1.",
		"optgroup": "Dons de distance",
		"bonus": {
		}
	},
	"embouts": {
		"label": "Embouts",
		"description": "<span style=\"font-weight:bold\"><i class=\"fa fa-exclamation-triangle\"></i> Ne concerne que les arcs et arbalètes. <i class=\"fa fa-exclamation-triangle\"></i></span><br> <b>Contre 2 points de mana</b>, l’archer peut modéliser différents embouts pour ses projectiles, permettant de lancer des jets de manœuvres afin de pousser, renverser, assommer ou désarmer une cible, tout en restant à distance.",
		"optgroup": "Dons de distance",
		"bonus": {
		}
	},
	"force_interieure": {
		"label": "Force intérieure",
		"description": "<span style=\"font-weight:bold\"><i class=\"fa fa-exclamation-triangle\"></i> Ne concerne que les armes de lancer. <i class=\"fa fa-exclamation-triangle\"></i></span><br> <b>Contre 2 points de mana</b>, le personnage peut choisir d’augmenter sa portée d’attaque de 10, ou d’augmenter sa classe de dé. L’effet n’est pas cumulable et n’est valable que pour un tour.",
		"optgroup": "Dons de distance",
		"bonus": {
		}
	},
	"lancer_redoutable": {
		"label": "Lancer redoutable",
		"description": "Lorsque le personnage lance une arme ou un projectile, le bonus de dégâts lié à la force est doublé.",
		"optgroup": "Dons de distance",
		"bonus": {
			"multiplicateur_bonus_force": 2
		}
	},
	"mouchard": {
		"label": "Mouchard",
		"description": "<b>Contre 2 points de mana</b>, le personnage peut forcer son projectile à continuer sa trajectoire après avoir touché une cible. Il pourra ainsi en atteindre une deuxième, sans réduction de dégât, tant que la cible se trouve alignée avec la première.",
		"optgroup": "Dons de distance",
		"bonus": {
		}
	},
	"pas_de_petit_profit": {
		"label": "Pas de petit profit",
		"description": "Le personnage peut désormais récupérer les projectiles et munitions utilisés des adversaires (qui étaient jusqu’ici considérés comme détruits). De plus, les flèches et carreaux coûtent 30 % moins cher.",
		"optgroup": "Dons de distance",
		"bonus": {
		}
	},
	"polyvalent": {
		"label": "Polyvalent",
		"description": "Le personnage est habitué à utiliser son arme et sait s’adapter à toutes les situations de combat. Un combattant à distance peut utiliser son arme au corps-à-corps, sur un jet de toucher habituel. Cependant, seuls les dégâts de basiques de l’arme seront autorisés..",
		"optgroup": "Dons de distance",
		"bonus": {
		}
	},
	"viser_juste": {
		"label": "Viser juste",
		"description": "Le personnage est un expert en tirs et gagne un bonus de 2 au toucher sur un type d’arme à distance.",
		"optgroup": "Dons de distance",
		"bonus": {
		},
		"choix": {
			"type": "distance",
			"bonus": {
				"toucher" : 2
			}
		}
	},
	"vision_absolue": {
		"label": "Vision absolue",
		"description": "<b>Contre 4 points de mana</b>, le personnage obtient la capacité de tirer au travers des obstacles pour le tour, mais ses incréments de portée sont divisés par deux.",
		"optgroup": "Dons de distance",
		"bonus": {
		}
	},
	"vision_daigle": {
		"label": "Vision d'aigle",
		"description": "Échange le nombre voulu de points (distribués) de la compétence perception, pour les transformer en points de compétence d’arme. Chaque point échangé consomme 1 point de mana et n'est effectif que pour un jet.",
		"optgroup": "Dons de distance",
		"bonus": {
		}
	},
	//PUGILAT
	"aerolithe": {
		"label": "Aérolithe",
		"description": "Le personnage gagne un bonus de 1 à son critique, sur le combat à mains nues.",
		"optgroup": "Dons de pugilat",
		"bonus": {
			"type_corpsacorps":{
				"critique": 1
			}
		}
	},
	"artifices": {
		"label": "Artifices",
		"description": "Jet de sable, frappe détournée, croche-pied, rien n’est trop déloyal pour le personnage ! Il gagne un bonus de 2 à ses jets de manœuvres. De plus, il peut relancer un jet de manœuvres une fois par tour.",
		"optgroup": "Dons de pugilat",
		"bonus": {
			"manoeuvres": 2
		}
	},
	"bonne_etoile": {
		"label": "Bonne étoile",
		"description": "<b>Contre 2 points de mana</b>, le joueur lance deux fois son jet de dégâts et garde celui qu’il préfère.",
		"optgroup": "Dons de pugilat",
		"bonus": {
		}
	},
	"capoeira": {
		"label": "Capoeira",
		"description": "Le personnage perd son malus de combat en surnombre et gagne un bonus à l'esquive de 1 par ennemi à son contact.",
		"optgroup": "Dons de pugilat",
		"bonus": { //TODO bonus situationnels
		}
	},
	"coup_de_pied_rotatif": {
		"label": "Coup de pied rotatif",
		"description": "Lorsqu’il effectue un coup critique lors d’une attaque, le personnage peut lancer une attaque supplémentaire, toujours sous les conditions habituelles de réussite.",
		"optgroup": "Dons de pugilat",
		"bonus": {
		}
	},	
	"jeu_de_jambes": {
		"label": "Jeu de jambes",
		"description": "Lorsque le personnage réussit une attaque ou une esquive, il peut se replacer où il le souhaite autour de l’ennemi sans déclencher d’attaque d’opportunité.",
		"optgroup": "Dons de pugilat",
		"bonus": {
		}
	},
	"mes_poings,_mes_armes": {
		"label": "Mes poings, mes armes",
		"description": "Les armes naturelles du personnage peuvent désormais bénéficier des enchantements, jusqu’ici réservés aux armes.",
		"optgroup": "Dons de pugilat",
		"bonus": {
		}
	},
	"poings_ravageurs": {
		"label": "Poings ravageurs",
		"description": "Le personnage gagne un bonus à ses dégâts au corps-à-corps de 2.",
		"optgroup": "Dons de pugilat",
		"bonus": {
			"type_corpsacorps":{
				"bonus_dmg": 2
			}
		}
	},
	"rage_de_vaincre": {
		"label": "Rage de vaincre",
		"description": "Pour chaque tranche de 10 points de vie manquant au personnage, celui-ci gagne un bonus de 1 dégât.",
		"optgroup": "Dons de pugilat",
		"bonus": { // TODO bonus optionnels
		}
	},	
	//MAGIE
	"absorption_magique": {
		"label": "Absorption magique",
		"description": "Permet de voler, sur une portée de 10 mètres, 1D6 + 1 par niveau points de mana d’une cible avec un jet de sagesse réussi.",
		"optgroup": "Dons de magie",
		"bonus": {
			"pm": 5
		}
	},
	"ames_liees": {
		"label": "Âmes liées",
		"description": "Le mage peut désormais avoir deux invocations actives sur le terrain.",
		"optgroup": "Dons de magie",
		"bonus": {
			"pm": 5
		}
	},
	"copieur": {
		"label": "Copieur",
		"description": "Le personnage peut apprendre un nouveau sort d’un grimoire qu’il ne maîtrise pas. Il ne pourra cependant pas prendre un sort de rang trop élevé pour lui.",
		"optgroup": "Dons de magie",
		"choix" : {
			"rang": 7,
			"type": "Magie"
		},
		"type" : "passif",		
		"bonus": {
			"pm": 5
		}
	},	
	"deflagration": {
		"label": "Déflagration",
		"description": "Pendant 1 tour et <b>contre 2 points de mana supplémentaires</b>, les sorts de zones voient leur effet augmenté de 1 D (les zones d’explosion ne sont pas concernées). Cumulable avec les dons Magie altruiste et Magie destructive.",
		"optgroup": "Dons de magie",
		"bonus": {
			"pm": 5
		}
	},
	"dernieres_forces": {
		"label": "Dernières forces",
		"description": "Le mage peut décider de puiser directement dans ses points de vie afin de les convertir en points de mana.",
		"optgroup": "Dons de magie",
		"bonus": {
			"pm": 5
		}
	},
	"expertise": {
		"label": "Expertise",
		"description": "Sur un sort au choix, le mage obtiendra un bonus au de 2 à l’incantation.",
		"optgroup": "Dons de magie",
		"bonus": {
			"pm": 5
		}
	},
	"habitude": {
		"label": "Habitude",
		"description": "Choisissez un sort de votre grimoire et son coût en mana de base sera divisé par deux. Habitude ne réduit pas le coût des diverses augmentations de sort.",
		"optgroup": "Dons de magie",
		"bonus": {
			"pm": 5
		}
	},
	"incantation_rapide": {
		"label": "Incantation rapide",
		"description": "Le personnage peut lancer deux sorts par tour, mais doit réussir deux jets de toucher, perdre deux fois le mana et subit un malus aux jets d’incantation de 4.",
		"optgroup": "Dons de magie",
		"bonus": {
			"pm": 5
		}
	},
	"incantation_silencieuse": {
		"label": "Incantation silencieuse",
		"description": "Le personnage ne fait pas de bruit pour lancer un sort. Ce qui permet notamment d’utiliser la magie sans malus, même en étant dans l’incapacité de parler.",
		"optgroup": "Dons de magie",
		"bonus": {
			"pm": 5
		}
	},
	"mage": {
		"label": "Mage",
		"description": "Le personnage obtient la compétence </b>incantation</b>, lui permettant de lancer des sorts et choisir un grimoire parmi la liste des dons évolutifs. De plus, chaque don magique acquis, y compris ce don, augmente le total de points de mana de 5.",
		"optgroup": "Dons de magie",
		"bonus": {
			"pm": 5
		}
	},
	"magie_altruiste": {
		"label": "Magie altruiste",
		"description": "<b>Contre 2 points de mana supplémentaires</b>, les sorts de soin et de buff gagnent 1D à leur effet le temps d’un tour.",
		"optgroup": "Dons de magie",
		"bonus": {
			"pm": 5
		}
	},
	"magie_destructive": {
		"label": "Magie destructive",
		"description": "<b>Contre 2 points de mana supplémentaires</b>, les sorts de dégâts et de debuff gagnent 1D à leur effet le temps d’un tour.",
		"optgroup": "Dons de magie",
		"bonus": {
			"pm": 5
		}
	},
	"magie_inebranlable": {
		"label": "Magie inébranlable",
		"description": "Le personnage peut lancer des sorts sans avoir à se servir de ses mains et ainsi utiliser sa magie avec le seul son de sa voix. Le mage pourra alors avoir les mains pleines, entravées ou se trouver métamorphosé et sera toujours apte à lancer une incantation.",
		"optgroup": "Dons de magie",
		"bonus": {
			"pm": 5
		}
	},
	"mana_pur": {
		"label": "Mana pur",
		"description": "Une fois par combat, le personnage peut lancer un sort, même augmenté, qui ne lui coûtera aucun mana.",
		"optgroup": "Dons de magie",
		"bonus": {
			"pm": 5
		}
	},
	"pompes_funebres": {
		"label": "Pompes funèbres",
		"description": "Lorsque le personnage achève un ennemi avec un sort, il regagne 1D6 + {{1}} points de mana.",
		"optgroup": "Dons de magie",
		"bonus": {
			"pm": 5
		}
	},
	"ponction": {
		"label": "Ponction",
		"description": "Lorsque le personnage achève un ennemi avec un sort, il regagne 1D6 + {{1}} points de vie.",
		"optgroup": "Dons de magie",
		"bonus": {
			"pm": 5
		}
	},
	"repioche": {
		"label": "Repioche",
		"description": "<span style=\"font-weight:bold\"><i class=\"fa fa-exclamation-triangle\"></i> Fonctionne également avec le don « Métronome ». <i class=\"fa fa-exclamation-triangle\"></i></span><br> Contre 2 points de mana, le personnage peut, une fois par tour, relancer la pioche d’une carte tirée ou le résultat d’un sort lancé. Il pourra choisir lequel des deux effets utiliser.",
		"optgroup": "Dons de magie",
		"bonus": {
			"pm": 5
		}
	},
	"scopomancien": {
		"label": "Scopomancien",
		"description": "<b>Contre 1 point de mana supplémentaire</b>, le mage peut augmenter la portée de son sort de {{2}}+5 mètres.",
		"optgroup": "Dons de magie",
		"bonus": {
			"pm": 5

		}
	},
	"sort_affaibli": {
		"label": "Sort affaibli",
		"description": "Pour <b>2 points de mana supplémentaires</b> (à utiliser avant de lancer le sort), la classe du dé est diminuée de 1 et le rang du sort baisse de 1.",
		"optgroup": "Dons de magie",
		"bonus": {
			"pm": 5
		}
	},
	"sort_maitrise": {
		"label": "Sort maîtrisé",
		"description": "Le mage augmente sa marche de critique de 1 sur un sort de son grimoire. De plus, l’échec critique devient désormais impossible sur ce sort.",
		"optgroup": "Dons de magie",
		"bonus": {
			"pm": 5
		}
	},
	"sort_puissant": {
		"label": "Sort puissant",
		"description": "Pour <b>2 points de mana supplémentaires</b> (à utiliser avant de lancer le sort), la classe du dé est augmentée de 1, tout comme le rang du sort.",
		"optgroup": "Dons de magie",
		"bonus": {
			"pm": 5
		}
	},
	"surcharge": {
		"label": "Surcharge",
		"description": "Contre <b>1 point de mana supplémentaire</b>, le sort gagne + 2 à son effet. Il est possible de dépenser plus de mana pour augmenter le gain, pour un maximum de 10 fois (donc + 20). L’effet ne peut se produire que sur un sort unique, ou le premier effet d’un sort à multiples projectiles.",
		"optgroup": "Dons de magie",
		"bonus": {
			"pm": 5
		}
	},
	"zele_charitable": {
		"label": "Zèle charitable",
		"description": "Lorsque le personnage lance 3 sorts de soins (réussis) sur une même cible, le prochain se fera avec une augmentation de 14 de la marge de critique.",
		"optgroup": "Dons de magie",
		"bonus": {
			"pm": 5
		}
	},
	//COMPETENCES
"theatreux": {
		"label": "Catalyseur",
		"description": "Le personnage gagne un bonus de 2 à ses jets de représentation et de provocation.",
		"optgroup": "Dons de competences",
		"bonus": {
			"representation" : 2,
			"provocation" : 2
		}
	},
	"analyste": {
		"label": "Analyste",
		"description": "Le personnage gagne un bonus de 2 à ses jets de psychologie et de bluff.",
		"optgroup": "Dons de competences",
		"bonus": {
			"psychologie" : 2,
			"bluff" : 2
		}
	},
	"marchand": {
		"label": "Marchand",
		"description": "Le personnage gagne un bonus de 1 à ses jets de commerce, ainsi que 2 dans un domaine d'artisanat.",
		"optgroup": "Dons de competences",
        "bonus": {
            "commerce": 1
        },
        "choix": { 
            "type": "Radio", 
            "list": { 
                "forgeron": 2, 
                "tanneur": 2,
                "alchimiste": 2,
                "tailleur": 2,
                "faconneur": 2
			}
        }
    },
	"regisseur": {
		"label": "Régisseur",
		"description": "Le personnage gagne un bonus de 2 à ses jets d'éloquence et de volonté.",
		"optgroup": "Dons de competences",
		"bonus": {
			"eloquence" : 2,
			"volonte" : 2
		}
	},
	"brillant": {
		"label": "Éleveur",
		"description": "Le personnage gagne un bonus de 2 à ses jets d'équitation et de dressage.",
		"optgroup": "Dons de competences",
		"bonus": {
			"equitation" : 2,
			"dressage" : 2
		}
	},
	"instruit": {
		"label": "Instruit",
		"description": "Le personnage gagne un bonus de 2 à ses jets d'érudition et de médecine.",
		"optgroup": "Dons de competences",
		"bonus": {
			"erudition" : 2,
			"medecine" : 2
		}
	},
	"performant": {
		"label": "Performant",
		"description": "Le personnage gagne un bonus de 2 à ses jets d’acrobatie et d’athlétisme.",
		"optgroup": "Dons de competences",
		"bonus": {
			"acrobatie" : 2,
			"athletisme" : 2
		}
	},
	"baraque": {
		"label": "Baraqué",
		"description": "Le personnage gagne un bonus de 2 à ses jets d’intimidation et de ténacité.",
		"optgroup": "Dons de competences",
		"bonus": {
			"intimidation" : 2,
			"tenacite" : 2
		}
	},
	"pisteur": {
		"label": "Pisteur",
		"description": "Le personnage gagne un bonus de 2 à ses jets de survie et de perception.",
		"optgroup": "Dons de competences",
		"bonus": {
			"survie" : 2,
			"perception" : 2
		}
	},
	"crapule": {
		"label": "Crapule",
		"description": "Le personnage gagne un bonus de 2 à ses jets de sabotage et de discretion.",
		"optgroup": "Dons de competences",
		"bonus": {
			"sabotage" : 2,
			"discretion" : 2
		}
	},
	//Armures
	"armures_familieres": {
		"label": "Armures familières",
		"description": "Le personnage est habitué à porter de lourdes armures et diminue de 2 les malus liés à celles-ci.",
		"optgroup": "Dons d'armure",
		"bonus": {
			"malus_armure": -2,
			"bonus_initiative": 2
		}
	},
	"defense_spirituelle": {
		"label": "Défense spirituelle",
		"description": "<span style=\"font-weight:bold\"><i class=\"fa fa-exclamation-triangle\"></i> Nécessite une armure légère ou moins. <i class=\"fa fa-exclamation-triangle\"></i></span><br> <b>Une fois par combat</b>, l’armure du personnage peut encaisser et neutraliser un sort dont il serait la cible. Le joueur doit se manifester pour activer l’effet.",
		"optgroup": "Dons d'armure",
		"bonus": {
		}
	},
	"glorification": {
		"label": "Glorification",
		"description": "<span style=\"font-weight:bold\"><i class=\"fa fa-exclamation-triangle\"></i> Nécessite une armure légère ou moins. <i class=\"fa fa-exclamation-triangle\"></i></span><br> <b>Contre 5 points de mana</b>, le personnage peut, une fois par jour, choisir un don magique en sa possession et doubler son effet.",
		"optgroup": "Dons d'armure",
		"bonus": {
		}
	},
	"incassable": {
		"label": "Incassable",
		"description": "<span style=\"font-weight:bold\"><i class=\"fa fa-exclamation-triangle\"></i> Nécessite une armure intermédiaire ou plus. <i class=\"fa fa-exclamation-triangle\"></i></span><br> Le personnage ne porte que de la haute qualité. De ce fait, son armure sans défauts devient impossible à endommager.",
		"optgroup": "Dons d'armure",
		"bonus": {
		}
	},	
	"renfort_ethere": {
		"label": "Renfort éthéré",
		"description": "<span style=\"font-weight:bold\"><i class=\"fa fa-exclamation-triangle\"></i> Nécessite une armure moyenne ou moins. <i class=\"fa fa-exclamation-triangle\"></i></span><br> <b>Contre 4 points de mana</b> et durant {{1}} tour(s), celui-ci recouvre son armure d’une aura spirituelle et renvoie son score de RD en dégâts (sans esquive possible) à l’attaquant.",
		"optgroup": "Dons d'armure",
		"bonus": {
		}
	},
	"resistance_a_la_magie": {
		"label": "Résistance à la magie",
		"description": "Le personnage a développé une certaine résistance à la magie et se trouve moins affecté par les sorts offensifs des mages, grâce à un bonus de 2 à la RM.",
		"optgroup": "Dons d'armure",
		"bonus": {
			"RM": 2
		}
	},
	//Spéciaux
	"bonification": {
		"label": "Bonification",
		"description": "Aux niveaux 3,6 et 9, le personnage gagne 1 point de caractéristique. Ce don n’est pas obligatoirement à choisir au niveau 1, si jamais un personnage de niveau 7 le prend, il aura de suite droit à ses deux points de caractéristique.",
		"optgroup": "Dons spéciaux",
		"bonus": {
		}
	},
	"coureur": {
		"label": "Coureur",
		"description": "Le personnage gagne 1 case de déplacement et peut désormais se déplacer en diagonale.",
		"optgroup": "Dons spéciaux",
		"bonus": {
			"deplacement": 1
		}
	},
	"deplacement_acrobatique": {
		"label": "Déplacement acrobatique",
		"description": "Le personnage n’est plus impacté par les terrains non subaquatiques imposants des malus au déplacement.",
		"optgroup": "Dons spéciaux",
		"bonus": {
		}
	},
	"coriace": {
		"label": "Coriace",
		"description": "Le personnage gagne un bonus de 4 points de vie supplémentaires par niveau. Si jamais ce don est pris après le niveau 1, il faudra ajouter les points de vie qui auraient dû être comptés depuis le niveau 1.",
		"optgroup": "Dons spéciaux",
		"bonus": {
			"pv_par_niveaux": 4
		}
	},
	"flux_arcanique": {
		"label": "Flux arcanique",
		"description": "Lorsque le personnage doit lancer son jet de régénération de mana (par le sommeil ou la méditation), le résultat de son jet sera doublé.",
		"optgroup": "Dons spéciaux",
		"bonus": {
		}
	},
	"regeneration_sanguine": {
		"label": "Régénération sanguine",
		"description": "Lorsque le personnage doit lancer son jet de régénération de points de vie, le résultat de son jet sera doublé.",
		"optgroup": "Dons spéciaux",
		"bonus": {
		}
	},
	"reserve_occulte": {
		"label": "Réserve occulte",
		"description": "Le personnage gagne instantanément 20 points de mana. Il est possible de reprendre ce don autant de fois que voulu.",
		"optgroup": "Dons spéciaux",
		"bonus": {
			"pm": 20
		}
	},
	"robustesse": {
		"label": "Robustesse",
		"description": "Le personnage gagne instantanément 20 points de vie. Il est possible de reprendre ce don autant de fois que voulu.",
		"optgroup": "Dons spéciaux",
		"bonus": {
			"pv": 20
		}
	},
	"source_de_pouvoir": {
		"label": "Source de pouvoir",
		"description": "Le personnage gagne un bonus de 4 points de mana supplémentaires par niveau. Si jamais ce don est pris après le niveau 1, il faudra ajouter les points de mana qui auraient dû être comptés depuis le niveau 1.",
		"optgroup": "Dons spéciaux",
		"bonus": {
			"pm_par_niveaux": 4
		}
	},
	"intrepide": {
		"label": "Intrépide",
		"description": "Le personnage a un sang-froid hors norme et résiste davantage aux effets de peur grâce à un bonus de 4 aux jets contre la peur et à l’intimidation.",
		"optgroup": "Dons spéciaux",
		"bonus": {
		}
	},
	"mule": {
		"label": "Mule",
		"description": "Le personnage à un dos particulièrement fort, ce qui lui permets de transporter un plus grand nombre d’objets. Le multiplicateur de charge est augmenté de 1,5.",
		"optgroup": "Dons spéciaux",
		"bonus": {
				"multiplicateur_charge": 1.5
		}
	},
	"resistance_à_la_torture": {
		"label": "Résistance à la torture",
		"description": "Le personnage a un mental d’acier et endure la souffrance avec calme et abnégation. Il restera de marbre grâce à un bonus de 4 pour résister à la torture.",
		"optgroup": "Dons spéciaux",
		"bonus": {
		}
	},
	"resistance_à_lempoisonnement": {
		"label": "Résistance à l’empoisonnement",
		"description": "Le personnage à développé une résistance aux divers poisons (dont l’alcool) et possède un bonus de 4 pour y résister.",
		"optgroup": "Dons spéciaux",
		"bonus": {
		}
	},
	"resistances_au_chaud_ou_au_froid": {
		"label": "Résistances au chaud ou au froid",
		"description": "Le personnage gagne une résistance bonus de 4 aux jets de ténacité dans un climat chaud ou froid.",
		"optgroup": "Dons spéciaux",
		"bonus": {
		}
	},
	"resistance_aux_maladies": {
		"label": "Résistance aux maladies",
		"description": "Le personnage est moins affecté par les germes et autres affections et jouit d’un bonus de 4 pour y résister.",
		"optgroup": "Dons spéciaux",
		"bonus": {
		}
	},
	"vision_etherique": {
		"label": "Vision éthérique",
		"description": "À la demande du joueur, <b>contre 2 points de mana</b> et un jet réussi en perception, le personnage à la possibilité de détecter les êtres  ou objets éthérés, ordinairement invisibles.",
		"optgroup": "Dons spéciaux",
		"bonus": {
		}
	},
	"vision_nocturne": {
		"label": "Vision nocturne",
		"description": "Le personnage à la possibilité de voir dans la pénombre naturelle aussi bien qu’en plein jour.",
		"optgroup": "Dons spéciaux",
		"bonus": {
		}
	},
	"perception_magique": {
		"label": "Perception magique",
		"description": "À la demande du joueur, <b>contre 1 point de mana</b> et un jet réussi en perception, le personnage à la possibilité de détecter le flux magique d’un être ou d’un objet",
		"optgroup": "Dons spéciaux",
		"bonus": {
		}
	},
	"travail_à_la_chaine": {
		"label": "Travail à la chaîne",
		"description": "Lors de la création de potions alchimiques, le personnage peut lancer 1D{{1}}, le résultat définira le nombre de potions réalisées.",
		"optgroup": "Dons spéciaux",
		"bonus": {
		}
	},
	"medecin_de_terrain": {
		"label": "Médecin de terrain",
		"description": "Ce don permet au personnage d’utiliser la médecine en situation de combat, à condition que la cible et le soigneur soient désengagés.",
		"optgroup": "Dons spéciaux",
		"bonus": {
		}
	},
"assurer_sa_prise": {
		"label": "Assurer sa prise",
		"description": "Lors d’un test d’escalade, le joueur peut décider de relancer son jet et choisira alors le dé qu’il préfère. Le don fonctionne même en cas d’échec critique.",
		"optgroup": "Dons spéciaux",
		"bonus": {
		}
	},
	"concentration": {
		"label": "Concentration",
		"description": "<b>Contre 4 points de mana</b>, permet au personnage de se donner un bonus de 2 à sa prochaine action liée à l’intelligence.",
		"optgroup": "Dons spéciaux",
		"bonus": {
		}
	},
	"dialogue_animal": {
		"label": "Dialogue animal",
		"description": "<b>Contre 3 points de mana</b>, le personnage a la capacité de communiquer avec un animal pendant une heure. Ce pouvoir ne fonctionne pas sur des créatures dotées d'intelligence ou de conscience complexe. Le dialogue se fait de manière fluide, sans jet nécessaire. Le caractère de l'animal sera aléatoire et ses réponses dépendront des propos et de l'attitude du personnage.",
		"optgroup": "Dons spéciaux",
		"bonus": {
		}
	},
	"dialogue_vegetal": {
		"label": "Dialogue végétal",
		"description": "<b>Contre 2 points de mana</b>, le personnage peut communiquer avec une plante pendant 1 heure. Bien que dépourvues de sens classiques, les plantes perçoivent leur environnement à travers des changements d'énergie, des vibrations du sol, des signatures énergétiques laissées par les créatures, des perturbations récentes, ainsi que des fluctuations naturelles telles que l'humidité et la température.",
		"optgroup": "Dons spéciaux",
		"bonus": {
		}
	},
	"exercice": {
		"label": "Exercice",
		"description": "Tous les deux niveaux, le personnage gagne 1 point gratuitement dans une compétence choisie au préalable. Le don peut être pris plusieurs fois pour des compétences différentes. Si ce don est pris tardivement, le joueur ne rattrapera pas les points des niveaux précédents.",
		"optgroup": "Dons spéciaux",
		"bonus": {
		}
	},
	"force_intimidante": {
		"label": "Force intimidante",
		"description": "Ajoute le bonus de force aux jets d’intimidation, même si celui-ci est amélioré d’une quelconque façon.",
		"optgroup": "Dons spéciaux",
		"bonus": { //TODO
		}
	},	
	"metronome": {
		"label": "Métronome",
		"description": "<b>Contre 4 points de mana</b>, le mage lance un sort au hasard. Deux jets seront nécessaires, un pour déterminer le grimoire (1D18) et un autre pour déterminer le sort de ce grimoire (1D21).",
		"optgroup": "Dons spéciaux",
		"bonus": {
		}
	},
	"polyglotte": {
		"label": "Polyglotte",
		"description": "Naturellement doué avec les langues, le personnage parle 2 langues supplémentaires.",
		"optgroup": "Dons spéciaux",
		"bonus": {
			"bonus_langue": 2
		}
	},
	"soif_de_sang": {
		"label": "Soif de sang",
		"description": "<span style=\"font-weight:bold\"><i class=\"fa fa-exclamation-triangle\"></i> Ce don est réservé aux eletavirs. <i class=\"fa fa-exclamation-triangle\"></i></span><br> Lorsque le personnage se nourrit du sang d’une victime (hors combat pour une cible vivante), celui-ci régénère {{1}} point(s) de vie, utilisable toutes les 8 heures.",
		"optgroup": "Dons spéciaux",
		"bonus": {
		}
	},
	"ventriloque": {
		"label": "Ventriloque",
		"description": "Le personnage a appris à parler sans bouger les lèvres. Un jet de représentation sera tout de même demandé.",
		"optgroup": "Dons spéciaux",
		"bonus": {	
		}
	},
};

export default dons;