const flight = {
    "vol": {
        "multiplicateur_deplacement": 2,
        "esquive": 2,
        "attaque_naturelle": -2,
        "couteaux": -2,
        "hast": -2,
        "epee": -2,
        "fleaux": -2,
        "hache": -2,
        "marteau": -2,
        "bouclier":  -2,
        "fouet": -2,
          
        "arbalete": -6,
        "arc": -6,
        "jet": -6,
        "incantation": -6,

        //En contrepartie, l’attaquant, au corps-à-corps, bénéficie d'un bonus de dégâts de 1D6. Tout comme athlétisme, le vol est assujetti aux malus d'armure.
    }
}

export default flight;